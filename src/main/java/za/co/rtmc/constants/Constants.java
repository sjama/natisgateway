package za.co.rtmc.constants;

/**
 * Created by ashrafmoosa on 2016/07/02.
 */
public class Constants {

    public static final String SESSION_ID = "JSESSIONID";
    public static final String PATH       = " path";

    public static final String OP_MOD_IND_TEST = "TEST";
    public static final String OP_MOD_IND_LIVE = "LIVE";

    public static final String X0000_LOGIN = "OPEN";
    public static final String X0000_LOGOUT = "CLOSE";

    public static final String MENUCD_X0000 = "X0000";
    public static final String MENUCD_X0001 = "X1001";
    public static final String MENUCD_X0002 = "X1002";
    public static final String MENUCD_X0007 = "X1007";
    public static final String MENUCD_X20C3 = "X20C3";


    public static final String PROP_URL                = "url";
    public static final String PROP_OPMODIND           = "opModInd";
    public static final String PROP_KEYSTORE_LOCATION  = "keystoreLocation";
    public static final String PROP_KEYSTORE_PASSWORD  = "keystorePass";
    public static final String PROP_ESUSERN            = "esUserN";
    public static final String PROP_ESUID              = "esUid";
    public static final String PROP_ESTERMID           = "esTermId";
    public static final String PROP_USERN              = "userNumber";
    public static final String PROP_PASSWD             = "password";



    public static final String PKCS12       = "PKCS12";
    public static final String TLS          = "TLS";
    public static final String HTTPS        = "https";
    public static final String HTTP        = "http";

    public static final String XML_REQ      = "XMLReq";
    public static final String COOKIE       = "Cookie";
    public static final String SET_COOKIE   = "Set-Cookie";

}
