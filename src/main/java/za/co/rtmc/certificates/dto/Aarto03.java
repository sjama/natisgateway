package za.co.rtmc.certificates.dto;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import za.co.rtmc.certificates.util.CertificateTemplate;
import za.co.rtmc.certificates.util.FieldKeyType;
import za.co.rtmc.certificates.util.TemplateField;

import java.math.BigDecimal;

/**
 * AARTO 03 - Printed and posted infringement notice
 *
 * @author Jacob.Marola
 * @since 04 June 2019
 */
@SuppressWarnings("unused")
@CertificateTemplate(templateFile = "aarto03.pdf", fieldKeyType = FieldKeyType.BASIC)
public class Aarto03 {

    @TemplateField(value = "InfringementNoticeNo")
    private String infringementNoticeNumber;


    /* ***** INFRINGER DETAILS ****** */
    @TemplateField(value = "SurnameNameOfOrganisation")
    private String surnameOrNameOfOrganisation;

    @TemplateField(value = "FullName")
    private String firstNames;

    @TemplateField(value = "Initials")
    private String initials;

    @TemplateField(value = "IdentificationType")
    private String identificationType;

    @TemplateField(value = "GenderTypeOfOrganisation")
    private String genderOrTypeOfOrganisation;

    @TemplateField(value = "DateOfBirth")
    private LocalDate dateOfBirth;

    @TemplateField(value = "TelHome")
    private String homePhone;

    @TemplateField(value = "TelWork")
    private String workPhone;

    @TemplateField(value = "FaxNumber")
    private String faxNumber;

    @TemplateField(value = "Cellular")
    private String cellPhone;

    @TemplateField(value = "E-mail")
    private String email;

    @TemplateField(value = "IdentificationNumber")
    private String identificationNumber;

    @TemplateField(value = "CountryOfIssue")
    private String countryOfIssue;

    @TemplateField(value = "LicenceCode")
    private String licenceCodes;

    @TemplateField(value = "PrDP_codes")
    private String prdpCodes;

    @TemplateField(value = "OperatorCardNumber")
    private String operatorCardNumber;



    /* ***** ADDRESS DETAILS ****** */
    @TemplateField(value = "StreetAddressLine1")
    private String streetAddressLine1;

    @TemplateField(value = "StreetAddressLine2")
    private String streetAddressLine2;

    @TemplateField(value = "StreetAddressLine3")
    private String streetAddressLine3;

    @TemplateField(value = "StreetAddressLine4")
    private String streetAddressLine4;

    @TemplateField(value = "StreetAddressLine5")
    private String streetAddressLine5;

    @TemplateField(value = "StreetAddressCode")
    private String streetAddressCode;

    @TemplateField(value = "PostAddressLine1")
    private String postAddressLine1;

    @TemplateField(value = "PostAddressLine2")
    private String postAddressLine2;

    @TemplateField(value = "PostAddressLine3")
    private String postAddressLine3;

    @TemplateField(value = "PostAddressLine4")
    private String postAddressLine4;

    @TemplateField(value = "PostAddressLine5")
    private String postAddressLine5;

    @TemplateField(value = "PostAddressCode")
    private String postAddressCode;

    @TemplateField(value = "EmployerName")
    private String ownerName;

    @TemplateField(value = "EmployerAddressLine1")
    private String ownerAddressLine1;

    @TemplateField(value = "EmployerAddressLine2")
    private String ownerAddressLine2;

    @TemplateField(value = "EmployerAddressLine3")
    private String ownerAddressLine3;

    @TemplateField(value = "EmployerAddressLine4")
    private String ownerAddressLine4;

    @TemplateField(value = "EmployerAddressLine5")
    private String ownerAddressLine5;

    @TemplateField(value = "EmployerAddressCode")
    private String ownerAddressCode;


    /* ***** VEHICLE DETAILS ****** */
    @TemplateField(value = "VehicleLicenceNo")
    private String vehicleLicenceNumber;

    @TemplateField(value = "LicenceDiscNo")
    private String vehicleLicenceDiscNumber;

    @TemplateField(value = "VehicleType")
    private String vehicleDescription;

    @TemplateField(value = "Vehicle_GVM")
    private String grossVehicleMass;

    @TemplateField(value = "Make")
    private String vehicleMake;

    @TemplateField(value = "Series")
    private String vehicleSeries;

    @TemplateField(value = "Colour")
    private String vehicleColour;

    @TemplateField(value = "MotorVehicleImage")
    private byte[] vehicleImage;

    @TemplateField(value = "LicencePlateImage")
    private byte[] licensePlateImage;


    /* ***** LOCATION & DATE DETAILS ****** */
    @TemplateField(value = "Province")
    private String province;

    @TemplateField(value = "CityTown")
    private String cityTown;

    @TemplateField(value = "Suburb")
    private String suburb;

    @TemplateField(value = "Date")
    private LocalDate infringementDate;

    @TemplateField(value = "Time")
    private String infringeTime;

    private LocalTime infringementTime;

    @TemplateField(value = "StreetA")
    private String streetNameA;

    @TemplateField(value = "StreetB")
    private String streetNameB;

    @TemplateField(value = "GeneralLocation")
    private String location;

    @TemplateField(value = "Route")
    private String routeNumber;

    @TemplateField(value = "From")
    private String directionFrom;

    @TemplateField(value = "To")
    private String directionTo;

    @TemplateField(value = "X")
    private String gpsX;

    @TemplateField(value = "Y")
    private String gpsY;


    /* ***** MAIN OFFENCE DETAILS ****** */
    @TemplateField(value = "ChargeCode")
    private String chargeCode;

    @TemplateField(value = "Description")
    private String chargeDescription;

    @TemplateField(value = "SpeedReadings")
    private Integer speed1;

    @TemplateField(value = "SpeedReading2")
    private Integer speed2;

    @TemplateField(value = "AmberTime")
    private Integer amberTime;

    @TemplateField(value = "RedTime")
    private Integer redTime;

    @TemplateField(value = "Type")
    private String chargeType;

    @TemplateField(value = "DemeritPoints")
    private String demeritPoints;

    @TemplateField(value = "PenaltyAmount", format = "#.00")
    private BigDecimal penaltyAmount;

    @TemplateField(value = "DiscountAmount", format = "#.00")
    private BigDecimal discountAmount;

    @TemplateField(value = "DiscountedAmount", format = "#.00")
    private BigDecimal discountedAmount;



    /* ***** ALTERNATIVE OFFENCE DETAILS ****** */
    @TemplateField(value = "AltChargeCode")
    private String alternateChargeCode;

    @TemplateField(value = "AltChargeDescription")
    private String alternateChargeDescription;

    @TemplateField(value = "AltType")
    private String alternateChargeType;

    @TemplateField(value = "AltDemeritPoints")
    private String alternateDemeritPoints;

    @TemplateField(value = "AltPenaltyAmount", format = "#.00")
    private BigDecimal alternatePenaltyAmount;

    @TemplateField(value = "AltDiscountAmount", format = "#.00")
    private BigDecimal alternateDiscountAmount;

    @TemplateField(value = "AltDiscountedAmount", format = "#.00")
    private BigDecimal alternateDiscountedAmount;


    /* **** OFFICER DETAILS **** */
    @TemplateField(value = "IssuingAuthority")
    private String issuingAuthority;

    @TemplateField(value = "OfficerName")
    private String officerName;

    @TemplateField(value = "InfrastructureNo")
    private String officerInfrastructureNumber;


    /* **** FOOTER **** */
    @TemplateField(value = "Cert_no")
    private String certificateNumber;

    @TemplateField(value = "Cr_date")
    private LocalDate creationDate;

    @TemplateField(value = "User_group")
    private String userGroup;

    private String ToEmailAddress;

    public String getToEmailAddress() {
        return ToEmailAddress;
    }

    public void setToEmailAddress(String toEmailAddress) {
        ToEmailAddress = toEmailAddress;
    }

    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    public void setInfringementNoticeNumber(String infringementNoticeNumber) {
        this.infringementNoticeNumber = infringementNoticeNumber;
    }

    public String getSurnameOrNameOfOrganisation() {
        return surnameOrNameOfOrganisation;
    }

    public void setSurnameOrNameOfOrganisation(String surnameOrNameOfOrganisation) {
        this.surnameOrNameOfOrganisation = surnameOrNameOfOrganisation;
    }

    public String getFirstNames() {
        return firstNames;
    }

    public void setFirstNames(String firstNames) {
        this.firstNames = firstNames;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getGenderOrTypeOfOrganisation() {
        return genderOrTypeOfOrganisation;
    }

    public void setGenderOrTypeOfOrganisation(String genderOrTypeOfOrganisation) {
        this.genderOrTypeOfOrganisation = genderOrTypeOfOrganisation;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getCountryOfIssue() {
        return countryOfIssue;
    }

    public void setCountryOfIssue(String countryOfIssue) {
        this.countryOfIssue = countryOfIssue;
    }

    public String getLicenceCodes() {
        return licenceCodes;
    }

    public void setLicenceCodes(String licenceCodes) {
        this.licenceCodes = licenceCodes;
    }

    public String getPrdpCodes() {
        return prdpCodes;
    }

    public void setPrdpCodes(String prdpCodes) {
        this.prdpCodes = prdpCodes;
    }

    public String getOperatorCardNumber() {
        return operatorCardNumber;
    }

    public void setOperatorCardNumber(String operatorCardNumber) {
        this.operatorCardNumber = operatorCardNumber;
    }

    public String getStreetAddressLine1() {
        return streetAddressLine1;
    }

    public void setStreetAddressLine1(String streetAddressLine1) {
        this.streetAddressLine1 = streetAddressLine1;
    }

    public String getStreetAddressLine2() {
        return streetAddressLine2;
    }

    public void setStreetAddressLine2(String streetAddressLine2) {
        this.streetAddressLine2 = streetAddressLine2;
    }

    public String getStreetAddressLine3() {
        return streetAddressLine3;
    }

    public void setStreetAddressLine3(String streetAddressLine3) {
        this.streetAddressLine3 = streetAddressLine3;
    }

    public String getStreetAddressLine4() {
        return streetAddressLine4;
    }

    public void setStreetAddressLine4(String streetAddressLine4) {
        this.streetAddressLine4 = streetAddressLine4;
    }

    public String getStreetAddressLine5() {
        return streetAddressLine5;
    }

    public void setStreetAddressLine5(String streetAddressLine5) {
        this.streetAddressLine5 = streetAddressLine5;
    }

    public String getStreetAddressCode() {
        return streetAddressCode;
    }

    public void setStreetAddressCode(String streetAddressCode) {
        this.streetAddressCode = streetAddressCode;
    }

    public String getPostAddressLine1() {
        return postAddressLine1;
    }

    public void setPostAddressLine1(String postAddressLine1) {
        this.postAddressLine1 = postAddressLine1;
    }

    public String getPostAddressLine2() {
        return postAddressLine2;
    }

    public void setPostAddressLine2(String postAddressLine2) {
        this.postAddressLine2 = postAddressLine2;
    }

    public String getPostAddressLine3() {
        return postAddressLine3;
    }

    public void setPostAddressLine3(String postAddressLine3) {
        this.postAddressLine3 = postAddressLine3;
    }

    public String getPostAddressLine4() {
        return postAddressLine4;
    }

    public void setPostAddressLine4(String postAddressLine4) {
        this.postAddressLine4 = postAddressLine4;
    }

    public String getPostAddressLine5() {
        return postAddressLine5;
    }

    public void setPostAddressLine5(String postAddressLine5) {
        this.postAddressLine5 = postAddressLine5;
    }

    public String getPostAddressCode() {
        return postAddressCode;
    }

    public void setPostAddressCode(String postAddressCode) {
        this.postAddressCode = postAddressCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerAddressLine1() {
        return ownerAddressLine1;
    }

    public void setOwnerAddressLine1(String ownerAddressLine1) {
        this.ownerAddressLine1 = ownerAddressLine1;
    }

    public String getOwnerAddressLine2() {
        return ownerAddressLine2;
    }

    public void setOwnerAddressLine2(String ownerAddressLine2) {
        this.ownerAddressLine2 = ownerAddressLine2;
    }

    public String getOwnerAddressLine3() {
        return ownerAddressLine3;
    }

    public void setOwnerAddressLine3(String ownerAddressLine3) {
        this.ownerAddressLine3 = ownerAddressLine3;
    }

    public String getOwnerAddressLine4() {
        return ownerAddressLine4;
    }

    public void setOwnerAddressLine4(String ownerAddressLine4) {
        this.ownerAddressLine4 = ownerAddressLine4;
    }

    public String getOwnerAddressLine5() {
        return ownerAddressLine5;
    }

    public void setOwnerAddressLine5(String ownerAddressLine5) {
        this.ownerAddressLine5 = ownerAddressLine5;
    }

    public String getOwnerAddressCode() {
        return ownerAddressCode;
    }

    public void setOwnerAddressCode(String ownerAddressCode) {
        this.ownerAddressCode = ownerAddressCode;
    }

    public String getVehicleLicenceNumber() {
        return vehicleLicenceNumber;
    }

    public void setVehicleLicenceNumber(String vehicleLicenceNumber) {
        this.vehicleLicenceNumber = vehicleLicenceNumber;
    }

    public String getVehicleLicenceDiscNumber() {
        return vehicleLicenceDiscNumber;
    }

    public void setVehicleLicenceDiscNumber(String vehicleLicenceDiscNumber) {
        this.vehicleLicenceDiscNumber = vehicleLicenceDiscNumber;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public String getGrossVehicleMass() {
        return grossVehicleMass;
    }

    public void setGrossVehicleMass(String grossVehicleMass) {
        this.grossVehicleMass = grossVehicleMass;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleSeries() {
        return vehicleSeries;
    }

    public void setVehicleSeries(String vehicleSeries) {
        this.vehicleSeries = vehicleSeries;
    }

    public String getVehicleColour() {
        return vehicleColour;
    }

    public void setVehicleColour(String vehicleColour) {
        this.vehicleColour = vehicleColour;
    }

    public byte[] getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(byte[] vehicleImage) {
        this.vehicleImage = vehicleImage;
    }

    public byte[] getLicensePlateImage() {
        return licensePlateImage;
    }

    public void setLicensePlateImage(byte[] licensePlateImage) {
        this.licensePlateImage = licensePlateImage;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCityTown() {
        return cityTown;
    }

    public void setCityTown(String cityTown) {
        this.cityTown = cityTown;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public LocalDate getInfringementDate() {
        return infringementDate;
    }

    public void setInfringementDate(LocalDate infringementDate) {
        this.infringementDate = infringementDate;
    }

    public LocalTime getInfringementTime() {
        return infringementTime;
    }

    public void setInfringementTime(LocalTime infringementTime) {
        this.infringementTime = infringementTime;
    }

    public String getStreetNameA() {
        return streetNameA;
    }

    public void setStreetNameA(String streetNameA) {
        this.streetNameA = streetNameA;
    }

    public String getStreetNameB() {
        return streetNameB;
    }

    public void setStreetNameB(String streetNameB) {
        this.streetNameB = streetNameB;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public String getDirectionFrom() {
        return directionFrom;
    }

    public void setDirectionFrom(String directionFrom) {
        this.directionFrom = directionFrom;
    }

    public String getDirectionTo() {
        return directionTo;
    }

    public void setDirectionTo(String directionTo) {
        this.directionTo = directionTo;
    }

    public String getGpsX() {
        return gpsX;
    }

    public void setGpsX(String gpsX) {
        this.gpsX = gpsX;
    }

    public String getGpsY() {
        return gpsY;
    }

    public void setGpsY(String gpsY) {
        this.gpsY = gpsY;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getChargeDescription() {
        return chargeDescription;
    }

    public void setChargeDescription(String chargeDescription) {
        this.chargeDescription = chargeDescription;
    }

    public Integer getSpeed1() {
        return speed1;
    }

    public void setSpeed1(Integer speed1) {
        this.speed1 = speed1;
    }

    public Integer getSpeed2() {
        return speed2;
    }

    public void setSpeed2(Integer speed2) {
        this.speed2 = speed2;
    }

    public Integer getAmberTime() {
        return amberTime;
    }

    public void setAmberTime(Integer amberTime) {
        this.amberTime = amberTime;
    }

    public Integer getRedTime() {
        return redTime;
    }

    public void setRedTime(Integer redTime) {
        this.redTime = redTime;
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public String getDemeritPoints() {
        return demeritPoints;
    }

    public void setDemeritPoints(String demeritPoints) {
        this.demeritPoints = demeritPoints;
    }

    public String getIssuingAuthority() {
        return issuingAuthority;
    }

    public void setIssuingAuthority(String issuingAuthority) {
        this.issuingAuthority = issuingAuthority;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getOfficerInfrastructureNumber() {
        return officerInfrastructureNumber;
    }

    public void setOfficerInfrastructureNumber(String officerInfrastructureNumber) {
        this.officerInfrastructureNumber = officerInfrastructureNumber;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getMagisterialDistrict() {
        return null;
    }

    public void setMagisterialDistrict(String megisterialDistrict) {

    }

    public BigDecimal getPenaltyAmount() {
        return penaltyAmount;
    }

    public void setPenaltyAmount(BigDecimal penaltyAmount) {
        this.penaltyAmount = penaltyAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountedAmount() {
        return discountedAmount;
    }

    public void setDiscountedAmount(BigDecimal discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    public String getAlternateChargeCode() {
        return alternateChargeCode;
    }

    public void setAlternateChargeCode(String alternateChargeCode) {
        this.alternateChargeCode = alternateChargeCode;
    }

    public String getAlternateChargeDescription() {
        return alternateChargeDescription;
    }

    public void setAlternateChargeDescription(String alternateChargeDescription) {
        this.alternateChargeDescription = alternateChargeDescription;
    }

    public String getAlternateChargeType() {
        return alternateChargeType;
    }

    public void setAlternateChargeType(String alternateChargeType) {
        this.alternateChargeType = alternateChargeType;
    }

    public String getAlternateDemeritPoints() {
        return alternateDemeritPoints;
    }

    public void setAlternateDemeritPoints(String alternateDemeritPoints) {
        this.alternateDemeritPoints = alternateDemeritPoints;
    }

    public BigDecimal getAlternatePenaltyAmount() {
        return alternatePenaltyAmount;
    }

    public void setAlternatePenaltyAmount(BigDecimal alternatePenaltyAmount) {
        this.alternatePenaltyAmount = alternatePenaltyAmount;
    }

    public BigDecimal getAlternateDiscountAmount() {
        return alternateDiscountAmount;
    }

    public void setAlternateDiscountAmount(BigDecimal alternateDiscountAmount) {
        this.alternateDiscountAmount = alternateDiscountAmount;
    }

    public BigDecimal getAlternateDiscountedAmount() {
        return alternateDiscountedAmount;
    }

    public void setAlternateDiscountedAmount(BigDecimal alternateDiscountedAmount) {
        this.alternateDiscountedAmount = alternateDiscountedAmount;
    }

    public String getInfringeTime() {
        return infringeTime;
    }

    public void setInfringeTime(String infringeTime) {
        this.infringeTime = infringeTime;
    }
}
