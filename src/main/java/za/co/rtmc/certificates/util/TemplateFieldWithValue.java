package za.co.rtmc.certificates.util;

import org.joda.time.base.BaseLocal;
import org.joda.time.format.DateTimeFormat;
import org.springframework.util.StringUtils;

import java.text.NumberFormat;

/**
 * <strong>Description</strong> <br>
 *  holds the (TemplateField) annotations assiged to a field on the DTO and the value to be assigned to the field
 *
 * @author Jacob marola
 */
public class TemplateFieldWithValue {

    private TemplateField templateField;
    private Object value;

    public TemplateFieldWithValue(TemplateField templateField, Object value) {
        this.templateField = templateField;
        this.value = value;
    }

    public TemplateField getTemplateField() {
        return templateField;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public String getValueAsString() {
        if (value == null) {
            return "";
        }
        if(StringUtils.hasText(templateField.format())) {
            if (BaseLocal.class.isAssignableFrom(value.getClass())) {
                return ((BaseLocal)value).toString(DateTimeFormat.forPattern(templateField.format()));
            } else if (Number.class.isAssignableFrom(value.getClass())) {
                NumberFormat formatter = NumberFormat.getInstance();
                return formatter.format(((Number) value).doubleValue());
            }
        }
        return value.toString();

    }
}
