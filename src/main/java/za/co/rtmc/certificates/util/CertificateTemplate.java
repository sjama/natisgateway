package za.co.rtmc.certificates.util;

import java.lang.annotation.*;

/**
 * Specifies that a class is a CertificateTemplate which a PDF can be generated from
 *
 * @author Jacob Marola
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CertificateTemplate {

    /**
     * Template filename
     */
    String templateFile();

    /**
     * How field keys are calculated
     */
    FieldKeyType fieldKeyType() default FieldKeyType.BASIC;

    /**
     * XmlRootElement for xml binding object (if applicable) 
     */
    String xmlRootElement() default "";

    /**
     * Selects the pages to keep in the document.
     * The pages are described as ranges. The page ordering can be changed but no page repetitions are allowed.
     * We might later need to allow different pageRanges based on context. Will need to refactor this then.
     */
    String pageRange() default "1";

    /**
     * Determines the certificate code (used as first entry in certificate code String) 
     */
    String certificateCode() default "";
}
