package za.co.rtmc.certificates.util;

/**
 * Retrieves key value
 * For the most part it seems that iText will resolve STRUCTURED fields even if set as BASIC
 * This might become an issue with different pages containing the same field name
 *
 * @author Jacob Marola
 */
public enum FieldKeyType {

    BASIC, // Plain name. Example: fieldKey
    STRUCTURED; // Created using Adobe Distiller. Example: topmostSubform[0].Page1[0].fieldKey[0]

    public String getKey(TemplateField field) {
        if (STRUCTURED.equals(this)) {
            // This can be expanded as necessary
            return "topmostSubform[0].Page" + field.page() + "[0]." + field.value() + "[" + field.instance() + "]";
        } else  {
            return field.value();
        }
    }
}
