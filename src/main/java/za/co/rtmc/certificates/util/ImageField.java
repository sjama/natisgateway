package za.co.rtmc.certificates.util;

/**
 * Represents an image on a Certificate
 * The file must be in the images directory
 *
 * Currently only supports fileName, we can change this to support passing byte[] or other formats
 * <p/>
 * @author Jacob Marola
 */
public class ImageField {

    private String fileName;

    public ImageField(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
