package za.co.rtmc.common;

public interface AartoService {
	/**
     * Create Invoice document 
     */
    byte[] createDocument(Object invoice, String fileName, String invKey);
    
    /**
     * Saves a document to the repository
     */
    void saveDocumentToRepository(byte[] document, Object CertificateDto, String fileName, String invKey);

}
