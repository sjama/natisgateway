package za.co.rtmc.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.rtmc.certificates.dto.Aarto03;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


@Service
public class Aarto03ServiceImpl {
	private static final String TEMPLATE_FILE = "/PreBookingResultsPage.pdf";
	private File invoiceTemplate = null;

	@Autowired
	public AartoBuilderServiceImpl letterBuilderService;

    @Autowired
	MailerService mailerService;

	public byte[] processFine(Aarto03 dto, String templateName) {

		// We first need to populate the dto
		byte[] letter = createLetter( dto, templateName);

		if(dto.getToEmailAddress() != null && !dto.getToEmailAddress().isEmpty())
		    sendNotification(dto, letter);

		return letter;
	}


	private byte[] createLetter(Aarto03 dto , String templateName) {
		// Lets build the AppResultsDto values

//		BookingConfirmationDto bookingConfirmationDto = new BookingConfirmationDto();
//		bookingConfirmationDto.setBookingDltcName(dto.getInfN());
//		bookingConfirmationDto.setBookingExpDate(dto.getPayD());
//		bookingConfirmationDto.setBusOrSurname(dto.getBusOrSurname());
//		bookingConfirmationDto.setDateTime(dto.getBookD());
//		bookingConfirmationDto.setDltcName(dto.getInfN());
//		bookingConfirmationDto.setIdentificationNo(dto.getIdDocN());
//		bookingConfirmationDto.setIdentificationType(dto.getIdDocTypeCd());
//		bookingConfirmationDto.setInitials(dto.getInitials());
//		bookingConfirmationDto.setLicenseCode(dto.getLicCd());
//
//		bookingConfirmationDto.setPostAddressCode(dto.getDltcPostCdStreet());
//		bookingConfirmationDto.setPostAddressLine1(dto.getDltcStreetAddress1());
//		bookingConfirmationDto.setPostAddressLine2(dto.getDltcStreetAddress2());
//		bookingConfirmationDto.setPostAddressLine3(dto.getDltcStreetAddress3());
//		bookingConfirmationDto.setPostAddressLine4(dto.getDltcStreetAddress4());
//
//		bookingConfirmationDto.setPreBookingRefNo(dto.getBookingReferenceN());
//		bookingConfirmationDto.setTestType(dto.getTestCat());
//        BigDecimal txanFee = new BigDecimal(dto.getPayAmt()).setScale(2);
//		bookingConfirmationDto.setTxanFee("R "+txanFee);
//        bookingConfirmationDto.setDateAndTime(DateUtils.getCurrDateString());
//
//
//		byte[] letter = letterBuilderService.createDocument(bookingConfirmationDto, "BOOKING_CONFIRMATION", templateName);
//
//		//writeBytesToFileClassic(letter, "/Users/Jay/Desktop/letter_" + dto.getIdNumber() + "_.pdf");

		return null;
		//saveToRepository( invNumber, fromDate, toDate, invKey);
	}

	private static void writeBytesToFileClassic(byte[] bFile, String fileDest) {

		FileOutputStream fileOuputStream = null;

		try {
			fileOuputStream = new FileOutputStream(fileDest);
			fileOuputStream.write(bFile);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fileOuputStream != null) {
				try {
					fileOuputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}



    private void sendNotification(Aarto03 dto, byte [] attach) {

		try {
			mailerService.sendMailAttachment(dto, attach, dto.getToEmailAddress() );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



}
