package za.co.rtmc.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AartoBuilderServiceImpl implements AartoService {
    @Autowired
    private PdfBuilderTemplate documentBuilder;


	@Override
	public byte[] createDocument(Object invoice, String fileName, String invKey) {
		return createDocumentAndSave(invoice, fileName, invKey);
	}

	/***************************************************************************************/
    private byte[] createDocumentAndSave(Object CertificateDto, String fileName, String templateName) {

        byte[] document = documentBuilder.buildDocument(CertificateDto, null, templateName);

        saveDocumentToRepository(document, CertificateDto, fileName, templateName);
        return document;
    }



	@Override
	public void saveDocumentToRepository(byte[] document, Object CertificateDto, String fileName, String invKey) {

//		DocRepos docRepos = new DocRepos();
//		docRepos.setCreatedDate(new Date());
//		docRepos.setFileData(document);
//		docRepos.setFileName(fileName);
//		docRepos.setRefNo(invKey);
//
//		docRepo.save(docRepos);

	}

}
