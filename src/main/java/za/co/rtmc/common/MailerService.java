package za.co.rtmc.common;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import za.co.rtmc.certificates.dto.Aarto03;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class MailerService {

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

//    public String sendMailAttachment(PreBookingSuccessDto preBookingSuccessDto, byte [] attachment, String toEmailAddress) throws MessagingException {
//        MimeMessage message = mailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message,true);
//        try {
//            helper.setTo(toEmailAddress);
//
//            if (preBookingSuccessDto.getTestType().equals("01") || preBookingSuccessDto.getTestType().equals("02")){
//                helper.setText("Dear " +preBookingSuccessDto.getBusOrSurname() +" " + preBookingSuccessDto.getInitials() +
//                        "\n\n The preferred date and time as indicated by you is herewith reserved and will ONLY be confirmed upon you " +
//                        "applying in person and paying the required fee at the DLTC, as indicated, on or before the expiry date.\n\n" +
//                        "Thank You\n" +
//                        "NaTIS Booking Team");
//            }else{
//                helper.setText("Dear " +preBookingSuccessDto.getBusOrSurname() +" " + preBookingSuccessDto.getInitials() +
//                        "\n\n The date and time, as per the booking information below, is reserved for you in order to renew your " +
//                        "Driving Licence Card (or Professional Driving Permit). You must apply in person and pay the prescribed fee on the date and time indicated below." +
//                        " Please Note: Late arrival on the day may result in delays.\n\n" +
//                        "Thank You\n" +
//                        "NaTIS Booking Team");
//            }
//
//            Context context = new Context();
//            context.setVariable("logo", "logo");
//            context.setVariable("footer", "footer");
//            context.setVariable("greeting", "Dear "+preBookingSuccessDto.getBusOrSurname() );
//            String html = null;
//            if (preBookingSuccessDto.getTestType().equals("01") || preBookingSuccessDto.getTestType().equals("02")){
//                 html = templateEngine.process("licence-email-template", context);
//            }else{
//                 html = templateEngine.process("prdp-email-templete", context);
//            }
//            helper.setText(html, true);
//            helper.addInline("footer", new ClassPathResource("static/img/arrive_Logo.png"));
//            helper.addInline("logo", new ClassPathResource("static/img/NaTIS_Logo.png"));
//
//            helper.setSubject("Booking Information");
//            helper.setFrom("noreply@rtmc.co.za");
//            DataSource dataSource1 = new ByteArrayDataSource(attachment, "application/pdf");
//            helper.addAttachment("Booking_information.pdf", dataSource1);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//            return "Error while sending mail ..";
//        }
//        mailSender.send(message);
//        return "Mail Sent Success!";
//    }

//    public void sendOtpMessage(String to, String subject, String message) {
//
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost("smtp.gmail.com");
//        mailSender.setPort(587);
//
//        mailSender.setUsername("sitholejaytee@gmail.com");
//        mailSender.setPassword("Nudule@21");
//
//        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", "smtp");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.debug", "true");
//
//
//        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
//        simpleMailMessage.setFrom(mailSender.getUsername());
//        simpleMailMessage.setTo(to);
//        simpleMailMessage.setSubject(subject);
//        simpleMailMessage.setText(message);
//
//        //Uncomment to send mail
//        mailSender.send(simpleMailMessage);
//    }

    // We shall update to take a standard email Object to make it reusable
    public void sendMailAttachment(Aarto03 aarto03, byte [] attachment, String toEmailAddress) {

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        final String username =  "sitholejaytee@gmail.com";
        final String password =  "Nudule@21";

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        try {
            // Get the Session object.
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            Message simpleMailMessage = new MimeMessage(session);

            simpleMailMessage.setFrom(new InternetAddress("sitholejaytee@gmail.com"));
            simpleMailMessage.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(aarto03.getToEmailAddress()));
            simpleMailMessage.setSubject("Infringement Notice");
            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText("Dear " + aarto03.getSurnameOrNameOfOrganisation() +" " + aarto03.getInitials() +
                    " \n Infringement Notice attached.\n" );
            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();

            DataSource dataSource1 = new ByteArrayDataSource(attachment, "application/pdf");
           // messageBodyPart.addAttachment("Infringement.pdf", dataSource1);
//
//
            messageBodyPart.setDataHandler(new DataHandler(dataSource1));
            messageBodyPart.setFileName("Infringement.pdf");
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            simpleMailMessage.setContent(multipart);
            //Uncomment to send mail
            Transport.send(simpleMailMessage);

        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    public void sendSimpleMessage(Aarto03 aarto03, byte [] attachment, String toEmailAddress) throws MessagingException, IOException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        Context context = new Context();
        Map model = new HashMap();
        model.put("name", aarto03.getSurnameOrNameOfOrganisation() + " " + aarto03.getInitials());
        //context.setVariables(model);
        String html = templateEngine.process("email-template", context);

        helper.setTo(toEmailAddress);
        helper.setText("Dear " + aarto03.getSurnameOrNameOfOrganisation() +" " + aarto03.getInitials() +
                " \n Infringement Notice attached.\n" );
        helper.setSubject("Infringement Notice");
//        helper.setText(html, true);
        helper.setFrom("sitholejaytee@gmail.com");
        DataSource dataSource1 = new ByteArrayDataSource(attachment, "application/pdf");
        helper.addAttachment("Infringement.pdf", dataSource1);

        mailSender.send(message);
    }




//    public static void main(String[] args) {
//
//        MailerService emailsService = new MailerService();
//        emailsService.sendOtpMessage("jamas@rtmc.co.za","Booking Confirmation",
//                "Test email");
//    }



//    public void sendMail(String fromEmailAddr,
//                         String toEmailAddr,
//                         String subject,
//                         MailDto mailDto,
//                         byte[] attachment,
//                         String attachmentName) throws InvalidEmailAddrException,
//            AddressException,
//            MessagingException {
//
//        // Setup the Message Header
//        MimeMessage message = mailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message,true);
//        try{
//            helper.setFrom(new InternetAddress(fromEmailAddr));
//            helper.setTo(InternetAddress.parse(toEmailAddr));
//            helper.setSubject(subject);
//            Context context = new Context();
//            context.setVariable("logo", "logo");
//            context.setVariable("footer", "footer");
//            context.setVariable("greeting", "Dear "+ mailDto.getNameSurname().getFieldValue() );
//            context.setVariable("regtn", mailDto.getRegTn().getFieldValue() );
//            context.setVariable("licexpridy", mailDto.getExpDate().getFieldValue());
//            String html = templateEngine.process("mvl2-email-template", context);
//            helper.setText(html, true);
//            helper.addInline("footer", new ClassPathResource("static/img/arrive_Logo.png"));
//            helper.addInline("logo", new ClassPathResource("static/img/NaTIS_Logo.png"));
//
//            // code to add attachment
//            DataSource dataSource1 = new ByteArrayDataSource(attachment, "application/pdf");
//            helper.addAttachment(attachmentName, dataSource1);
//
//            mailSender.send(message);
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }


}


