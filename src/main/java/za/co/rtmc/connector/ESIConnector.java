package za.co.rtmc.connector;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.cocoon.configuration.Settings;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultSchemePortResolver;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import tasima.common.ws.schema.ENaTIS;

import tasima.common.ws.schema.ENaTISService;
import tasima.common.ws.schema.InfringementQueryRequest;
import tasima.common.ws.schema.InfringementQueryResponse;
import tasima.common.ws.schema.MicrodotRequest;
import tasima.common.ws.schema.QueryInfringementsForWebsiteRequest;
import tasima.common.ws.schema.QueryInfringementsForWebsiteRequest.Infringer;
import tasima.common.ws.schema.QueryInfringementsForWebsiteResponse;
import tasima.common.ws.schema.QueryInfringer;
import tasima.common.ws.schema.UploadMicrodotRequest;
import tasima.common.ws.schema.UploadMicrodotResponse;
import za.co.rtmc.constants.Constants;
import za.co.rtmc.datadefs.BaseHeader;
import za.co.rtmc.datadefs.BaseReq;
import za.co.rtmc.datadefs.EsUserInfo;
import za.co.rtmc.datadefs.Ext01Req;
import za.co.rtmc.datadefs.Ext12Req;
import za.co.rtmc.datadefs.Ext13Req;
import za.co.rtmc.datadefs.Ext18Req;
import za.co.rtmc.datadefs.Ext25Req;
import za.co.rtmc.datadefs.PayDetail;
import za.co.rtmc.datadefs.SecurityCredentials;
import za.co.rtmc.datadefs.TransferDetail;
import za.co.rtmc.utils.XmlUtils;

@Service
public class ESIConnector {

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Autowired
    private Settings settings;

    private String sessionId;
    private String path;

    private String url;
    private String opModInd;
    private String keystoreLocation;
    private String keystorePass;
    private String esUserN;
    private String esUid;
    private String esTermId;
    private String userN;
    private String password;

    private HttpClient httpClient;

    public String queryVehicleFull(String mvRegn, String vinOrchassis, String regtN) throws Exception {

        Ext12Req req = new Ext12Req();

        req.setHeader(getHeader());

        req.setMvRegN(mvRegn);
        req.setVinOrChassis(vinOrchassis);
        req.setRegtN(regtN);

        return postForTxan(Constants.MENUCD_X0001, req);

    }

    public String queryVehicleSummary(String mvRegn, String vinOrchassis, String regtN) throws Exception {

        Ext18Req req = new Ext18Req();

        req.setHeader(getHeader());

        req.setMvRegN(mvRegn);
        req.setVinOrChassis(vinOrchassis);
        req.setRegtN(regtN);

        return postForTxan(Constants.MENUCD_X0007, req);

    }
    /*
    Only to be used for Section 56. AARTO not in scope.
    */
    
    public String payInfringement(String aofn, String payAmount, String reference) throws Exception {

        Ext25Req req = new Ext25Req();

        req.setHeader(getHeader());
        
        PayDetail payDetail =  new PayDetail();
        payDetail.setPayAmount(payAmount);
        payDetail.setPayDate(new Date().toString());
        
        TransferDetail transDetail = new TransferDetail();
        transDetail.setTransferDate(new Date().toString());
        transDetail.setTransferReference(reference);

        req.setAofAssessN(aofn);
        req.setPayment(payDetail);
        req.setTransfer(transDetail);

        return postForTxan(Constants.MENUCD_X0007, req);

    }

    
    

    public String queryPersonFull(String idDocTypeCd, String idDocN, String busOrSurname,
            String cardN, String licCertN) throws Exception {

        Ext13Req req = new Ext13Req();

        req.setHeader(getHeader());

        req.setIdDocN(idDocN);
        req.setIdDocTypeCd(idDocTypeCd);
        req.setBusOrSurname(busOrSurname);
        req.setCardN(cardN);
        req.setLrnCertN(licCertN);

        return postForTxan(Constants.MENUCD_X0002, req);

    }

    @Scheduled(fixedDelay = 1800000)
    public void keepAlive() throws Exception {

        login();
    }

    public String login() throws Exception {
        return postForLogin();

    }

    public String logout() throws Exception {
        String ret = postForTxan(Constants.MENUCD_X0000, getExt01Req(false));
        this.sessionId = null;
        return ret;
    }

    private BaseHeader getHeader() {
        BaseHeader header = new BaseHeader();

        header.setTxanId(RandomStringUtils.random(4, String.valueOf(System.currentTimeMillis())));
        header.setOpModInd(opModInd);
        EsUserInfo esUserInfo = new EsUserInfo();
        esUserInfo.setEsUserN(esUserN);
        esUserInfo.setEsUid(esUid);
        esUserInfo.setEsTermId(esTermId);
        header.setEsUserInfo(esUserInfo);

        return header;
    }

    public UploadMicrodotResponse postMicroDot(MicrodotRequest microdot) {
        UploadMicrodotResponse microdotResponse = null;
        try {
            //System.setProperty("javax.net.ssl.trustStore", "/usr/java/jdk1.8.0_131/jre/lib/security/cacerts");
           
            System.setProperty("javax.net.ssl.trustStore", "/Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home/jre/lib/security/cacerts");
           //System.setProperty("javax.net.ssl.trustStore", "/Users/jamasithole/jssecacerts");  
           System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
            System.setProperty("javax.net.ssl.keyStore", keystoreLocation);
            System.setProperty("javax.net.ssl.keyStorePassword", keystorePass);
            System.setProperty("javax.net.ssl.keyStoreType", "pkcs12");
            System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//		System.setProperty("javax.net.ssl.keyStore", keystoreLocation);

            ENaTISService eNaTISService = new ENaTISService();
            ENaTIS myServicePort = eNaTISService.getENaTISSoap11();

            // This is the block that apply the Ws Security to the request
            BindingProvider bindingProvider = (BindingProvider) myServicePort;
            @SuppressWarnings("rawtypes")
            List<Handler> handlerChain = new ArrayList<Handler>();
            handlerChain.add(new WSSecurityHeaderSOAPHandler("4604A001", "B0W757"));
            bindingProvider.getBinding().setHandlerChain(handlerChain);

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = null;
            try {
                startDate = df.parse(microdot.getEffectiveDate());
                String newDateString = df.format(startDate);
                System.out.println(newDateString);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String FORMATER = "yyyy-MM-dd";

            DateFormat format = new SimpleDateFormat(FORMATER);

            XMLGregorianCalendar gDateFormatted1
                    = DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(startDate));
            System.out.println("Formatted using SimpleDateFormat: " + gDateFormatted1.toString());

            UploadMicrodotRequest microdotRequest = new UploadMicrodotRequest();
            microdotRequest.setMicrodotPIN(microdot.getMicrodotPIN());
            microdotRequest.setVinOrChassisNumber(microdot.getVinOrChassisNumber());
            microdotRequest.setEffectiveDate(gDateFormatted1);

            microdotResponse = myServicePort.uploadMicrodot(microdotRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return microdotResponse;
    }
    
   
    public InfringementQueryResponse queryInfring( QueryInfringer infRequest ) {
		InfringementQueryResponse response =  null;
		try {
                        System.setProperty("javax.net.ssl.trustStore", "/Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home/jre/lib/security/cacerts");
			//System.setProperty("javax.net.ssl.trustStore", "/usr/java/default/jre/lib/security/cacerts");
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
			System.setProperty("javax.net.ssl.keyStore", keystoreLocation);
			System.setProperty("javax.net.ssl.keyStorePassword", keystorePass);
			System.setProperty("javax.net.ssl.keyStoreType", "pkcs12");
			System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");

			ENaTISService  eNaTISService = new ENaTISService();
			ENaTIS myServicePort = eNaTISService.getENaTISSoap11();

			// This is the block that apply the Ws Security to the request
			BindingProvider bindingProvider = (BindingProvider) myServicePort;
			@SuppressWarnings("rawtypes")
			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new WSSecurityHeaderSOAPHandler("4823A014", "TESTER01"));
			bindingProvider.getBinding().setHandlerChain(handlerChain);


			InfringementQueryRequest  request = new InfringementQueryRequest();
			QueryInfringer infringer = new QueryInfringer();
			infringer.setIdDocumentTypeCode("02");
			infringer.setIdDocumentNumber("8512265513081");

			request.setQueryInfringer(infringer);

			response =  myServicePort.infringementQuery( request );
		} catch (Exception e) {
			e.printStackTrace();
		}


		return response;
	}
    
     public QueryInfringementsForWebsiteResponse queryInfringer( QueryInfringer infRequest, String licCardN ) {
		QueryInfringementsForWebsiteResponse response =  null;
		try {
                        System.setProperty("javax.net.ssl.trustStore", "/Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home/jre/lib/security/cacerts");
			//System.setProperty("javax.net.ssl.trustStore", "/usr/java/default/jre/lib/security/cacerts");
			System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
			System.setProperty("javax.net.ssl.keyStore", keystoreLocation);
			System.setProperty("javax.net.ssl.keyStorePassword", keystorePass);
			System.setProperty("javax.net.ssl.keyStoreType", "pkcs12");
			System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");

			ENaTISService  eNaTISService = new ENaTISService();
			ENaTIS myServicePort = eNaTISService.getENaTISSoap11();

			// This is the block that apply the Ws Security to the request
			BindingProvider bindingProvider = (BindingProvider) myServicePort;
			@SuppressWarnings("rawtypes")
			List<Handler> handlerChain = new ArrayList<Handler>();
			handlerChain.add(new WSSecurityHeaderSOAPHandler("4823A014", "TESTER01"));
			bindingProvider.getBinding().setHandlerChain(handlerChain);


			QueryInfringementsForWebsiteRequest  request = new QueryInfringementsForWebsiteRequest();
			Infringer infringer = new Infringer();
			infringer.setIdDocumentTypeCode(infRequest.getIdDocumentTypeCode());
                        infringer.setLicCardN(licCardN);
			infringer.setIdDocumentNumber(infRequest.getIdDocumentNumber());
			

			request.setInfringer(infringer);
			

//			UploadMicrodotRequest microdotRequest = new  UploadMicrodotRequest();
//			microdotRequest.setMicrodotPIN( microdot.getMicrodotPIN() );
//			microdotRequest.setVinOrChassisNumber( microdot.getVinOrChassisNumber() );
//			microdotRequest.setEffectiveDate( gDateFormatted1 );

		  response =  myServicePort.queryInfringementsForWebsite(request);
		} catch (Exception e) {
			e.printStackTrace();
		}


		return response;
	}

    private Ext01Req getExt01Req(boolean login) {

        Ext01Req ext01Req = new Ext01Req();

        ext01Req.setExTxanCd("X0000");

        ext01Req.setHeader(getHeader());

        ext01Req.setTxanType((login) ? Constants.X0000_LOGIN : Constants.X0000_LOGOUT);
        SecurityCredentials creds = new SecurityCredentials();
        creds.setUserNumber(userN);
        creds.setCurrPwd(password);
        ext01Req.setSecurityCredentials(creds);

        return ext01Req;
    }

    private String postForTxan(String txanName, BaseReq req) throws Exception {

        HttpPost httppost = new HttpPost(url + txanName);
        ArrayList<NameValuePair> postParameters;

        postParameters = new ArrayList<NameValuePair>();
        String request = XmlUtils.marshalObject(req);
        System.out.println(request);
        postParameters.add(new BasicNameValuePair(Constants.XML_REQ, request));
        httppost.setEntity(new UrlEncodedFormEntity(postParameters));

        httppost.setHeader(Constants.COOKIE, this.sessionId);

        HttpResponse response = httpClient.execute(httppost);

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result);

        EntityUtils.consume(response.getEntity());

        return result.toString();

    }

    private String postForLogin() throws Exception {

        HttpPost httppost = new HttpPost(url + Constants.MENUCD_X0000);
        ArrayList<NameValuePair> postParameters;
        postParameters = new ArrayList<NameValuePair>();
        String request = XmlUtils.marshalObject(getExt01Req(true));
        System.out.println(request);
        postParameters.add(new BasicNameValuePair(Constants.XML_REQ, request));

        httppost.setEntity(new UrlEncodedFormEntity(postParameters));

        if (this.sessionId != null) {
            httppost.setHeader(Constants.COOKIE, this.sessionId);

        }

        // execute the method
        HttpResponse response = httpClient.execute(httppost);

        //		UploadMicrodotRequest microdotRequest = new UploadMicrodotRequest();
        //		microdotRequest.setMicrodotPIN( "kkkk" );
        ////		microdotRequest.setEffectiveDate( "kkkk" );
        //		microdotRequest.setVinOrChassisNumber("kkkk");
        //
        //		new ENaTISService().getENaTISSoap11().uploadMicrodot(microdotRequest);
        Header header = response.getFirstHeader(Constants.SET_COOKIE);
        String cookieVal = null;
        if (!ObjectUtils.isEmpty(header)) {
            cookieVal = header.getValue();
        }

        if (cookieVal != null) {
            StringTokenizer values = new StringTokenizer(cookieVal, ";");
            while (values.hasMoreTokens()) {
                String token = values.nextToken();
                if (token.startsWith(Constants.SESSION_ID)) {
                    sessionId = token;
                    System.out.println(sessionId);

                }
                if (token.startsWith(Constants.PATH)) {
                    path = token.trim();
                    System.out.println(path);

                }
            }
        }

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result);

        EntityUtils.consume(response.getEntity());

        return result.toString();
    }

    @PostConstruct
    public void afterPropertiesSet() throws Exception {

        url = settings.getProperty(Constants.PROP_URL);
        opModInd = settings.getProperty(Constants.PROP_OPMODIND);
        keystoreLocation = settings.getProperty(Constants.PROP_KEYSTORE_LOCATION);
        keystorePass = settings.getProperty(Constants.PROP_KEYSTORE_PASSWORD);
        esUserN = settings.getProperty(Constants.PROP_ESUSERN);
        esUid = settings.getProperty(Constants.PROP_ESUID);
        esTermId = settings.getProperty(Constants.PROP_ESTERMID);
        userN = settings.getProperty(Constants.PROP_USERN);
        password = settings.getProperty(Constants.PROP_PASSWD);

        KeyStore clientKeyStore = KeyStore.getInstance(Constants.PKCS12);
        clientKeyStore.load(new FileInputStream(keystoreLocation), keystorePass.toCharArray());

        SSLContextBuilder sslContextBuilder = SSLContexts.custom().useProtocol(Constants.TLS);

        if (!ObjectUtils.isEmpty(clientKeyStore)) {
            sslContextBuilder.loadKeyMaterial(clientKeyStore, keystorePass.toCharArray());
        }
        SSLContext sslContext = sslContextBuilder.build();
        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext, new AllowAllHostnameVerifier());

        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register(Constants.HTTPS, socketFactory)
                .build();

        httpClient = HttpClientBuilder.create()
                .setSSLSocketFactory(socketFactory)
                .setHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                .setConnectionManager(new PoolingHttpClientConnectionManager(registry))
                .setSchemePortResolver(new DefaultSchemePortResolver())
                .build();

//		login();
        //		HttpClientConnectionManager poolingConnManager = new PoolingHttpClientConnectionManager();
        //		httpClient = HttpClients.custom().setConnectionManager(poolingConnManager).build();
    }

    @PreDestroy
    public void close() throws Exception {

        logout();

    }

}
