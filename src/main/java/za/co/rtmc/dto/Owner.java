package za.co.rtmc.dto;

import za.co.rtmc.dto.per.PerDet;

public class Owner
{
    private za.co.rtmc.dto.per.PerDet PerDet;

    public PerDet getPerDet ()
    {
        return PerDet;
    }

    public void setPerDet ( PerDet PerDet)
    {
        this.PerDet = PerDet;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PerDet = "+PerDet+"]";
    }
}