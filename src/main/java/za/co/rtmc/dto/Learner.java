
package za.co.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Learner {

    @SerializedName("LrnType")
    @Expose
    private LrnType lrnType;
    @SerializedName("LrnValidFromD")
    @Expose
    private String lrnValidFromD;
    @SerializedName("SpeclAdapInd")
    @Expose
    private String speclAdapInd;

    /**
     * 
     * @return
     *     The lrnType
     */
    public LrnType getLrnType() {
        return lrnType;
    }

    /**
     * 
     * @param lrnType
     *     The LrnType
     */
    public void setLrnType(LrnType lrnType) {
        this.lrnType = lrnType;
    }

    /**
     * 
     * @return
     *     The lrnValidFromD
     */
    public String getLrnValidFromD() {
        return lrnValidFromD;
    }

    /**
     * 
     * @param lrnValidFromD
     *     The LrnValidFromD
     */
    public void setLrnValidFromD(String lrnValidFromD) {
        this.lrnValidFromD = lrnValidFromD;
    }

    /**
     * 
     * @return
     *     The speclAdapInd
     */
    public String getSpeclAdapInd() {
        return speclAdapInd;
    }

    /**
     * 
     * @param speclAdapInd
     *     The SpeclAdapInd
     */
    public void setSpeclAdapInd(String speclAdapInd) {
        this.speclAdapInd = speclAdapInd;
    }

}
