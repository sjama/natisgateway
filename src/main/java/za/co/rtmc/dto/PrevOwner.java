package za.co.rtmc.dto;

public class PrevOwner
{
    private long idDocN;

    public long getIdDocN() {
        return idDocN;
    }

    public void setIdDocN(long idDocN) {
        this.idDocN = idDocN;
    }

    public String getOwnerShipD() {
        return ownerShipD;
    }

    public void setOwnerShipD(String ownerShipD) {
        this.ownerShipD = ownerShipD;
    }

    private String ownerShipD;

}