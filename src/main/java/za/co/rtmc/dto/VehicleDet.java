
package za.co.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleDet {

    @SerializedName("licN")
    @Expose
    private String licN;
    @SerializedName("vinOrChassis")
    @Expose
    private String vinOrChassis;
    @SerializedName("engineN")
    @Expose
    private String engineN;
    @SerializedName("regAuthOfLic")
    @Expose
    private RegAuthOfLic regAuthOfLic;
    @SerializedName("regAuthOfRegtN")
    @Expose
    private RegAuthOfRegtN regAuthOfRegtN;
    @SerializedName("modelName")
    @Expose
    private ModelName modelName;
    @SerializedName("driven")
    @Expose
    private Driven driven;
    @SerializedName("make")
    @Expose
    private Make make;
    @SerializedName("mainColour")
    @Expose
    private MainColour mainColour;
    @SerializedName("gvm")
    @Expose
    private String gvm;
    @SerializedName("engineDisp")
    @Expose
    private String engineDisp;
    @SerializedName("tare")
    @Expose
    private String tare;
    @SerializedName("fuelType")
    @Expose
    private FuelType fuelType;
    @SerializedName("netPower")
    @Expose
    private String netPower;
    @SerializedName("prevMVCertN")
    @Expose
    private String prevMVCertN;
    @SerializedName("vehType")
    @Expose
    private VehType vehType;
    @SerializedName("exemption")
    @Expose
    private Exemption exemption;
    @SerializedName("mvcat")
    @Expose
    private Mvcat mvcat;
    @SerializedName("mvregN")
    @Expose
    private String mvregN;
    @SerializedName("sapmark")
    @Expose
    private Sapmark sapmark;
    @SerializedName("mvlicChgD")
    @Expose
    private String mvlicChgD;
    @SerializedName("mvlicExpiryD")
    @Expose
    private String mvlicExpiryD;
    @SerializedName("mvdesc")
    @Expose
    private Mvdesc mvdesc;
    @SerializedName("mvregtD")
    @Expose
    private String mvregtD;
    @SerializedName("mvregtTypeQual")
    @Expose
    private MvregtTypeQual mvregtTypeQual;
    @SerializedName("rwstat")
    @Expose
    private Rwstat rwstat;
    @SerializedName("mvstate")
    @Expose
    private Mvstate mvstate;
    @SerializedName("mvstateChgD")
    @Expose
    private String mvstateChgD;
    @SerializedName("mvlifeStat")
    @Expose
    private MvlifeStat mvlifeStat;
    @SerializedName("nmodelN")
    @Expose
    private String nmodelN;
    @SerializedName("rwtstD")
    @Expose
    private String rwtstD;
    @SerializedName("sapclrStat")
    @Expose
    private SapclrStat sapclrStat;
    @SerializedName("mvusage")
    @Expose
    private Mvusage mvusage;
    @SerializedName("mveconSector")
    @Expose
    private MveconSector mveconSector;
    @SerializedName("mvregtType")
    @Expose
    private MvregtType mvregtType;

    public String getLicN() {
        return licN;
    }

    public void setLicN(String licN) {
        this.licN = licN;
    }

    public String getVinOrChassis() {
        return vinOrChassis;
    }

    public void setVinOrChassis(String vinOrChassis) {
        this.vinOrChassis = vinOrChassis;
    }

    public String getEngineN() {
        return engineN;
    }

    public void setEngineN(String engineN) {
        this.engineN = engineN;
    }

    public RegAuthOfLic getRegAuthOfLic() {
        return regAuthOfLic;
    }

    public void setRegAuthOfLic(RegAuthOfLic regAuthOfLic) {
        this.regAuthOfLic = regAuthOfLic;
    }

    public RegAuthOfRegtN getRegAuthOfRegtN() {
        return regAuthOfRegtN;
    }

    public void setRegAuthOfRegtN(RegAuthOfRegtN regAuthOfRegtN) {
        this.regAuthOfRegtN = regAuthOfRegtN;
    }

    public ModelName getModelName() {
        return modelName;
    }

    public void setModelName(ModelName modelName) {
        this.modelName = modelName;
    }

    public Driven getDriven() {
        return driven;
    }

    public void setDriven(Driven driven) {
        this.driven = driven;
    }

    public Make getMake() {
        return make;
    }

    public void setMake(Make make) {
        this.make = make;
    }

    public MainColour getMainColour() {
        return mainColour;
    }

    public void setMainColour(MainColour mainColour) {
        this.mainColour = mainColour;
    }

    public String getGvm() {
        return gvm;
    }

    public void setGvm(String gvm) {
        this.gvm = gvm;
    }

    public String getEngineDisp() {
        return engineDisp;
    }

    public void setEngineDisp(String engineDisp) {
        this.engineDisp = engineDisp;
    }

    public String getTare() {
        return tare;
    }

    public void setTare(String tare) {
        this.tare = tare;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public String getNetPower() {
        return netPower;
    }

    public void setNetPower(String netPower) {
        this.netPower = netPower;
    }

    public String getPrevMVCertN() {
        return prevMVCertN;
    }

    public void setPrevMVCertN(String prevMVCertN) {
        this.prevMVCertN = prevMVCertN;
    }

    public VehType getVehType() {
        return vehType;
    }

    public void setVehType(VehType vehType) {
        this.vehType = vehType;
    }

    public Exemption getExemption() {
        return exemption;
    }

    public void setExemption(Exemption exemption) {
        this.exemption = exemption;
    }

    public Mvcat getMvcat() {
        return mvcat;
    }

    public void setMvcat(Mvcat mvcat) {
        this.mvcat = mvcat;
    }

    public String getMvregN() {
        return mvregN;
    }

    public void setMvregN(String mvregN) {
        this.mvregN = mvregN;
    }

    public Sapmark getSapmark() {
        return sapmark;
    }

    public void setSapmark(Sapmark sapmark) {
        this.sapmark = sapmark;
    }

    public String getMvlicChgD() {
        return mvlicChgD;
    }

    public void setMvlicChgD(String mvlicChgD) {
        this.mvlicChgD = mvlicChgD;
    }

    public String getMvlicExpiryD() {
        return mvlicExpiryD;
    }

    public void setMvlicExpiryD(String mvlicExpiryD) {
        this.mvlicExpiryD = mvlicExpiryD;
    }

    public Mvdesc getMvdesc() {
        return mvdesc;
    }

    public void setMvdesc(Mvdesc mvdesc) {
        this.mvdesc = mvdesc;
    }

    public String getMvregtD() {
        return mvregtD;
    }

    public void setMvregtD(String mvregtD) {
        this.mvregtD = mvregtD;
    }

    public MvregtTypeQual getMvregtTypeQual() {
        return mvregtTypeQual;
    }

    public void setMvregtTypeQual(MvregtTypeQual mvregtTypeQual) {
        this.mvregtTypeQual = mvregtTypeQual;
    }

    public Rwstat getRwstat() {
        return rwstat;
    }

    public void setRwstat(Rwstat rwstat) {
        this.rwstat = rwstat;
    }

    public Mvstate getMvstate() {
        return mvstate;
    }

    public void setMvstate(Mvstate mvstate) {
        this.mvstate = mvstate;
    }

    public String getMvstateChgD() {
        return mvstateChgD;
    }

    public void setMvstateChgD(String mvstateChgD) {
        this.mvstateChgD = mvstateChgD;
    }

    public MvlifeStat getMvlifeStat() {
        return mvlifeStat;
    }

    public void setMvlifeStat(MvlifeStat mvlifeStat) {
        this.mvlifeStat = mvlifeStat;
    }

    public String getNmodelN() {
        return nmodelN;
    }

    public void setNmodelN(String nmodelN) {
        this.nmodelN = nmodelN;
    }

    public String getRwtstD() {
        return rwtstD;
    }

    public void setRwtstD(String rwtstD) {
        this.rwtstD = rwtstD;
    }

    public SapclrStat getSapclrStat() {
        return sapclrStat;
    }

    public void setSapclrStat(SapclrStat sapclrStat) {
        this.sapclrStat = sapclrStat;
    }

    public Mvusage getMvusage() {
        return mvusage;
    }

    public void setMvusage(Mvusage mvusage) {
        this.mvusage = mvusage;
    }

    public MveconSector getMveconSector() {
        return mveconSector;
    }

    public void setMveconSector(MveconSector mveconSector) {
        this.mveconSector = mveconSector;
    }

    public MvregtType getMvregtType() {
        return mvregtType;
    }

    public void setMvregtType(MvregtType mvregtType) {
        this.mvregtType = mvregtType;
    }

}
