
package za.co.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LicCard {

    @SerializedName("DriLicNr")
    @Expose
    private String driLicNr;

    /**
     * 
     * @return
     *     The driLicNr
     */
    public String getDriLicNr() {
        return driLicNr;
    }

    /**
     * 
     * @param driLicNr
     *     The DriLicNr
     */
    public void setDriLicNr(String driLicNr) {
        this.driLicNr = driLicNr;
    }

}
