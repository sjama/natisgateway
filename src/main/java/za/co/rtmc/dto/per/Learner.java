package za.co.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class Learner
{
    private LrnType lrnType;

    public LrnType getLrnType() { return this.lrnType; }

    public void setLrnType(LrnType lrnType) { this.lrnType = lrnType; }

    private String lrnValidFromD;

    public String getLrnValidFromD() { return this.lrnValidFromD; }

    public void setLrnValidFromD(String lrnValidFromD) { this.lrnValidFromD = lrnValidFromD; }

    private String speclAdapInd;

    public String getSpeclAdapInd() { return this.speclAdapInd; }

    public void setSpeclAdapInd(String speclAdapInd) { this.speclAdapInd = speclAdapInd; }
}
