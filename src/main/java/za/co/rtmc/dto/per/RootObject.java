package za.co.rtmc.dto.per;

import java.util.ArrayList;

public class RootObject
{
    private PerDet perDet;

    public PerDet getPerDet() { return this.perDet; }

    public void setPerDet(PerDet perDet) { this.perDet = perDet; }

    private ArrayList<Licence> licence;

    public ArrayList<Licence> getLicence() { return this.licence; }

    public void setLicence(ArrayList<Licence> licence) { this.licence = licence; }

    private LicCard licCard;

    public LicCard getLicCard() { return this.licCard; }

    public void setLicCard(LicCard licCard) { this.licCard = licCard; }

    private PrDP prDP;

    public PrDP getPrDP() { return this.prDP; }

    public void setPrDP(PrDP prDP) { this.prDP = prDP; }
}


