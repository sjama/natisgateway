package za.co.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/14.
 */

public class PrDP {
    private PrDPCatG prDPCatG;

    public PrDPCatG getPrDPCatD() {
        return prDPCatD;
    }

    public void setPrDPCatD(PrDPCatG prDPCatD) {
        this.prDPCatD = prDPCatD;
    }

    private PrDPCatG prDPCatD;

    public PrDPCatG getPrDPCatG() {
        return this.prDPCatG;
    }

    public void setPrDPCatG(PrDPCatG prDPCatG) {
        this.prDPCatG = prDPCatG;
    }

    private PrDPCatP prDPCatP;

    public PrDPCatP getPrDPCatP() {
        return this.prDPCatP;
    }

    public void setPrDPCatP(PrDPCatP prDPCatP) {
        this.prDPCatP = prDPCatP;
    }

    private String prDPExpiryD;

    public String getPrDPExpiryD() {
        return this.prDPExpiryD;
    }

    public void setPrDPExpiryD(String prDPExpiryD) {
        this.prDPExpiryD = prDPExpiryD;
    }

    private String prDPAuthD;

    public String getPrDPAuthD() {
        return this.prDPAuthD;
    }

    public void setPrDPAuthD(String prDPAuthD) {
        this.prDPAuthD = prDPAuthD;
    }

    private String prDPValidFromD;

    public String getPrDPValidFromD() {
        return this.prDPValidFromD;
    }

    public void setPrDPValidFromD(String prDPValidFromD) {
        this.prDPValidFromD = prDPValidFromD;
    }
}
