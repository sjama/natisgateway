package za.co.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class PostalCd2
{
    private String code;

    public String getCode() { return this.code; }

    public void setCode(String code) { this.code = code; }

    private String desc;

    public String getDesc() { return this.desc; }

    public void setDesc(String desc) { this.desc = desc; }
}
