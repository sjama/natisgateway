package za.co.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class Licence
{
    private DriLic driLic;

    public DriLic getDriLic() { return this.driLic; }

    public void setDriLic(DriLic driLic) { this.driLic = driLic; }

    private String dateFirstIss;

    public String getDateFirstIss() { return this.dateFirstIss; }

    public void setDateFirstIss(String dateFirstIss) { this.dateFirstIss = dateFirstIss; }

    private String driLicAuthD;

    public String getDriLicAuthD() { return this.driLicAuthD; }

    public void setDriLicAuthD(String driLicAuthD) { this.driLicAuthD = driLicAuthD; }

    private VehRestr vehRestr;

    public VehRestr getVehRestr() { return this.vehRestr; }

    public void setVehRestr(VehRestr vehRestr) { this.vehRestr = vehRestr; }

    private String driLicValidFromD;

    public String getDriLicValidFromD() { return this.driLicValidFromD; }

    public void setDriLicValidFromD(String driLicValidFromD) { this.driLicValidFromD = driLicValidFromD; }

    private String driLicValidToD;

    public String getDriLicValidToD() { return this.driLicValidToD; }

    public void setDriLicValidToD(String driLicValidToD) { this.driLicValidToD = driLicValidToD; }
}
