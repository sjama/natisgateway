
package za.co.rtmc.dto;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PostalCd_ {

    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("Desc")
    @Expose
    private String desc;

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The Code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * 
     * @param desc
     *     The Desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

}
