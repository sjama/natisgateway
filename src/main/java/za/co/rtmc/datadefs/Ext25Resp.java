package za.co.rtmc.datadefs;

import java.util.Collection;


/**
 * Ext25Resp
 */
public class Ext25Resp extends BaseResp
{

  private String payResult;
  
  
  /**
   * @param successful
   *          Indicates whether the transaction was successful.
   * @param errors
   *          errors that occurred during the processing of the transaction.
   */
  public Ext25Resp(boolean isSuccessful, Collection errors)
  {
    super(isSuccessful, errors);
  }

  /**
   * Default constructor.
   */
  public Ext25Resp()
  {
  }
  
  public String getPayResult()
  {
    return payResult;
  }

  public void setPayResult(String payResult)
  {
    this.payResult = payResult;
  }
}
