package za.co.rtmc.datadefs;

import java.io.Serializable;

public class ErrorDet implements Serializable
{

  protected String errorField;
  protected String errorDesc;
  public String getErrorField()
  {
    return errorField;
  }
  public void setErrorField(String errorField)
  {
    this.errorField = errorField;
  }
  public String getErrorDesc()
  {
    return errorDesc;
  }
  public void setErrorDesc(String errorDesc)
  {
    this.errorDesc = errorDesc;
  }
  
  
  
  
}
