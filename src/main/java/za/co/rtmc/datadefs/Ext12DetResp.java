package za.co.rtmc.datadefs;

import java.io.Serializable;

/**
 * Class representing a single Ext12 detail response object
 */
public class Ext12DetResp implements Serializable
{

  private VehicleQryDetail     vehilceDetail;
  private PerQryMvOwnDriDetail ownerDetail;
  private PerQryMvOwnDriDetail ownerProxyDetail;
  private PerQryMvOwnDriDetail ownerRepDetail;
  private PerQryMvOwnDriDetail thOwnerDetail;
  private PerQryMvOwnDriDetail thOwnerProxyDetail;
  private PerQryMvOwnDriDetail thOwnerRepDetail;

  /**
   * @return Returns the ownerDetail.
   */
  public PerQryMvOwnDriDetail getOwnerDetail()
  {
    return ownerDetail;
  }

  /**
   * @param ownerDetail
   *          The ownerDetail to set.
   */
  public void setOwnerDetail(PerQryMvOwnDriDetail ownerDetail)
  {
    this.ownerDetail = ownerDetail;
  }

  /**
   * @return Returns the ownerProxyDetail.
   */
  public PerQryMvOwnDriDetail getOwnerProxyDetail()
  {
    return ownerProxyDetail;
  }

  /**
   * @param ownerProxyDetail
   *          The ownerProxyDetail to set.
   */
  public void setOwnerProxyDetail(PerQryMvOwnDriDetail ownerProxyDetail)
  {
    this.ownerProxyDetail = ownerProxyDetail;
  }

  /**
   * @return Returns the ownerRepDetail.
   */
  public PerQryMvOwnDriDetail getOwnerRepDetail()
  {
    return ownerRepDetail;
  }

  /**
   * @param ownerRepDetail
   *          The ownerRepDetail to set.
   */
  public void setOwnerRepDetail(PerQryMvOwnDriDetail ownerRepDetail)
  {
    this.ownerRepDetail = ownerRepDetail;
  }

  /**
   * @return Returns the thOwnerDetail.
   */
  public PerQryMvOwnDriDetail getThOwnerDetail()
  {
    return thOwnerDetail;
  }

  /**
   * @param thOwnerDetail
   *          The thOwnerDetail to set.
   */
  public void setThOwnerDetail(PerQryMvOwnDriDetail thOwnerDetail)
  {
    this.thOwnerDetail = thOwnerDetail;
  }

  /**
   * @return Returns the thOwnerProxyDetail.
   */
  public PerQryMvOwnDriDetail getThOwnerProxyDetail()
  {
    return thOwnerProxyDetail;
  }

  /**
   * @param thOwnerProxyDetail
   *          The thOwnerProxyDetail to set.
   */
  public void setThOwnerProxyDetail(PerQryMvOwnDriDetail thOwnerProxyDetail)
  {
    this.thOwnerProxyDetail = thOwnerProxyDetail;
  }

  /**
   * @return Returns the thOwnerRepDetail.
   */
  public PerQryMvOwnDriDetail getThOwnerRepDetail()
  {
    return thOwnerRepDetail;
  }

  /**
   * @param thOwnerRepDetail
   *          The thOwnerRepDetail to set.
   */
  public void setThOwnerRepDetail(PerQryMvOwnDriDetail thOwnerRepDetail)
  {
    this.thOwnerRepDetail = thOwnerRepDetail;
  }

  /**
   * @return Returns the vehilceDetail.
   */
  public VehicleQryDetail getVehilceDetail()
  {
    return vehilceDetail;
  }

  /**
   * @param vehilceDetail
   *          The vehilceDetail to set.
   */
  public void setVehilceDetail(VehicleQryDetail vehilceDetail)
  {
    this.vehilceDetail = vehilceDetail;
  }
}