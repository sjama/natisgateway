package za.co.rtmc.datadefs;

import java.io.Serializable;

public class DriEndorseDetails implements Serializable
{
  private String endorseTypeCd;
  private String endorseTypeCdDesc;
  
  /**
   * @return Returns the endorseTypeCd.
   */
  public String getEndorseTypeCd()
  {
    return endorseTypeCd;
  }

  /**
   * @param endorseTypeCd
   *          The endorseTypeCd to set.
   */
  public void setEndorseTypeCd(String endorseTypeCd)
  {
    this.endorseTypeCd = endorseTypeCd;
  }

  /**
   * @return Returns the endorseTypeCdDesc.
   */
  public String getEndorseTypeCdDesc()
  {
    return endorseTypeCdDesc;
  }

  /**
   * @param endorseTypeCdDesc
   *          The endorseTypeCdDesc to set.
   */
  public void setEndorseTypeCdDesc(String endorseTypeCdDesc)
  {
    this.endorseTypeCdDesc = endorseTypeCdDesc;
  }
}
