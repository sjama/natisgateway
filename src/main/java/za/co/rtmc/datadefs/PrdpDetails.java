package za.co.rtmc.datadefs;

import java.io.Serializable;

public class PrdpDetails implements Serializable
{

  protected String prdpCatD;
  protected String prdpCatDDesc;
  protected String prdpCatG;
  protected String prdpCatGDesc;
  protected String prdpCatP;
  protected String prdpCatPDesc;
  protected String prdpCatX;
  protected String prdpCatXDesc;
  protected String prdpCatY;
  protected String prdpCatYDesc;
  protected String prdpAuthD;
  protected String prdpValidFromD;
  protected String prdpExpiryD;
  protected String prdpSuspFromD;
  protected String prdpSuspToD;

  /**
   * @return Returns the prdpAuthD.
   */
  public String getPrdpAuthD()
  {
    return prdpAuthD;
  }

  /**
   * @param prdpAuthD
   *          The prdpAuthD to set.
   */
  public void setPrdpAuthD(String prdpAuthD)
  {
    this.prdpAuthD = prdpAuthD;
  }

  /**
   * @return Returns the prdpCatD.
   */
  public String getPrdpCatD()
  {
    return prdpCatD;
  }

  /**
   * @param prdpCatD
   *          The prdpCatD to set.
   */
  public void setPrdpCatD(String prdpCatD)
  {
    this.prdpCatD = prdpCatD;
  }

  /**
   * @return Returns the prdpCatDDesc.
   */
  public String getPrdpCatDDesc()
  {
    return prdpCatDDesc;
  }

  /**
   * @param prdpCatDDesc
   *          The prdpCatDDesc to set.
   */
  public void setPrdpCatDDesc(String prdpCatDDesc)
  {
    this.prdpCatDDesc = prdpCatDDesc;
  }

  /**
   * @return Returns the prdpCatG.
   */
  public String getPrdpCatG()
  {
    return prdpCatG;
  }

  /**
   * @param prdpCatG
   *          The prdpCatG to set.
   */
  public void setPrdpCatG(String prdpCatG)
  {
    this.prdpCatG = prdpCatG;
  }

  /**
   * @return Returns the prdpCatGDesc.
   */
  public String getPrdpCatGDesc()
  {
    return prdpCatGDesc;
  }

  /**
   * @param prdpCatGDesc
   *          The prdpCatGDesc to set.
   */
  public void setPrdpCatGDesc(String prdpCatGDesc)
  {
    this.prdpCatGDesc = prdpCatGDesc;
  }

  /**
   * @return Returns the prdpCatP.
   */
  public String getPrdpCatP()
  {
    return prdpCatP;
  }

  /**
   * @param prdpCatP
   *          The prdpCatP to set.
   */
  public void setPrdpCatP(String prdpCatP)
  {
    this.prdpCatP = prdpCatP;
  }

  /**
   * @return Returns the prdpCatPDesc.
   */
  public String getPrdpCatPDesc()
  {
    return prdpCatPDesc;
  }

  /**
   * @param prdpCatPDesc
   *          The prdpCatPDesc to set.
   */
  public void setPrdpCatPDesc(String prdpCatPDesc)
  {
    this.prdpCatPDesc = prdpCatPDesc;
  }

  /**
   * @return Returns the prdpCatX.
   */
  public String getPrdpCatX()
  {
    return prdpCatX;
  }

  /**
   * @param prdpCatX
   *          The prdpCatX to set.
   */
  public void setPrdpCatX(String prdpCatX)
  {
    this.prdpCatX = prdpCatX;
  }

  /**
   * @return Returns the prdpCatXDesc.
   */
  public String getPrdpCatXDesc()
  {
    return prdpCatXDesc;
  }

  /**
   * @param prdpCatXDesc
   *          The prdpCatXDesc to set.
   */
  public void setPrdpCatXDesc(String prdpCatXDesc)
  {
    this.prdpCatXDesc = prdpCatXDesc;
  }

  /**
   * @return Returns the prdpCatY.
   */
  public String getPrdpCatY()
  {
    return prdpCatY;
  }

  /**
   * @param prdpCatY
   *          The prdpCatY to set.
   */
  public void setPrdpCatY(String prdpCatY)
  {
    this.prdpCatY = prdpCatY;
  }

  /**
   * @return Returns the prdpCatYDesc.
   */
  public String getPrdpCatYDesc()
  {
    return prdpCatYDesc;
  }

  /**
   * @param prdpCatYDesc
   *          The prdpCatYDesc to set.
   */
  public void setPrdpCatYDesc(String prdpCatYDesc)
  {
    this.prdpCatYDesc = prdpCatYDesc;
  }

  /**
   * @return Returns the prdpExpiryD.
   */
  public String getPrdpExpiryD()
  {
    return prdpExpiryD;
  }

  /**
   * @param prdpExpiryD
   *          The prdpExpiryD to set.
   */
  public void setPrdpExpiryD(String prdpExpiryD)
  {
    this.prdpExpiryD = prdpExpiryD;
  }

  /**
   * @return Returns the prdpValidFromD.
   */
  public String getPrdpValidFromD()
  {
    return prdpValidFromD;
  }

  /**
   * @param prdpValidFromD
   *          The prdpValidFromD to set.
   */
  public void setPrdpValidFromD(String prdpValidFromD)
  {
    this.prdpValidFromD = prdpValidFromD;
  }

  public String getPrdpSuspFromD()
  {
    return prdpSuspFromD;
  }

  public void setPrdpSuspFromD(String prdpSuspFromD)
  {
    this.prdpSuspFromD = prdpSuspFromD;
  }

  public String getPrdpSuspToD()
  {
    return prdpSuspToD;
  }

  public void setPrdpSuspToD(String prdpSuspToD)
  {
    this.prdpSuspToD = prdpSuspToD;
  }
}
