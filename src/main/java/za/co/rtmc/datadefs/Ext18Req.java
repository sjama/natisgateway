package za.co.rtmc.datadefs;

/**
 * Ext18Req represents a request for txan X1007.
 */
public class Ext18Req extends BaseReq
{
  /** MV register number */
  private String mvRegN;

  /** MV licence number */
  private String regtN;

  /** MV register number */
  private String vinOrChassis;

  /** Engine number */
  private String engineN;

  /**
   * @return Returns the engineN.
   */
  public String getEngineN()
  {
    return engineN;
  }

  /**
   * @param engineN
   *          The engineN to set.
   */
  public void setEngineN(String engineN)
  {
    this.engineN = engineN;
  }

  /**
   * @return Returns the mvRegN.
   */
  public String getMvRegN()
  {
    return mvRegN;
  }

  /**
   * @param mvRegN
   *          The mvRegN to set.
   */
  public void setMvRegN(String mvRegN)
  {
    this.mvRegN = mvRegN;
  }

  /**
   * @return Returns the regtN.
   */
  public String getRegtN()
  {
    return regtN;
  }

  /**
   * @param regtN
   *          The regtN to set.
   */
  public void setRegtN(String regtN)
  {
    this.regtN = regtN;
  }

  /**
   * @return Returns the vinOrChassis.
   */
  public String getVinOrChassis()
  {
    return vinOrChassis;
  }

  /**
   * @param vinOrChassis
   *          The vinOrChassis to set.
   */
  public void setVinOrChassis(String vinOrChassis)
  {
    this.vinOrChassis = vinOrChassis;
  }
}
