/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.rtmc.datadefs;

/**
 *
 * @author jamasithole
 */
/**
 * Ext25Req represents a request for txan X20C3.
 */
public class Ext25Req extends BaseReq
{

  /** Person detail required */
  private String aofAssessN;
  
  /** Financial detail required */
  private PayDetail   payment;  
  
  private TransferDetail transfer;
  
  /**
   * @param creds The security credentials.
   */
  public Ext25Req(SecurityCredentials creds)
  {
    super(creds, ExtTxanConstants.EXT25_TXAN_CD);    
  }

  /**
   * Default constructor.
   */
  public Ext25Req()
  {
    setExTxanCd(ExtTxanConstants.EXT25_TXAN_CD);
  }

  public PayDetail getPayment()
  {
    return payment;
  }

  public void setPayment(PayDetail payment)
  {
    this.payment = payment;
  }

  public String getAofAssessN()
  {
    return aofAssessN;
  }

  public void setAofAssessN(String aofAssessN)
  {
    this.aofAssessN = aofAssessN;
  }

  public TransferDetail getTransfer()
  {
    return transfer;
  }

  public void setTransfer(TransferDetail transfer)
  {
    this.transfer = transfer;
  }
}
