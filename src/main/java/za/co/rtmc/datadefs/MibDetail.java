package za.co.rtmc.datadefs;


import java.io.Serializable;

/**
 * This class contains the detail associated with an MIB.
 */
public class MibDetail implements Serializable
{
  private String typeOfBusCd;
  private String natOfStatOwnShipCd; 
    
  public String getNatOfStatOwnShipCd() 
  {
    return natOfStatOwnShipCd;
  }

  public String getTypeOfBusCd() 
  {
    return typeOfBusCd;
  }

  public void setNatOfStatOwnShipCd(String natOfStatOwnShipCd) 
  {
    this.natOfStatOwnShipCd = natOfStatOwnShipCd;
  }

  public void setTypeOfBusCd(String typeOfBusCd) 
  {
    this.typeOfBusCd = typeOfBusCd;
  }
}
