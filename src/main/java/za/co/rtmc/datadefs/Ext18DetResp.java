package za.co.rtmc.datadefs;

import java.io.Serializable;

/**
 * Class representing a single Ext14 detail response object
 */
public class Ext18DetResp implements Serializable
{
  VehicleQryDetail vehicleDetail;
  PersonQryDetail  personDetail;

  /**
   * @return Returns the personDetail.
   */
  public PersonQryDetail getPersonDetail()
  {
    return personDetail;
  }

  /**
   * @param personDetail
   *          The personDetail to set.
   */
  public void setPersonDetail(PersonQryDetail personDetail)
  {
    this.personDetail = personDetail;
  }

  /**
   * @return Returns the vehicleDetail.
   */
  public VehicleQryDetail getVehicleDetail()
  {
    return vehicleDetail;
  }

  /**
   * @param vehicleDetail
   *          The vehicleDetail to set.
   */
  public void setVehicleDetail(VehicleQryDetail vehicleDetail)
  {
    this.vehicleDetail = vehicleDetail;
  }
}
