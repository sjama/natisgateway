package za.co.rtmc.datadefs;

import java.io.Serializable;

public class PersonDetail implements Serializable
{
  private String       idDocTypeCd;
  private String       idDocTypeCdDesc;
  private String       idDocN;  
  private String       initials;
  private String       busOrSurname;
  private String       natnPopGrpCd;  
  private String       natnPopGrpCdDesc;
  private String       natOfPer;
  private String       natOfPerDesc;  
  private String       fullName1;
  private String       fullName2;
  private String       fullName3;  
  private String       birthD;
  private String       age;
  private String       prefLangCd;
  private String       emailAddr;
  private String       hTelCd;
  private String       hTelN;
  private String       wTelCd;
  private String       wTelN;
  private String       faxCd;
  private String       faxN;
  private String       cellN;
  private String       postAddr1;
  private String       postAddr2;
  private String       postAddr3;
  private String       postAddr4;
  private String       postAddr5;
  private String       postCdPost;
  private String       postCdPostDesc;
  private String       streetAddr1;
  private String       streetAddr2;
  private String       streetAddr3;
  private String       streetAddr4;
  private String       postCdStreet;
  private String       postCdStreetDesc;
  private String       dceeAddr;
  private String       autCd;
  private String       autCdDesc;
  
  public String getIdDocN()
  {
    return idDocN;
  }

  public String getIdDocTypeCd()
  {
    return idDocTypeCd;
  }

  public void setIdDocN(String idDocN)
  {
    this.idDocN = idDocN;
  }

  public void setIdDocTypeCd(String idDocTypeCd)
  {
    this.idDocTypeCd = idDocTypeCd;
  }

  /**
   * @return Returns the idDocTypeCdDesc.
   */
  public String getIdDocTypeCdDesc()
  {
    return idDocTypeCdDesc;
  }

  /**
   * @param idDocTypeCdDesc
   *          The idDocTypeCdDesc to set.
   */
  public void setIdDocTypeCdDesc(String idDocTypeCdDesc)
  {
    this.idDocTypeCdDesc = idDocTypeCdDesc;
  }

  /**
   * @return Returns the initials.
   */
  public String getInitials()
  {
    return initials;
  }

  /**
   * @param initials
   *          The initials to set.
   */
  public void setInitials(String initials)
  {
    this.initials = initials;
  }

  /**
   * @return Returns the natnPopGrpCd.
   */
  public String getNatnPopGrpCd()
  {
    return natnPopGrpCd;
  }

  /**
   * @param natnPopGrpCd
   *          The natnPopGrpCd to set.
   */
  public void setNatnPopGrpCd(String natnPopGrpCd)
  {
    this.natnPopGrpCd = natnPopGrpCd;
  }

  /**
   * @return Returns the natnPopGrpCdDesc.
   */
  public String getNatnPopGrpCdDesc()
  {
    return natnPopGrpCdDesc;
  }

  /**
   * @param natnPopGrpCdDesc
   *          The natnPopGrpCdDesc to set.
   */
  public void setNatnPopGrpCdDesc(String natnPopGrpCdDesc)
  {
    this.natnPopGrpCdDesc = natnPopGrpCdDesc;
  }

  /**
   * @return Returns the natOfPer.
   */
  public String getNatOfPer()
  {
    return natOfPer;
  }

  /**
   * @param natOfPer
   *          The natOfPer to set.
   */
  public void setNatOfPer(String natOfPer)
  {
    this.natOfPer = natOfPer;
  }

  /**
   * @return Returns the natOfPerDesc.
   */
  public String getNatOfPerDesc()
  {
    return natOfPerDesc;
  }

  /**
   * @param natOfPerDesc
   *          The natOfPerDesc to set.
   */
  public void setNatOfPerDesc(String natOfPerDesc)
  {
    this.natOfPerDesc = natOfPerDesc;
  }


  /**
   * @return Returns the birthD.
   */
  public String getBirthD()
  {
    return birthD;
  }

  /**
   * @param birthD
   *          The birthD to set.
   */
  public void setBirthD(String birthD)
  {
    this.birthD = birthD;
  }

  /**
   * @return Returns the busOrSurname.
   */
  public String getBusOrSurname()
  {
    return busOrSurname;
  }

  /**
   * @param busOrSurname
   *          The busOrSurname to set.
   */
  public void setBusOrSurname(String busOrSurname)
  {
    this.busOrSurname = busOrSurname;
  }

  /**
   * @return Returns the dceeAddr.
   */
  public String getDceeAddr()
  {
    return dceeAddr;
  }

  /**
   * @param dceeAddr
   *          The dceeAddr to set.
   */
  public void setDceeAddr(String dceeAddr)
  {
    this.dceeAddr = dceeAddr;
  }

  /**
   * @return Returns the age.
   */
  public String getAge()
  {
    return age;
  }

  /**
   * @param age
   *          The age to set.
   */
  public void setAge(String age)
  {
    this.age = age;
  }


  /**
   * @return Returns the postAddr1.
   */
  public String getPostAddr1()
  {
    return postAddr1;
  }

  /**
   * @param postAddr1
   *          The postAddr1 to set.
   */
  public void setPostAddr1(String postAddr1)
  {
    this.postAddr1 = postAddr1;
  }

  /**
   * @return Returns the postAddr2.
   */
  public String getPostAddr2()
  {
    return postAddr2;
  }

  /**
   * @param postAddr2
   *          The postAddr2 to set.
   */
  public void setPostAddr2(String postAddr2)
  {
    this.postAddr2 = postAddr2;
  }

  /**
   * @return Returns the postAddr3.
   */
  public String getPostAddr3()
  {
    return postAddr3;
  }

  /**
   * @param postAddr3
   *          The postAddr3 to set.
   */
  public void setPostAddr3(String postAddr3)
  {
    this.postAddr3 = postAddr3;
  }

  /**
   * @return Returns the postAddr4.
   */
  public String getPostAddr4()
  {
    return postAddr4;
  }

  /**
   * @param postAddr4
   *          The postAddr4 to set.
   */
  public void setPostAddr4(String postAddr4)
  {
    this.postAddr4 = postAddr4;
  }

  /**
   * @return Returns the postAddr5.
   */
  public String getPostAddr5()
  {
    return postAddr5;
  }

  /**
   * @param postAddr5
   *          The postAddr5 to set.
   */
  public void setPostAddr5(String postAddr5)
  {
    this.postAddr5 = postAddr5;
  }

  /**
   * @return Returns the postCdPost.
   */
  public String getPostCdPost()
  {
    return postCdPost;
  }

  /**
   * @param postCdPost
   *          The postCdPost to set.
   */
  public void setPostCdPost(String postCdPost)
  {
    this.postCdPost = postCdPost;
  }

  /**
   * @return Returns the postCdPostDesc.
   */
  public String getPostCdPostDesc()
  {
    return postCdPostDesc;
  }

  /**
   * @param postCdPostDesc
   *          The postCdPostDesc to set.
   */
  public void setPostCdPostDesc(String postCdPostDesc)
  {
    this.postCdPostDesc = postCdPostDesc;
  }

  /**
   * @return Returns the postCdStreet.
   */
  public String getPostCdStreet()
  {
    return postCdStreet;
  }

  /**
   * @param postCdStreet
   *          The postCdStreet to set.
   */
  public void setPostCdStreet(String postCdStreet)
  {
    this.postCdStreet = postCdStreet;
  }

  /**
   * @return Returns the postCdStreetDesc.
   */
  public String getPostCdStreetDesc()
  {
    return postCdStreetDesc;
  }

  /**
   * @param postCdStreetDesc
   *          The postCdStreetDesc to set.
   */
  public void setPostCdStreetDesc(String postCdStreetDesc)
  {
    this.postCdStreetDesc = postCdStreetDesc;
  }

  /**
   * @return Returns the streetAddr1.
   */
  public String getStreetAddr1()
  {
    return streetAddr1;
  }

  /**
   * @param streetAddr1
   *          The streetAddr1 to set.
   */
  public void setStreetAddr1(String streetAddr1)
  {
    this.streetAddr1 = streetAddr1;
  }

  /**
   * @return Returns the streetAddr2.
   */
  public String getStreetAddr2()
  {
    return streetAddr2;
  }

  /**
   * @param streetAddr2
   *          The streetAddr2 to set.
   */
  public void setStreetAddr2(String streetAddr2)
  {
    this.streetAddr2 = streetAddr2;
  }

  /**
   * @return Returns the streetAddr3.
   */
  public String getStreetAddr3()
  {
    return streetAddr3;
  }

  /**
   * @param streetAddr3
   *          The streetAddr3 to set.
   */
  public void setStreetAddr3(String streetAddr3)
  {
    this.streetAddr3 = streetAddr3;
  }

  /**
   * @return Returns the streetAddr4.
   */
  public String getStreetAddr4()
  {
    return streetAddr4;
  }

  /**
   * @param streetAddr4
   *          The streetAddr4 to set.
   */
  public void setStreetAddr4(String streetAddr4)
  {
    this.streetAddr4 = streetAddr4;
  }

  public String getCellN()
  {
    return cellN;
  }
  
  public void setCellN(String cellN)
  {
    this.cellN = cellN;
  }
  
  public String getEmailAddr()
  {
    return emailAddr;
  }
  
  public void setEmailAddr(String emailAddr)
  {
    this.emailAddr = emailAddr;
  }
  
  public String getHTelNumber()
  {
    String result = "";
    if (hTelCd != null) 
      result += hTelCd;
    if (hTelN != null)
      result += hTelN;
    
    return result;
  }
  
  public String getWTelNumber()
  {
    String result = "";
    if (wTelCd != null) 
      result += wTelCd;
    if (wTelN != null)
      result += wTelN;
    
    return result;
  }
  
  public String getFaxNumber()
  {
    String result = "";
    if (faxCd != null) 
      result += faxCd;
    if (faxN != null)
      result += faxN;
    
    return result;
  }
  
  public String getFaxCd()
  {
    return faxCd;
  }
  
  public void setFaxCd(String faxCd)
  {
    this.faxCd = faxCd;
  }

  public String getFaxN()
  {
    return faxN;
  }
  
  public void setFaxN(String faxN)
  {
    this.faxN = faxN;
  }
  
  public String getFullName1()
  {
    return fullName1;
  }
  
  public void setFullName1(String fullName1)
  {
    this.fullName1 = fullName1;
  }
  
  public String getFullName2()
  {
    return fullName2;
  }
  
  public void setFullName2(String fullName2)
  {
    this.fullName2 = fullName2;
  }
  
  public String getFullName3()
  {
    return fullName3;
  }
  
  public void setFullName3(String fullName3)
  {
    this.fullName3 = fullName3;
  }
  
  public String getHTelCd()
  {
    return hTelCd;
  }
  
  public void setHTelCd(String telCd)
  {
    hTelCd = telCd;
  }
  
  public String getHTelN()
  {
    return hTelN;
  }
  
  public void setHTelN(String telN)
  {
    hTelN = telN;
  }
  
  public String getPrefLangCd()
  {
    return prefLangCd;
  }
  
  public void setPrefLangCd(String prefLangCd)
  {
    this.prefLangCd = prefLangCd;
  }
  
  public String getWTelCd()
  {
    return wTelCd;
  }
  
  public void setWTelCd(String telCd)
  {
    wTelCd = telCd;
  }
  
  public String getWTelN()
  {
    return wTelN;
  }
  
  public void setWTelN(String telN)
  {
    wTelN = telN;
  }

  /**
   * @return Returns the autCd.
   */
  public String getAutCd()
  {
    return autCd;
  }

  /**
   * @param autCd
   *          The autCd to set.
   */
  public void setAutCd(String autCd)
  {
    this.autCd = autCd;
  }

  /**
   * @return Returns the autCd.
   */
  public String getAutCdDesc()
  {
    return autCdDesc;
  }

  /**
   * @param autCd
   *          The autCd to set.
   */
  public void setAutCdDesc(String autCdDesc)
  {
    this.autCdDesc = autCdDesc;
  }

}
