package za.co.rtmc.datadefs;


/**
 * The response class containing data that must be send back to the external 
 * system.
 */
public class Ext7Resp extends BaseResp
{
  protected VehicleDetail vehicle;

  public VehicleDetail getVehicle()
  {
    return vehicle;
  }

  public void setVehicle(VehicleDetail vehicle)
  {
    this.vehicle = vehicle;
  }
}
