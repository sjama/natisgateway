package za.co.rtmc.datadefs;

public class Ext0Req extends BaseReq
{
  /**
   * Constructor.
   * 
   * @param creds    The security credentials of the user.
   * @param exTxanCd The external systems interface transaction code to process.
   */
  public Ext0Req(SecurityCredentials creds, String exTxanCd)
  {
    super(creds, exTxanCd); 
  }

  /**
   * Default constructor.
   */
  public Ext0Req()
  {
    super();
  }

}
