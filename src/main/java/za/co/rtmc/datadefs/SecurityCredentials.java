package za.co.rtmc.datadefs;

import java.io.Serializable;

/**
 * SecurityCredentials is used for user security credentials
 * of external interfaces.
 */
public class SecurityCredentials implements Serializable
{
  /**
   * The workstation identifier of the user performing the transaction, 
   * ie the adapter.
   */
  private String wksId;
  
  /**
   * The concatenated userGrpCd and userN specifying the 
   * eNaTIS user of the external party.
   */
  private String userNumber;
  
  /**
   * The current password of the user. This is used if 
   * password authentication is indicated for the user.
   */
  private String currPwd;
  
  /**
   * The distinguised name from the X.509 client certificate 
   * presented to the server. This is used if X.509 
   * authentication is indicated for the user.
   */
  private String distName;
  
  /**Holds a reference to the encoded properties.*/
  private String encodedProps;
  
  /**Holds a reference to the encoded Ejb handle.*/
  private String encodedEjbHandle;

  /**Holds a reference to the http session id*/
  private String httpSessionId;
  
  /**Holds a reference to the es id*/
  private String esId;
  
  /**The support user password.*/
  private String supportUserPwd;
  
  /**
   * The concatenated userGrpCd and userN specifying the 
   * Tcs system user number.
   */
  private String tcsUserNumber;
  
  /**
   * @param userNumber The distinguised name from the X.509 client 
   *                   certificate presented to the server.
   * 
   * @param currPwd    The current password of the user.
   * @param distName   The distinguised name from the X.509 client certificate
   */
  public SecurityCredentials(String userNumber, String currPwd, String distName)
  {
    this.userNumber = userNumber;
    this.currPwd = currPwd;
    this.distName = distName;
  }
  
  
  /**
   * Default constructor. 
   */
  public SecurityCredentials()
  {
  }


  /**
   * @return Returns the currPwd.
   */
  public String getCurrPwd()
  {
    return currPwd;
  }
  
  /**
   * @param currPwd The currPwd to set.
   */
  public void setCurrPwd(String currPwd)
  {
    this.currPwd = currPwd;
  }
  
  /**
   * @return Returns the distName.
   */
  public String getDistName()
  {
    return distName;
  }
  
  /**
   * @param distName The distName to set.
   */
  public void setDistName(String distName)
  {
    this.distName = distName;
  }
  
  /**
   * @return Returns the userNumber.
   */
  public String getUserNumber()
  {
    return userNumber;
  }
  
  /**
   * @param userNumber The userNumber to set.
   */
  public void setUserNumber(String userNumber)
  {
    this.userNumber = userNumber;
  }
  
  /**
   * @return Returns the workstation identifier.
   */
  public String getWksId()
  {
    return wksId;
  }
  
  /**
   * @param wksId The workstation identifier to set.
   */
  public void setWksId(String wksId)
  {
    this.wksId = wksId;
  }
  
  /**
   * @return Returns the encodedEjbHandle.
   */
  public String getEncodedEjbHandle()
  {
    return encodedEjbHandle;
  }

  /**
   * @param encodedEjbHandle The encodedEjbHandle to set.
   */
  public void setEncodedEjbHandle(String encodedEjbHandle)
  {
    this.encodedEjbHandle = encodedEjbHandle;
  }

  /**
   * @return Returns the encodedProps.
   */
  public String getEncodedProps()
  {
    return encodedProps;
  }
  
  /**
   * @param encodedProps The encodedProps to set.
   */
  public void setEncodedProps(String encodedProps)
  {
    this.encodedProps = encodedProps;
  }

  /**
   * 
   * @return Returns the httpSessionId.
   */
  public String getHttpSessionId()
  {
    return httpSessionId == null ? "" : httpSessionId;
  }

  /**
   * 
   * @param httpSessionId  The httpSessionId to set.
   */
  public void setHttpSessionId(String httpSessionId)
  {
    this.httpSessionId = httpSessionId;
  }
 
  /**
   * Returns the userNumber & httpSessionId concatenated to form a unique key.
   * 
   * @return the userNumber & httpSessionId concatenated to form a unique key.
   */
  public String getSessionKey()
  {
    return getUserNumber() + getHttpSessionId();
  }

  /**
   * @return Returns the esId.
   */
  public String getEsId()
  {
    return esId;
  }

  /**
   * @param esId The esId to set.
   */
  public void setEsId(String esId)
  {
    this.esId = esId;
  }


  /**
   * @return Returns the supportUserPwd.
   */
  public String getSupportUserPwd()
  {
    return supportUserPwd;
  }


  /**
   * @param supportUserPwd The supportUserPwd to set.
   */
  public void setSupportUserPwd(String supportUserPwd)
  {
    this.supportUserPwd = supportUserPwd;
  }  
  
  /**
   * @return Returns the userNumber.
   */
  public String getTcsUserNumber()
  {
    return tcsUserNumber;
  }
  
  /**
   * @param userNumber The tcsUserNumber to set.
   */
  public void setTcsUserNumber(String tcsUserNumber)
  {
    this.tcsUserNumber = tcsUserNumber;
  }


  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((currPwd == null) ? 0 : currPwd.hashCode());
    result = prime * result + ((distName == null) ? 0 : distName.hashCode());
    result = prime * result + ((esId == null) ? 0 : esId.hashCode());
    result = prime * result
             + ((httpSessionId == null) ? 0 : httpSessionId.hashCode());
    result = prime * result
             + ((supportUserPwd == null) ? 0 : supportUserPwd.hashCode());
    result = prime * result
             + ((tcsUserNumber == null) ? 0 : tcsUserNumber.hashCode());
    result = prime * result
             + ((userNumber == null) ? 0 : userNumber.hashCode());
    return result;
  }


  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SecurityCredentials other = (SecurityCredentials) obj;
    if (currPwd == null)
    {
      if (other.currPwd != null)
        return false;
    }
    else if (!currPwd.equals(other.currPwd))
      return false;
    if (distName == null)
    {
      if (other.distName != null)
        return false;
    }
    else if (!distName.equals(other.distName))
      return false;
    if (esId == null)
    {
      if (other.esId != null)
        return false;
    }
    else if (!esId.equals(other.esId))
      return false;
    if (httpSessionId == null)
    {
      if (other.httpSessionId != null)
        return false;
    }
    else if (!httpSessionId.equals(other.httpSessionId))
      return false;
    if (supportUserPwd == null)
    {
      if (other.supportUserPwd != null)
        return false;
    }
    else if (!supportUserPwd.equals(other.supportUserPwd))
      return false;
    if (tcsUserNumber == null)
    {
      if (other.tcsUserNumber != null)
        return false;
    }
    else if (!tcsUserNumber.equals(other.tcsUserNumber))
      return false;
    if (userNumber == null)
    {
      if (other.userNumber != null)
        return false;
    }
    else if (!userNumber.equals(other.userNumber))
      return false;
    return true;
  }

}
