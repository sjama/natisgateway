package za.co.rtmc.datadefs;

/**
 * Ext13Req represents a request for txan X1001.
 */
public class Ext13Req extends BaseReq
{
   
  private String idDocTypeCd;
  private String idDocN;
  private String busOrSurname;
  private String cardN;
  private String lrnCertN;
  
  /**
   * @return Returns the busOrSurname.
   */
  public String getBusOrSurname()
  {
    return busOrSurname;
  }
  
  /**
   * @param busOrSurname The busOrSurname to set.
   */
  public void setBusOrSurname(String busOrSurname)
  {
    this.busOrSurname = busOrSurname;
  }
  
  /**
   * @return Returns the cardN.
   */
  public String getCardN()
  {
    return cardN;
  }
  
  /**
   * @param cardN The cardN to set.
   */
  public void setCardN(String cardN)
  {
    this.cardN = cardN;
  }
  
  /**
   * @return Returns the idDocN.
   */
  public String getIdDocN()
  {
    return idDocN;
  }
  
  /**
   * @param idDocN The idDocN to set.
   */
  public void setIdDocN(String idDocN)
  {
    this.idDocN = idDocN;
  }
  
  /**
   * @return Returns the idDocTyoeCd.
   */
  public String getIdDocTypeCd()
  {
    return idDocTypeCd;
  }

  /**
   * @param idDocTyoeCd The idDocTyoeCd to set.
   */
  public void setIdDocTypeCd(String idDocTypeCd)
  {
    this.idDocTypeCd = idDocTypeCd;
  }
  
  /**
   * @return Returns the lrnCertN.
   */
  public String getLrnCertN()
  {
    return lrnCertN;
  }
  
  /**
   * @param lrnCertN The lrnCertN to set.
   */
  public void setLrnCertN(String lrnCertN)
  {
    this.lrnCertN = lrnCertN;
  }
}
