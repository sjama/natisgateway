package za.co.rtmc.datadefs;

import java.util.Collection;

/**
 * Ext0Resp is the response to the login request.
 */
public class Ext0Resp extends BaseResp
{
  private String         txState;
  
  /**
   * @param successful   Indicates whether the transaction was successful.
   * @param errors       errors that occurred during the processing of the 
   *                     transaction.
   */
  public Ext0Resp(boolean isSuccessful, Collection errors)
  {
    super(isSuccessful, errors);
  }

  /**
   * Default constructor.
   */
  public Ext0Resp()
  {
  }
  
  public String getTxState()
  {
    return txState;
  }

  public void setTxState(String txState)
  {
    this.txState = txState;
  }
}

