package za.co.rtmc.datadefs;

public class PerQryMvOwnDriDetail extends PersonQryDetail
{
  private String     mvOwnXmtStat;     
  private String     mvOwnXmtStatDesc;    
  private String     genOperCat;       
  private String     genOperCatDesc;       
  private String     ownShipStartD;        
  private String     currStatMVOwn;    
  private String     currStatMVOwnDesc;   
  private String     natOfSOship;          
  private String     natOfSOshipDesc;

  /**
   * @return Returns the currStatMVOwn.
   */
  public String getCurrStatMVOwn()
  {
    return currStatMVOwn;
  }
  /**
   * @param currStatMVOwn
   *          The currStatMVOwn to set.
   */
  public void setCurrStatMVOwn(String currStatMVOwn)
  {
    this.currStatMVOwn = currStatMVOwn;
  }
  /**
   * @return Returns the currStatMVOwnDesc.
   */
  public String getCurrStatMVOwnDesc()
  {
    return currStatMVOwnDesc;
  }
  /**
   * @param currStatMVOwnDesc
   *          The currStatMVOwnDesc to set.
   */
  public void setCurrStatMVOwnDesc(String currStatMVOwnDesc)
  {
    this.currStatMVOwnDesc = currStatMVOwnDesc;
  }
  
  /**
   * @return Returns the genOperCat.
   */
  public String getGenOperCat()
  {
    return genOperCat;
  }
  /**
   * @param genOperCat
   *          The genOperCat to set.
   */
  public void setGenOperCat(String genOperCat)
  {
    this.genOperCat = genOperCat;
  }
  /**
   * @return Returns the genOperCatDesc.
   */
  public String getGenOperCatDesc()
  {
    return genOperCatDesc;
  }
  /**
   * @param genOperCatDesc
   *          The genOperCatDesc to set.
   */
  public void setGenOperCatDesc(String genOperCatDesc)
  {
    this.genOperCatDesc = genOperCatDesc;
  }
  
  /**
   * @return Returns the mvOwnXmtStat.
   */
  public String getMvOwnXmtStat()
  {
    return mvOwnXmtStat;
  }
  /**
   * @param mvOwnXmtStat
   *          The mvOwnXmtStat to set.
   */
  public void setMvOwnXmtStat(String mvOwnXmtStat)
  {
    this.mvOwnXmtStat = mvOwnXmtStat;
  }
  /**
   * @return Returns the mvOwnXmtStatDesc.
   */
  public String getMvOwnXmtStatDesc()
  {
    return mvOwnXmtStatDesc;
  }
  /**
   * @param mvOwnXmtStatDesc
   *          The mvOwnXmtStatDesc to set.
   */
  public void setMvOwnXmtStatDesc(String mvOwnXmtStatDesc)
  {
    this.mvOwnXmtStatDesc = mvOwnXmtStatDesc;
  }
  
  /**
   * @return Returns the natOfSOship.
   */
  public String getNatOfSOship()
  {
    return natOfSOship;
  }
  /**
   * @param natOfSOship
   *          The natOfSOship to set.
   */
  public void setNatOfSOship(String natOfSOship)
  {
    this.natOfSOship = natOfSOship;
  }
  /**
   * @return Returns the natOfSOshipDesc.
   */
  public String getNatOfSOshipDesc()
  {
    return natOfSOshipDesc;
  }
  /**
   * @param natOfSOshipDesc
   *          The natOfSOshipDesc to set.
   */
  public void setNatOfSOshipDesc(String natOfSOshipDesc)
  {
    this.natOfSOshipDesc = natOfSOshipDesc;
  }
  
  /**
   * @return Returns the ownShipStartD.
   */
  public String getOwnShipStartD()
  {
    return ownShipStartD;
  }
  /**
   * @param ownShipStartD
   *          The ownShipStartD to set.
   */
  public void setOwnShipStartD(String ownShipStartD)
  {
    this.ownShipStartD = ownShipStartD;
  }
}
