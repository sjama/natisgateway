package za.co.rtmc.datadefs;

/**
 * ExtTxanConstants holds all external interface txan constants.
 */
public final class ExtTxanConstants
{
  /** Processed by <code>GenWsTp</code>. */
  public static final int SP_TYPE_GEN_WS_TP  = 1;
  
  /** Processed by <code>WebLookupUtils</code>.*/
  public static final int SP_TYPE_WS_LOOKUP_UTILS = 2;

  /** Processed by <code>ClltUtils</code>.*/
  public static final int SP_TYPE_CLLT_UTILS = 3;
  
  /** Represents the exTxanCd for the login txan for Saps.*/
  public static final String EXT0_TXAN_CD_SAPS = "X0000";

  /** Represents the exTxanCd for the login txan for Cpf.*/
  public static final String EXT0_TXAN_CD_CPF = "0";

  /** Represents the exTxanCd for the login txan for TCS.*/
  public static final String EXT0_TXAN_CD_TCS = "0";

  /** Represents the exTxanCd for the X2019 txan.*/
  public static final String EXT1_TXAN_CD = "X2019";

  /** Represents the exTxanCd for the X21A1 txan.*/
  public static final String EXT2_TXAN_CD = "X21A1";

  /** Represents the exTxanCd for the X21A2 txan.*/
  public static final String EXT3_TXAN_CD = "X21A2";

  /** Represents the exTxanCd for the X1161 txan.*/
  public static final String EXT4_TXAN_CD = "X1161";

  /** Represents the exTxanCd for the X2002 txan - CPF status update */
  public static final String EXT5_TXAN_CD = "X2002";

  /** Represents the exTxanCd for the X2415 txan on Http Interface*/
  public static final String EXT7_TXAN_CD = "X2415";

  /** Represents the exTxanCd for the X2416 txan on Http Interface*/
  public static final String EXT8_TXAN_CD = "X2416";

  /** Represents the exTxanCd for the X2003 txan - CPF acknowledge receipt */
  public static final String EXT9_TXAN_CD = "X2003";

  /** Represents the exTxanCd for the X2001 txan - CPF card order request */
  public static final String EXT10_TXAN_CD = "X2001";

  /** Represents the exTxanCd for the X2004 txan - CPF image check request */
  public static final String EXT11_TXAN_CD = "X2004";

  /** Represents the exTxanCd for the X1001 txan - General Motor Vehicle Query */
  public static final String EXT12_TXAN_CD = "X1001";

  /** Represents the exTxanCd for the X1002 txan -  */
  public static final String EXT13_TXAN_CD = "X1002";

  public static final String EXT14_TXAN_CD = "X1003";

  public static final String EXT15_TXAN_CD = "X1004";

  /** Represents the exTxanCd for the X1005 txan - Fin Cash Up Query */
  public static final String EXT16_TXAN_CD = "X1005";

  /** Represents the exTxanCd for the X1008 txan - Lookup table Contents Query */
  public static final String EXT17_TXAN_CD = "X1008";

  public static final String EXT18_TXAN_CD = "X1007";

  /** Represents the exTxanCd for the X1009 txan- TCS General Person Query*/
  public static final String EXT19_TXAN_CD = "X1009";

  /** Represents the exTxanCd for the X1010 txan- TCS General Vehicle Query*/
  public static final String EXT20_TXAN_CD = "X1010";

  /** Represents the exTxanCd for the X1011 txan- TCS Driver Sumamry Detials Query*/
  public static final String EXT21_TXAN_CD = "X1011";

  /** Represents the exTxanCd for the X2141 txan- Registration of Motor Vehicle */
  public static final String EXT22_TXAN_CD = "X2141";

  /** Represents the exTxanCd for the X2144 txan - Licensing of Motor Vehicle */
  public static final String EXT23_TXAN_CD = "X2144";

  /** Represents the exTxanCd for the X2057 txan - Introduction of Natural Person */
  public static final String EXT24_TXAN_CD = "X2057";

  /** Represents the exTxanCd for the X20C3 txan - Pay AOF */
  public static final String EXT25_TXAN_CD = "X20C3";

  /** Represents the exTxanCd for the login txan for Http.*/
  public static final String EXT0_TXAN_CD_HTTP = "X0000Http";

  /** Represents the exTxanCd for the X1015 txan - Delete Accident */
  public static final String EXT26_TXAN_CD = "X2015";

  /** Represents the exTxanCd for the X1013 txan - Record Accident */
  public static final String EXT27_TXAN_CD = "X2013";

  /** Represents the exTxanCd for the X1014 txan - Record Accident Pedestrian */
  public static final String EXT28_TXAN_CD = "X2014";

  /** Represents the exTxanCd for the C95 txan - Cancel Aof */
  public static final String EXT29_TXAN_CD = "C95";

  /** Represents the exTxanCd for the X3C95 txan - Cancel Aof */
  public static final String EXT29_X3C95_TXAN_CD = "X3C95";
  
  /** Represents the exTxanCd for the 707 txan - Cancel Pre-booking */
  public static final String EXT30_TXAN_CD = "707";

  /** Represents the exTxanCd for the 5406 txan - Update Move/Merge Indicator */
  public static final String EXT31_TXAN_CD = "5406";

  /** Represents the exTxanCd for the X2901 txan - Process Camera File */
  public static final String EXT33_TXAN_CD = "X2901";
  
  /** Represents the exTxanCd for the X2902 txan - Pay Infringement */
  public static final String EXT34_TXAN_CD = "X2902";
  
  /** Represents the exTxanCd for the X2C95 txan - Delete AOF */
  public static final String EXT35_TXAN_CD = "X2C95";
  
  /** Represents the exTxanCd for the X1904 txan - Query Infringement */
  public static final String EXT36_TXAN_CD = "X1904";
  
  /** Represents the exTxanCd for the X2903 txan - Update Status of Infringement Documentation */
  public static final String EXT37_TXAN_CD = "X2903";
  
  /** Represents the exTxanCd for the X1B01 txan - Query Infrastructure */
  public static final String EXT38_TXAN_CD = "X1B01";
  
  /** Represents the exTxanCd for the X2905 txan - Upload Infringement from Handheld Device */
  public static final String EXT39_TXAN_CD = "X2905";
 
  /** Represents the exTxanCd for the X3005 txan - Request PLN Availability */
  public static final String EXT40_TXAN_CD = "X3005";
  
  /** Represents the exTxanCd for the X3003 txan - Query Infringement */
  public static final String EXT41_TXAN_CD = "X3003";

  /** Represents the exTxanCd for the X3004 txan - Pay Infringement */
  public static final String EXT42_TXAN_CD = "X3004";

  /** Represents the exTxanCd for the C3 txan - Pay Aof */
  public static final String EXT43_TXAN_CD = "X30C3";

  /** Represents the exTxanCd for the X3001 txan - Query Motor Vehicle Licence Renewal */
  public static final String EXT44_TXAN_CD = "X3001";

  /** Represents the exTxanCd for the X3002 txan - Pay Motor Vehicle Licence Renewal */
  public static final String EXT45_TXAN_CD = "X3002";  

  /** Represents the exTxanCd for the Web Lookup - getLookups operation */
  public static final String EXT46_TXAN_CD = null;  

  /** Represents the exTxanCd for the Web Lookup - getProvinces operation */
  public static final String EXT47_TXAN_CD = null;  

  /** Represents the exTxanCd for the Web Lookup - getRegtMarks operation */
  public static final String EXT48_TXAN_CD = null;
  
  /** Represents the exTxanCd for the X3018 - StartLLTest */
  public static final String EXT49_TXAN_CD = "X3018";  
  
  /** Represents the exTxanCd for the X3017 - StartDemoLLTest. */
  public static final String EXT50_TXAN_CD = "X3017";  
  
  /** Represents the exTxanCd for the X3019 - SaveLLTestAnswer */
  public static final String EXT51_TXAN_CD = "X3019";  
  
  /** Represents the exTxanCd for the X3016 - RetrieveQuestions */
  public static final String EXT52_TXAN_CD = "X3016";  
  
  /** Represents the exTxanCd for the X3020 - SubmitLLTest */
  public static final String EXT53_TXAN_CD = "X3020";
  
  /** Represents the exTxanCd for the X3015 - CLLTLogin */
  public static final String EXT54_TXAN_CD = "X3015"; 
  
  /** Represents the exTxanCd for the X3026 - Query Infringement for Authority */
  public static final String EXT55_TXAN_CD = "X3026"; 
  
  /** Represents the exTxanCd for the X3027 - Query Infringements */
  public static final String EXT56_TXAN_CD = "X3027";
  
  /** Represents the exTxanCd for the X3028 - Query Infringement for external parties */
  public static final String EXT57_TXAN_CD = "X3028";
  
  /** Represents the exTxanCd for the X3029 - Pay Infringement for external parties */
  public static final String EXT58_TXAN_CD = "X3029";
  
  /** Represents the exTxanCd for the X3C9A - Cancel Payment */
  public static final String EXT59_TXAN_CD = "X3C9A";
 
  /** Represents the exTxanCd for the CLLT Connection - getConnectionInfo operation */
  public static final String EXT60_TXAN_CD = null;    

  /** Represents the exTxanCd for the CLLT Error Reporting - reportClltError operation */
  public static final String EXT61_TXAN_CD = null;    
}