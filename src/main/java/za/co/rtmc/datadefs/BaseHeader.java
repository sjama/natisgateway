package za.co.rtmc.datadefs;

import java.io.Serializable;



/**
 * BaseHeader holds header information sent by the external system.
 */
public class BaseHeader implements Serializable
{
  /** Information regarding the external user performing this transaction */
  private EsUserInfo esUserInfo;

  /** 
   * The transaction identification number. This number is used by the 
   * external party to correlate a response to the original request. 
   */
  private String     txanId;
  
  /** 
   * The operation mode indicator, i.e. "LIVE" or "TEST"
   */
  private String opModInd;

  private String txanName;

  /**
   * Retrieve the External User Information currently set in the header.
   * 
   * @return The External User Information currently set in the header.
   */
  public EsUserInfo getEsUserInfo()
  {
    return esUserInfo;
  }

  /**
   * Save the External User Information.
   * 
   * @param esUserInfo The External User Information to save.
   */
  public void setEsUserInfo(EsUserInfo esUserInfo)
  {
    this.esUserInfo = esUserInfo;
  }

  /**
   * Returns the Transaction Identification Number.
   * 
   * @return The Transaction Identification Number.
   */
  public String getTxanId()
  {
    return txanId;
  }

  /**
   * Save the transaction identification number.
   * 
   * @param txanId The new transaction identification number.
   */
  public void setTxanId(String txanId)
  {
    this.txanId = txanId;
  }
  
  /**
   * Returns the Operation Mode Indicator.
   * 
   * @return The Operation Mode Indicator.
   */
  public String getOpModInd()
  {
    return opModInd;
  }

  /**
   * Save the Operation Mode Indicator.
   * 
   * @param opModInd The new Operation Mode Indicator.
   */
  public void setOpModInd(String opModInd)
  {
    this.opModInd = opModInd;
  }

  /**
   * Constructs a <code>String</code> with all attributes
   * in name = value format.
   *
   * @return a <code>String</code> representation 
   * of this object.
   */
  public String toString()
  {
      final String TAB = "    ";
      
      String retValue = "";
      
      retValue = "BaseHeader ( "
          + super.toString() + TAB
          + "esUserInfo = " + this.esUserInfo + TAB
          + "txanId = " + this.txanId + TAB
          + "opModInd = " + this.opModInd + TAB
          + " )";
  
      return retValue;
  }

  public String getTxanName()
  {
    return txanName;
  }

  public void setTxanName(String txanName)
  {
    this.txanName = txanName;
  }
}
