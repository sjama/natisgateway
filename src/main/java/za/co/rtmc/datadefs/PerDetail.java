package za.co.rtmc.datadefs;


import java.io.Serializable;

/**
 * This class contains the detail associated with a title holder.
 */
public class PerDetail implements Serializable
{
  protected String idDocTypeCd;
  protected String idDocN;
  
  public String getIdDocN() 
  {
    return idDocN;
  }

  public String getIdDocTypeCd() 
  {
    return idDocTypeCd;
  }

  public void setIdDocN(String idDocN) 
  {
    this.idDocN = idDocN;
  }

  public void setIdDocTypeCd(String idDocTypeCd) 
  {
    this.idDocTypeCd = idDocTypeCd;
  }
}