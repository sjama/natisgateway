package za.co.rtmc.datadefs;

/**
 * This request class is a subclass of the normal request class for login. It 
 * indicates that the marshalling and unmarshalling maps need to be retrieved by
 * the Transaction processor. This request is specific to the Http external 
 * interface 
 */
public class Ext01Req extends Ext0Req
{
  private String txanType;
  
  public String getTxanType()
  {
    return txanType;
  }

  public void setTxanType(String txanType)
  {
    this.txanType = txanType;
  }
  
  /**
   * Constructor.
   * 
   * @param creds    The security credentials of the user.
   * @param exTxanCd The external systems interface transaction code to process.
   */
  public Ext01Req(SecurityCredentials creds, String exTxanCd)
  {
    //Switch to Http specific request in order to create the correct processor
    super(creds, ExtTxanConstants.EXT0_TXAN_CD_HTTP); 
  }
  
  /**
   * Default constructor.
   */
  public Ext01Req()
  {
    super();
  }
  
  /**
   * Set the external transaction code of this request to that of an Http login
   * regardless of what it set to.
   * 
   * @param exTxanCd The external transaction code to set.
   */
  public void setExTxanCd(String exTxanCd)
  {
    super.setExTxanCd(ExtTxanConstants.EXT0_TXAN_CD_HTTP);
  }
}

