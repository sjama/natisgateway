package za.co.rtmc.datadefs;

import java.io.Serializable;

/**
 * Class representing a single Ext13 detail response object
 */
public class Ext13DetResp implements Serializable
{

  PerQryDriLrnDetail personDetail;

  /**
   * @return Returns the personDetail.
   */
  public PerQryDriLrnDetail getPersonDetail()
  {
    return personDetail;
  }

  /**
   * @param personDetail The personDetail to set.
   */
  public void setPersonDetail(PerQryDriLrnDetail personDetail)
  {
    this.personDetail = personDetail;
  }
}
