package za.co.rtmc.datadefs;
import java.io.Serializable;

/**
 *
 * @author jamasithole
 */



public class TransferDetail implements Serializable
{
  private String transferDate;

  private String transferReference;
  
  
  public String getTransferDate()
  {
    return transferDate;
  }

  public void setTransferDate(String transferDate)
  {
    this.transferDate = transferDate;
  }

  public String getTransferReference()
  {
    return transferReference;
  }

  public void setTransferReference(String transferReference)
  {
    this.transferReference = transferReference;
  }
}
