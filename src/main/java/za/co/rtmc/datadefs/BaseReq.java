package za.co.rtmc.datadefs;

import java.io.Serializable;

/**
 * BaseReq is the base class for requests from external interfaces.
 */
public class BaseReq implements Serializable
{
  /** Holds the service processor type.*/
  private int serviceProcessorType = -1;
  
  /**
   * The security credentials of the user performing the transaction. Used for
   * correlation purposes to ensure that parallel transactions for an external 
   * user is executed on the same session, as retrieved from ExtSessionManager.
   */
  private SecurityCredentials creds;

  /** The external systems interface transaction code to process. */
  private String              exTxanCd;

  /** Holds the header infomation. */
  private BaseHeader          header;

  /**
   * @param creds    The security credentials of the user.
   * @param exTxanCd The external systems interface transaction code to process.
   */
  public BaseReq(SecurityCredentials creds, String exTxanCd)
  {
    this.creds = creds;
    this.exTxanCd = exTxanCd;
  }

  /**
   * Default constructor.
   */
  public BaseReq()
  {
  }

  /**
   * Retrieve the current security credentials.
   * @return Returns the current security credentials.
   */
  public SecurityCredentials getSecurityCredentials()
  {
    return creds;
  }

  /**
   * Save new security credentials.
   * 
   * @param creds The new security credentials to set.
   */
  public void setSecurityCredentials(SecurityCredentials creds)
  {
    this.creds = creds;
  }

  /**
   * Retrieve the external transaction code of this request.
   * 
   * @return Returns the external transaction code of this request.
   */
  public String getExTxanCd()
  {
    return exTxanCd;
  }

  /**
   * Set the external transaction code of this request.
   * 
   * @param exTxanCd The external transaction code to set.
   */
  public void setExTxanCd(String exTxanCd)
  {
    this.exTxanCd = exTxanCd;
  }

  /**
   * Retrieve the header of this request.
   * 
   * @return Returns the header.
   */
  public BaseHeader getHeader()
  {
    return header;
  }

  /**
   * Set the header of this request.
   * 
   * @param header The new header to set.
   */
  public void setHeader(BaseHeader header)
  {
    this.header = header;
  }
  
  /**
   * Returns the service processor type for this request.
   * 
   * @return the service processor type for this request.
   */
  public int getServiceProcessorType()
  {
    return serviceProcessorType;
  }

  /**
   * Sets the service processor.
   * 
   * @param serviceProcessorType  the service processor type for this request.
   */
  public void setServiceProcessorType(int serviceProcessorType)
  {
    this.serviceProcessorType = serviceProcessorType;
  }

  /**
   * Constructs a <code>String</code> with all attributes
   * in name = value format.
   *
   * @return a <code>String</code> representation 
   * of this object.
   */
  public String toString()
  {
      final String TAB = "    ";
      
      String retValue = "";
      
      retValue = "BaseReq ( "
          + super.toString() + TAB
          + "serviceProcessorType = " + this.serviceProcessorType + TAB
          + "creds = " + this.creds + TAB
          + "exTxanCd = " + this.exTxanCd + TAB
          + "header = " + this.header + TAB
          + " )";
  
      return retValue;
  }
}