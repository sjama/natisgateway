package za.co.rtmc.datadefs;

import java.io.Serializable;

public class DriLicDetails implements Serializable
{
  private String licTypeCd;
  private String licTypeCdDesc;
  private String licAuthD;
  private String idLicIssD;
  private String infNDtc;
  private String infNDtcDesc;
  private String licRestr;
  private String licRestrDesc;
  private String licValidFromD;
  private String licValidToD;

  /**
   * @return Returns the infNDtc.
   */
  public String getInfNDtc()
  {
    return infNDtc;
  }

  /**
   * @param infNDtc
   *          The infNDtc to set.
   */
  public void setInfNDtc(String infNDtc)
  {
    this.infNDtc = infNDtc;
  }

  /**
   * @return Returns the infNDtcDesc.
   */
  public String getInfNDtcDesc()
  {
    return infNDtcDesc;
  }

  /**
   * @param infNDtcDesc
   *          The infNDtcDesc to set.
   */
  public void setInfNDtcDesc(String infNDtcDesc)
  {
    this.infNDtcDesc = infNDtcDesc;
  }

  /**
   * @return Returns the licAuthD.
   */
  public String getLicAuthD()
  {
    return licAuthD;
  }

  /**
   * @param licAuthD
   *          The licAuthD to set.
   */
  public void setLicAuthD(String licAuthD)
  {
    this.licAuthD = licAuthD;
  }

  /**
   * @return Returns the licRestr.
   */
  public String getLicRestr()
  {
    return licRestr;
  }

  /**
   * @param licRestr
   *          The licRestr to set.
   */
  public void setLicRestr(String licRestr)
  {
    this.licRestr = licRestr;
  }

  /**
   * @return Returns the licRestrDesc.
   */
  public String getLicRestrDesc()
  {
    return licRestrDesc;
  }

  /**
   * @param licRestrDesc
   *          The licRestrDesc to set.
   */
  public void setLicRestrDesc(String licRestrDesc)
  {
    this.licRestrDesc = licRestrDesc;
  }

  /**
   * @return Returns the licTypeCd.
   */
  public String getLicTypeCd()
  {
    return licTypeCd;
  }

  /**
   * @param licTypeCd
   *          The licTypeCd to set.
   */
  public void setLicTypeCd(String licTypeCd)
  {
    this.licTypeCd = licTypeCd;
  }

  /**
   * @return Returns the licTypeCdDesc.
   */
  public String getLicTypeCdDesc()
  {
    return licTypeCdDesc;
  }

  /**
   * @param licTypeCdDesc
   *          The licTypeCdDesc to set.
   */
  public void setLicTypeCdDesc(String licTypeCdDesc)
  {
    this.licTypeCdDesc = licTypeCdDesc;
  }

  /**
   * @return Returns the licValidFromD.
   */
  public String getLicValidFromD()
  {
    return licValidFromD;
  }

  /**
   * @param licValidFromD
   *          The licValidFromD to set.
   */
  public void setLicValidFromD(String licValidFromD)
  {
    this.licValidFromD = licValidFromD;
  }

  /**
   * @return Returns the licValidToD.
   */
  public String getLicValidToD()
  {
    return licValidToD;
  }

  /**
   * @param licValidToD
   *          The licValidToD to set.
   */
  public void setLicValidToD(String licValidToD)
  {
    this.licValidToD = licValidToD;
  }

  public String getIdLicIssD()
  {
    return idLicIssD;
  }

  public void setIdLicIssD(String idLicIssD)
  {
    this.idLicIssD = idLicIssD;
  }
}