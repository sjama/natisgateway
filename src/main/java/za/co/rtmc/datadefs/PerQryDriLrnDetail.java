package za.co.rtmc.datadefs;

public class PerQryDriLrnDetail extends PersonQryDetail
{
  private String lrnCertN;
  private String lrnTypeCd;
  private String lrnTypeCdDesc;
  private String lrnStat;
  private String lrnStatDesc;
  private String lrnValidFromD;
  private String speclAdapInd;

  /**
   * @return Returns the lrnCertN.
   */
  public String getLrnCertN()
  {
    return lrnCertN;
  }

  /**
   * @param lrnCertN The lrnCertN to set.
   */
  public void setLrnCertN(String lrnCertN)
  {
    this.lrnCertN = lrnCertN;
  }

  /**
   * @return Returns the lrnStat.
   */
  public String getLrnStat()
  {
    return lrnStat;
  }

  /**
   * @param lrnStat The lrnStat to set.
   */
  public void setLrnStat(String lrnStat)
  {
    this.lrnStat = lrnStat;
  }

  /**
   * @return Returns the lrnStatDesc.
   */
  public String getLrnStatDesc()
  {
    return lrnStatDesc;
  }

  /**
   * @param lrnStatDesc The lrnStatDesc to set.
   */
  public void setLrnStatDesc(String lrnStatDesc)
  {
    this.lrnStatDesc = lrnStatDesc;
  }

  /**
   * @return Returns the lrnTypeCd.
   */
  public String getLrnTypeCd()
  {
    return lrnTypeCd;
  }

  /**
   * @param lrnTypeCd The lrnTypeCd to set.
   */
  public void setLrnTypeCd(String lrnTypeCd)
  {
    this.lrnTypeCd = lrnTypeCd;
  }

  /**
   * @return Returns the lrnTypeCdDesc.
   */
  public String getLrnTypeCdDesc()
  {
    return lrnTypeCdDesc;
  }

  /**
   * @param lrnTypeCdDesc The lrnTypeCdDesc to set.
   */
  public void setLrnTypeCdDesc(String lrnTypeCdDesc)
  {
    this.lrnTypeCdDesc = lrnTypeCdDesc;
  }

  /**
   * @return Returns the lrnValidFromD.
   */
  public String getLrnValidFromD()
  {
    return lrnValidFromD;
  }

  /**
   * @param lrnValidFromD The lrnValidFromD to set.
   */
  public void setLrnValidFromD(String lrnValidFromD)
  {
    this.lrnValidFromD = lrnValidFromD;
  }

  /**
   * @return Returns the speclAdapInd.
   */
  public String getSpeclAdapInd()
  {
    return speclAdapInd;
  }

  /**
   * @param speclAdapInd The speclAdapInd to set.
   */
  public void setSpeclAdapInd(String speclAdapInd)
  {
    this.speclAdapInd = speclAdapInd;
  }
}
