package za.co.rtmc.datadefs;

/**
 * The request class containing data that came from external system to 
 * introduce a motor vehicle.
 */
public class Ext7Req extends BaseReq
{
  protected VehicleDetail vehicle;
  protected MibDetail     mib;
  protected ThDetail      th; 
  protected OwnDetail     own;

  public MibDetail getMib() 
  {
    return mib;
  }

  public OwnDetail getOwn() 
  {
    return own;
  }

  public ThDetail getTh() 
  {
    return th;
  }

  public VehicleDetail getVehicle() 
  {
    return vehicle;
  }

  public void setMib(MibDetail detail) 
  {
    mib = detail;
  }

  public void setOwn(OwnDetail detail) 
  {
    own = detail;
  }

  public void setTh(ThDetail detail) 
  {
    th = detail;
  }

  public void setVehicle(VehicleDetail detail) 
  {
    vehicle = detail;
  }
}