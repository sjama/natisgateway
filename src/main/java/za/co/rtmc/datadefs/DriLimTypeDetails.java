package za.co.rtmc.datadefs;

import java.io.Serializable;

public class DriLimTypeDetails implements Serializable
{
  protected String driLimTypeCd;
  protected String driLimTypeCdDesc;

  /**
   * @return Returns the driLimTypeCd.
   */
  public String getDriLimTypeCd()
  {
    return driLimTypeCd;
  }

  /**
   * @param driLimTypeCd The driLimTypeCd to set.
   */
  public void setDriLimTypeCd(String driLimTypeCd)
  {
    this.driLimTypeCd = driLimTypeCd;
  }

  /**
   * @return Returns the driLimTypeCdDesc.
   */
  public String getDriLimTypeCdDesc()
  {
    return driLimTypeCdDesc;
  }

  /**
   * @param driLimTypeCdDesc The driLimTypeCdDesc to set.
   */
  public void setDriLimTypeCdDesc(String driLimTypeCdDesc)
  {
    this.driLimTypeCdDesc = driLimTypeCdDesc;
  }
}
