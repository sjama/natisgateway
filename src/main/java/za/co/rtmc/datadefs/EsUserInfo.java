package za.co.rtmc.datadefs;

import java.io.Serializable;

/**
 * Information regarding the connected external user.
 */
public class EsUserInfo implements Serializable
{
  /** The node name from where the connection was established */
  private String esNodeName;

  /** The IP address from where the connection was established */
  private String esIpAddress;

  /**
   * Terminal Id. of external system on which transaction is being performed.
   */
  private String esTermId;

  /** User number of the external system's user. (External Party specified) */
  private String esUid;

  /** The username of the remote user (e.g. Force Number of a SAPS user). */
  private String esUserN;
  
  public String getEsNodeName()
  {
    return esNodeName;
  }

  public void setEsNodeName(String esNodeName)
  {
    this.esNodeName = esNodeName;
  }

  public String getEsIpAddress()
  {
    return esIpAddress;
  }

  public void setEsIpAddress(String esIpAddress)
  {
    this.esIpAddress = esIpAddress;
  }

  public String getEsTermId()
  {
    return esTermId;
  }

  public void setEsTermId(String esTermId)
  {
    this.esTermId = esTermId;
  }

  public String getEsUid()
  {
    return esUid;
  }

  public void setEsUid(String esUid)
  {
    this.esUid = esUid;
  }

  public String getEsUserN()
  {
    return esUserN;
  }

  public void setEsUserN(String esUserN)
  {
    this.esUserN = esUserN;
  }
}
