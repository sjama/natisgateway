package za.co.rtmc.datadefs;


public class VehicleQryDetail extends VehicleDetail
{
  private String regtN;
  private String sapMark;
  private String sapMarkDesc;
  private String mvLicChgD;
  private String mvLicLiaD;
  private String mvLicExpiryD;
  private String regAutOfLic;
  private String regAutOfLicDesc;
  private String regAutOfRegtN;
  private String regAutOfRegtNDesc;
  private String modelName;
  private String modelNameDesc;
  private String mvCatCd;
  private String mvCatCdDesc;
  private String mvDescCd;
  private String mvDescCdDesc;
  private String mvTrailTypeCd;
  private String mvTrailTypeCdDesc;
  private String makeCd;
  private String makeCdDesc;
  private String mainColourCdDesc;
  private String fuelTypeCdDesc;
  private String mvRegtD;
  private String mvXmtStat;
  private String mvXmtStatDesc;
  private String mvRegtTypeCd;
  private String mvRegtTypeCdDesc;
  private String mvRegtQualCd;
  private String mvRegtQualCdDesc;
  private String mvRegtQualD;
  private String rwStat;
  private String rwStatDesc;
  private String rwStatusD;
  private String mvState;
  private String mvStateDesc;
  private String mvStateD;
  private String transmission;
  private String transmissionDesc;
  private String mvLicFirstD;
  private String cntryOfImport;
  private String cntryOfImportDesc;
  private String cntryOfExport;
  private String cntryOfExportDesc;
  private String mvLifeStat;
  private String mvLifeStatDesc;
  private String modelN;
  private String sapClrRsnCd;
  private String sapClrRsnCdDesc;
  private String rwTstD;
  private String infNVts;
  private String infNVtsDesc;
  private String infNExamr;
  private String infNExamrDesc;
  private String sapClrStat;
  private String sapClrStatDesc;
  private String sapClrD;
  private String sapMarkD;
  private String dtOwn;
  private String dtOwnDesc;
  private String timestamp;
  private String localLicFee;
  private String mvUsageCd;
  private String mvUsageCdDesc;
  private String vehUsage;
  private String vehUsageDesc;
  private String vehType;
  private String vehTypeDesc;
  private String mvEconSectorCd;
  private String mvEconSectorCdDesc;
  private String mvPublicRdInd;
  private String certN;
  private String preCertN;
  private String preRegtN;
  private String prePreRegtN;
  private String prevMvOwnOwnShipStartD;
  private String prevMvOwnIdDocN;
  private String queryDateTime;
  private String infringementOverdue;

  /**
   * @return Returns the certN.
   */
  public String getCertN()
  {
    return certN;
  }

  /**
   * @param certN The certN to set.
   */
  public void setCertN(String certN)
  {
    this.certN = certN;
  }

  /**
   * @return Returns the cntryOfExport.
   */
  public String getCntryOfExport()
  {
    return cntryOfExport;
  }

  /**
   * @param cntryOfExport The cntryOfExport to set.
   */
  public void setCntryOfExport(String cntryOfExport)
  {
    this.cntryOfExport = cntryOfExport;
  }

  /**
   * @return Returns the cntryOfExportDesc.
   */
  public String getCntryOfExportDesc()
  {
    return cntryOfExportDesc;
  }

  /**
   * @param cntryOfExportDesc The cntryOfExportDesc to set.
   */
  public void setCntryOfExportDesc(String cntryOfExportDesc)
  {
    this.cntryOfExportDesc = cntryOfExportDesc;
  }

  /**
   * @return Returns the cntryOfImport.
   */
  public String getCntryOfImport()
  {
    return cntryOfImport;
  }

  /**
   * @param cntryOfImport The cntryOfImport to set.
   */
  public void setCntryOfImport(String cntryOfImport)
  {
    this.cntryOfImport = cntryOfImport;
  }

  /**
   * @return Returns the cntryOfImportDesc.
   */
  public String getCntryOfImportDesc()
  {
    return cntryOfImportDesc;
  }

  /**
   * @param cntryOfImportDesc The cntryOfImportDesc to set.
   */
  public void setCntryOfImportDesc(String cntryOfImportDesc)
  {
    this.cntryOfImportDesc = cntryOfImportDesc;
  }

  /**
   * @return Returns the dtOwn.
   */
  public String getDtOwn()
  {
    return dtOwn;
  }

  /**
   * @param dtOwn The dtOwn to set.
   */
  public void setDtOwn(String dtOwn)
  {
    this.dtOwn = dtOwn;
  }

  /**
   * @return Returns the dtOwnDesc.
   */
  public String getDtOwnDesc()
  {
    return dtOwnDesc;
  }

  /**
   * @param dtOwnDesc The dtOwnDesc to set.
   */
  public void setDtOwnDesc(String dtOwnDesc)
  {
    this.dtOwnDesc = dtOwnDesc;
  }

  /**
   * @return Returns the fuelTypeCdDesc.
   */
  public String getFuelTypeCdDesc()
  {
    return fuelTypeCdDesc;
  }

  /**
   * @param fuelTypeCdDesc The fuelTypeCdDesc to set.
   */
  public void setFuelTypeCdDesc(String fuelTypeCdDesc)
  {
    this.fuelTypeCdDesc = fuelTypeCdDesc;
  }

  /**
   * @return Returns the infNExamr.
   */
  public String getInfNExamr()
  {
    return infNExamr;
  }

  /**
   * @param infNExamr The infNExamr to set.
   */
  public void setInfNExamr(String infNExamr)
  {
    this.infNExamr = infNExamr;
  }

  /**
   * @return Returns the infNExamrDesc.
   */
  public String getInfNExamrDesc()
  {
    return infNExamrDesc;
  }

  /**
   * @param infNExamrDesc The infNExamrDesc to set.
   */
  public void setInfNExamrDesc(String infNExamrDesc)
  {
    this.infNExamrDesc = infNExamrDesc;
  }

  /**
   * @return Returns the infNVts.
   */
  public String getInfNVts()
  {
    return infNVts;
  }

  /**
   * @param infNVts The infNVts to set.
   */
  public void setInfNVts(String infNVts)
  {
    this.infNVts = infNVts;
  }

  /**
   * @return Returns the infNVtsDesc.
   */
  public String getInfNVtsDesc()
  {
    return infNVtsDesc;
  }

  /**
   * @param infNVtsDesc The infNVtsDesc to set.
   */
  public void setInfNVtsDesc(String infNVtsDesc)
  {
    this.infNVtsDesc = infNVtsDesc;
  }

  /**
   * @return Returns the localLicFee.
   */
  public String getLocalLicFee()
  {
    return localLicFee;
  }

  /**
   * @param localLicFee The localLicFee to set.
   */
  public void setLocalLicFee(String localLicFee)
  {
    this.localLicFee = localLicFee;
  }

  /**
   * @return Returns the mainColourCdDesc.
   */
  public String getMainColourCdDesc()
  {
    return mainColourCdDesc;
  }

  /**
   * @param mainColourCdDesc The mainColourCdDesc to set.
   */
  public void setMainColourCdDesc(String mainColourCdDesc)
  {
    this.mainColourCdDesc = mainColourCdDesc;
  }

  /**
   * @return Returns the makeCd.
   */
  public String getMakeCd()
  {
    return makeCd;
  }

  /**
   * @param makeCd The makeCd to set.
   */
  public void setMakeCd(String makeCd)
  {
    this.makeCd = makeCd;
  }

  /**
   * @return Returns the makeCdDesc.
   */
  public String getMakeCdDesc()
  {
    return makeCdDesc;
  }

  /**
   * @param makeCdDesc The makeCdDesc to set.
   */
  public void setMakeCdDesc(String makeCdDesc)
  {
    this.makeCdDesc = makeCdDesc;
  }

  /**
   * @return Returns the modelN.
   */
  public String getModelN()
  {
    return modelN;
  }

  /**
   * @param modelN The modelN to set.
   */
  public void setModelN(String modelN)
  {
    this.modelN = modelN;
  }

  /**
   * @return Returns the modelName.
   */
  public String getModelName()
  {
    return modelName;
  }

  /**
   * @param modelName The modelName to set.
   */
  public void setModelName(String modelName)
  {
    this.modelName = modelName;
  }

  /**
   * @return Returns the modelNameDesc.
   */
  public String getModelNameDesc()
  {
    return modelNameDesc;
  }

  /**
   * @param modelNameDesc The modelNameDesc to set.
   */
  public void setModelNameDesc(String modelNameDesc)
  {
    this.modelNameDesc = modelNameDesc;
  }

  /**
   * @return Returns the mvCatCd.
   */
  public String getMvCatCd()
  {
    return mvCatCd;
  }

  /**
   * @param mvCatCd The mvCatCd to set.
   */
  public void setMvCatCd(String mvCatCd)
  {
    this.mvCatCd = mvCatCd;
  }

  /**
   * @return Returns the mvCatCdDesc.
   */
  public String getMvCatCdDesc()
  {
    return mvCatCdDesc;
  }

  /**
   * @param mvCatCdDesc The mvCatCdDesc to set.
   */
  public void setMvCatCdDesc(String mvCatCdDesc)
  {
    this.mvCatCdDesc = mvCatCdDesc;
  }

  /**
   * @return Returns the mvDescCd.
   */
  public String getMvDescCd()
  {
    return mvDescCd;
  }

  /**
   * @param mvDescCd The mvDescCd to set.
   */
  public void setMvDescCd(String mvDescCd)
  {
    this.mvDescCd = mvDescCd;
  }

  /**
   * @return Returns the mvDescCdDesc.
   */
  public String getMvDescCdDesc()
  {
    return mvDescCdDesc;
  }

  /**
   * @param mvDescCdDesc The mvDescCdDesc to set.
   */
  public void setMvDescCdDesc(String mvDescCdDesc)
  {
    this.mvDescCdDesc = mvDescCdDesc;
  }

  /**
   * @return Returns the mvEconSectorCd.
   */
  public String getMvEconSectorCd()
  {
    return mvEconSectorCd;
  }

  /**
   * @param mvEconSectorCd The mvEconSectorCd to set.
   */
  public void setMvEconSectorCd(String mvEconSectorCd)
  {
    this.mvEconSectorCd = mvEconSectorCd;
  }

  /**
   * @return Returns the mvEconSectorCdDesc.
   */
  public String getMvEconSectorCdDesc()
  {
    return mvEconSectorCdDesc;
  }

  /**
   * @param mvEconSectorCdDesc The mvEconSectorCdDesc to set.
   */
  public void setMvEconSectorCdDesc(String mvEconSectorCdDesc)
  {
    this.mvEconSectorCdDesc = mvEconSectorCdDesc;
  }

  /**
   * @return Returns the mvLicChgD.
   */
  public String getMvLicChgD()
  {
    return mvLicChgD;
  }

  /**
   * @param mvLicChgD The mvLicChgD to set.
   */
  public void setMvLicChgD(String mvLicChgD)
  {
    this.mvLicChgD = mvLicChgD;
  }

  /**
   * @return Returns the mvLicExpiryD.
   */
  public String getMvLicExpiryD()
  {
    return mvLicExpiryD;
  }

  /**
   * @param mvLicExpiryD The mvLicExpiryD to set.
   */
  public void setMvLicExpiryD(String mvLicExpiryD)
  {
    this.mvLicExpiryD = mvLicExpiryD;
  }

  /**
   * @return Returns the mvLicFirstD.
   */
  public String getMvLicFirstD()
  {
    return mvLicFirstD;
  }

  /**
   * @param mvLicFirstD The mvLicFirstD to set.
   */
  public void setMvLicFirstD(String mvLicFirstD)
  {
    this.mvLicFirstD = mvLicFirstD;
  }

  /**
   * @return Returns the mvLicLiaD.
   */
  public String getMvLicLiaD()
  {
    return mvLicLiaD;
  }

  /**
   * @param mvLicLiaD The mvLicLiaD to set.
   */
  public void setMvLicLiaD(String mvLicLiaD)
  {
    this.mvLicLiaD = mvLicLiaD;
  }

  /**
   * @return Returns the mvLifeStat.
   */
  public String getMvLifeStat()
  {
    return mvLifeStat;
  }

  /**
   * @param mvLifeStat The mvLifeStat to set.
   */
  public void setMvLifeStat(String mvLifeStat)
  {
    this.mvLifeStat = mvLifeStat;
  }

  /**
   * @return Returns the mvLifeStatDesc.
   */
  public String getMvLifeStatDesc()
  {
    return mvLifeStatDesc;
  }

  /**
   * @param mvLifeStatDesc The mvLifeStatDesc to set.
   */
  public void setMvLifeStatDesc(String mvLifeStatDesc)
  {
    this.mvLifeStatDesc = mvLifeStatDesc;
  }

  /**
   * @return Returns the mvPublicRdInd.
   */
  public String getMvPublicRdInd()
  {
    return mvPublicRdInd;
  }

  /**
   * @param mvPublicRdInd The mvPublicRdInd to set.
   */
  public void setMvPublicRdInd(String mvPublicRdInd)
  {
    this.mvPublicRdInd = mvPublicRdInd;
  }

  /**
   * @return Returns the mvRegtD.
   */
  public String getMvRegtD()
  {
    return mvRegtD;
  }

  /**
   * @param mvRegtD The mvRegtD to set.
   */
  public void setMvRegtD(String mvRegtD)
  {
    this.mvRegtD = mvRegtD;
  }

  /**
   * @return Returns the mvRegtQualCd.
   */
  public String getMvRegtQualCd()
  {
    return mvRegtQualCd;
  }

  /**
   * @param mvRegtQualCd The mvRegtQualCd to set.
   */
  public void setMvRegtQualCd(String mvRegtQualCd)
  {
    this.mvRegtQualCd = mvRegtQualCd;
  }

  /**
   * @return Returns the mvRegtQualCdDesc.
   */
  public String getMvRegtQualCdDesc()
  {
    return mvRegtQualCdDesc;
  }

  /**
   * @param mvRegtQualCdDesc The mvRegtQualCdDesc to set.
   */
  public void setMvRegtQualCdDesc(String mvRegtQualCdDesc)
  {
    this.mvRegtQualCdDesc = mvRegtQualCdDesc;
  }

  /**
   * @return Returns the mvRegtQualD.
   */
  public String getMvRegtQualD()
  {
    return mvRegtQualD;
  }

  /**
   * @param mvRegtQualD The mvRegtQualD to set.
   */
  public void setMvRegtQualD(String mvRegtQualD)
  {
    this.mvRegtQualD = mvRegtQualD;
  }

  /**
   * @return Returns the mvRegtTypeCd.
   */
  public String getMvRegtTypeCd()
  {
    return mvRegtTypeCd;
  }

  /**
   * @param mvRegtTypeCd The mvRegtTypeCd to set.
   */
  public void setMvRegtTypeCd(String mvRegtTypeCd)
  {
    this.mvRegtTypeCd = mvRegtTypeCd;
  }

  /**
   * @return Returns the mvRegtTypeCdDesc.
   */
  public String getMvRegtTypeCdDesc()
  {
    return mvRegtTypeCdDesc;
  }

  /**
   * @param mvRegtTypeCdDesc The mvRegtTypeCdDesc to set.
   */
  public void setMvRegtTypeCdDesc(String mvRegtTypeCdDesc)
  {
    this.mvRegtTypeCdDesc = mvRegtTypeCdDesc;
  }

  /**
   * @return Returns the mvState.
   */
  public String getMvState()
  {
    return mvState;
  }

  /**
   * @param mvState The mvState to set.
   */
  public void setMvState(String mvState)
  {
    this.mvState = mvState;
  }

  /**
   * @return Returns the mvStateD.
   */
  public String getMvStateD()
  {
    return mvStateD;
  }

  /**
   * @param mvStateD The mvStateD to set.
   */
  public void setMvStateD(String mvStateD)
  {
    this.mvStateD = mvStateD;
  }

  /**
   * @return Returns the mvStateDesc.
   */
  public String getMvStateDesc()
  {
    return mvStateDesc;
  }

  /**
   * @param mvStateDesc The mvStateDesc to set.
   */
  public void setMvStateDesc(String mvStateDesc)
  {
    this.mvStateDesc = mvStateDesc;
  }

  /**
   * @return Returns the mvTrailTypeCd.
   */
  public String getMvTrailTypeCd()
  {
    return mvTrailTypeCd;
  }

  /**
   * @param mvTrailTypeCd The mvTrailTypeCd to set.
   */
  public void setMvTrailTypeCd(String mvTrailTypeCd)
  {
    this.mvTrailTypeCd = mvTrailTypeCd;
  }

  /**
   * @return Returns the mvTrailTypeCdDesc.
   */
  public String getMvTrailTypeCdDesc()
  {
    return mvTrailTypeCdDesc;
  }

  /**
   * @param mvTrailTypeCdDesc The mvTrailTypeCdDesc to set.
   */
  public void setMvTrailTypeCdDesc(String mvTrailTypeCdDesc)
  {
    this.mvTrailTypeCdDesc = mvTrailTypeCdDesc;
  }

  /**
   * @return Returns the mvUsageCd.
   */
  public String getMvUsageCd()
  {
    return mvUsageCd;
  }

  /**
   * @param mvUsageCd The mvUsageCd to set.
   */
  public void setMvUsageCd(String mvUsageCd)
  {
    this.mvUsageCd = mvUsageCd;
  }

  /**
   * @return Returns the mvUsageCdDesc.
   */
  public String getMvUsageCdDesc()
  {
    return mvUsageCdDesc;
  }

  /**
   * @param mvUsageCdDesc The mvUsageCdDesc to set.
   */
  public void setMvUsageCdDesc(String mvUsageCdDesc)
  {
    this.mvUsageCdDesc = mvUsageCdDesc;
  }

  /**
   * @return Returns the mvXmtStat.
   */
  public String getMvXmtStat()
  {
    return mvXmtStat;
  }

  /**
   * @param mvXmtStat The mvXmtStat to set.
   */
  public void setMvXmtStat(String mvXmtStat)
  {
    this.mvXmtStat = mvXmtStat;
  }

  /**
   * @return Returns the mvXmtStatDesc.
   */
  public String getMvXmtStatDesc()
  {
    return mvXmtStatDesc;
  }

  /**
   * @param mvXmtStatDesc The mvXmtStatDesc to set.
   */
  public void setMvXmtStatDesc(String mvXmtStatDesc)
  {
    this.mvXmtStatDesc = mvXmtStatDesc;
  }

  /**
   * @return Returns the preCertN.
   */
  public String getPreCertN()
  {
    return preCertN;
  }

  /**
   * @param preCertN The preCertN to set.
   */
  public void setPreCertN(String preCertN)
  {
    this.preCertN = preCertN;
  }

  /**
   * @return Returns the prePreRegtN.
   */
  public String getPrePreRegtN()
  {
    return prePreRegtN;
  }

  /**
   * @param prePreRegtN The prePreRegtN to set.
   */
  public void setPrePreRegtN(String prePreRegtN)
  {
    this.prePreRegtN = prePreRegtN;
  }

  /**
   * @return Returns the preRegtN.
   */
  public String getPreRegtN()
  {
    return preRegtN;
  }

  /**
   * @param preRegtN The preRegtN to set.
   */
  public void setPreRegtN(String preRegtN)
  {
    this.preRegtN = preRegtN;
  }

  /**
   * @return Returns the regAutOfLic.
   */
  public String getRegAutOfLic()
  {
    return regAutOfLic;
  }

  /**
   * @param regAutOfLic The regAutOfLic to set.
   */
  public void setRegAutOfLic(String regAutOfLic)
  {
    this.regAutOfLic = regAutOfLic;
  }

  /**
   * @return Returns the regAutOfLicDesc.
   */
  public String getRegAutOfLicDesc()
  {
    return regAutOfLicDesc;
  }

  /**
   * @param regAutOfLicDesc The regAutOfLicDesc to set.
   */
  public void setRegAutOfLicDesc(String regAutOfLicDesc)
  {
    this.regAutOfLicDesc = regAutOfLicDesc;
  }

  /**
   * @return Returns the regAutOfRegtN.
   */
  public String getRegAutOfRegtN()
  {
    return regAutOfRegtN;
  }

  /**
   * @param regAutOfRegtN The regAutOfRegtN to set.
   */
  public void setRegAutOfRegtN(String regAutOfRegtN)
  {
    this.regAutOfRegtN = regAutOfRegtN;
  }

  /**
   * @return Returns the regAutOfRegtNDesc.
   */
  public String getRegAutOfRegtNDesc()
  {
    return regAutOfRegtNDesc;
  }

  /**
   * @param regAutOfRegtNDesc The regAutOfRegtNDesc to set.
   */
  public void setRegAutOfRegtNDesc(String regAutOfRegtNDesc)
  {
    this.regAutOfRegtNDesc = regAutOfRegtNDesc;
  }

  /**
   * @return Returns the regtN.
   */
  public String getRegtN()
  {
    return regtN;
  }

  /**
   * @param regtN The regtN to set.
   */
  public void setRegtN(String regtN)
  {
    this.regtN = regtN;
  }

  /**
   * @return Returns the rwStat.
   */
  public String getRwStat()
  {
    return rwStat;
  }

  /**
   * @param rwStat The rwStat to set.
   */
  public void setRwStat(String rwStat)
  {
    this.rwStat = rwStat;
  }

  /**
   * @return Returns the rwStatDesc.
   */
  public String getRwStatDesc()
  {
    return rwStatDesc;
  }

  /**
   * @param rwStatDesc The rwStatDesc to set.
   */
  public void setRwStatDesc(String rwStatDesc)
  {
    this.rwStatDesc = rwStatDesc;
  }

  /**
   * @return Returns the rwStatusD.
   */
  public String getRwStatusD()
  {
    return rwStatusD;
  }

  /**
   * @param rwStatusD The rwStatusD to set.
   */
  public void setRwStatusD(String rwStatusD)
  {
    this.rwStatusD = rwStatusD;
  }

  /**
   * @return Returns the rwTstD.
   */
  public String getRwTstD()
  {
    return rwTstD;
  }

  /**
   * @param rwTstD The rwTstD to set.
   */
  public void setRwTstD(String rwTstD)
  {
    this.rwTstD = rwTstD;
  }

  /**
   * @return Returns the sapClrRsnCd.
   */
  public String getSapClrRsnCd()
  {
    return sapClrRsnCd;
  }

  /**
   * @param sapClrRsnCd The sapClrRsnCd to set.
   */
  public void setSapClrRsnCd(String sapClrRsnCd)
  {
    this.sapClrRsnCd = sapClrRsnCd;
  }

  /**
   * @return Returns the sapClrD.
   */
  public String getSapClrD()
  {
    return sapClrD;
  }

  /**
   * @param sapClrD The sapClrD to set.
   */
  public void setSapClrD(String sapClrD)
  {
    this.sapClrD = sapClrD;
  }

  /**
   * @return Returns the sapClrStat.
   */
  public String getSapClrStat()
  {
    return sapClrStat;
  }

  /**
   * @param sapClrStat The sapClrStat to set.
   */
  public void setSapClrStat(String sapClrStat)
  {
    this.sapClrStat = sapClrStat;
  }

  /**
   * @return Returns the sapClrStatDesc.
   */
  public String getSapClrStatDesc()
  {
    return sapClrStatDesc;
  }

  /**
   * @param sapClrStatDesc The sapClrStatDesc to set.
   */
  public void setSapClrStatDesc(String sapClrStatDesc)
  {
    this.sapClrStatDesc = sapClrStatDesc;
  }

  /**
   * @return Returns the sapMark.
   */
  public String getSapMark()
  {
    return sapMark;
  }

  /**
   * @param sapMark The sapMark to set.
   */
  public void setSapMark(String sapMark)
  {
    this.sapMark = sapMark;
  }

  /**
   * @return Returns the sapMarkD.
   */
  public String getSapMarkD()
  {
    return sapMarkD;
  }

  /**
   * @param sapMarkD The sapMarkD to set.
   */
  public void setSapMarkD(String sapMarkD)
  {
    this.sapMarkD = sapMarkD;
  }

  /**
   * @return Returns the sapMarkDesc.
   */
  public String getSapMarkDesc()
  {
    return sapMarkDesc;
  }

  /**
   * @param sapMarkDesc The sapMarkDesc to set.
   */
  public void setSapMarkDesc(String sapMarkDesc)
  {
    this.sapMarkDesc = sapMarkDesc;
  }

  /**
   * @return Returns the timestamp.
   */
  public String getTimestamp()
  {
    return timestamp;
  }

  /**
   * @param timestamp The timestamp to set.
   */
  public void setTimestamp(String timestamp)
  {
    this.timestamp = timestamp;
  }

  /**
   * @return Returns the transmission.
   */
  public String getTransmission()
  {
    return transmission;
  }

  /**
   * @param transmission The transmission to set.
   */
  public void setTransmission(String transmission)
  {
    this.transmission = transmission;
  }

  /**
   * @return Returns the transmissionDesc.
   */
  public String getTransmissionDesc()
  {
    return transmissionDesc;
  }

  /**
   * @param transmissionDesc The transmissionDesc to set.
   */
  public void setTransmissionDesc(String transmissionDesc)
  {
    this.transmissionDesc = transmissionDesc;
  }

  /**
   * @return Returns the vehUsage.
   */
  public String getVehUsage()
  {
    return vehUsage;
  }

  /**
   * @param vehUsage The vehUsage to set.
   */
  public void setVehUsage(String vehUsage)
  {
    this.vehUsage = vehUsage;
  }

  /**
   * @return Returns the vehUsageDesc.
   */
  public String getVehUsageDesc()
  {
    return vehUsageDesc;
  }

  /**
   * @param vehUsageDesc The vehUsageDesc to set.
   */
  public void setVehUsageDesc(String vehUsageDesc)
  {
    this.vehUsageDesc = vehUsageDesc;
  }

  /**
   * @return Returns the sapClrRsnCdDesc.
   */
  public String getSapClrRsnCdDesc()
  {
    return sapClrRsnCdDesc;
  }

  /**
   * @param sapClrRsnCdDesc The sapClrRsnCdDesc to set.
   */
  public void setSapClrRsnCdDesc(String sapClrRsnCdDesc)
  {
    this.sapClrRsnCdDesc = sapClrRsnCdDesc;
  }

  /**
   * @return Returns the vehType.
   */
  public String getVehType()
  {
    return vehType;
  }

  /**
   * @param vehType The vehType to set.
   */
  public void setVehType(String vehType)
  {
    this.vehType = vehType;
  }

  /**
   * @return Returns the vehTypeDesc.
   */
  public String getVehTypeDesc()
  {
    return vehTypeDesc;
  }

  /**
   * @param vehTypeDesc The vehTypeDesc to set.
   */
  public void setVehTypeDesc(String vehTypeDesc)
  {
    this.vehTypeDesc = vehTypeDesc;
  }

  public String getPrevMvOwnIdDocN()
  {
    return prevMvOwnIdDocN;
  }

  public void setPrevMvOwnIdDocN(String prevMvOwnIdDocN)
  {
    this.prevMvOwnIdDocN = prevMvOwnIdDocN;
  }

  public String getPrevMvOwnOwnShipStartD()
  {
    return prevMvOwnOwnShipStartD;
  }

  public void setPrevMvOwnOwnShipStartD(String prevMvOwnOwnShipStartD)
  {
    this.prevMvOwnOwnShipStartD = prevMvOwnOwnShipStartD;
  }

  public String getQueryDateTime()
  {
    return queryDateTime;
  }

  public void setQueryDateTime(String currSysDate)
  {
    this.queryDateTime = currSysDate;
  }
  
  public String getInfringementOverdue()
  {
    return infringementOverdue;
  }

  public void setInfringementOverdue(String infringementOverdue)
  {
    this.infringementOverdue = infringementOverdue;
  }
}
