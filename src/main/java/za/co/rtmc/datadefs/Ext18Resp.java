package za.co.rtmc.datadefs;

import java.util.Collection;

/**
 * Ext18Resp
 */
public class Ext18Resp extends BaseResp
{
  private Collection ext18DetResponseList;

  /**
   * @param successful   Indicates whether the transaction was successful.
   * @param errors       errors that occurred during the processing of the 
   *                     transaction.
   */
  public Ext18Resp(boolean isSuccessful, Collection errors)
  {
    super(isSuccessful, errors);
  }

  /**
   * Default constructor.
   */
  public Ext18Resp()
  {
  }

  /**
   * @return Returns the ext18DetResponseList.
   */
  public Collection getExt18DetResponseList()
  {
    return ext18DetResponseList;
  }

  /**
   * @param ext12DetResponseList The ext12DetResponseList to set.
   */
  public void setExt18DetResponseList(Collection ext18DetResponseList)
  {
    this.ext18DetResponseList = ext18DetResponseList;
  }
}
