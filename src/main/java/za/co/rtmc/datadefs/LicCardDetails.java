package za.co.rtmc.datadefs;

import java.io.Serializable;

public class LicCardDetails implements Serializable
{
  private String cardN;
  private String cardExpiryD;
  private String licTypeCd;
  private String licTypeCdDesc;
  private String idLicIssD;
  private String cardIssN;
  private String cardSuspFromD;
  private String cardSuspToD;
  
  public String getCardN()
  {
    return cardN;
  }

  public void setCardN(String cardN)
  {
    this.cardN = cardN;
  }

  public String getCardExpiryD()
  {
    return cardExpiryD;
  }

  public void setCardExpiryD(String cardExpiryD)
  {
    this.cardExpiryD = cardExpiryD;
  }

  public String getLicTypeCd()
  {
    return licTypeCd;
  }

  public void setLicTypeCd(String licTypeCd)
  {
    this.licTypeCd = licTypeCd;
  }

  public String getLicTypeCdDesc()
  {
    return licTypeCdDesc;
  }

  public void setLicTypeCdDesc(String licTypeCdDesc)
  {
    this.licTypeCdDesc = licTypeCdDesc;
  }

  public String getIdLicIssD()
  {
    return idLicIssD;
  }

  public void setIdLicIssD(String idLicIssD)
  {
    this.idLicIssD = idLicIssD;
  }

  public String getCardIssN()
  {
    return cardIssN;
  }

  public void setCardIssN(String cardIssN)
  {
    this.cardIssN = cardIssN;
  }

  public String getCardSuspFromD()
  {
    return cardSuspFromD;
  }

  public void setCardSuspFromD(String cardSuspFromD)
  {
    this.cardSuspFromD = cardSuspFromD;
  }

  public String getCardSuspToD()
  {
    return cardSuspToD;
  }

  public void setCardSuspToD(String cardSuspToD)
  {
    this.cardSuspToD = cardSuspToD;
  }

}
