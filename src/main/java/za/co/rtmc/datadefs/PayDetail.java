/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.rtmc.datadefs;

/**
 *
 * @author jamasithole
 */
import java.io.Serializable;


public class PayDetail implements Serializable
{

  private String payDate;
  private String payAmount;

  public String getPayDate()
  {
    return payDate;
  }

  public void setPayDate(String payDate)
  {
    this.payDate = payDate;
  }

  public String getPayAmount()
  {
    return payAmount;
  }

  public void setPayAmount(String payAmount)
  {
    this.payAmount = payAmount;
  }

}
