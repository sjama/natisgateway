package za.co.rtmc.datadefs;

import java.io.Serializable;
import java.util.ArrayList;

public class PersonQryDetail extends PersonDetail implements Serializable
{
  private String       perIdN;
  private String       addrChgD;
  private String       userGrpCd;
  private String       userGrpCdDesc;
  private String       tradeName;
  private String       cardN;
  private String       cardExpiryD;
  private String       proxRepInd;
  
  protected ArrayList driLimDetailsList;
  protected ArrayList driEndorseDetailsList;
  protected ArrayList driLicDetailsList;
  protected ArrayList prdpDetailsList;
  protected ArrayList licCardList;
  
  /**
   * @return Returns the addrChgD.
   */
  public String getAddrChgD()
  {
    return addrChgD;
  }

  /**
   * @param addrChgD
   *          The addrChgD to set.
   */
  public void setAddrChgD(String addrChgD)
  {
    this.addrChgD = addrChgD;
  }

  /**
   * @return Returns the cardExpiryD.
   */
  public String getCardExpiryD()
  {
    return cardExpiryD;
  }

  /**
   * @param cardExpiryD
   *          The cardExpiryD to set.
   */
  public void setCardExpiryD(String cardExpiryD)
  {
    this.cardExpiryD = cardExpiryD;
  }

  /**
   * @return Returns the cardN.
   */
  public String getCardN()
  {
    return cardN;
  }

  /**
   * @param cardN
   *          The cardN to set.
   */
  public void setCardN(String cardN)
  {
    this.cardN = cardN;
  }


  /**
   * @return Returns the driEndorseDetailsList.
   */
  public ArrayList getDriEndorseDetailsList()
  {
    return driEndorseDetailsList;
  }

  /**
   * @param driEndorseDetailsList
   *          The driEndorseDetailsList to set.
   */
  public void setDriEndorseDetailsList(ArrayList driEndorseDetailsList)
  {
    this.driEndorseDetailsList = driEndorseDetailsList;
  }

  /**
   * @return Returns the driLicDetailsList.
   */
  public ArrayList getDriLicDetailsList()
  {
    return driLicDetailsList;
  }

  /**
   * @param driLicDetailsList
   *          The driLicDetailsList to set.
   */
  public void setDriLicDetailsList(ArrayList driLicDetailsList)
  {
    this.driLicDetailsList = driLicDetailsList;
  }

  /**
   * @return Returns the driLimDetailsList.
   */
  public ArrayList getDriLimDetailsList()
  {
    return driLimDetailsList;
  }

  /**
   * @param driLimDetailsList
   *          The driLimDetailsList to set.
   */
  public void setDriLimDetailsList(ArrayList driLimDetailsList)
  {
    this.driLimDetailsList = driLimDetailsList;
  }


  /**
   * @return Returns the perIdN.
   */
  public String getPerIdN()
  {
    return perIdN;
  }

  /**
   * @param perIdN
   *          The perIdN to set.
   */
  public void setPerIdN(String perIdN)
  {
    this.perIdN = perIdN;
  }

  
  /**
   * @return Returns the prdpDetailsList.
   */
  public ArrayList getPrdpDetailsList()
  {
    return prdpDetailsList;
  }

  /**
   * @param prdpDetailsList
   *          The prdpDetailsList to set.
   */
  public void setPrdpDetailsList(ArrayList prdpDetailsList)
  {
    this.prdpDetailsList = prdpDetailsList;
  }

 
  /**
   * @return Returns the tradeName.
   */
  public String getTradeName()
  {
    return tradeName;
  }

  /**
   * @param tradeName
   *          The tradeName to set.
   */
  public void setTradeName(String tradeName)
  {
    this.tradeName = tradeName;
  }

  /**
   * @return Returns the userGrpCd.
   */
  public String getUserGrpCd()
  {
    return userGrpCd;
  }

  /**
   * @param userGrpCd
   *          The userGrpCd to set.
   */
  public void setUserGrpCd(String userGrpCd)
  {
    this.userGrpCd = userGrpCd;
  }

  /**
   * @return Returns the userGrpCdDesc.
   */
  public String getUserGrpCdDesc()
  {
    return userGrpCdDesc;
  }

  /**
   * @param userGrpCdDesc
   *          The userGrpCdDesc to set.
   */
  public void setUserGrpCdDesc(String userGrpCdDesc)
  {
    this.userGrpCdDesc = userGrpCdDesc;
  }
  
  /**
   * @return Returns the proxRepInd.
   */
  public String getProxRepInd()
  {
    return proxRepInd;
  }
  
  /**
   * @param proxRepInd The proxRepInd to set.
   */
  public void setProxRepInd(String proxRepInd)
  {
    this.proxRepInd = proxRepInd;
  }

   public ArrayList getLicCardList()
  {
    return licCardList;
  }

  public void setLicCardList(ArrayList licCardList)
  {
    this.licCardList = licCardList;
  }
}