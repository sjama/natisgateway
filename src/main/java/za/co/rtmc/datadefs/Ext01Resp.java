package za.co.rtmc.datadefs;

import java.util.Collection;

/**
 * This is a subclass of tbe normal login response because it also contains a 
 * list for fields the user is not allowed to see, as well as a list of fields
 * that are required as input. This class is specific to the Http external 
 * interface.
 */
public class Ext01Resp extends Ext0Resp
{
  /** The list of MarshallVo's that define the fields the user is not allowed 
   * to view */
  private Collection     marshallingList;
  
  /** The list of UnmarshallVo's that define the field input requirements for 
   *  the user */
  private Collection     unMarshallingList;
  
  /** The session timeout (stored in  minutes) */
  private int            sesIdleTime;
  
  private String         referenceN;
  
  public String getReferenceN()
  {
    return referenceN;
  }

  public void setReferenceN(String referenceN)
  {
    this.referenceN = referenceN;
  }

  /**
   * Method to obtain the list of MarshallVo's.
   * 
   * @return  Collection The list of MarshallVo's
   */
  public Collection getMarshallingList() 
  {
    return marshallingList;
  }

  /**
   * Method to obtain the list of UnmarshallVo's.
   * 
   * @return  Collection The list of UnmarshallVo's. 
   */
  public Collection getUnMarshallingList() 
  {
    return unMarshallingList;
  }

  /**
   * Method to set the list of MarshallVo's.
   * 
   * @param list The list of MarshallVo's.
   */
  public void setMarshallingList(Collection list) 
  {
    marshallingList = list;
  }

  /**
   * Method to set the list of UnmarshallVo's.
   * 
   * @param list The list of UnmarshallVo's.
   */
  public void setUnMarshallingList(Collection list) 
  {
    unMarshallingList = list;
  }

  /**
   * Method to fet the sesIdleTime
   * 
   * @return  sesIdleTime
   */
  public int getSesIdleTime()
  {
    return sesIdleTime;
  }

  /**
   * Method to set the sesIdleTime
   * 
   * @param sesIdleTime The sesIdleTime to set
   */
  public void setSesIdleTime(int sesIdleTime)
  {
    this.sesIdleTime = sesIdleTime;
  }
}
