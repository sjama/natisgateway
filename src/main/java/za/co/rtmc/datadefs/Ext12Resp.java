package za.co.rtmc.datadefs;

import java.util.ArrayList;
import java.util.Collection;

/**
 *  Ext12Resp
 */
public class Ext12Resp extends BaseResp
{

  private ArrayList ext12DetResponseList;
  
  /**
   * @param successful   Indicates whether the transaction was successful.
   * @param errors       errors that occurred during the processing of the 
   *                     transaction.
   */
  public Ext12Resp(boolean isSuccessful, Collection errors)
  {
    super(isSuccessful, errors);
  }

  /**
   * Default constructor.
   */
  public Ext12Resp()
  {
  }
  
  /**
   * @return Returns the ext12DetResponseList.
   */
  public ArrayList getExt12DetResponseList()
  {
    return ext12DetResponseList;
  }
  
  /**
   * @param ext12DetResponseList The ext12DetResponseList to set.
   */
  public void setExt12DetResponseList(ArrayList ext12DetResponseList)
  {
    this.ext12DetResponseList = ext12DetResponseList;
  }
}
