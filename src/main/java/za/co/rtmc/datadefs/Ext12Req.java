package za.co.rtmc.datadefs;

/**
 * Ext12Req represents a request for txan X1001.
 */
public class Ext12Req extends BaseReq
{
  
  /** MV register number */
  private String mvRegN;

  /** MV licence number */
  private String regtN;

  /** MV register number */
  private String vinOrChassis;

  /** Engine number */
  private String engineN;

  /** Previous licence number */
  private String preRegtN;

  /** Reference Date */
  private String refereneD;
  
  /**
   * @param creds The security credentials.
   */
  public Ext12Req(SecurityCredentials creds)
  {
    super(creds, ExtTxanConstants.EXT12_TXAN_CD);    
  }

  /**
   * Default constructor.
   */
  public Ext12Req()
  {
    setExTxanCd(ExtTxanConstants.EXT12_TXAN_CD);
  }
  
  /**
   * @return Returns the engineN.
   */
  public String getEngineN()
  {
    return engineN;
  }

  /**
   * @param engineN The engineN to set.
   */
  public void setEngineN(String engineN)
  {
    this.engineN = engineN;
  }

  /**
   * @return Returns the mvRegN.
   */
  public String getMvRegN()
  {
    return mvRegN;
  }

  /**
   * @param mvRegN The mvRegN to set.
   */
  public void setMvRegN(String mvRegN)
  {
    this.mvRegN = mvRegN;
  }

  /**
   * @return Returns the preRegtN.
   */
  public String getPreRegtN()
  {
    return preRegtN;
  }

  /**
   * @param preRegtN The preRegtN to set.
   */
  public void setPreRegtN(String preRegtN)
  {
    this.preRegtN = preRegtN;
  }

  /**
   * @return Returns the regtN.
   */
  public String getRegtN()
  {
    return regtN;
  }

  /**
   * @param regtN The regtN to set.
   */
  public void setRegtN(String regtN)
  {
    this.regtN = regtN;
  }

  /**
   * @return Returns the vinOrChassis.
   */
  public String getVinOrChassis()
  {
    return vinOrChassis;
  }

  /**
   * @param vinOrChassis The vinOrChassis to set.
   */
  public void setVinOrChassis(String vinOrChassis)
  {
    this.vinOrChassis = vinOrChassis;
  }
  
  /**
   * @return Returns the refereneD.
   */
  public String getRefereneD()
  {
    return refereneD;
  }
  
  /**
   * @param refereneD The refereneD to set.
   */
  public void setRefereneD(String refereneD)
  {
    this.refereneD = refereneD;
  }
}
