package za.co.rtmc.datadefs;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Ext13Resp
 */
public class Ext13Resp extends BaseResp
{
  
  private ArrayList ext13DetResponseList;
  
  /**
   * @param successful   Indicates whether the transaction was successful.
   * @param errors       errors that occurred during the processing of the 
   *                     transaction.
   */
  public Ext13Resp(boolean isSuccessful, Collection errors)
  {
    super(isSuccessful, errors);
  }

  /**
   * Default constructor.
   */
  public Ext13Resp()
  {
  }
  
  /**
   * @return Returns the ext13DetResponseList.
   */
  public ArrayList getExt13DetResponseList()
  {
    return ext13DetResponseList;
  }
  
  /**
   * @param ext13DetResponseList The ext13DetResponseList to set.
   */
  public void setExt13DetResponseList(ArrayList ext13DetResponseList)
  {
    this.ext13DetResponseList = ext13DetResponseList;
  }
}
