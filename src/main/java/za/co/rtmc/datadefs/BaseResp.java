package za.co.rtmc.datadefs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * BaseResp is the base class for responses to requests from external
 * interfaces.
 */
public class BaseResp implements Serializable
{
  /** Indicates whether the transaction was successful. */
  protected boolean    successful;

  /** A list of errors that occurred during the processing of the transaction. */
  protected Collection errors;

  /** Holds the header infomation. */
  protected BaseHeader header;

  /** 
   * When an Mdb catches an exception it will set this property to the 
   * exception caught. 
   */
  protected Exception  exception;

  /**
   * Default constructor.
   */
  public BaseResp()
  {
  }

  /**
   * Construct a new response with a given success state and error list.
   * 
   * @param successful Indicates whether the transaction was successful.
   * @param errors     Errors that occurred during the processing of the 
   *                   transaction.
   */
  public BaseResp(boolean successful, Collection errors)
  {
    this.successful = successful;
    this.errors = errors;
  }

  /**
   * Constructor. NB This contructor should only be used by Mdb's.
   * 
   * @param e The exception that occurred.
   */
  public BaseResp(Exception e)
  {
    this.exception = e;
  }

  /**
   * Return the processing errors that occurred during transaction processing.
   * 
   * @return Returns the errors.
   */
  public Collection getErrors()
  {
    return errors;
  }

  /**
   * Set the processing errors that occurred during transaction processing.
   * 
   * @param errors The list of processing errors.
   */
  public void setErrors(Collection errors)
  {
    this.errors = errors;
  }
  
  /**
   * Add a  processing error that occurred during transaction processing.
   * 
   * @param error A processing error.
   */
  public void addError(Object error)
  {
     if(errors == null)
     {
       errors = new ArrayList();
     }
     
     // Only add the error if it is not already there
     if (!errors.contains(error))
    {
       errors.add(error);  
    }
  }

  /**
   * Return the success state of transaction processing.
   * 
   * @return Returns <code>true</code> if the transaction was successful.
   */
  public boolean isSuccessful()
  {
    return successful;
  }

  /**
   * Set the success state of transaction processing.
   * @param successful The success state to set.
   */
  public void setSuccessful(boolean successful)
  {
    this.successful = successful;
  }

  /**
   * The header of this response.
   * 
   * @return Returns the header.
   */
  public BaseHeader getHeader()
  {
    return header;
  }

  /**
   * Set the header of this response.
   * @param header The header to set.
   */
  public void setHeader(BaseHeader header)
  {
    this.header = header;
  }

  /**
   * Retrieve the exception (if any) that occurred during transaction 
   * processing.
   * 
   * @return Returns the exception.
   */
  public Exception getException()
  {
    return exception;
  }

  /**
   * Save the exception that occurred during transaction processing.
   * 
   * @param exception The exception to set.
   */
  public void setException(Exception exception)
  {
    this.exception = exception;
  }

  /**
   * Constructs a <code>String</code> with all attributes
   * in name = value format.
   *
   * @return a <code>String</code> representation 
   * of this object.
   */
  public String toString()
  {
      final String TAB = "    ";
      
      String retValue = "";
      
      retValue = "BaseResp ( "
          + super.toString() + TAB
          + "successful = " + this.successful + TAB
          + "errors = " + this.errors + TAB
          + "header = " + this.header + TAB
          + "exception = " + this.exception + TAB
          + " )";
  
      return retValue;
  }

}