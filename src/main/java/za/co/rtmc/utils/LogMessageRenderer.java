package za.co.rtmc.utils;

import java.text.MessageFormat;
import org.apache.log4j.or.ObjectRenderer;

public class LogMessageRenderer implements ObjectRenderer
{
  /** null input value rendered */
  private static final String NULL_STRING = "null";
  
  /**
   * Render the given input messages to a string
   * 
   * @param msg  The message object to render
   */
  public String doRender(Object msg) 
  {

    // Check for null input
    if (msg == null) return NULL_STRING;

    // Render message according to type
    if (msg instanceof LogMessage) 
    {
      // Render LogMessage
      LogMessage  logMsg = (LogMessage) msg;
      String      msgKey = logMsg.getMsgKey();

      try 
      {
        String pattern = logMsg.getLogMessageBundle().getString(msgKey);

        // Return the key if it cannot be resolved
        if (pattern == null) return msgKey;

        // Return the formatted message
        return MessageFormat.format(pattern, logMsg.getMsgParams());
      }
      catch (Throwable e) 
      {
        // Return the message key, if en exception occurs
        return msgKey;
      }
    }
    else 
    {
      // Unknown key - return the String representation thereof
      return msg.toString();
    }
  }
}