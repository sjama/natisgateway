package za.co.rtmc.utils;

import java.util.ResourceBundle;

public class MsgCodeConverter
{
  /** Resource bundle to use for these messages */
  private static final ResourceBundle RESOURCE_BUNDLE  = ResourceBundle.getBundle("za.co.mibsa.utils.LogMessageBundle");
  
  /**
   * Error Log Message: An unhandled runtime exception occured.
   * 
   * @return  LogMessage  The LogMessage representing this message code
   */
  public static final LogMessage ERROR_LOG_RUNTIME()
  {
    return new LogMessage(LogMessageBundle.ERROR_LOG_RUNTIME, RESOURCE_BUNDLE);
  }
  
  public static final LogMessage ERROR_LOG_DATA()
  {
    return new LogMessage(LogMessageBundle.ERROR_LOG_DATA_NOT_FOUND, RESOURCE_BUNDLE);
  }
}
