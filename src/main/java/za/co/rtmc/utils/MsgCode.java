package za.co.rtmc.utils;

import java.util.HashMap;

public class MsgCode
{

  public static HashMap<String, String> errorMap;
  
  static
  {
    errorMap = new HashMap<String, String>();
    
    errorMap.put("00001", "The length of a specified VIN, must be 17 characters.");
    errorMap.put("00002", "First 3 characters of Chassis number must be a recognised WMI.");
    errorMap.put("00003", "Last 4 characters of Chassis number must be numeric.");
    errorMap.put("00004", "No duplicates are allowed.  An existing record with the same value for the field was found.");
    errorMap.put("00005", "Invalid value entered.  Refer to applicable lookup table for a list of valid codes.");
    errorMap.put("00006", "Entity not allowed to introduce this model.");
    errorMap.put("00007", "A value must be entered in this field.  Compulsory entry when the model is self-propelled.");
    errorMap.put("00008", "Date may not be in the future.");
    errorMap.put("00009", "Date may not be older than one year.");
    errorMap.put("00010", "When tare is not equal to 99999 kg, tare must be within the range specified for the vehicle category and vehicle trailer type.");
    errorMap.put("00011", "Invalid identification.  The holder of the identification must go to his/her local registering authority to rectify the identification.");
    errorMap.put("00012", "Invalid value entered.  If the type of identification code entered is 01, 02, or 04 the ID number must be 13 characters long.");
    errorMap.put("00013", "Invalid value entered.  If the type of identification code entered is 01, 02, or 04 the number�s check digit must be valid.");
    errorMap.put("00014", "Person does not exist on NaTIS or may be moving.  If the motor manufacturer is not the title holder, the title holder must exist on NaTIS and may not be busy moving.");
    errorMap.put("00015", "Invalid axle configuration. The number of driving axles cannot be more than total number of axles.");
    errorMap.put("00016", "Motor vehicle does not exist on the local region.  A motor vehicle with the entered characteristic must exist for the usergroup of the user.");
    errorMap.put("00017", "Vehicle in invalid state for change. The valid state is 01.");
    errorMap.put("00018", "Date may not be before the current state date.");
    errorMap.put("00019", "Date may not be before the current change date.");
    errorMap.put("00020", "Agent must be allowed to introduce motor vehicle as the specific type of business.");
    errorMap.put("00021", "Nature of ownership may only be 3 or 4.");
    errorMap.put("00022", "The motor vehicle state must be 01 or 02.");
    errorMap.put("00023", "The business type must be a 1 or 2");
    errorMap.put("00024", "Manufacturer/Importer may not introduce a motor vehicle of any model.");
    errorMap.put("00025", "A password is required.");
    errorMap.put("00026", "This user has expired.");
    errorMap.put("00027", "User not authorised to do any transactions.");
    errorMap.put("00028", "Number of concurrent sessions is exceeded.");
    errorMap.put("00029", "The user is not currently logged in.");
    errorMap.put("00030", "Entity has administration marks.");
    errorMap.put("00031", "If entity is an importer, a country from where vehicle is imported, must be specified.");
    errorMap.put("00032", "Compulsory field if vehicle is allocated for export.");
    errorMap.put("00033", "Motor vehicle with entered characteristic, does not exist on the eNaTIS.");
    errorMap.put("00034", "The motor vehicle record may not be marked for moving.");
    errorMap.put("00035", "Entity does not exist.");
    errorMap.put("00036", "Entity may not be busy moving.");
    errorMap.put("00037", "Entity must be registered as a driver.");
    errorMap.put("00038", "If a 17 character VIN/Chassis number is entered the characters �I�, �O� or �Q� must not be used.");
    errorMap.put("00039", "At least one of the vehicle identifiers must be entered.");
    errorMap.put("00040", "Person with entered identifiers, does not exist on the NaTIS.");
    errorMap.put("00041", "VIN/Chassis and at least one other vehicle identifier must be entered.");
    errorMap.put("00042", "Field is compulsory and must be entered.");
    errorMap.put("00043", "Field value does not match the one on the database for the entered identifiers.");
    errorMap.put("00044", "Field value must be equal to or greater than TARE field's value.");
    errorMap.put("00045", "Indicated fields not allowed simultaneously.");
    errorMap.put("00046", "Missing fields in XML request.");
    errorMap.put("00047", "Missing data in XML request field.");
    errorMap.put("00048", "Invalid User number entered in request.");
    errorMap.put("00049", "No valid address exists for postage of licence disk. Please see Registering Authority.");
    errorMap.put("00050", "Field must contain a value greater than zero and equal to the value sent.");
    errorMap.put("00051", "More than one unlicenced vehicle/arrears and penalties exists. Please pay at Registering Authority.");
    errorMap.put("00052", "Record can not be processed at an ATM.  Please pay at Registering Authority.");
    errorMap.put("00053", "Invalid date format.");
    errorMap.put("00054", "Document control number must be the latest one.");
    errorMap.put("00055", "Identity document number must belong to the current Titleholder and/or belong to the external User that's logged in..");
    errorMap.put("00056", "Identity number must belong to the current Owner.");
    errorMap.put("00057", "Invalid value entered.  Refer to IR-X1008 for a list of valid Lookup names.");
    errorMap.put("00058", "No vehicle due for licencing were found.");
    errorMap.put("00059", "MV Licence already renewed.");
    errorMap.put("00060", "Person data owner must be the same as the person area code, it is not. Please see Registering Authority.");
    errorMap.put("00061", "Additional roadworthy requirements - please use manual transactions.");
    errorMap.put("00062", "Police involvement - please use manual transactions.");
    errorMap.put("00063", "For this transaction the new titleholder must be the current owner.");
    errorMap.put("00064", "Vehicle must be licenced or transaction authorised - please use manual transactions.");
    errorMap.put("00065", "Current and new titleholder may not be the same.");
    errorMap.put("00066", "Change of Titleholder not required, valid authorisation exists.");
    errorMap.put("00067", "The vehicle must be licenced (12), exempt from licencing (5) or liable for licencing (6).");
    errorMap.put("00068", "One of the Vin / Chassis number or vehicle register number must be sent.");
    errorMap.put("00069", "The Vin/Chassis number does not match the identified vehicle (MVRegN).");
    errorMap.put("00070", "The vehicle register number does not match the identified vehicle (MVRegN).");
    errorMap.put("00071", "Identification document type (IdDocTypeCd) not valid for the entity.");
    errorMap.put("00072", "The bank and the province of the vehicle not yet connected, please use manual transactions.");
    errorMap.put("00073", "The external user does not match the identity of the entity.");
    errorMap.put("00074", "Current vehicle state invalid for queries - stolen/scrapped. Owned by insurance company.");
    errorMap.put("00075", "Processing of previous transaction on this session not yet completed.");
    errorMap.put("00076", "User not allowed to query vehicle with entered identifiers.");
    errorMap.put("00077", "Value must be numeric only.");
    errorMap.put("00078", "The entered identification is not indicated on the system as the current identification");
    errorMap.put("00079", "The entered identification is not an acceptable form of identification.");
    errorMap.put("00080", "Applicant has fine outstanding infringements.");
    errorMap.put("00081", "Vehicle identification requires two of VIN, licence number or register number.");
    errorMap.put("00082", "Invalid combination of vehicle identifiers.");
    errorMap.put("00083", "At least two of the vehicle identifiers must be entered.");
    errorMap.put("00084", "Ownership of this vehicle was changed after theft, scrapping or demolition.");
    errorMap.put("00085", "This vehicle must be registered by an insurance company.");
    errorMap.put("00086", "Registration of this vehicle requires authorisation.");
    errorMap.put("00087", "Another person has been authorised to register this vehicle.");
    errorMap.put("00088", "No registration/deregistration certificate exists.");
    errorMap.put("00089", "No valid registration/deregistration certificate or authorisation exists.");
    errorMap.put("00090", "No valid registration/deregistration certificate but authorisation not possible.");
    errorMap.put("00091", "Applicant has outstanding arrears and penalties for other vehicles.");
    errorMap.put("00092", "Date may not be before NaTIS start date.");
    errorMap.put("00093", "No valid authorisation found for backdating this transaction.");
    errorMap.put("00094", "Authorisation was performed by the same user.");
    errorMap.put("00095", "The applicant is not the new title holder indicated by the current title holder.");
    errorMap.put("00096", "Date is not within the allowed range.");
    errorMap.put("00097", "No change of owner allowed.");
    errorMap.put("00098", "No change of title holder or owner allowed.");
    errorMap.put("00099", "The motor vehicle record is still controlled by the MIB agent.");
    errorMap.put("00100", "Date may not be before date liable for registration.");
    errorMap.put("00101", "The motor vehicle has not been deregistered yet, upon notification of theft or scrapping.");
    errorMap.put("00102", "Motor vehicle is not currently licensed.");
    errorMap.put("00103", "Vehicle in invalid state for change.");
    errorMap.put("N/A", "N/A");
  }
  
  public static synchronized String getErrorMsg(String errCode)
  {
    return errorMap.get(errCode); 
  }
}
