
package za.co.rtmc.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;


public class MsgLogger
{
  private static  Logger logger = Logger.getLogger("co.za.mib.logger");
  
  /**
   * Report a "fatal" level error message in the error log
   * 
   * @param errorMsg  The error message object to log
   * @param t         Any throwable exception which should be logged in 
   *                    conjunction with the error message
   * @return String   The referenece number assigned to the error message
   */
  public static final String logFatal(LogMessage errorMsg, Throwable t)
  {
    return logError(Level.FATAL, errorMsg, t);
  }
  
  /**
   * Report an "error" level error message in the error log
   * 
   * @param errorMsg  The error message object to log
   * @param t         Any throwable exception which should be logged in 
   *                    conjunction with the error message
   * @return String   The referenece number assigned to the error message
   */
  public static final String logError(LogMessage errorMsg, Throwable t)
  {
    return logError(Level.ERROR, errorMsg, t);
  }
  
  private static final String logError(Level level, LogMessage errorMsg,
      Throwable t)
  {
    String refNum = "";


    refNum += System.currentTimeMillis();

    // Store the reference to be included in the message in the
    // Nested Diagnostic Context (NDC)
    NDC.push("Error Ref# " + refNum);

    // Log the error message
    logger.log(level, errorMsg, t);

    // Clear the reference number from the NDC
    NDC.pop();

    // Return the reference number
    return String.valueOf(refNum);
  }

}
