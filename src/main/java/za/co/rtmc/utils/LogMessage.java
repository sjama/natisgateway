package za.co.rtmc.utils;

import java.util.ResourceBundle;

public class LogMessage {

  /** Message identifier */
  private String msgKey;

  /** Message parameters */
  private Object[] msgParams;

  /** Resource bundle for this message */
  private ResourceBundle resourceBundle;

  /** The renderer for this message */
  private static final LogMessageRenderer RENDERER        = new LogMessageRenderer();

  /**
   * Build a new log message from the given message key
   * 
   * @param msgKey          The resource bundle index of the log message
   * @param resourceBundle  The resource bundle to use for this message
   */
  public LogMessage(String msgKey, ResourceBundle resBundle) {
    this.msgKey         = msgKey;
    this.msgParams      = null;
    this.resourceBundle = resBundle;
  }

  /**
   * Build a new log message from the given message key and message parameters
   * 
   * @param msgKey    The resource bundle index of the log message
   * @param msgParams The parameters required by the message
   * @param resourceBundle  The resource bundle to use for this message
   */
  public LogMessage(String msgKey, Object[] msgParams, ResourceBundle resBundle)
  {
    this.msgKey         = msgKey;
    this.msgParams      = msgParams;
    this.resourceBundle = resBundle;
  }

  /**
   * Retrieve the current message key
   * 
   * @return String the current value of the message key.
   */
  public String getMsgKey() {
    return msgKey;
  }

  /**
   * Retrieve the current message parameters
   * 
   * @return Object[] The current value of the message parameters.
   */
  public Object[] getMsgParams() {
    return msgParams;
  }

  /**
   * Retrieve the LogMessageBundle associated with this log message
   *
   * @return ResourceBundle The current ResourceBundle handle
   */
  public ResourceBundle getLogMessageBundle() {
    return resourceBundle;
  }

  /**
   * Returns the message represented by this LogMessage.
   */
  public String getMessage() {
    return RENDERER.doRender(this);
  }
}

