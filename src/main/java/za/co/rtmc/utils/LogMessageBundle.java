package za.co.rtmc.utils;

import java.util.ListResourceBundle;



/**
 * This class represents all the static log message formats for the eNaTIS 
 * system. These formats include the error log messages, as well as the messages
 * for the audit roll.
 */
public class LogMessageBundle extends ListResourceBundle
{
  // Indicates the starting number of the message bundle
  private static final int BUNDLE_BASE            = 0;

  // Indicates the start of the audit roll messages range
  private static final int AUDIT_ROLL_BASE        = BUNDLE_BASE;

  // The number available for audit roll messages
  private static final int AUDIT_ROLL_RANGE_SIZE  = 100;

  // Indcates the start of the error log messages range
  private static final int ERROR_LOG_BASE         =  AUDIT_ROLL_BASE + 
                                                        AUDIT_ROLL_RANGE_SIZE;
  


  /* ******************* */
  /* MESSAGES START HERE */
  /* ******************* */

  // Error log messages
  public static final String ERROR_LOG_RUNTIME                  = intToStr(ERROR_LOG_BASE + 1);
  public static final String ERROR_LOG_DATA_NOT_FOUND           = intToStr(ERROR_LOG_BASE + 2);

  private static final Object[][] sMessageStrings = new String[][]
  {


    // Error log messages
    { ERROR_LOG_RUNTIME, "An unhandled runtime exception occured."},
    { ERROR_LOG_DATA_NOT_FOUND, "No data found"},

  };
  
  /**
   *
   * Return String Identifiers and corresponding Messages in a two-dimensional array.
   */
  protected Object[][] getContents()
  {
    return sMessageStrings;
  }

  /**
   * Method to convert an int to String given a specified length.
   * @param   intValue    The int value.
   * @return  String      The String representation of the int.
   */
  private static final String intToStr(int intValue)
  {
    return StringUtils.justify(String.valueOf(intValue), 
                                StringUtils.JUST_RIGHT, 5, '0');
  }
}