package za.co.rtmc.utils;

import java.util.Arrays;

public class StringUtils
{

  // These are the justification options

  /**
   * Justify left
   */
  public static final byte    JUST_LEFT       = -1;

  /**
   * Justify right
   */
  public static final byte    JUST_RIGHT      = -2;

  /**
   * Justify center
   */
  public static final byte    JUST_CENTER     = -3;  
  
  /** Character ranges for number generation */
  private static final String ALPHANUM_RANGE = "0123456789BCDFGHJKLMNPRSTVWXYZ";
  private static final String NUMERIC_RANGE  = "0123456789";
    
  
  /**
   * Private constructor to prevent instances of this utility class
   */
  private StringUtils()
  {
  }

  /**
   * Justify a given string by padding it with a given character. 
   * If the given string is shorter than justifyLen, the
   * resultant string will have length justifyLen. If not, the input string is 
   * left unchanged.
   *
   * @param   inText      String to justify
   * @param   justifyOp   Justification option to apply
   * @param   justifyLen  Length to justify to
   * @param   padChar     Character to pad val with (if needed)
   * @return  String      The justified string
   * @throws  IllegalArgumentException  If an invalid justify operation is 
   *  specified.
   */
  public static final String justify(String inText, byte justifyOp, int justifyLen, 
                          char padChar) throws IllegalArgumentException
  {
    String outText  = new String(inText);
    int    copyPos  = 0;

    // Only pad if length is less than padLen
    if (outText.length() < justifyLen) {
      char inputChars[]   = inText.toCharArray();
      char outputChars[]  = new char[justifyLen];

      Arrays.fill(outputChars, padChar);

      switch (justifyOp) {
        case JUST_LEFT:                            // Pad on right side
          copyPos = 0;
          break;

        case JUST_RIGHT:                           // Pad on left side
          copyPos = justifyLen - inputChars.length;
          break;

        case JUST_CENTER:                          // Pad on both left and right
          copyPos = (justifyLen - inputChars.length)/2;
          break;
          
        default:
          throw new IllegalArgumentException(
              "Invalid justify operation specified");
      }

      System.arraycopy(inputChars, 0, outputChars, copyPos, inputChars.length);

      outText = new String(outputChars);
    }

    return outText;
  }

  /**
   * Generate the next alphanumeric number in a series, making use of the digits
   * 0-9 and A-Z, excluding vowels and Q. The number is generated with a maximum
   * length, following the algorithm:
   * 
   * A 9 will rollover to B, B to C, ... Z to 0. Thus
   * 
   *  Number : 99     maxGenLen : 2   Result : 9B
   *  Number : ZY     maxGenLen : 2   Result : ZZ
   *  Number : ZZ     maxGenLen : 2   Result : 100  --> Results in an Exception
   * 
   * @param   currNum   The value of the current number
   * @param   maxGenLen The maximum generate length of the number
   * @return  String    The next number in the series
   * @throws  IllegalArgumentException  If currNum is not supplied
   * @throws  NumberFormatException     If illegal characters are found or the 
   *    maximum length is exceeded
   */
  public static final String genNextNumber(
                                String  currNum, 
                                int     maxGenLen) 
                                          throws IllegalArgumentException
  {    
    return genNextNumber(currNum, maxGenLen, true);
  }
  
  /**
   * Generate the next number in a series, making use of the eNaTIS curNumArreric
   * (0-9 and A-Z, excluding vowels and Q) or standard Numeric range. 
   * The number is generated with a maximum length, following the algorithm:
   * 
   * curNumArreric:
   * -------------
   * A 9 will rollover to B, B to C, ... Z to 0. Thus
   * 
   *  Number : 99     maxGenLen : 2   Result : 9B
   *  Number : ZY     maxGenLen : 2   Result : ZZ
   *  Number : ZZ     maxGenLen : 2   Result : 100  --> Results in an Exception
   * 
   * @param   currNum         The value of the current number
   * @param   maxGenLen       The maximum generate length of the number
   * @param   isAlphaNumeric  True when the curNumArreric range should be used
   * 
   * @return  String    The next number in the series
   * 
   * @throws  IllegalArgumentException  If currNum is not supplied
   * @throws  NumberFormatException     If illegal characters are found or the 
   *    maximum length is exceeded
   */
  public static final String genNextNumber(
                                String  currNum, 
                                int     maxGenLen,
                                boolean isAlphaNumeric) 
                                                  throws 
                                                    IllegalArgumentException
  {   
    
    int    RANGE_MAX_INDEX;
    String workingRange;
    
    if (isAlphaNumeric)
    {
      workingRange = ALPHANUM_RANGE;
    }
    else
    {
      workingRange = NUMERIC_RANGE;      
    }
    
    RANGE_MAX_INDEX  = workingRange.length() - 1;
    
    StringBuffer  nextNumber = new StringBuffer();
    char[]        curNumArr   = null;
    char          firstChar;

    // Check validity of parameters
    if (currNum == null)
    {
      throw new IllegalArgumentException(
        "Current Number NOT supplied for number generation");
    }

    // Process characters in current Number
    curNumArr = currNum.toCharArray();
    firstChar = curNumArr[0];
    
    int index;
    
    for (int i = curNumArr.length - 1; i >= 0; i--) 
    {
      index = workingRange.indexOf(curNumArr[i]); 
       
      // Throw exception if the current character is invalid
      if (index < 0)
      {
        throw new NumberFormatException("Illegal character [" + curNumArr[i] + 
           "] found in value [" + currNum + "].");
      }
        
      // Set new value for the character
      if (index == RANGE_MAX_INDEX) 
      {
        // End of series, wrap number and check the next character
        curNumArr[i] = workingRange.charAt(0);
      } 
      else 
      {
        // Increment only this character
        curNumArr[i] = workingRange.charAt(index + 1);
        break;
      }
    }

    // Was the first character changed past the end of the range?
    if (firstChar != curNumArr[0]) 
    {
      // First character was changed, thus it wrapped if it equals 
      // workingRange.charAt(0)
      if (curNumArr[0] == workingRange.charAt(0)) 
      {
        // Cannot increase length if maximum reached
        if (curNumArr.length >= maxGenLen) 
        {
           throw new NumberFormatException("Maximum number range exceeded");
        }
         
        // Increase length
        nextNumber.append(workingRange.charAt(1));
      }
    }

    // Append next generated number
    nextNumber.append(curNumArr);
    
    return nextNumber.toString();
  }

  /**
   * Generate the next number in a series, making use of the eNaTIS curNumArreric
   * (0-9 and A-Z, excluding vowels and Q) or standard Numeric range. 
   * The number is generated with a maximum length, following the algorithm:
   * 
   * curNumArreric:
   * -------------
   * A 9 will rollover to B, B to C, ... Z to 0. Thus
   * 
   *  Number : 99     maxGenLen : 2   Result : 9B
   *  Number : ZY     maxGenLen : 2   Result : ZZ
   *  Number : ZZ     maxGenLen : 2   Result : 100  --> Results in an Exception
   * 
   * @param   currNum         The value of the current number
   * @param   maxGenLen       The maximum generate length of the number
   * @param   isAlphaNumeric  True when the curNumArreric range should be used
   * 
   * @return  String    The next number in the series
   * 
   * @throws  IllegalArgumentException  If currNum is not supplied
   * @throws  NumberFormatException     If illegal characters are found or the 
   *    maximum length is exceeded
   */
  public static final String genPreviousNumber(
                                String  currNum, 
                                int     maxGenLen) 
                                                  throws 
                                                    IllegalArgumentException
  {   
    
    int    RANGE_MIN_INDEX;
    String workingRange = NUMERIC_RANGE;      
    
    
    RANGE_MIN_INDEX  = 0;
    
    StringBuffer  nextNumber = new StringBuffer();
    char[]        curNumArr   = null;

    // Check validity of parameters
    if (currNum == null)
    {
      throw new IllegalArgumentException(
        "Current Number NOT supplied for number generation");
    }

    // Process characters in current Number
    curNumArr = currNum.toCharArray();
    
    // Throw exception if the current number is invalid
    if (currNum.equals("000000000"))
    {
      throw new NumberFormatException(
          "Illegal character number value [" + currNum + "].");
    }
    
    int index;
    
    for (int i = curNumArr.length - 1; i >= 0; i--) 
    {
      index = workingRange.indexOf(curNumArr[i]); 
      

      // Throw exception if the current character is invalid
      if (index < 0)
      {
        throw new NumberFormatException("Illegal character [" + curNumArr[i] + 
           "] found in value [" + currNum + "].");
      }
        
      // Set new value for the character
      if (index == RANGE_MIN_INDEX) 
      {
        // End of series, wrap number and check the next character
        curNumArr[i] = workingRange.charAt(workingRange.length() - 1);
      } 
      else 
      {
        // Decrement only this character
        curNumArr[i] = workingRange.charAt(index - 1);
        break;
      }
    }

    // Append next generated number
    nextNumber.append(curNumArr);
    
    return nextNumber.toString();
  }
  
  /**
   * Returns the next certificate number.
   * The certificate number must be 8 or 12 characters.
   *
   * @return The next certificate number.
   */
  public static final String genNextCertN(String certN)
          throws NumberFormatException
  {
    if (certN == null || (certN.length() != 12 && certN.length() != 8))
    {
      return certN;
    }
    String userGrpCd = certN.substring(0, 4);

    String nextCertN = null;

    if (certN.length() == 12 )
    {
      nextCertN = genNextNumber(certN.substring(4), 8, true);
    }
    else if (certN.length() == 8)
    {
      nextCertN = genNextNumber(certN.substring(4), 4, true);
    }

    return userGrpCd + nextCertN;
  }

  /**
   * Convert a given string to uppercase, ignoring string values set to null
   * 
   * @param inString  The string to convert to uppercase
   * @return String A string value that has been converted to uppercase
   */
  public static final String toUpperCase(String inString) 
  {
    return (inString == null) ? inString : inString.toUpperCase();
  }

  /**
   * Convert the String to a default value when null.
   * @param   aString   The string to be converted.
   * @param   aDefault  The default String to use.
   * @return  String    The new string.
   */
  public static final String ifNull(String aString, String aDefault)
  {
    if (aString == null) 
    {
      return aDefault;
    }
    else
    {
      return aString;
    }
  }

  /**
   * This method converts the first character of the given string to upper-case.
   *
   * @param string The string for which the first character is converted to
   *               upper-case.
   * @return       The converted string is returned.
   */
  public static final String toUpperFirstChar(String string)
  {
    
    // Only handle valid Strings
    if (string == null)
    {
      return null;
    }
    if (string.equals(""))
    {
      return "";
    }

    return (string.substring(0, 1).toUpperCase() + 
            string.substring(1, string.length()));
  }
  
  /**
   * This method converts the first character of the given string to lower-case.
   *
   * @param string The string for which the first character is converted to
   *               lower-case.
   * @return       The converted string is returned.
   */
  public static final String toLowerFirstChar(String string)
  {
    
    // Only handle valid Strings
    if (string == null)
    {
      return null;
    }
    if (string.equals(""))
    {
      return "";
    }

    return (string.substring(0, 1).toLowerCase() + 
            string.substring(1, string.length()));
  }

  /**
   * Extract the class name out of the fully qualified class name.
   * 
   * @param  packageName   The name of the package
   * @param  fullClassName The fully qualified class name
   * @return               The class name stripped from the package name 
   */
  public static String getClassName(String packageName, String fullClassName)
  {
    return fullClassName.substring(packageName.length()+1, fullClassName.length());
  }

  
  

}
