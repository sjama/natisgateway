package za.co.rtmc.utils;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.IUnmarshallingContext;
import org.jibx.runtime.JiBXException;

import za.co.rtmc.datadefs.BaseResp;
import za.co.rtmc.datadefs.Ext01Resp;
import za.co.rtmc.datadefs.ErrorDet;
public class XmlUtils
{
  
  /** Character encoding to be used */
  private static final String CHAR_ENCODING = "UTF-8";

 /**
  * Marshals the response.
  * 
  * @param response      The transaction response.
  * 
  * @throws Exception    May be thrown marshalling. 
  */
 public static String marshalObject(Object response) throws Exception
 {
   IBindingFactory respFact = BindingDirectory.getFactory(response.getClass());
   IMarshallingContext mctx = respFact.createMarshallingContext();
   StringWriter s = new StringWriter();
   mctx.marshalDocument(response, CHAR_ENCODING, null, s);
     return s.getBuffer().toString();
 }
 
 /**
  * Unmarshals the request object.
  * 
  * @param xmlRequest     The xml request.
  * @param requestClass   The request class.
  * 
  * @throws JiBXException May be thrown unmarshalling. 
  */
 public static Object unmarshal(String xmlRequest,
                                Class<? extends Object> requestClass)
                      throws JiBXException
 {
   IBindingFactory bfact;

   bfact = BindingDirectory.getFactory(requestClass);
   IUnmarshallingContext uctx = bfact.createUnmarshallingContext();

   Object request = uctx.unmarshalDocument(new StringReader(xmlRequest), null);

   return request;
 }

 public static void main(String[] args) throws Exception
{
 /*  String login = "<X0000Req><StdReqHeader><OpModInd>TEST</OpModInd> <ESTxanID>"
     + 1
     + "</ESTxanID>   <ESUserN>1234</ESUserN>   <ESUID>1234</ESUID>   <ESTermID>1</ESTermID>  </StdReqHeader> <UserID>307A001</UserID> <Password>TESTER01</Password> <TxanType>OPEN</TxanType></X0000Req>";
   
   BaseReq req = (BaseReq) XmlUtils.unmarshal(login, Ext01Req.class);
   System.out.println(req.getHeader().getEsUserInfo().getEsUserN());
   
   String out = XmlUtils.marshalObject(req);
   System.out.println(out);
   */
   String error = "<X4000Resp><StdRespHeader><TxanName>X2415</TxanName><ESTxanID>X2415</ESTxanID><ESUID>12345</ESUID><ESTermID>1</ESTermID></StdRespHeader><TxState>FIELD_INVALID</TxState><ErrorFields><ErrField>Vehicle.UnitN</ErrField><ErrDesc>00004</ErrDesc></ErrorFields></X4000Resp>";
   BaseResp resp = (BaseResp) XmlUtils.unmarshal(error, Ext01Resp.class);
   ArrayList list = (ArrayList) resp.getErrors();
   ErrorDet err = (ErrorDet) list.get(0);
   System.out.println(err.getErrorDesc());
   System.out.println(err.getErrorField());
   
   
   
   
}
}
