package za.co.rtmc.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import tasima.common.ws.schema.InfringementQueryResponse;

import tasima.common.ws.schema.MicrodotRequest;
import tasima.common.ws.schema.QueryInfringementsForWebsiteResponse;
import tasima.common.ws.schema.QueryInfringer;
import tasima.common.ws.schema.UploadMicrodotResponse;
import za.co.rtmc.connector.ESIConnector;

/**
 * Created by ashrafmoosa on 2016/07/02.
 */
@Controller
@RequestMapping("/rest/")
public class EnatisGatewayController {


    @Autowired
    private ESIConnector esiConnector;


    @RequestMapping(value = "/xml//login", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> login() {

        String resp = null;

        try {

            resp = esiConnector.login();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);


        }
        return new ResponseEntity<String>(resp, new HttpHeaders(), HttpStatus.OK);

    }

    @RequestMapping(value = "/json/uploadMicrodot", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> uploadMicrodot(@RequestBody MicrodotRequest microdot)  {

        UploadMicrodotResponse resp = null;

        try {

            resp = esiConnector.postMicroDot(microdot);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Object>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);


        }

        return new ResponseEntity<Object>(resp, new HttpHeaders(), HttpStatus.OK);

    }

    @RequestMapping(value = "/xml/logout", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> logout()  {

        String resp = null;

        try {

            resp = esiConnector.logout();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);


        }

        return new ResponseEntity<String>(resp, new HttpHeaders(), HttpStatus.OK);

    }
    
    @RequestMapping(value = "/json/trigger", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> trigger()  {

        Object resp = null;
		Date date = new Date();

        try {
        	GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), 0);
			MicrodotRequest st = new MicrodotRequest();
			st.setEffectiveDate( "2017-10-01" );
			st.setMicrodotPIN("AHT12345");
			st.setVinOrChassisNumber("AHT123456");
            resp = esiConnector.postMicroDot(st);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Object>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);


        }

        return new ResponseEntity<Object>(resp, new HttpHeaders(), HttpStatus.OK);

    }


    @RequestMapping(value = "/xml/X1001", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> queryVehicleFull(@RequestParam(value = "vinOrChassis", required = false) String vinOrChassis,
                                                   @RequestParam(value = "mvRegN", required = false) String mvRegN,
                                                   @RequestParam(value = "licN", required = false) String licN)  {

        String resp = null;

        try {

            resp = esiConnector.queryVehicleFull(mvRegN, vinOrChassis, licN);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return new ResponseEntity<String>(resp, new HttpHeaders(), HttpStatus.OK);

    }

    @RequestMapping(value = "/xml/X1002", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> queryPersonFull(@RequestParam(value = "idDocTypeCd", required = false) String idDocTypeCd,
                                                  @RequestParam(value = "idDocN", required = false) String idDocN,
                                                  @RequestParam(value = "busOrSurname", required = false) String busOrSurname,
                                                  @RequestParam(value = "cardN", required = false) String cardN,
                                                  @RequestParam(value = "licCertN", required = false) String licCertN) {

        String resp = null;


        try {

            resp = esiConnector.queryPersonFull(idDocTypeCd, idDocN, busOrSurname, cardN, licCertN);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return new ResponseEntity<String>(resp, new HttpHeaders(), HttpStatus.OK);

    }

    @RequestMapping(value = "/xml/X1007", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> queryVehicleSummary(@RequestParam(value = "vinOrChassis", required = false) String vinOrChassis,
                                                      @RequestParam(value = "mvRegN", required = false) String mvRegN,
                                                      @RequestParam(value = "licN", required = false) String licN)  {

        String resp = null;

        try {

            resp = esiConnector.queryVehicleSummary(mvRegN, vinOrChassis, licN);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

        }

        return new ResponseEntity<String>(resp, new HttpHeaders(), HttpStatus.OK);

    }
    
    @RequestMapping(value = "/json/queryInfring", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> queryInfr(@RequestParam(value = "idDocumentTypeCode", required = true) String idDocumentTypeCode,
            @RequestParam(value = "idDocumentNumber", required = true) String idDocumentNumber, 
            @RequestParam(value = "licCardN", required = true) String licCardN)  {

    	//InfringementQueryResponse resp = null;
        QueryInfringementsForWebsiteResponse resp = null;

        try {
        	QueryInfringer infRequest = new QueryInfringer();
        	infRequest.setIdDocumentNumber(idDocumentNumber);
        	infRequest.setIdDocumentTypeCode(idDocumentTypeCode);
        	

            resp = esiConnector.queryInfringer(infRequest, licCardN);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Object>(e.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);


        }

        return new ResponseEntity<Object>(resp, new HttpHeaders(), HttpStatus.OK);

    }

}
    