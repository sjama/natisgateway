package za.co.rtmc.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements ApplicationContextAware {

        @Override
        public void onStartup(ServletContext container) throws ServletException {

            container.addFilter("CORSFilter", CorsFilter.class).addMappingForUrlPatterns(null, false, "/*");

            super.onStartup(container);
        }

        @Override
        protected String[] getServletMappings() {
            return new String[] { "/" };
        }

        @Override
        protected Class<?>[] getRootConfigClasses() {

            return new Class<?>[]{

            };
        }

        @Override
        protected Class<?>[] getServletConfigClasses() {
            return new Class<?>[] { MvcConfig.class};
        }

        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        }
}
