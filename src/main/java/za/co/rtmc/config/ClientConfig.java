package za.co.rtmc.config;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.xml.soap.MessageFactory;

import org.apache.cocoon.configuration.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.soap.security.support.KeyStoreFactoryBean;
import org.springframework.ws.soap.security.support.TrustManagersFactoryBean;
import org.springframework.ws.transport.http.HttpsUrlConnectionMessageSender;

import za.co.rtmc.constants.Constants;

@Configuration
public class ClientConfig {

    private String defaultUri;

    @Value("classpath:clientcert.jks")
    private Resource trustStore;

    private String trustStorePassword;

    @Autowired
    private Settings settings;

    private String keystoreLocation;
    private String keystorePass;

    @Bean
    Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setContextPath("tasima.common.ws.schema");
        try {
            jaxb2Marshaller.afterPropertiesSet();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jaxb2Marshaller;
    }

    @Bean
    public WebServiceTemplate webServiceTemplate() throws Exception {
        SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory(MessageFactory.newInstance());
        messageFactory.afterPropertiesSet();
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate(messageFactory);
        webServiceTemplate.setMarshaller(jaxb2Marshaller());
        webServiceTemplate.setUnmarshaller(jaxb2Marshaller());
        webServiceTemplate.afterPropertiesSet();

        webServiceTemplate.setDefaultUri("https://iftst.enatis.co.za/enatis/ws/");
        // set a httpsUrlConnectionMessageSender to handle the HTTPS session
        webServiceTemplate.setMessageSender(httpsUrlConnectionMessageSender());

        return webServiceTemplate;
    }

    @Bean
    public HttpsUrlConnectionMessageSender httpsUrlConnectionMessageSender() throws Exception {
        HttpsUrlConnectionMessageSender httpsUrlConnectionMessageSender
                = new HttpsUrlConnectionMessageSender();
        httpsUrlConnectionMessageSender.setTrustManagers(trustManagersFactoryBean().getObject());
        // allows the client to skip host name verification as otherwise following error is thrown:
        // java.security.cert.CertificateException: No name matching localhost found
        httpsUrlConnectionMessageSender.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                if ("localhost".equals(hostname)) {
                    return true;
                }
                return false;
            }
        });

        return httpsUrlConnectionMessageSender;
    }

    @Bean
    public KeyStoreFactoryBean trustStore() {

        keystoreLocation = settings.getProperty(Constants.PROP_KEYSTORE_LOCATION);
        keystorePass = settings.getProperty(Constants.PROP_KEYSTORE_PASSWORD);
        KeyStoreFactoryBean keyStoreFactoryBean = new KeyStoreFactoryBean();
        keyStoreFactoryBean.setLocation(trustStore);
        keyStoreFactoryBean.setPassword("IDd0ts$eN4");

        return keyStoreFactoryBean;
    }

    @Bean
    public TrustManagersFactoryBean trustManagersFactoryBean() {
        TrustManagersFactoryBean trustManagersFactoryBean = new TrustManagersFactoryBean();
        trustManagersFactoryBean.setKeyStore(trustStore().getObject());

        return trustManagersFactoryBean;
    }

    @PostConstruct
    public void init() {
        
        //System.setProperty("javax.net.ssl.trustStore", "/usr/java/jdk1.8.0_131/jre/lib/security/cacerts");

       System.setProperty("javax.net.ssl.trustStore", "/Library/Java/JavaVirtualMachines/jdk1.8.0_111.jdk/Contents/Home/jre/lib/security/cacerts");
       //System.setProperty("javax.net.ssl.trustStore", "/Users/jamasithole/jssecacerts"); 
       System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        System.setProperty("javax.net.ssl.keyStore", "/Users/jamasithole/Documents/Certs/IdDots/IDDots.pfx");
        System.setProperty("javax.net.ssl.keyStorePassword", "IDd0ts$eN4");
        System.setProperty("javax.net.ssl.keyStoreType", "pkcs12");
        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//		System.setProperty("javax.net.ssl.keyStore", keystoreLocation);
//		System.setProperty("javax.net.ssl.keyStorePassword", keystorePass);
    }
}
    