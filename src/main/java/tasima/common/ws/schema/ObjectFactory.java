
package tasima.common.ws.schema;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the tasima.common.ws.schema package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _QueryVehicleDetails_QNAME = new QName("http://tasima/common/ws/schema/", "QueryVehicleDetails");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: tasima.common.ws.schema
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link X3029Request }
     * 
     */
    public X3029Request createX3029Request() {
        return new X3029Request();
    }

    /**
     * Create an instance of {@link X3048Request }
     * 
     */
    public X3048Request createX3048Request() {
        return new X3048Request();
    }

    /**
     * Create an instance of {@link X3004Request }
     * 
     */
    public X3004Request createX3004Request() {
        return new X3004Request();
    }

    /**
     * Create an instance of {@link X3050Request }
     * 
     */
    public X3050Request createX3050Request() {
        return new X3050Request();
    }

    /**
     * Create an instance of {@link X3027Response }
     * 
     */
    public X3027Response createX3027Response() {
        return new X3027Response();
    }

    /**
     * Create an instance of {@link MinimalVehicleQueryResponse }
     * 
     */
    public MinimalVehicleQueryResponse createMinimalVehicleQueryResponse() {
        return new MinimalVehicleQueryResponse();
    }

    /**
     * Create an instance of {@link X31A7Request }
     * 
     */
    public X31A7Request createX31A7Request() {
        return new X31A7Request();
    }

    /**
     * Create an instance of {@link QueryAccidentsResponse }
     * 
     */
    public QueryAccidentsResponse createQueryAccidentsResponse() {
        return new QueryAccidentsResponse();
    }

    /**
     * Create an instance of {@link X3141Request }
     * 
     */
    public X3141Request createX3141Request() {
        return new X3141Request();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryResponse }
     * 
     */
    public GenericVehicleQueryResponse createGenericVehicleQueryResponse() {
        return new GenericVehicleQueryResponse();
    }

    /**
     * Create an instance of {@link X3067Request }
     * 
     */
    public X3067Request createX3067Request() {
        return new X3067Request();
    }

    /**
     * Create an instance of {@link X3039Response }
     * 
     */
    public X3039Response createX3039Response() {
        return new X3039Response();
    }

    /**
     * Create an instance of {@link ConfirmVehicleDetailsResponse }
     * 
     */
    public ConfirmVehicleDetailsResponse createConfirmVehicleDetailsResponse() {
        return new ConfirmVehicleDetailsResponse();
    }

    /**
     * Create an instance of {@link ConfirmVehicleDetailsRequest }
     * 
     */
    public ConfirmVehicleDetailsRequest createConfirmVehicleDetailsRequest() {
        return new ConfirmVehicleDetailsRequest();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryRequest }
     * 
     */
    public GenericVehicleQueryRequest createGenericVehicleQueryRequest() {
        return new GenericVehicleQueryRequest();
    }

    /**
     * Create an instance of {@link ChangeOfTitleHolderRequest }
     * 
     */
    public ChangeOfTitleHolderRequest createChangeOfTitleHolderRequest() {
        return new ChangeOfTitleHolderRequest();
    }

    /**
     * Create an instance of {@link QueryInfringementsForWebsiteRequest }
     * 
     */
    public QueryInfringementsForWebsiteRequest createQueryInfringementsForWebsiteRequest() {
        return new QueryInfringementsForWebsiteRequest();
    }

    /**
     * Create an instance of {@link X314ARequest }
     * 
     */
    public X314ARequest createX314ARequest() {
        return new X314ARequest();
    }

    /**
     * Create an instance of {@link X3026Response }
     * 
     */
    public X3026Response createX3026Response() {
        return new X3026Response();
    }

    /**
     * Create an instance of {@link InfringementQueryRequest }
     * 
     */
    public InfringementQueryRequest createInfringementQueryRequest() {
        return new InfringementQueryRequest();
    }

    /**
     * Create an instance of {@link GetPostOfficeDestinationsResponse }
     * 
     */
    public GetPostOfficeDestinationsResponse createGetPostOfficeDestinationsResponse() {
        return new GetPostOfficeDestinationsResponse();
    }

    /**
     * Create an instance of {@link X3028Response }
     * 
     */
    public X3028Response createX3028Response() {
        return new X3028Response();
    }

    /**
     * Create an instance of {@link X3050Infringement }
     * 
     */
    public X3050Infringement createX3050Infringement() {
        return new X3050Infringement();
    }

    /**
     * Create an instance of {@link X3050Fees }
     * 
     */
    public X3050Fees createX3050Fees() {
        return new X3050Fees();
    }

    /**
     * Create an instance of {@link FeesElement }
     * 
     */
    public FeesElement createFeesElement() {
        return new FeesElement();
    }

    /**
     * Create an instance of {@link X3028Response.Infringement }
     * 
     */
    public X3028Response.Infringement createX3028ResponseInfringement() {
        return new X3028Response.Infringement();
    }

    /**
     * Create an instance of {@link X3028Response.Infringement.FeesList }
     * 
     */
    public X3028Response.Infringement.FeesList createX3028ResponseInfringementFeesList() {
        return new X3028Response.Infringement.FeesList();
    }

    /**
     * Create an instance of {@link GetPostOfficeDestinationsResponse.Suburb }
     * 
     */
    public GetPostOfficeDestinationsResponse.Suburb createGetPostOfficeDestinationsResponseSuburb() {
        return new GetPostOfficeDestinationsResponse.Suburb();
    }

    /**
     * Create an instance of {@link X314ASuccessResult }
     * 
     */
    public X314ASuccessResult createX314ASuccessResult() {
        return new X314ASuccessResult();
    }

    /**
     * Create an instance of {@link X314ASuccessResult.RC2 }
     * 
     */
    public X314ASuccessResult.RC2 createX314ASuccessResultRC2() {
        return new X314ASuccessResult.RC2();
    }

    /**
     * Create an instance of {@link InfringementQueryRequest.Filter }
     * 
     */
    public InfringementQueryRequest.Filter createInfringementQueryRequestFilter() {
        return new InfringementQueryRequest.Filter();
    }

    /**
     * Create an instance of {@link X3026Response.Infringement }
     * 
     */
    public X3026Response.Infringement createX3026ResponseInfringement() {
        return new X3026Response.Infringement();
    }

    /**
     * Create an instance of {@link X314ARequest.ReceiverDetails }
     * 
     */
    public X314ARequest.ReceiverDetails createX314ARequestReceiverDetails() {
        return new X314ARequest.ReceiverDetails();
    }

    /**
     * Create an instance of {@link SapQueryResult }
     * 
     */
    public SapQueryResult createSapQueryResult() {
        return new SapQueryResult();
    }

    /**
     * Create an instance of {@link SapQueryResult.Vehicle }
     * 
     */
    public SapQueryResult.Vehicle createSapQueryResultVehicle() {
        return new SapQueryResult.Vehicle();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryResponse.Vehicle }
     * 
     */
    public GenericVehicleQueryResponse.Vehicle createGenericVehicleQueryResponseVehicle() {
        return new GenericVehicleQueryResponse.Vehicle();
    }

    /**
     * Create an instance of {@link SearchOptionInfringerType }
     * 
     */
    public SearchOptionInfringerType createSearchOptionInfringerType() {
        return new SearchOptionInfringerType();
    }

    /**
     * Create an instance of {@link InfringementElement }
     * 
     */
    public InfringementElement createInfringementElement() {
        return new InfringementElement();
    }

    /**
     * Create an instance of {@link X3141SucessResult }
     * 
     */
    public X3141SucessResult createX3141SucessResult() {
        return new X3141SucessResult();
    }

    /**
     * Create an instance of {@link X3050Request.Filter }
     * 
     */
    public X3050Request.Filter createX3050RequestFilter() {
        return new X3050Request.Filter();
    }

    /**
     * Create an instance of {@link QueryVehicleDetails }
     * 
     */
    public QueryVehicleDetails createQueryVehicleDetails() {
        return new QueryVehicleDetails();
    }

    /**
     * Create an instance of {@link X3003ResponseDetail }
     * 
     */
    public X3003ResponseDetail createX3003ResponseDetail() {
        return new X3003ResponseDetail();
    }

    /**
     * Create an instance of {@link X3004ResponseDetail }
     * 
     */
    public X3004ResponseDetail createX3004ResponseDetail() {
        return new X3004ResponseDetail();
    }

    /**
     * Create an instance of {@link MaintainPasswordRequest }
     * 
     */
    public MaintainPasswordRequest createMaintainPasswordRequest() {
        return new MaintainPasswordRequest();
    }

    /**
     * Create an instance of {@link GetVehicleLicenseRenewalInformationRequest }
     * 
     */
    public GetVehicleLicenseRenewalInformationRequest createGetVehicleLicenseRenewalInformationRequest() {
        return new GetVehicleLicenseRenewalInformationRequest();
    }

    /**
     * Create an instance of {@link RetrieveDocRequest }
     * 
     */
    public RetrieveDocRequest createRetrieveDocRequest() {
        return new RetrieveDocRequest();
    }

    /**
     * Create an instance of {@link InfringementForNominationResponse }
     * 
     */
    public InfringementForNominationResponse createInfringementForNominationResponse() {
        return new InfringementForNominationResponse();
    }

    /**
     * Create an instance of {@link ExecutionResult }
     * 
     */
    public ExecutionResult createExecutionResult() {
        return new ExecutionResult();
    }

    /**
     * Create an instance of {@link InfringementForNominationResponseDetail }
     * 
     */
    public InfringementForNominationResponseDetail createInfringementForNominationResponseDetail() {
        return new InfringementForNominationResponseDetail();
    }

    /**
     * Create an instance of {@link X3029Request.Payment }
     * 
     */
    public X3029Request.Payment createX3029RequestPayment() {
        return new X3029Request.Payment();
    }

    /**
     * Create an instance of {@link X3004Response }
     * 
     */
    public X3004Response createX3004Response() {
        return new X3004Response();
    }

    /**
     * Create an instance of {@link X3003Response }
     * 
     */
    public X3003Response createX3003Response() {
        return new X3003Response();
    }

    /**
     * Create an instance of {@link OnlinePaymentReceiptRequest }
     * 
     */
    public OnlinePaymentReceiptRequest createOnlinePaymentReceiptRequest() {
        return new OnlinePaymentReceiptRequest();
    }

    /**
     * Create an instance of {@link X31A7Response }
     * 
     */
    public X31A7Response createX31A7Response() {
        return new X31A7Response();
    }

    /**
     * Create an instance of {@link InformationMessage }
     * 
     */
    public InformationMessage createInformationMessage() {
        return new InformationMessage();
    }

    /**
     * Create an instance of {@link X31A7Result }
     * 
     */
    public X31A7Result createX31A7Result() {
        return new X31A7Result();
    }

    /**
     * Create an instance of {@link TestPreBookingScheduleAppointmentResponse }
     * 
     */
    public TestPreBookingScheduleAppointmentResponse createTestPreBookingScheduleAppointmentResponse() {
        return new TestPreBookingScheduleAppointmentResponse();
    }

    /**
     * Create an instance of {@link TestPreBookingScheduleAppointmentResponseDetail }
     * 
     */
    public TestPreBookingScheduleAppointmentResponseDetail createTestPreBookingScheduleAppointmentResponseDetail() {
        return new TestPreBookingScheduleAppointmentResponseDetail();
    }

    /**
     * Create an instance of {@link X3048Request.Infringement }
     * 
     */
    public X3048Request.Infringement createX3048RequestInfringement() {
        return new X3048Request.Infringement();
    }

    /**
     * Create an instance of {@link X3048Request.Charge }
     * 
     */
    public X3048Request.Charge createX3048RequestCharge() {
        return new X3048Request.Charge();
    }

    /**
     * Create an instance of {@link X3048Request.Vehicle }
     * 
     */
    public X3048Request.Vehicle createX3048RequestVehicle() {
        return new X3048Request.Vehicle();
    }

    /**
     * Create an instance of {@link X3048Request.Officer }
     * 
     */
    public X3048Request.Officer createX3048RequestOfficer() {
        return new X3048Request.Officer();
    }

    /**
     * Create an instance of {@link GetInfringementForInstalmentApplicationRequest }
     * 
     */
    public GetInfringementForInstalmentApplicationRequest createGetInfringementForInstalmentApplicationRequest() {
        return new GetInfringementForInstalmentApplicationRequest();
    }

    /**
     * Create an instance of {@link X3004Request.Person }
     * 
     */
    public X3004Request.Person createX3004RequestPerson() {
        return new X3004Request.Person();
    }

    /**
     * Create an instance of {@link X3004Request.Vehicle }
     * 
     */
    public X3004Request.Vehicle createX3004RequestVehicle() {
        return new X3004Request.Vehicle();
    }

    /**
     * Create an instance of {@link X3050Request.Query }
     * 
     */
    public X3050Request.Query createX3050RequestQuery() {
        return new X3050Request.Query();
    }

    /**
     * Create an instance of {@link X3050Request.QueryIssuingAuthority }
     * 
     */
    public X3050Request.QueryIssuingAuthority createX3050RequestQueryIssuingAuthority() {
        return new X3050Request.QueryIssuingAuthority();
    }

    /**
     * Create an instance of {@link X3050Request.QueryServiceProvider }
     * 
     */
    public X3050Request.QueryServiceProvider createX3050RequestQueryServiceProvider() {
        return new X3050Request.QueryServiceProvider();
    }

    /**
     * Create an instance of {@link QueryTaskManagementSystemResponse }
     * 
     */
    public QueryTaskManagementSystemResponse createQueryTaskManagementSystemResponse() {
        return new QueryTaskManagementSystemResponse();
    }

    /**
     * Create an instance of {@link E3024Task }
     * 
     */
    public E3024Task createE3024Task() {
        return new E3024Task();
    }

    /**
     * Create an instance of {@link TestPreBookingAvailabilityResponse }
     * 
     */
    public TestPreBookingAvailabilityResponse createTestPreBookingAvailabilityResponse() {
        return new TestPreBookingAvailabilityResponse();
    }

    /**
     * Create an instance of {@link TestPreBookingAvailabilityResponseDetail }
     * 
     */
    public TestPreBookingAvailabilityResponseDetail createTestPreBookingAvailabilityResponseDetail() {
        return new TestPreBookingAvailabilityResponseDetail();
    }

    /**
     * Create an instance of {@link X3141Response }
     * 
     */
    public X3141Response createX3141Response() {
        return new X3141Response();
    }

    /**
     * Create an instance of {@link UpdatePreferedAddressResponse }
     * 
     */
    public UpdatePreferedAddressResponse createUpdatePreferedAddressResponse() {
        return new UpdatePreferedAddressResponse();
    }

    /**
     * Create an instance of {@link NominationOfDriverRequest }
     * 
     */
    public NominationOfDriverRequest createNominationOfDriverRequest() {
        return new NominationOfDriverRequest();
    }

    /**
     * Create an instance of {@link FleetInfringementsResponse }
     * 
     */
    public FleetInfringementsResponse createFleetInfringementsResponse() {
        return new FleetInfringementsResponse();
    }

    /**
     * Create an instance of {@link InfringementType }
     * 
     */
    public InfringementType createInfringementType() {
        return new InfringementType();
    }

    /**
     * Create an instance of {@link X3027Response.Infringement }
     * 
     */
    public X3027Response.Infringement createX3027ResponseInfringement() {
        return new X3027Response.Infringement();
    }

    /**
     * Create an instance of {@link MinimalVehicleQueryResponse.Vehicle }
     * 
     */
    public MinimalVehicleQueryResponse.Vehicle createMinimalVehicleQueryResponseVehicle() {
        return new MinimalVehicleQueryResponse.Vehicle();
    }

    /**
     * Create an instance of {@link MinimalVehicleQueryResponse.Owner }
     * 
     */
    public MinimalVehicleQueryResponse.Owner createMinimalVehicleQueryResponseOwner() {
        return new MinimalVehicleQueryResponse.Owner();
    }

    /**
     * Create an instance of {@link IsFleetUserResponse }
     * 
     */
    public IsFleetUserResponse createIsFleetUserResponse() {
        return new IsFleetUserResponse();
    }

    /**
     * Create an instance of {@link ChangeOfTitleHolderResponse }
     * 
     */
    public ChangeOfTitleHolderResponse createChangeOfTitleHolderResponse() {
        return new ChangeOfTitleHolderResponse();
    }

    /**
     * Create an instance of {@link InfringementForNominationRequest }
     * 
     */
    public InfringementForNominationRequest createInfringementForNominationRequest() {
        return new InfringementForNominationRequest();
    }

    /**
     * Create an instance of {@link OptForCourtRequest }
     * 
     */
    public OptForCourtRequest createOptForCourtRequest() {
        return new OptForCourtRequest();
    }

    /**
     * Create an instance of {@link PersonAddress }
     * 
     */
    public PersonAddress createPersonAddress() {
        return new PersonAddress();
    }

    /**
     * Create an instance of {@link X3003Request }
     * 
     */
    public X3003Request createX3003Request() {
        return new X3003Request();
    }

    /**
     * Create an instance of {@link QueryTaskManagementSystemRequest }
     * 
     */
    public QueryTaskManagementSystemRequest createQueryTaskManagementSystemRequest() {
        return new QueryTaskManagementSystemRequest();
    }

    /**
     * Create an instance of {@link E3024SearchOptionType }
     * 
     */
    public E3024SearchOptionType createE3024SearchOptionType() {
        return new E3024SearchOptionType();
    }

    /**
     * Create an instance of {@link X31A7Request.Owner }
     * 
     */
    public X31A7Request.Owner createX31A7RequestOwner() {
        return new X31A7Request.Owner();
    }

    /**
     * Create an instance of {@link X31A7Request.OwnerProxy }
     * 
     */
    public X31A7Request.OwnerProxy createX31A7RequestOwnerProxy() {
        return new X31A7Request.OwnerProxy();
    }

    /**
     * Create an instance of {@link X31A7Request.OwnerRepresentative }
     * 
     */
    public X31A7Request.OwnerRepresentative createX31A7RequestOwnerRepresentative() {
        return new X31A7Request.OwnerRepresentative();
    }

    /**
     * Create an instance of {@link X31A7Request.Vehicle }
     * 
     */
    public X31A7Request.Vehicle createX31A7RequestVehicle() {
        return new X31A7Request.Vehicle();
    }

    /**
     * Create an instance of {@link Transaction915DUpdateResponse }
     * 
     */
    public Transaction915DUpdateResponse createTransaction915DUpdateResponse() {
        return new Transaction915DUpdateResponse();
    }

    /**
     * Create an instance of {@link GetProvincesResponse }
     * 
     */
    public GetProvincesResponse createGetProvincesResponse() {
        return new GetProvincesResponse();
    }

    /**
     * Create an instance of {@link ProvinceElement }
     * 
     */
    public ProvinceElement createProvinceElement() {
        return new ProvinceElement();
    }

    /**
     * Create an instance of {@link QueryPersonToNominateResponse }
     * 
     */
    public QueryPersonToNominateResponse createQueryPersonToNominateResponse() {
        return new QueryPersonToNominateResponse();
    }

    /**
     * Create an instance of {@link QueryPersonToNominateResponseDetail }
     * 
     */
    public QueryPersonToNominateResponseDetail createQueryPersonToNominateResponseDetail() {
        return new QueryPersonToNominateResponseDetail();
    }

    /**
     * Create an instance of {@link X3050Response }
     * 
     */
    public X3050Response createX3050Response() {
        return new X3050Response();
    }

    /**
     * Create an instance of {@link X3050ResponseDetail }
     * 
     */
    public X3050ResponseDetail createX3050ResponseDetail() {
        return new X3050ResponseDetail();
    }

    /**
     * Create an instance of {@link RenewVehicleLicenseRequest }
     * 
     */
    public RenewVehicleLicenseRequest createRenewVehicleLicenseRequest() {
        return new RenewVehicleLicenseRequest();
    }

    /**
     * Create an instance of {@link X5009PersonAddress }
     * 
     */
    public X5009PersonAddress createX5009PersonAddress() {
        return new X5009PersonAddress();
    }

    /**
     * Create an instance of {@link IsFleetUserRequest }
     * 
     */
    public IsFleetUserRequest createIsFleetUserRequest() {
        return new IsFleetUserRequest();
    }

    /**
     * Create an instance of {@link GetInfringementImagesRequest }
     * 
     */
    public GetInfringementImagesRequest createGetInfringementImagesRequest() {
        return new GetInfringementImagesRequest();
    }

    /**
     * Create an instance of {@link X3061Response }
     * 
     */
    public X3061Response createX3061Response() {
        return new X3061Response();
    }

    /**
     * Create an instance of {@link X3061ErrorType }
     * 
     */
    public X3061ErrorType createX3061ErrorType() {
        return new X3061ErrorType();
    }

    /**
     * Create an instance of {@link QueryAccidentsResponse.Accident }
     * 
     */
    public QueryAccidentsResponse.Accident createQueryAccidentsResponseAccident() {
        return new QueryAccidentsResponse.Accident();
    }

    /**
     * Create an instance of {@link ApplyForRepresentationRequest }
     * 
     */
    public ApplyForRepresentationRequest createApplyForRepresentationRequest() {
        return new ApplyForRepresentationRequest();
    }

    /**
     * Create an instance of {@link QueryInfringementsForWebsiteResponse }
     * 
     */
    public QueryInfringementsForWebsiteResponse createQueryInfringementsForWebsiteResponse() {
        return new QueryInfringementsForWebsiteResponse();
    }

    /**
     * Create an instance of {@link MinimalInfringementType }
     * 
     */
    public MinimalInfringementType createMinimalInfringementType() {
        return new MinimalInfringementType();
    }

    /**
     * Create an instance of {@link QueryPersonToNominateRequest }
     * 
     */
    public QueryPersonToNominateRequest createQueryPersonToNominateRequest() {
        return new QueryPersonToNominateRequest();
    }

    /**
     * Create an instance of {@link QueryPaymentStatusResponse }
     * 
     */
    public QueryPaymentStatusResponse createQueryPaymentStatusResponse() {
        return new QueryPaymentStatusResponse();
    }

    /**
     * Create an instance of {@link InfringementQueryResponse }
     * 
     */
    public InfringementQueryResponse createInfringementQueryResponse() {
        return new InfringementQueryResponse();
    }

    /**
     * Create an instance of {@link GetInfringementForRepresentationResponse }
     * 
     */
    public GetInfringementForRepresentationResponse createGetInfringementForRepresentationResponse() {
        return new GetInfringementForRepresentationResponse();
    }

    /**
     * Create an instance of {@link GetOnlinePaymentGatewayConfigurationRequest }
     * 
     */
    public GetOnlinePaymentGatewayConfigurationRequest createGetOnlinePaymentGatewayConfigurationRequest() {
        return new GetOnlinePaymentGatewayConfigurationRequest();
    }

    /**
     * Create an instance of {@link QueryDltcRequest }
     * 
     */
    public QueryDltcRequest createQueryDltcRequest() {
        return new QueryDltcRequest();
    }

    /**
     * Create an instance of {@link X3028Request }
     * 
     */
    public X3028Request createX3028Request() {
        return new X3028Request();
    }

    /**
     * Create an instance of {@link SearchOptionInfringementType }
     * 
     */
    public SearchOptionInfringementType createSearchOptionInfringementType() {
        return new SearchOptionInfringementType();
    }

    /**
     * Create an instance of {@link GetVehiclesAndLicenseExpiryDatesResponse }
     * 
     */
    public GetVehiclesAndLicenseExpiryDatesResponse createGetVehiclesAndLicenseExpiryDatesResponse() {
        return new GetVehiclesAndLicenseExpiryDatesResponse();
    }

    /**
     * Create an instance of {@link X5008VehicleDetail }
     * 
     */
    public X5008VehicleDetail createX5008VehicleDetail() {
        return new X5008VehicleDetail();
    }

    /**
     * Create an instance of {@link ThirdpartyInformationMessage }
     * 
     */
    public ThirdpartyInformationMessage createThirdpartyInformationMessage() {
        return new ThirdpartyInformationMessage();
    }

    /**
     * Create an instance of {@link X3141PersonIdentification }
     * 
     */
    public X3141PersonIdentification createX3141PersonIdentification() {
        return new X3141PersonIdentification();
    }

    /**
     * Create an instance of {@link X3141Request.Vehicle }
     * 
     */
    public X3141Request.Vehicle createX3141RequestVehicle() {
        return new X3141Request.Vehicle();
    }

    /**
     * Create an instance of {@link GetLicenseRenewalQuoteResponse }
     * 
     */
    public GetLicenseRenewalQuoteResponse createGetLicenseRenewalQuoteResponse() {
        return new GetLicenseRenewalQuoteResponse();
    }

    /**
     * Create an instance of {@link X5008LicenceRenewalFeeAssessment }
     * 
     */
    public X5008LicenceRenewalFeeAssessment createX5008LicenceRenewalFeeAssessment() {
        return new X5008LicenceRenewalFeeAssessment();
    }

    /**
     * Create an instance of {@link X5008PersonElement }
     * 
     */
    public X5008PersonElement createX5008PersonElement() {
        return new X5008PersonElement();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryResponse.Owner }
     * 
     */
    public GenericVehicleQueryResponse.Owner createGenericVehicleQueryResponseOwner() {
        return new GenericVehicleQueryResponse.Owner();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryResponse.TitleHolder }
     * 
     */
    public GenericVehicleQueryResponse.TitleHolder createGenericVehicleQueryResponseTitleHolder() {
        return new GenericVehicleQueryResponse.TitleHolder();
    }

    /**
     * Create an instance of {@link SAPVehicleQueryResponse }
     * 
     */
    public SAPVehicleQueryResponse createSAPVehicleQueryResponse() {
        return new SAPVehicleQueryResponse();
    }

    /**
     * Create an instance of {@link InstalmentAccountRequest }
     * 
     */
    public InstalmentAccountRequest createInstalmentAccountRequest() {
        return new InstalmentAccountRequest();
    }

    /**
     * Create an instance of {@link X3061Request }
     * 
     */
    public X3061Request createX3061Request() {
        return new X3061Request();
    }

    /**
     * Create an instance of {@link X3061VehicleType }
     * 
     */
    public X3061VehicleType createX3061VehicleType() {
        return new X3061VehicleType();
    }

    /**
     * Create an instance of {@link X3061InfringerType }
     * 
     */
    public X3061InfringerType createX3061InfringerType() {
        return new X3061InfringerType();
    }

    /**
     * Create an instance of {@link X3061IssuingAuthorityType }
     * 
     */
    public X3061IssuingAuthorityType createX3061IssuingAuthorityType() {
        return new X3061IssuingAuthorityType();
    }

    /**
     * Create an instance of {@link X3061ContraventionLocationType }
     * 
     */
    public X3061ContraventionLocationType createX3061ContraventionLocationType() {
        return new X3061ContraventionLocationType();
    }

    /**
     * Create an instance of {@link X3061ContraventionType }
     * 
     */
    public X3061ContraventionType createX3061ContraventionType() {
        return new X3061ContraventionType();
    }

    /**
     * Create an instance of {@link CreateOnlinePaymentResponse }
     * 
     */
    public CreateOnlinePaymentResponse createCreateOnlinePaymentResponse() {
        return new CreateOnlinePaymentResponse();
    }

    /**
     * Create an instance of {@link X3026Request }
     * 
     */
    public X3026Request createX3026Request() {
        return new X3026Request();
    }

    /**
     * Create an instance of {@link X3026SearchOptionType }
     * 
     */
    public X3026SearchOptionType createX3026SearchOptionType() {
        return new X3026SearchOptionType();
    }

    /**
     * Create an instance of {@link GetInfringementToOptForCourtResponse }
     * 
     */
    public GetInfringementToOptForCourtResponse createGetInfringementToOptForCourtResponse() {
        return new GetInfringementToOptForCourtResponse();
    }

    /**
     * Create an instance of {@link X3067Request.VehicleQuery }
     * 
     */
    public X3067Request.VehicleQuery createX3067RequestVehicleQuery() {
        return new X3067Request.VehicleQuery();
    }

    /**
     * Create an instance of {@link X3039Response.Response }
     * 
     */
    public X3039Response.Response createX3039ResponseResponse() {
        return new X3039Response.Response();
    }

    /**
     * Create an instance of {@link InstalmentApplicationResponse }
     * 
     */
    public InstalmentApplicationResponse createInstalmentApplicationResponse() {
        return new InstalmentApplicationResponse();
    }

    /**
     * Create an instance of {@link QueryDltcResponse }
     * 
     */
    public QueryDltcResponse createQueryDltcResponse() {
        return new QueryDltcResponse();
    }

    /**
     * Create an instance of {@link E0706DltcInfo }
     * 
     */
    public E0706DltcInfo createE0706DltcInfo() {
        return new E0706DltcInfo();
    }

    /**
     * Create an instance of {@link ConfirmVehicleDetailsResponse.TitleHolder }
     * 
     */
    public ConfirmVehicleDetailsResponse.TitleHolder createConfirmVehicleDetailsResponseTitleHolder() {
        return new ConfirmVehicleDetailsResponse.TitleHolder();
    }

    /**
     * Create an instance of {@link ConfirmVehicleDetailsResponse.Vehicle }
     * 
     */
    public ConfirmVehicleDetailsResponse.Vehicle createConfirmVehicleDetailsResponseVehicle() {
        return new ConfirmVehicleDetailsResponse.Vehicle();
    }

    /**
     * Create an instance of {@link OnlinePaymentReceiptResponse }
     * 
     */
    public OnlinePaymentReceiptResponse createOnlinePaymentReceiptResponse() {
        return new OnlinePaymentReceiptResponse();
    }

    /**
     * Create an instance of {@link GetVehicleLicenseRenewalInformationResponse }
     * 
     */
    public GetVehicleLicenseRenewalInformationResponse createGetVehicleLicenseRenewalInformationResponse() {
        return new GetVehicleLicenseRenewalInformationResponse();
    }

    /**
     * Create an instance of {@link VehicleElement }
     * 
     */
    public VehicleElement createVehicleElement() {
        return new VehicleElement();
    }

    /**
     * Create an instance of {@link VehicleLicenceRenewalAmounts }
     * 
     */
    public VehicleLicenceRenewalAmounts createVehicleLicenceRenewalAmounts() {
        return new VehicleLicenceRenewalAmounts();
    }

    /**
     * Create an instance of {@link TestPreBookingAvailabilityRequest }
     * 
     */
    public TestPreBookingAvailabilityRequest createTestPreBookingAvailabilityRequest() {
        return new TestPreBookingAvailabilityRequest();
    }

    /**
     * Create an instance of {@link DltcId }
     * 
     */
    public DltcId createDltcId() {
        return new DltcId();
    }

    /**
     * Create an instance of {@link ApplyForRevocationResponse }
     * 
     */
    public ApplyForRevocationResponse createApplyForRevocationResponse() {
        return new ApplyForRevocationResponse();
    }

    /**
     * Create an instance of {@link X3903Response }
     * 
     */
    public X3903Response createX3903Response() {
        return new X3903Response();
    }

    /**
     * Create an instance of {@link GetProvincesRequest }
     * 
     */
    public GetProvincesRequest createGetProvincesRequest() {
        return new GetProvincesRequest();
    }

    /**
     * Create an instance of {@link QueryInfringementDetailForWebsiteResponse }
     * 
     */
    public QueryInfringementDetailForWebsiteResponse createQueryInfringementDetailForWebsiteResponse() {
        return new QueryInfringementDetailForWebsiteResponse();
    }

    /**
     * Create an instance of {@link ConfirmVehicleDetailsRequest.Person }
     * 
     */
    public ConfirmVehicleDetailsRequest.Person createConfirmVehicleDetailsRequestPerson() {
        return new ConfirmVehicleDetailsRequest.Person();
    }

    /**
     * Create an instance of {@link ConfirmVehicleDetailsRequest.Vehicle }
     * 
     */
    public ConfirmVehicleDetailsRequest.Vehicle createConfirmVehicleDetailsRequestVehicle() {
        return new ConfirmVehicleDetailsRequest.Vehicle();
    }

    /**
     * Create an instance of {@link QueryAccidentsRequest }
     * 
     */
    public QueryAccidentsRequest createQueryAccidentsRequest() {
        return new QueryAccidentsRequest();
    }

    /**
     * Create an instance of {@link QueryDriverQualificationStatusRequest }
     * 
     */
    public QueryDriverQualificationStatusRequest createQueryDriverQualificationStatusRequest() {
        return new QueryDriverQualificationStatusRequest();
    }

    /**
     * Create an instance of {@link InstalmentAccountResponse }
     * 
     */
    public InstalmentAccountResponse createInstalmentAccountResponse() {
        return new InstalmentAccountResponse();
    }

    /**
     * Create an instance of {@link InstalAccountType }
     * 
     */
    public InstalAccountType createInstalAccountType() {
        return new InstalAccountType();
    }

    /**
     * Create an instance of {@link GetLicenseRenewalQuoteRequest }
     * 
     */
    public GetLicenseRenewalQuoteRequest createGetLicenseRenewalQuoteRequest() {
        return new GetLicenseRenewalQuoteRequest();
    }

    /**
     * Create an instance of {@link QueryPaymentStatusRequest }
     * 
     */
    public QueryPaymentStatusRequest createQueryPaymentStatusRequest() {
        return new QueryPaymentStatusRequest();
    }

    /**
     * Create an instance of {@link OptForCourtResponse }
     * 
     */
    public OptForCourtResponse createOptForCourtResponse() {
        return new OptForCourtResponse();
    }

    /**
     * Create an instance of {@link ProcessOnlinePaymentAuthorizationResponse }
     * 
     */
    public ProcessOnlinePaymentAuthorizationResponse createProcessOnlinePaymentAuthorizationResponse() {
        return new ProcessOnlinePaymentAuthorizationResponse();
    }

    /**
     * Create an instance of {@link X3027Request }
     * 
     */
    public X3027Request createX3027Request() {
        return new X3027Request();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryRequest.VehicleQuery }
     * 
     */
    public GenericVehicleQueryRequest.VehicleQuery createGenericVehicleQueryRequestVehicleQuery() {
        return new GenericVehicleQueryRequest.VehicleQuery();
    }

    /**
     * Create an instance of {@link X3903Request }
     * 
     */
    public X3903Request createX3903Request() {
        return new X3903Request();
    }

    /**
     * Create an instance of {@link X3903Document }
     * 
     */
    public X3903Document createX3903Document() {
        return new X3903Document();
    }

    /**
     * Create an instance of {@link tasima.common.ws.schema.Person }
     * 
     */
    public tasima.common.ws.schema.Person createPerson() {
        return new tasima.common.ws.schema.Person();
    }

    /**
     * Create an instance of {@link ChangeOfTitleHolderRequest.Vehicle }
     * 
     */
    public ChangeOfTitleHolderRequest.Vehicle createChangeOfTitleHolderRequestVehicle() {
        return new ChangeOfTitleHolderRequest.Vehicle();
    }

    /**
     * Create an instance of {@link GetInfringementForInstalmentApplicationResponse }
     * 
     */
    public GetInfringementForInstalmentApplicationResponse createGetInfringementForInstalmentApplicationResponse() {
        return new GetInfringementForInstalmentApplicationResponse();
    }

    /**
     * Create an instance of {@link FleetInfringementsRequest }
     * 
     */
    public FleetInfringementsRequest createFleetInfringementsRequest() {
        return new FleetInfringementsRequest();
    }

    /**
     * Create an instance of {@link GetInfringementToOptForCourtRequest }
     * 
     */
    public GetInfringementToOptForCourtRequest createGetInfringementToOptForCourtRequest() {
        return new GetInfringementToOptForCourtRequest();
    }

    /**
     * Create an instance of {@link QueryInfringementsForWebsiteRequest.Infringer }
     * 
     */
    public QueryInfringementsForWebsiteRequest.Infringer createQueryInfringementsForWebsiteRequestInfringer() {
        return new QueryInfringementsForWebsiteRequest.Infringer();
    }

    /**
     * Create an instance of {@link GetInfringementForRepresentationRequest }
     * 
     */
    public GetInfringementForRepresentationRequest createGetInfringementForRepresentationRequest() {
        return new GetInfringementForRepresentationRequest();
    }

    /**
     * Create an instance of {@link PlnAvailabilityResponse }
     * 
     */
    public PlnAvailabilityResponse createPlnAvailabilityResponse() {
        return new PlnAvailabilityResponse();
    }

    /**
     * Create an instance of {@link X3039Request }
     * 
     */
    public X3039Request createX3039Request() {
        return new X3039Request();
    }

    /**
     * Create an instance of {@link PlnAvailabilityRequest }
     * 
     */
    public PlnAvailabilityRequest createPlnAvailabilityRequest() {
        return new PlnAvailabilityRequest();
    }

    /**
     * Create an instance of {@link ProvincialPersonalisedLicenceNumberElement }
     * 
     */
    public ProvincialPersonalisedLicenceNumberElement createProvincialPersonalisedLicenceNumberElement() {
        return new ProvincialPersonalisedLicenceNumberElement();
    }

    /**
     * Create an instance of {@link SpecificLicenceNumberElement }
     * 
     */
    public SpecificLicenceNumberElement createSpecificLicenceNumberElement() {
        return new SpecificLicenceNumberElement();
    }

    /**
     * Create an instance of {@link X314ARequest.Receiver }
     * 
     */
    public X314ARequest.Receiver createX314ARequestReceiver() {
        return new X314ARequest.Receiver();
    }

    /**
     * Create an instance of {@link X314ARequest.Vehicle }
     * 
     */
    public X314ARequest.Vehicle createX314ARequestVehicle() {
        return new X314ARequest.Vehicle();
    }

    /**
     * Create an instance of {@link CreateOnlinePaymentRequest }
     * 
     */
    public CreateOnlinePaymentRequest createCreateOnlinePaymentRequest() {
        return new CreateOnlinePaymentRequest();
    }

    /**
     * Create an instance of {@link UpdatePreferedAddressRequest }
     * 
     */
    public UpdatePreferedAddressRequest createUpdatePreferedAddressRequest() {
        return new UpdatePreferedAddressRequest();
    }

    /**
     * Create an instance of {@link BusinessFault }
     * 
     */
    public BusinessFault createBusinessFault() {
        return new BusinessFault();
    }

    /**
     * Create an instance of {@link QueryInfringementDetailForWebsiteRequest }
     * 
     */
    public QueryInfringementDetailForWebsiteRequest createQueryInfringementDetailForWebsiteRequest() {
        return new QueryInfringementDetailForWebsiteRequest();
    }

    /**
     * Create an instance of {@link GetInfringementForRevocationResponse }
     * 
     */
    public GetInfringementForRevocationResponse createGetInfringementForRevocationResponse() {
        return new GetInfringementForRevocationResponse();
    }

    /**
     * Create an instance of {@link QueryDriverQualificationStatusResponse }
     * 
     */
    public QueryDriverQualificationStatusResponse createQueryDriverQualificationStatusResponse() {
        return new QueryDriverQualificationStatusResponse();
    }

    /**
     * Create an instance of {@link InstalmentApplicationRequest }
     * 
     */
    public InstalmentApplicationRequest createInstalmentApplicationRequest() {
        return new InstalmentApplicationRequest();
    }

    /**
     * Create an instance of {@link BankDetails }
     * 
     */
    public BankDetails createBankDetails() {
        return new BankDetails();
    }

    /**
     * Create an instance of {@link MaintainPasswordResponse }
     * 
     */
    public MaintainPasswordResponse createMaintainPasswordResponse() {
        return new MaintainPasswordResponse();
    }

    /**
     * Create an instance of {@link GetVehiclesAndLicenseExpiryDatesRequest }
     * 
     */
    public GetVehiclesAndLicenseExpiryDatesRequest createGetVehiclesAndLicenseExpiryDatesRequest() {
        return new GetVehiclesAndLicenseExpiryDatesRequest();
    }

    /**
     * Create an instance of {@link LookupQueryResponse }
     * 
     */
    public LookupQueryResponse createLookupQueryResponse() {
        return new LookupQueryResponse();
    }

    /**
     * Create an instance of {@link CodeAndDescriptionElement }
     * 
     */
    public CodeAndDescriptionElement createCodeAndDescriptionElement() {
        return new CodeAndDescriptionElement();
    }

    /**
     * Create an instance of {@link RetrieveDocResponse }
     * 
     */
    public RetrieveDocResponse createRetrieveDocResponse() {
        return new RetrieveDocResponse();
    }

    /**
     * Create an instance of {@link X3029Response }
     * 
     */
    public X3029Response createX3029Response() {
        return new X3029Response();
    }

    /**
     * Create an instance of {@link UploadMicrodotRequest }
     * 
     */
    public UploadMicrodotRequest createUploadMicrodotRequest() {
        return new UploadMicrodotRequest();
    }

    /**
     * Create an instance of {@link GetInfringementImagesResponse }
     * 
     */
    public GetInfringementImagesResponse createGetInfringementImagesResponse() {
        return new GetInfringementImagesResponse();
    }

    /**
     * Create an instance of {@link ProcessOnlinePaymentAuthorizationRequest }
     * 
     */
    public ProcessOnlinePaymentAuthorizationRequest createProcessOnlinePaymentAuthorizationRequest() {
        return new ProcessOnlinePaymentAuthorizationRequest();
    }

    /**
     * Create an instance of {@link MinimalVehicleQueryRequest }
     * 
     */
    public MinimalVehicleQueryRequest createMinimalVehicleQueryRequest() {
        return new MinimalVehicleQueryRequest();
    }

    /**
     * Create an instance of {@link TestPreBookingScheduleAppointmentRequest }
     * 
     */
    public TestPreBookingScheduleAppointmentRequest createTestPreBookingScheduleAppointmentRequest() {
        return new TestPreBookingScheduleAppointmentRequest();
    }

    /**
     * Create an instance of {@link Transaction915DUpdateRequest }
     * 
     */
    public Transaction915DUpdateRequest createTransaction915DUpdateRequest() {
        return new Transaction915DUpdateRequest();
    }

    /**
     * Create an instance of {@link CentralDocumentRepositoryType }
     * 
     */
    public CentralDocumentRepositoryType createCentralDocumentRepositoryType() {
        return new CentralDocumentRepositoryType();
    }

    /**
     * Create an instance of {@link X3048Response }
     * 
     */
    public X3048Response createX3048Response() {
        return new X3048Response();
    }

    /**
     * Create an instance of {@link QueryInfringer }
     * 
     */
    public QueryInfringer createQueryInfringer() {
        return new QueryInfringer();
    }

    /**
     * Create an instance of {@link tasima.common.ws.schema.QueryIssuingAuthority }
     * 
     */
    public tasima.common.ws.schema.QueryIssuingAuthority createQueryIssuingAuthority() {
        return new tasima.common.ws.schema.QueryIssuingAuthority();
    }

    /**
     * Create an instance of {@link tasima.common.ws.schema.QueryServiceProvider }
     * 
     */
    public tasima.common.ws.schema.QueryServiceProvider createQueryServiceProvider() {
        return new tasima.common.ws.schema.QueryServiceProvider();
    }

    /**
     * Create an instance of {@link X314AResponse }
     * 
     */
    public X314AResponse createX314AResponse() {
        return new X314AResponse();
    }

    /**
     * Create an instance of {@link ApplyForRepresentationResponse }
     * 
     */
    public ApplyForRepresentationResponse createApplyForRepresentationResponse() {
        return new ApplyForRepresentationResponse();
    }

    /**
     * Create an instance of {@link SAPVehicleQueryRequest }
     * 
     */
    public SAPVehicleQueryRequest createSAPVehicleQueryRequest() {
        return new SAPVehicleQueryRequest();
    }

    /**
     * Create an instance of {@link X3067Response }
     * 
     */
    public X3067Response createX3067Response() {
        return new X3067Response();
    }

    /**
     * Create an instance of {@link GetInfringementForRevocationRequest }
     * 
     */
    public GetInfringementForRevocationRequest createGetInfringementForRevocationRequest() {
        return new GetInfringementForRevocationRequest();
    }

    /**
     * Create an instance of {@link UploadMicrodotResponse }
     * 
     */
    public UploadMicrodotResponse createUploadMicrodotResponse() {
        return new UploadMicrodotResponse();
    }

    /**
     * Create an instance of {@link ApplyForRevocationRequest }
     * 
     */
    public ApplyForRevocationRequest createApplyForRevocationRequest() {
        return new ApplyForRevocationRequest();
    }

    /**
     * Create an instance of {@link GetOnlinePaymentGatewayConfigurationResponse }
     * 
     */
    public GetOnlinePaymentGatewayConfigurationResponse createGetOnlinePaymentGatewayConfigurationResponse() {
        return new GetOnlinePaymentGatewayConfigurationResponse();
    }

    /**
     * Create an instance of {@link RenewVehicleLicenseResponse }
     * 
     */
    public RenewVehicleLicenseResponse createRenewVehicleLicenseResponse() {
        return new RenewVehicleLicenseResponse();
    }

    /**
     * Create an instance of {@link LookupQueryRequest }
     * 
     */
    public LookupQueryRequest createLookupQueryRequest() {
        return new LookupQueryRequest();
    }

    /**
     * Create an instance of {@link NominationOfDriverResponse }
     * 
     */
    public NominationOfDriverResponse createNominationOfDriverResponse() {
        return new NominationOfDriverResponse();
    }

    /**
     * Create an instance of {@link NominationOfDriverResponseDetail }
     * 
     */
    public NominationOfDriverResponseDetail createNominationOfDriverResponseDetail() {
        return new NominationOfDriverResponseDetail();
    }

    /**
     * Create an instance of {@link GetPostOfficeDestinationsRequest }
     * 
     */
    public GetPostOfficeDestinationsRequest createGetPostOfficeDestinationsRequest() {
        return new GetPostOfficeDestinationsRequest();
    }

    /**
     * Create an instance of {@link X3050Location }
     * 
     */
    public X3050Location createX3050Location() {
        return new X3050Location();
    }

    /**
     * Create an instance of {@link LocationType }
     * 
     */
    public LocationType createLocationType() {
        return new LocationType();
    }

    /**
     * Create an instance of {@link X3050Vehicle }
     * 
     */
    public X3050Vehicle createX3050Vehicle() {
        return new X3050Vehicle();
    }

    /**
     * Create an instance of {@link LandlineNumberElement }
     * 
     */
    public LandlineNumberElement createLandlineNumberElement() {
        return new LandlineNumberElement();
    }

    /**
     * Create an instance of {@link X3028Payment }
     * 
     */
    public X3028Payment createX3028Payment() {
        return new X3028Payment();
    }

    /**
     * Create an instance of {@link TestPreBookingApplicant }
     * 
     */
    public TestPreBookingApplicant createTestPreBookingApplicant() {
        return new TestPreBookingApplicant();
    }

    /**
     * Create an instance of {@link X3026SearchOptionInfringementCaptureDate }
     * 
     */
    public X3026SearchOptionInfringementCaptureDate createX3026SearchOptionInfringementCaptureDate() {
        return new X3026SearchOptionInfringementCaptureDate();
    }

    /**
     * Create an instance of {@link X3061PersonAddress }
     * 
     */
    public X3061PersonAddress createX3061PersonAddress() {
        return new X3061PersonAddress();
    }

    /**
     * Create an instance of {@link X3004CodeDesc }
     * 
     */
    public X3004CodeDesc createX3004CodeDesc() {
        return new X3004CodeDesc();
    }

    /**
     * Create an instance of {@link E3024SearchOptionVinOrChassisNumber }
     * 
     */
    public E3024SearchOptionVinOrChassisNumber createE3024SearchOptionVinOrChassisNumber() {
        return new E3024SearchOptionVinOrChassisNumber();
    }

    /**
     * Create an instance of {@link X3061ChargeType }
     * 
     */
    public X3061ChargeType createX3061ChargeType() {
        return new X3061ChargeType();
    }

    /**
     * Create an instance of {@link X3027OfficerInformation }
     * 
     */
    public X3027OfficerInformation createX3027OfficerInformation() {
        return new X3027OfficerInformation();
    }

    /**
     * Create an instance of {@link X3026SearchOptionInfringer }
     * 
     */
    public X3026SearchOptionInfringer createX3026SearchOptionInfringer() {
        return new X3026SearchOptionInfringer();
    }

    /**
     * Create an instance of {@link X3050CodeDesc }
     * 
     */
    public X3050CodeDesc createX3050CodeDesc() {
        return new X3050CodeDesc();
    }

    /**
     * Create an instance of {@link X3026InfringementCertificate }
     * 
     */
    public X3026InfringementCertificate createX3026InfringementCertificate() {
        return new X3026InfringementCertificate();
    }

    /**
     * Create an instance of {@link X3061LandLineType }
     * 
     */
    public X3061LandLineType createX3061LandLineType() {
        return new X3061LandLineType();
    }

    /**
     * Create an instance of {@link X3027InfringmentVehicleDetails }
     * 
     */
    public X3027InfringmentVehicleDetails createX3027InfringmentVehicleDetails() {
        return new X3027InfringmentVehicleDetails();
    }

    /**
     * Create an instance of {@link E3024TaskParticulars }
     * 
     */
    public E3024TaskParticulars createE3024TaskParticulars() {
        return new E3024TaskParticulars();
    }

    /**
     * Create an instance of {@link ChargeType }
     * 
     */
    public ChargeType createChargeType() {
        return new ChargeType();
    }

    /**
     * Create an instance of {@link X3027InfringementFees }
     * 
     */
    public X3027InfringementFees createX3027InfringementFees() {
        return new X3027InfringementFees();
    }

    /**
     * Create an instance of {@link X3027InfringementCertificate }
     * 
     */
    public X3027InfringementCertificate createX3027InfringementCertificate() {
        return new X3027InfringementCertificate();
    }

    /**
     * Create an instance of {@link PostalAddress }
     * 
     */
    public PostalAddress createPostalAddress() {
        return new PostalAddress();
    }

    /**
     * Create an instance of {@link AartoOptionsType }
     * 
     */
    public AartoOptionsType createAartoOptionsType() {
        return new AartoOptionsType();
    }

    /**
     * Create an instance of {@link X3027InfringerDetails }
     * 
     */
    public X3027InfringerDetails createX3027InfringerDetails() {
        return new X3027InfringerDetails();
    }

    /**
     * Create an instance of {@link GenericInfringementLocationType }
     * 
     */
    public GenericInfringementLocationType createGenericInfringementLocationType() {
        return new GenericInfringementLocationType();
    }

    /**
     * Create an instance of {@link X3027SummonsCertificate }
     * 
     */
    public X3027SummonsCertificate createX3027SummonsCertificate() {
        return new X3027SummonsCertificate();
    }

    /**
     * Create an instance of {@link E3024SearchOptionTaskNumber }
     * 
     */
    public E3024SearchOptionTaskNumber createE3024SearchOptionTaskNumber() {
        return new E3024SearchOptionTaskNumber();
    }

    /**
     * Create an instance of {@link InfringementVehicleElement }
     * 
     */
    public InfringementVehicleElement createInfringementVehicleElement() {
        return new InfringementVehicleElement();
    }

    /**
     * Create an instance of {@link X3003CodeAndDescriptionElement }
     * 
     */
    public X3003CodeAndDescriptionElement createX3003CodeAndDescriptionElement() {
        return new X3003CodeAndDescriptionElement();
    }

    /**
     * Create an instance of {@link SapoDocumentUpdateType }
     * 
     */
    public SapoDocumentUpdateType createSapoDocumentUpdateType() {
        return new SapoDocumentUpdateType();
    }

    /**
     * Create an instance of {@link X3027MainChargeDetails }
     * 
     */
    public X3027MainChargeDetails createX3027MainChargeDetails() {
        return new X3027MainChargeDetails();
    }

    /**
     * Create an instance of {@link X3050Charge }
     * 
     */
    public X3050Charge createX3050Charge() {
        return new X3050Charge();
    }

    /**
     * Create an instance of {@link ExecutionMessage }
     * 
     */
    public ExecutionMessage createExecutionMessage() {
        return new ExecutionMessage();
    }

    /**
     * Create an instance of {@link X3026SearchOptionInfringementStatusAndChangeDate }
     * 
     */
    public X3026SearchOptionInfringementStatusAndChangeDate createX3026SearchOptionInfringementStatusAndChangeDate() {
        return new X3026SearchOptionInfringementStatusAndChangeDate();
    }

    /**
     * Create an instance of {@link InfringementChargeElement }
     * 
     */
    public InfringementChargeElement createInfringementChargeElement() {
        return new InfringementChargeElement();
    }

    /**
     * Create an instance of {@link VehicleType }
     * 
     */
    public VehicleType createVehicleType() {
        return new VehicleType();
    }

    /**
     * Create an instance of {@link FeeType }
     * 
     */
    public FeeType createFeeType() {
        return new FeeType();
    }

    /**
     * Create an instance of {@link X3027DataCaptureDetails }
     * 
     */
    public X3027DataCaptureDetails createX3027DataCaptureDetails() {
        return new X3027DataCaptureDetails();
    }

    /**
     * Create an instance of {@link AddressElement }
     * 
     */
    public AddressElement createAddressElement() {
        return new AddressElement();
    }

    /**
     * Create an instance of {@link X3027AlternativeChargeDetails }
     * 
     */
    public X3027AlternativeChargeDetails createX3027AlternativeChargeDetails() {
        return new X3027AlternativeChargeDetails();
    }

    /**
     * Create an instance of {@link AuthorityElement }
     * 
     */
    public AuthorityElement createAuthorityElement() {
        return new AuthorityElement();
    }

    /**
     * Create an instance of {@link E3024SearchOptionVehicleRegisterNumber }
     * 
     */
    public E3024SearchOptionVehicleRegisterNumber createE3024SearchOptionVehicleRegisterNumber() {
        return new E3024SearchOptionVehicleRegisterNumber();
    }

    /**
     * Create an instance of {@link AttachmentType }
     * 
     */
    public AttachmentType createAttachmentType() {
        return new AttachmentType();
    }

    /**
     * Create an instance of {@link X3050Infringement.Charge }
     * 
     */
    public X3050Infringement.Charge createX3050InfringementCharge() {
        return new X3050Infringement.Charge();
    }

    /**
     * Create an instance of {@link X3050Fees.Fee }
     * 
     */
    public X3050Fees.Fee createX3050FeesFee() {
        return new X3050Fees.Fee();
    }

    /**
     * Create an instance of {@link FeesElement.Fee }
     * 
     */
    public FeesElement.Fee createFeesElementFee() {
        return new FeesElement.Fee();
    }

    /**
     * Create an instance of {@link X3028Response.Infringement.Location }
     * 
     */
    public X3028Response.Infringement.Location createX3028ResponseInfringementLocation() {
        return new X3028Response.Infringement.Location();
    }

    /**
     * Create an instance of {@link X3028Response.Infringement.Vehicle }
     * 
     */
    public X3028Response.Infringement.Vehicle createX3028ResponseInfringementVehicle() {
        return new X3028Response.Infringement.Vehicle();
    }

    /**
     * Create an instance of {@link X3028Response.Infringement.Charge }
     * 
     */
    public X3028Response.Infringement.Charge createX3028ResponseInfringementCharge() {
        return new X3028Response.Infringement.Charge();
    }

    /**
     * Create an instance of {@link X3028Response.Infringement.FeesList.Debt }
     * 
     */
    public X3028Response.Infringement.FeesList.Debt createX3028ResponseInfringementFeesListDebt() {
        return new X3028Response.Infringement.FeesList.Debt();
    }

    /**
     * Create an instance of {@link GetPostOfficeDestinationsResponse.Suburb.CityOrTown }
     * 
     */
    public GetPostOfficeDestinationsResponse.Suburb.CityOrTown createGetPostOfficeDestinationsResponseSuburbCityOrTown() {
        return new GetPostOfficeDestinationsResponse.Suburb.CityOrTown();
    }

    /**
     * Create an instance of {@link X314ASuccessResult.RC2 .Seller }
     * 
     */
    public X314ASuccessResult.RC2 .Seller createX314ASuccessResultRC2Seller() {
        return new X314ASuccessResult.RC2 .Seller();
    }

    /**
     * Create an instance of {@link X314ASuccessResult.RC2 .Purchaser }
     * 
     */
    public X314ASuccessResult.RC2 .Purchaser createX314ASuccessResultRC2Purchaser() {
        return new X314ASuccessResult.RC2 .Purchaser();
    }

    /**
     * Create an instance of {@link InfringementQueryRequest.Filter.CaptureDate }
     * 
     */
    public InfringementQueryRequest.Filter.CaptureDate createInfringementQueryRequestFilterCaptureDate() {
        return new InfringementQueryRequest.Filter.CaptureDate();
    }

    /**
     * Create an instance of {@link X3026Response.Infringement.Infringer }
     * 
     */
    public X3026Response.Infringement.Infringer createX3026ResponseInfringementInfringer() {
        return new X3026Response.Infringement.Infringer();
    }

    /**
     * Create an instance of {@link X314ARequest.ReceiverDetails.Proxy }
     * 
     */
    public X314ARequest.ReceiverDetails.Proxy createX314ARequestReceiverDetailsProxy() {
        return new X314ARequest.ReceiverDetails.Proxy();
    }

    /**
     * Create an instance of {@link X314ARequest.ReceiverDetails.Representative }
     * 
     */
    public X314ARequest.ReceiverDetails.Representative createX314ARequestReceiverDetailsRepresentative() {
        return new X314ARequest.ReceiverDetails.Representative();
    }

    /**
     * Create an instance of {@link SapQueryResult.Owner }
     * 
     */
    public SapQueryResult.Owner createSapQueryResultOwner() {
        return new SapQueryResult.Owner();
    }

    /**
     * Create an instance of {@link SapQueryResult.Vehicle.Microdot }
     * 
     */
    public SapQueryResult.Vehicle.Microdot createSapQueryResultVehicleMicrodot() {
        return new SapQueryResult.Vehicle.Microdot();
    }

    /**
     * Create an instance of {@link GenericVehicleQueryResponse.Vehicle.Microdot }
     * 
     */
    public GenericVehicleQueryResponse.Vehicle.Microdot createGenericVehicleQueryResponseVehicleMicrodot() {
        return new GenericVehicleQueryResponse.Vehicle.Microdot();
    }

    /**
     * Create an instance of {@link SearchOptionInfringerType.Infringer }
     * 
     */
    public SearchOptionInfringerType.Infringer createSearchOptionInfringerTypeInfringer() {
        return new SearchOptionInfringerType.Infringer();
    }

    /**
     * Create an instance of {@link InfringementElement.Charge }
     * 
     */
    public InfringementElement.Charge createInfringementElementCharge() {
        return new InfringementElement.Charge();
    }

    /**
     * Create an instance of {@link X3141SucessResult.VehicleParticulars }
     * 
     */
    public X3141SucessResult.VehicleParticulars createX3141SucessResultVehicleParticulars() {
        return new X3141SucessResult.VehicleParticulars();
    }

    /**
     * Create an instance of {@link X3050Request.Filter.CaptureDate }
     * 
     */
    public X3050Request.Filter.CaptureDate createX3050RequestFilterCaptureDate() {
        return new X3050Request.Filter.CaptureDate();
    }

    /**
     * Create an instance of {@link QueryVehicleDetails.Vehicle }
     * 
     */
    public QueryVehicleDetails.Vehicle createQueryVehicleDetailsVehicle() {
        return new QueryVehicleDetails.Vehicle();
    }

    /**
     * Create an instance of {@link X3003ResponseDetail.Vehicle }
     * 
     */
    public X3003ResponseDetail.Vehicle createX3003ResponseDetailVehicle() {
        return new X3003ResponseDetail.Vehicle();
    }

    /**
     * Create an instance of {@link X3004ResponseDetail.Person }
     * 
     */
    public X3004ResponseDetail.Person createX3004ResponseDetailPerson() {
        return new X3004ResponseDetail.Person();
    }

    /**
     * Create an instance of {@link X3004ResponseDetail.TitleHolder }
     * 
     */
    public X3004ResponseDetail.TitleHolder createX3004ResponseDetailTitleHolder() {
        return new X3004ResponseDetail.TitleHolder();
    }

    /**
     * Create an instance of {@link X3004ResponseDetail.Owner }
     * 
     */
    public X3004ResponseDetail.Owner createX3004ResponseDetailOwner() {
        return new X3004ResponseDetail.Owner();
    }

    /**
     * Create an instance of {@link X3004ResponseDetail.Vehicle }
     * 
     */
    public X3004ResponseDetail.Vehicle createX3004ResponseDetailVehicle() {
        return new X3004ResponseDetail.Vehicle();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryVehicleDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tasima/common/ws/schema/", name = "QueryVehicleDetails")
    public JAXBElement<QueryVehicleDetails> createQueryVehicleDetails(QueryVehicleDetails value) {
        return new JAXBElement<QueryVehicleDetails>(_QueryVehicleDetails_QNAME, QueryVehicleDetails.class, null, value);
    }

}
