
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * Request data based on current Infringement Status and Status Change Date range
 * 
 * 
 * <p>Java class for X3026SearchOptionInfringementStatusAndChangeDate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3026SearchOptionInfringementStatusAndChangeDate">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tasima/common/ws/schema/}X3026SearchOptionType">
 *       &lt;sequence>
 *         &lt;element name="InfringeStatus" type="{http://tasima/common/ws/schema/}InfringementStatusType" minOccurs="0"/>
 *         &lt;element name="InfringementStatusChangeFromDate" type="{http://tasima/common/ws/schema/}Date"/>
 *         &lt;element name="InfringementStatusChangeToDate" type="{http://tasima/common/ws/schema/}Date"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3026SearchOptionInfringementStatusAndChangeDate", propOrder = {
    "infringeStatus",
    "infringementStatusChangeFromDate",
    "infringementStatusChangeToDate"
})
public class X3026SearchOptionInfringementStatusAndChangeDate
    extends X3026SearchOptionType
{

    @XmlElement(name = "InfringeStatus")
    protected String infringeStatus;
    @XmlElement(name = "InfringementStatusChangeFromDate", required = true)
    protected XMLGregorianCalendar infringementStatusChangeFromDate;
    @XmlElement(name = "InfringementStatusChangeToDate", required = true)
    protected XMLGregorianCalendar infringementStatusChangeToDate;

    /**
     * Gets the value of the infringeStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringeStatus() {
        return infringeStatus;
    }

    /**
     * Sets the value of the infringeStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringeStatus(String value) {
        this.infringeStatus = value;
    }

    /**
     * Gets the value of the infringementStatusChangeFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementStatusChangeFromDate() {
        return infringementStatusChangeFromDate;
    }

    /**
     * Sets the value of the infringementStatusChangeFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementStatusChangeFromDate(XMLGregorianCalendar value) {
        this.infringementStatusChangeFromDate = value;
    }

    /**
     * Gets the value of the infringementStatusChangeToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementStatusChangeToDate() {
        return infringementStatusChangeToDate;
    }

    /**
     * Sets the value of the infringementStatusChangeToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementStatusChangeToDate(XMLGregorianCalendar value) {
        this.infringementStatusChangeToDate = value;
    }

}
