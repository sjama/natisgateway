
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="registerNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
 *                   &lt;element name="licenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="vinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
 *                   &lt;element name="engineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
 *                   &lt;element name="licenceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="modelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="category" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="description" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="roadworthyStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="vehicleState" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="sapsMark" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="sapsClearanceStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="owner" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="idDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="idDocumentType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="businessOrSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="outstandingInfringements" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="licenceCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="postalAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
 *                   &lt;element name="streetAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "MinimalVehicleQueryResponse")
public class MinimalVehicleQueryResponse {

    @XmlElement(required = true)
    protected MinimalVehicleQueryResponse.Vehicle vehicle;
    protected MinimalVehicleQueryResponse.Owner owner;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link MinimalVehicleQueryResponse.Vehicle }
     *     
     */
    public MinimalVehicleQueryResponse.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimalVehicleQueryResponse.Vehicle }
     *     
     */
    public void setVehicle(MinimalVehicleQueryResponse.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link MinimalVehicleQueryResponse.Owner }
     *     
     */
    public MinimalVehicleQueryResponse.Owner getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimalVehicleQueryResponse.Owner }
     *     
     */
    public void setOwner(MinimalVehicleQueryResponse.Owner value) {
        this.owner = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="idDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="idDocumentType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="businessOrSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="outstandingInfringements" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="licenceCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="postalAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
     *         &lt;element name="streetAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Owner {

        @XmlElement(required = true)
        protected String idDocumentNumber;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement idDocumentType;
        protected String initials;
        @XmlElement(required = true)
        protected String businessOrSurname;
        protected boolean outstandingInfringements;
        protected String licenceCardNumber;
        protected PersonAddress postalAddress;
        protected PersonAddress streetAddress;

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

        /**
         * Gets the value of the idDocumentType property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getIdDocumentType() {
            return idDocumentType;
        }

        /**
         * Sets the value of the idDocumentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setIdDocumentType(CodeAndDescriptionElement value) {
            this.idDocumentType = value;
        }

        /**
         * Gets the value of the initials property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInitials() {
            return initials;
        }

        /**
         * Sets the value of the initials property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInitials(String value) {
            this.initials = value;
        }

        /**
         * Gets the value of the businessOrSurname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusinessOrSurname() {
            return businessOrSurname;
        }

        /**
         * Sets the value of the businessOrSurname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusinessOrSurname(String value) {
            this.businessOrSurname = value;
        }

        /**
         * Gets the value of the outstandingInfringements property.
         * 
         */
        public boolean isOutstandingInfringements() {
            return outstandingInfringements;
        }

        /**
         * Sets the value of the outstandingInfringements property.
         * 
         */
        public void setOutstandingInfringements(boolean value) {
            this.outstandingInfringements = value;
        }

        /**
         * Gets the value of the licenceCardNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceCardNumber() {
            return licenceCardNumber;
        }

        /**
         * Sets the value of the licenceCardNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceCardNumber(String value) {
            this.licenceCardNumber = value;
        }

        /**
         * Gets the value of the postalAddress property.
         * 
         * @return
         *     possible object is
         *     {@link PersonAddress }
         *     
         */
        public PersonAddress getPostalAddress() {
            return postalAddress;
        }

        /**
         * Sets the value of the postalAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonAddress }
         *     
         */
        public void setPostalAddress(PersonAddress value) {
            this.postalAddress = value;
        }

        /**
         * Gets the value of the streetAddress property.
         * 
         * @return
         *     possible object is
         *     {@link PersonAddress }
         *     
         */
        public PersonAddress getStreetAddress() {
            return streetAddress;
        }

        /**
         * Sets the value of the streetAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonAddress }
         *     
         */
        public void setStreetAddress(PersonAddress value) {
            this.streetAddress = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="registerNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
     *         &lt;element name="licenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="vinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
     *         &lt;element name="engineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
     *         &lt;element name="licenceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="modelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="category" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="description" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="roadworthyStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="vehicleState" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="sapsMark" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="sapsClearanceStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Vehicle {

        @XmlElement(required = true)
        protected String registerNumber;
        protected String licenceNumber;
        protected String vinOrChassis;
        protected String engineNumber;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar licenceExpiryDate;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement modelName;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement category;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement description;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement make;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement roadworthyStatus;
        @XmlElement(required = true)
        protected CodeAndDescriptionElement vehicleState;
        protected CodeAndDescriptionElement sapsMark;
        protected CodeAndDescriptionElement sapsClearanceStatus;

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the licenceExpiryDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceExpiryDate() {
            return licenceExpiryDate;
        }

        /**
         * Sets the value of the licenceExpiryDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceExpiryDate(XMLGregorianCalendar value) {
            this.licenceExpiryDate = value;
        }

        /**
         * Gets the value of the modelName property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getModelName() {
            return modelName;
        }

        /**
         * Sets the value of the modelName property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setModelName(CodeAndDescriptionElement value) {
            this.modelName = value;
        }

        /**
         * Gets the value of the category property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getCategory() {
            return category;
        }

        /**
         * Sets the value of the category property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setCategory(CodeAndDescriptionElement value) {
            this.category = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDescription(CodeAndDescriptionElement value) {
            this.description = value;
        }

        /**
         * Gets the value of the make property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getMake() {
            return make;
        }

        /**
         * Sets the value of the make property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setMake(CodeAndDescriptionElement value) {
            this.make = value;
        }

        /**
         * Gets the value of the roadworthyStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRoadworthyStatus() {
            return roadworthyStatus;
        }

        /**
         * Sets the value of the roadworthyStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRoadworthyStatus(CodeAndDescriptionElement value) {
            this.roadworthyStatus = value;
        }

        /**
         * Gets the value of the vehicleState property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getVehicleState() {
            return vehicleState;
        }

        /**
         * Sets the value of the vehicleState property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setVehicleState(CodeAndDescriptionElement value) {
            this.vehicleState = value;
        }

        /**
         * Gets the value of the sapsMark property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapsMark() {
            return sapsMark;
        }

        /**
         * Sets the value of the sapsMark property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapsMark(CodeAndDescriptionElement value) {
            this.sapsMark = value;
        }

        /**
         * Gets the value of the sapsClearanceStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapsClearanceStatus() {
            return sapsClearanceStatus;
        }

        /**
         * Sets the value of the sapsClearanceStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapsClearanceStatus(CodeAndDescriptionElement value) {
            this.sapsClearanceStatus = value;
        }

    }

}
