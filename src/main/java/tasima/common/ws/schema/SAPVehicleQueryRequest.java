
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="VehicleRegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
 *         &lt;element name="VinOrChassisNumber" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
 *         &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
 *         &lt;element name="VehicleLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *         &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "SAPVehicleQueryRequest")
public class SAPVehicleQueryRequest {

    @XmlElement(name = "VehicleRegisterNumber")
    protected String vehicleRegisterNumber;
    @XmlElement(name = "VinOrChassisNumber")
    protected String vinOrChassisNumber;
    @XmlElement(name = "EngineNumber")
    protected String engineNumber;
    @XmlElement(name = "VehicleLicenceNumber")
    protected String vehicleLicenceNumber;
    @XmlElement(name = "PreviousLicenceNumber")
    protected String previousLicenceNumber;

    /**
     * Gets the value of the vehicleRegisterNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleRegisterNumber() {
        return vehicleRegisterNumber;
    }

    /**
     * Sets the value of the vehicleRegisterNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleRegisterNumber(String value) {
        this.vehicleRegisterNumber = value;
    }

    /**
     * Gets the value of the vinOrChassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinOrChassisNumber() {
        return vinOrChassisNumber;
    }

    /**
     * Sets the value of the vinOrChassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinOrChassisNumber(String value) {
        this.vinOrChassisNumber = value;
    }

    /**
     * Gets the value of the engineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineNumber() {
        return engineNumber;
    }

    /**
     * Sets the value of the engineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineNumber(String value) {
        this.engineNumber = value;
    }

    /**
     * Gets the value of the vehicleLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleLicenceNumber() {
        return vehicleLicenceNumber;
    }

    /**
     * Sets the value of the vehicleLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleLicenceNumber(String value) {
        this.vehicleLicenceNumber = value;
    }

    /**
     * Gets the value of the previousLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousLicenceNumber() {
        return previousLicenceNumber;
    }

    /**
     * Sets the value of the previousLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousLicenceNumber(String value) {
        this.previousLicenceNumber = value;
    }

}
