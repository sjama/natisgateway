
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchOption" type="{http://tasima/common/ws/schema/}X3026SearchOptionType"/>
 *         &lt;element name="AutCd" type="{http://tasima/common/ws/schema/}AuthorityCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchOption",
    "autCd"
})
@XmlRootElement(name = "X3026Request")
public class X3026Request {

    @XmlElement(name = "SearchOption", required = true)
    protected X3026SearchOptionType searchOption;
    @XmlElement(name = "AutCd", required = true)
    protected String autCd;

    /**
     * Gets the value of the searchOption property.
     * 
     * @return
     *     possible object is
     *     {@link X3026SearchOptionType }
     *     
     */
    public X3026SearchOptionType getSearchOption() {
        return searchOption;
    }

    /**
     * Sets the value of the searchOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3026SearchOptionType }
     *     
     */
    public void setSearchOption(X3026SearchOptionType value) {
        this.searchOption = value;
    }

    /**
     * Gets the value of the autCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutCd() {
        return autCd;
    }

    /**
     * Sets the value of the autCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutCd(String value) {
        this.autCd = value;
    }

}
