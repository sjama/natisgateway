
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SapQueryResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SapQueryResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleRegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="PrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
 *                   &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
 *                   &lt;element name="SapMark" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="LicenceChangeDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="LicenceLiabilityDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="LicenceExpiryDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="RegAuthorityOfLicensing" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="RegAuthorityOfRegistration" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="ModelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Category" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Driven" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Description" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="MainColour" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="GVM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="EngineDisplacement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="Tare" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="FuelType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="NetPower" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RegistrationDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="RegistrationQualifier" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="RegistrationQualifierDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="RoadworthyStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="VehicleState" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="VehicleStateDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="DataOwner" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="Transmission" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="GearboxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DifferentialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UnitNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FirstLicensingDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="CountryOfExport" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="LifeStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="SapClearanceReasons" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="RoadworthyTestDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="SapClearanceStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="SapClearanceDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="SapMarkDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="LicenseCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Microdot" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;all>
 *                             &lt;element name="MicrodotUserGroup" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                             &lt;element name="MicrodotPin">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;whiteSpace value="collapse"/>
 *                                   &lt;pattern value="[A-Z0-9]{1,17}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="MicrodotAppliedDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                           &lt;/all>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="owner" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PersonNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OwnershipType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="OwnershipStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="OwnershipStartDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="NatureOfOwnership" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="VehicleUsage" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="EconomicSector" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="NatureOfPerson" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Nationality" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="postalAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
 *                   &lt;element name="streetAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
 *                   &lt;element name="dcceAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="businessOrSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="IdDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AreaAuthority" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="AreaUserGroup" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SapQueryResult", propOrder = {
    "vehicle",
    "owner"
})
public class SapQueryResult {

    @XmlElement(name = "Vehicle", required = true)
    protected SapQueryResult.Vehicle vehicle;
    protected SapQueryResult.Owner owner;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link SapQueryResult.Vehicle }
     *     
     */
    public SapQueryResult.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link SapQueryResult.Vehicle }
     *     
     */
    public void setVehicle(SapQueryResult.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link SapQueryResult.Owner }
     *     
     */
    public SapQueryResult.Owner getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link SapQueryResult.Owner }
     *     
     */
    public void setOwner(SapQueryResult.Owner value) {
        this.owner = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PersonNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OwnershipType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="OwnershipStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="OwnershipStartDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="NatureOfOwnership" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="VehicleUsage" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="EconomicSector" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="NatureOfPerson" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Nationality" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="postalAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
     *         &lt;element name="streetAddress" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
     *         &lt;element name="dcceAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="businessOrSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="IdDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AreaAuthority" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="AreaUserGroup" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personNumber",
        "ownershipType",
        "ownershipStatus",
        "ownershipStartDate",
        "natureOfOwnership",
        "vehicleUsage",
        "economicSector",
        "natureOfPerson",
        "nationality",
        "postalAddress",
        "streetAddress",
        "dcceAddress",
        "businessOrSurname",
        "initials",
        "idDocumentType",
        "idDocumentNumber",
        "areaAuthority",
        "areaUserGroup"
    })
    public static class Owner {

        @XmlElement(name = "PersonNumber", required = true)
        protected String personNumber;
        @XmlElement(name = "OwnershipType", required = true)
        protected CodeAndDescriptionElement ownershipType;
        @XmlElement(name = "OwnershipStatus", required = true)
        protected CodeAndDescriptionElement ownershipStatus;
        @XmlElement(name = "OwnershipStartDate")
        protected XMLGregorianCalendar ownershipStartDate;
        @XmlElement(name = "NatureOfOwnership")
        protected CodeAndDescriptionElement natureOfOwnership;
        @XmlElement(name = "VehicleUsage")
        protected CodeAndDescriptionElement vehicleUsage;
        @XmlElement(name = "EconomicSector")
        protected CodeAndDescriptionElement economicSector;
        @XmlElement(name = "NatureOfPerson", required = true)
        protected CodeAndDescriptionElement natureOfPerson;
        @XmlElement(name = "Nationality", required = true)
        protected CodeAndDescriptionElement nationality;
        protected PersonAddress postalAddress;
        protected PersonAddress streetAddress;
        @XmlElement(required = true)
        protected String dcceAddress;
        @XmlElement(required = true)
        protected String businessOrSurname;
        @XmlElement(name = "Initials")
        protected String initials;
        @XmlElement(name = "IdDocumentType", required = true)
        protected CodeAndDescriptionElement idDocumentType;
        @XmlElement(name = "IdDocumentNumber", required = true)
        protected String idDocumentNumber;
        @XmlElement(name = "AreaAuthority", required = true)
        protected CodeAndDescriptionElement areaAuthority;
        @XmlElement(name = "AreaUserGroup", required = true)
        protected CodeAndDescriptionElement areaUserGroup;

        /**
         * Gets the value of the personNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPersonNumber() {
            return personNumber;
        }

        /**
         * Sets the value of the personNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPersonNumber(String value) {
            this.personNumber = value;
        }

        /**
         * Gets the value of the ownershipType property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getOwnershipType() {
            return ownershipType;
        }

        /**
         * Sets the value of the ownershipType property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setOwnershipType(CodeAndDescriptionElement value) {
            this.ownershipType = value;
        }

        /**
         * Gets the value of the ownershipStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getOwnershipStatus() {
            return ownershipStatus;
        }

        /**
         * Sets the value of the ownershipStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setOwnershipStatus(CodeAndDescriptionElement value) {
            this.ownershipStatus = value;
        }

        /**
         * Gets the value of the ownershipStartDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getOwnershipStartDate() {
            return ownershipStartDate;
        }

        /**
         * Sets the value of the ownershipStartDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setOwnershipStartDate(XMLGregorianCalendar value) {
            this.ownershipStartDate = value;
        }

        /**
         * Gets the value of the natureOfOwnership property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getNatureOfOwnership() {
            return natureOfOwnership;
        }

        /**
         * Sets the value of the natureOfOwnership property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setNatureOfOwnership(CodeAndDescriptionElement value) {
            this.natureOfOwnership = value;
        }

        /**
         * Gets the value of the vehicleUsage property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getVehicleUsage() {
            return vehicleUsage;
        }

        /**
         * Sets the value of the vehicleUsage property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setVehicleUsage(CodeAndDescriptionElement value) {
            this.vehicleUsage = value;
        }

        /**
         * Gets the value of the economicSector property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getEconomicSector() {
            return economicSector;
        }

        /**
         * Sets the value of the economicSector property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setEconomicSector(CodeAndDescriptionElement value) {
            this.economicSector = value;
        }

        /**
         * Gets the value of the natureOfPerson property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getNatureOfPerson() {
            return natureOfPerson;
        }

        /**
         * Sets the value of the natureOfPerson property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setNatureOfPerson(CodeAndDescriptionElement value) {
            this.natureOfPerson = value;
        }

        /**
         * Gets the value of the nationality property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getNationality() {
            return nationality;
        }

        /**
         * Sets the value of the nationality property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setNationality(CodeAndDescriptionElement value) {
            this.nationality = value;
        }

        /**
         * Gets the value of the postalAddress property.
         * 
         * @return
         *     possible object is
         *     {@link PersonAddress }
         *     
         */
        public PersonAddress getPostalAddress() {
            return postalAddress;
        }

        /**
         * Sets the value of the postalAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonAddress }
         *     
         */
        public void setPostalAddress(PersonAddress value) {
            this.postalAddress = value;
        }

        /**
         * Gets the value of the streetAddress property.
         * 
         * @return
         *     possible object is
         *     {@link PersonAddress }
         *     
         */
        public PersonAddress getStreetAddress() {
            return streetAddress;
        }

        /**
         * Sets the value of the streetAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonAddress }
         *     
         */
        public void setStreetAddress(PersonAddress value) {
            this.streetAddress = value;
        }

        /**
         * Gets the value of the dcceAddress property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDcceAddress() {
            return dcceAddress;
        }

        /**
         * Sets the value of the dcceAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDcceAddress(String value) {
            this.dcceAddress = value;
        }

        /**
         * Gets the value of the businessOrSurname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBusinessOrSurname() {
            return businessOrSurname;
        }

        /**
         * Sets the value of the businessOrSurname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBusinessOrSurname(String value) {
            this.businessOrSurname = value;
        }

        /**
         * Gets the value of the initials property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInitials() {
            return initials;
        }

        /**
         * Sets the value of the initials property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInitials(String value) {
            this.initials = value;
        }

        /**
         * Gets the value of the idDocumentType property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getIdDocumentType() {
            return idDocumentType;
        }

        /**
         * Sets the value of the idDocumentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setIdDocumentType(CodeAndDescriptionElement value) {
            this.idDocumentType = value;
        }

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

        /**
         * Gets the value of the areaAuthority property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getAreaAuthority() {
            return areaAuthority;
        }

        /**
         * Sets the value of the areaAuthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setAreaAuthority(CodeAndDescriptionElement value) {
            this.areaAuthority = value;
        }

        /**
         * Gets the value of the areaUserGroup property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getAreaUserGroup() {
            return areaUserGroup;
        }

        /**
         * Sets the value of the areaUserGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setAreaUserGroup(CodeAndDescriptionElement value) {
            this.areaUserGroup = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleRegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="PrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
     *         &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
     *         &lt;element name="SapMark" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="LicenceChangeDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="LicenceLiabilityDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="LicenceExpiryDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="RegAuthorityOfLicensing" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="RegAuthorityOfRegistration" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="ModelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Category" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Driven" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Description" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="MainColour" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="GVM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="EngineDisplacement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="Tare" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="FuelType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="NetPower" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RegistrationDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="RegistrationQualifier" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="RegistrationQualifierDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="RoadworthyStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="VehicleState" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="VehicleStateDate" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="DataOwner" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="Transmission" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="GearboxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DifferentialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UnitNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FirstLicensingDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="CountryOfExport" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="LifeStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="SapClearanceReasons" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="RoadworthyTestDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="SapClearanceStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="SapClearanceDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="SapMarkDate" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="LicenseCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Microdot" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;all>
     *                   &lt;element name="MicrodotUserGroup" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *                   &lt;element name="MicrodotPin">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;whiteSpace value="collapse"/>
     *                         &lt;pattern value="[A-Z0-9]{1,17}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="MicrodotAppliedDate" type="{http://tasima/common/ws/schema/}Date"/>
     *                 &lt;/all>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleRegisterNumber",
        "licenceNumber",
        "previousLicenceNumber",
        "prePreviousLicenceNumber",
        "vinOrChassis",
        "engineNumber",
        "sapMark",
        "licenceChangeDate",
        "licenceLiabilityDate",
        "licenceExpiryDate",
        "regAuthorityOfLicensing",
        "regAuthorityOfRegistration",
        "modelName",
        "category",
        "driven",
        "description",
        "make",
        "mainColour",
        "gvm",
        "engineDisplacement",
        "tare",
        "fuelType",
        "netPower",
        "registrationDate",
        "registrationQualifier",
        "registrationQualifierDate",
        "roadworthyStatus",
        "vehicleState",
        "vehicleStateDate",
        "dataOwner",
        "timestamp",
        "transmission",
        "gearboxNumber",
        "differentialNumber",
        "unitNumber",
        "firstLicensingDate",
        "countryOfExport",
        "lifeStatus",
        "modelNumber",
        "sapClearanceReasons",
        "roadworthyTestDate",
        "sapClearanceStatus",
        "sapClearanceDate",
        "sapMarkDate",
        "licenseCertificateNumber",
        "microdot"
    })
    public static class Vehicle {

        @XmlElement(name = "VehicleRegisterNumber", required = true)
        protected String vehicleRegisterNumber;
        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;
        @XmlElement(name = "PreviousLicenceNumber")
        protected String previousLicenceNumber;
        @XmlElement(name = "PrePreviousLicenceNumber")
        protected String prePreviousLicenceNumber;
        @XmlElement(name = "VinOrChassis")
        protected String vinOrChassis;
        @XmlElement(name = "EngineNumber")
        protected String engineNumber;
        @XmlElement(name = "SapMark", required = true)
        protected CodeAndDescriptionElement sapMark;
        @XmlElement(name = "LicenceChangeDate")
        protected XMLGregorianCalendar licenceChangeDate;
        @XmlElement(name = "LicenceLiabilityDate")
        protected XMLGregorianCalendar licenceLiabilityDate;
        @XmlElement(name = "LicenceExpiryDate")
        protected XMLGregorianCalendar licenceExpiryDate;
        @XmlElement(name = "RegAuthorityOfLicensing")
        protected CodeAndDescriptionElement regAuthorityOfLicensing;
        @XmlElement(name = "RegAuthorityOfRegistration", required = true)
        protected CodeAndDescriptionElement regAuthorityOfRegistration;
        @XmlElement(name = "ModelName", required = true)
        protected CodeAndDescriptionElement modelName;
        @XmlElement(name = "Category", required = true)
        protected CodeAndDescriptionElement category;
        @XmlElement(name = "Driven", required = true)
        protected CodeAndDescriptionElement driven;
        @XmlElement(name = "Description", required = true)
        protected CodeAndDescriptionElement description;
        @XmlElement(name = "Make", required = true)
        protected CodeAndDescriptionElement make;
        @XmlElement(name = "MainColour")
        protected CodeAndDescriptionElement mainColour;
        @XmlElement(name = "GVM")
        protected Integer gvm;
        @XmlElement(name = "EngineDisplacement")
        protected Integer engineDisplacement;
        @XmlElement(name = "Tare")
        protected int tare;
        @XmlElement(name = "FuelType")
        protected CodeAndDescriptionElement fuelType;
        @XmlElement(name = "NetPower")
        protected Integer netPower;
        @XmlElement(name = "RegistrationDate")
        protected XMLGregorianCalendar registrationDate;
        @XmlElement(name = "RegistrationQualifier", required = true)
        protected CodeAndDescriptionElement registrationQualifier;
        @XmlElement(name = "RegistrationQualifierDate")
        protected XMLGregorianCalendar registrationQualifierDate;
        @XmlElement(name = "RoadworthyStatus", required = true)
        protected CodeAndDescriptionElement roadworthyStatus;
        @XmlElement(name = "VehicleState", required = true)
        protected CodeAndDescriptionElement vehicleState;
        @XmlElement(name = "VehicleStateDate", required = true)
        protected XMLGregorianCalendar vehicleStateDate;
        @XmlElement(name = "DataOwner", required = true)
        protected CodeAndDescriptionElement dataOwner;
        @XmlElement(name = "Timestamp")
        protected Long timestamp;
        @XmlElement(name = "Transmission")
        protected CodeAndDescriptionElement transmission;
        @XmlElement(name = "GearboxNumber")
        protected String gearboxNumber;
        @XmlElement(name = "DifferentialNumber")
        protected String differentialNumber;
        @XmlElement(name = "UnitNumber")
        protected String unitNumber;
        @XmlElement(name = "FirstLicensingDate")
        protected XMLGregorianCalendar firstLicensingDate;
        @XmlElement(name = "CountryOfExport")
        protected CodeAndDescriptionElement countryOfExport;
        @XmlElement(name = "LifeStatus", required = true)
        protected CodeAndDescriptionElement lifeStatus;
        @XmlElement(name = "ModelNumber")
        protected Integer modelNumber;
        @XmlElement(name = "SapClearanceReasons")
        protected List<CodeAndDescriptionElement> sapClearanceReasons;
        @XmlElement(name = "RoadworthyTestDate")
        protected XMLGregorianCalendar roadworthyTestDate;
        @XmlElement(name = "SapClearanceStatus", required = true)
        protected CodeAndDescriptionElement sapClearanceStatus;
        @XmlElement(name = "SapClearanceDate")
        protected XMLGregorianCalendar sapClearanceDate;
        @XmlElement(name = "SapMarkDate")
        protected XMLGregorianCalendar sapMarkDate;
        @XmlElement(name = "LicenseCertificateNumber")
        protected String licenseCertificateNumber;
        @XmlElement(name = "Microdot")
        protected SapQueryResult.Vehicle.Microdot microdot;

        /**
         * Gets the value of the vehicleRegisterNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleRegisterNumber() {
            return vehicleRegisterNumber;
        }

        /**
         * Sets the value of the vehicleRegisterNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleRegisterNumber(String value) {
            this.vehicleRegisterNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the previousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousLicenceNumber() {
            return previousLicenceNumber;
        }

        /**
         * Sets the value of the previousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousLicenceNumber(String value) {
            this.previousLicenceNumber = value;
        }

        /**
         * Gets the value of the prePreviousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrePreviousLicenceNumber() {
            return prePreviousLicenceNumber;
        }

        /**
         * Sets the value of the prePreviousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrePreviousLicenceNumber(String value) {
            this.prePreviousLicenceNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the sapMark property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapMark() {
            return sapMark;
        }

        /**
         * Sets the value of the sapMark property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapMark(CodeAndDescriptionElement value) {
            this.sapMark = value;
        }

        /**
         * Gets the value of the licenceChangeDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceChangeDate() {
            return licenceChangeDate;
        }

        /**
         * Sets the value of the licenceChangeDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceChangeDate(XMLGregorianCalendar value) {
            this.licenceChangeDate = value;
        }

        /**
         * Gets the value of the licenceLiabilityDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceLiabilityDate() {
            return licenceLiabilityDate;
        }

        /**
         * Sets the value of the licenceLiabilityDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceLiabilityDate(XMLGregorianCalendar value) {
            this.licenceLiabilityDate = value;
        }

        /**
         * Gets the value of the licenceExpiryDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceExpiryDate() {
            return licenceExpiryDate;
        }

        /**
         * Sets the value of the licenceExpiryDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceExpiryDate(XMLGregorianCalendar value) {
            this.licenceExpiryDate = value;
        }

        /**
         * Gets the value of the regAuthorityOfLicensing property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRegAuthorityOfLicensing() {
            return regAuthorityOfLicensing;
        }

        /**
         * Sets the value of the regAuthorityOfLicensing property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRegAuthorityOfLicensing(CodeAndDescriptionElement value) {
            this.regAuthorityOfLicensing = value;
        }

        /**
         * Gets the value of the regAuthorityOfRegistration property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRegAuthorityOfRegistration() {
            return regAuthorityOfRegistration;
        }

        /**
         * Sets the value of the regAuthorityOfRegistration property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRegAuthorityOfRegistration(CodeAndDescriptionElement value) {
            this.regAuthorityOfRegistration = value;
        }

        /**
         * Gets the value of the modelName property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getModelName() {
            return modelName;
        }

        /**
         * Sets the value of the modelName property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setModelName(CodeAndDescriptionElement value) {
            this.modelName = value;
        }

        /**
         * Gets the value of the category property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getCategory() {
            return category;
        }

        /**
         * Sets the value of the category property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setCategory(CodeAndDescriptionElement value) {
            this.category = value;
        }

        /**
         * Gets the value of the driven property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDriven() {
            return driven;
        }

        /**
         * Sets the value of the driven property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDriven(CodeAndDescriptionElement value) {
            this.driven = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDescription(CodeAndDescriptionElement value) {
            this.description = value;
        }

        /**
         * Gets the value of the make property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getMake() {
            return make;
        }

        /**
         * Sets the value of the make property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setMake(CodeAndDescriptionElement value) {
            this.make = value;
        }

        /**
         * Gets the value of the mainColour property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getMainColour() {
            return mainColour;
        }

        /**
         * Sets the value of the mainColour property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setMainColour(CodeAndDescriptionElement value) {
            this.mainColour = value;
        }

        /**
         * Gets the value of the gvm property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGVM() {
            return gvm;
        }

        /**
         * Sets the value of the gvm property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGVM(Integer value) {
            this.gvm = value;
        }

        /**
         * Gets the value of the engineDisplacement property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getEngineDisplacement() {
            return engineDisplacement;
        }

        /**
         * Sets the value of the engineDisplacement property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setEngineDisplacement(Integer value) {
            this.engineDisplacement = value;
        }

        /**
         * Gets the value of the tare property.
         * 
         */
        public int getTare() {
            return tare;
        }

        /**
         * Sets the value of the tare property.
         * 
         */
        public void setTare(int value) {
            this.tare = value;
        }

        /**
         * Gets the value of the fuelType property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getFuelType() {
            return fuelType;
        }

        /**
         * Sets the value of the fuelType property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setFuelType(CodeAndDescriptionElement value) {
            this.fuelType = value;
        }

        /**
         * Gets the value of the netPower property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNetPower() {
            return netPower;
        }

        /**
         * Sets the value of the netPower property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNetPower(Integer value) {
            this.netPower = value;
        }

        /**
         * Gets the value of the registrationDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRegistrationDate() {
            return registrationDate;
        }

        /**
         * Sets the value of the registrationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRegistrationDate(XMLGregorianCalendar value) {
            this.registrationDate = value;
        }

        /**
         * Gets the value of the registrationQualifier property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRegistrationQualifier() {
            return registrationQualifier;
        }

        /**
         * Sets the value of the registrationQualifier property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRegistrationQualifier(CodeAndDescriptionElement value) {
            this.registrationQualifier = value;
        }

        /**
         * Gets the value of the registrationQualifierDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRegistrationQualifierDate() {
            return registrationQualifierDate;
        }

        /**
         * Sets the value of the registrationQualifierDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRegistrationQualifierDate(XMLGregorianCalendar value) {
            this.registrationQualifierDate = value;
        }

        /**
         * Gets the value of the roadworthyStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRoadworthyStatus() {
            return roadworthyStatus;
        }

        /**
         * Sets the value of the roadworthyStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRoadworthyStatus(CodeAndDescriptionElement value) {
            this.roadworthyStatus = value;
        }

        /**
         * Gets the value of the vehicleState property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getVehicleState() {
            return vehicleState;
        }

        /**
         * Sets the value of the vehicleState property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setVehicleState(CodeAndDescriptionElement value) {
            this.vehicleState = value;
        }

        /**
         * Gets the value of the vehicleStateDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVehicleStateDate() {
            return vehicleStateDate;
        }

        /**
         * Sets the value of the vehicleStateDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVehicleStateDate(XMLGregorianCalendar value) {
            this.vehicleStateDate = value;
        }

        /**
         * Gets the value of the dataOwner property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDataOwner() {
            return dataOwner;
        }

        /**
         * Sets the value of the dataOwner property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDataOwner(CodeAndDescriptionElement value) {
            this.dataOwner = value;
        }

        /**
         * Gets the value of the timestamp property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getTimestamp() {
            return timestamp;
        }

        /**
         * Sets the value of the timestamp property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setTimestamp(Long value) {
            this.timestamp = value;
        }

        /**
         * Gets the value of the transmission property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getTransmission() {
            return transmission;
        }

        /**
         * Sets the value of the transmission property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setTransmission(CodeAndDescriptionElement value) {
            this.transmission = value;
        }

        /**
         * Gets the value of the gearboxNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGearboxNumber() {
            return gearboxNumber;
        }

        /**
         * Sets the value of the gearboxNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGearboxNumber(String value) {
            this.gearboxNumber = value;
        }

        /**
         * Gets the value of the differentialNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDifferentialNumber() {
            return differentialNumber;
        }

        /**
         * Sets the value of the differentialNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDifferentialNumber(String value) {
            this.differentialNumber = value;
        }

        /**
         * Gets the value of the unitNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitNumber() {
            return unitNumber;
        }

        /**
         * Sets the value of the unitNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitNumber(String value) {
            this.unitNumber = value;
        }

        /**
         * Gets the value of the firstLicensingDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFirstLicensingDate() {
            return firstLicensingDate;
        }

        /**
         * Sets the value of the firstLicensingDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFirstLicensingDate(XMLGregorianCalendar value) {
            this.firstLicensingDate = value;
        }

        /**
         * Gets the value of the countryOfExport property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getCountryOfExport() {
            return countryOfExport;
        }

        /**
         * Sets the value of the countryOfExport property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setCountryOfExport(CodeAndDescriptionElement value) {
            this.countryOfExport = value;
        }

        /**
         * Gets the value of the lifeStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getLifeStatus() {
            return lifeStatus;
        }

        /**
         * Sets the value of the lifeStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setLifeStatus(CodeAndDescriptionElement value) {
            this.lifeStatus = value;
        }

        /**
         * Gets the value of the modelNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getModelNumber() {
            return modelNumber;
        }

        /**
         * Sets the value of the modelNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setModelNumber(Integer value) {
            this.modelNumber = value;
        }

        /**
         * Gets the value of the sapClearanceReasons property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the sapClearanceReasons property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSapClearanceReasons().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CodeAndDescriptionElement }
         * 
         * 
         */
        public List<CodeAndDescriptionElement> getSapClearanceReasons() {
            if (sapClearanceReasons == null) {
                sapClearanceReasons = new ArrayList<CodeAndDescriptionElement>();
            }
            return this.sapClearanceReasons;
        }

        /**
         * Gets the value of the roadworthyTestDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRoadworthyTestDate() {
            return roadworthyTestDate;
        }

        /**
         * Sets the value of the roadworthyTestDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRoadworthyTestDate(XMLGregorianCalendar value) {
            this.roadworthyTestDate = value;
        }

        /**
         * Gets the value of the sapClearanceStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapClearanceStatus() {
            return sapClearanceStatus;
        }

        /**
         * Sets the value of the sapClearanceStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapClearanceStatus(CodeAndDescriptionElement value) {
            this.sapClearanceStatus = value;
        }

        /**
         * Gets the value of the sapClearanceDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSapClearanceDate() {
            return sapClearanceDate;
        }

        /**
         * Sets the value of the sapClearanceDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSapClearanceDate(XMLGregorianCalendar value) {
            this.sapClearanceDate = value;
        }

        /**
         * Gets the value of the sapMarkDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSapMarkDate() {
            return sapMarkDate;
        }

        /**
         * Sets the value of the sapMarkDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSapMarkDate(XMLGregorianCalendar value) {
            this.sapMarkDate = value;
        }

        /**
         * Gets the value of the licenseCertificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenseCertificateNumber() {
            return licenseCertificateNumber;
        }

        /**
         * Sets the value of the licenseCertificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenseCertificateNumber(String value) {
            this.licenseCertificateNumber = value;
        }

        /**
         * Gets the value of the microdot property.
         * 
         * @return
         *     possible object is
         *     {@link SapQueryResult.Vehicle.Microdot }
         *     
         */
        public SapQueryResult.Vehicle.Microdot getMicrodot() {
            return microdot;
        }

        /**
         * Sets the value of the microdot property.
         * 
         * @param value
         *     allowed object is
         *     {@link SapQueryResult.Vehicle.Microdot }
         *     
         */
        public void setMicrodot(SapQueryResult.Vehicle.Microdot value) {
            this.microdot = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;all>
         *         &lt;element name="MicrodotUserGroup" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
         *         &lt;element name="MicrodotPin">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;whiteSpace value="collapse"/>
         *               &lt;pattern value="[A-Z0-9]{1,17}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="MicrodotAppliedDate" type="{http://tasima/common/ws/schema/}Date"/>
         *       &lt;/all>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {

        })
        public static class Microdot {

            @XmlElement(name = "MicrodotUserGroup", required = true)
            protected CodeAndDescriptionElement microdotUserGroup;
            @XmlElement(name = "MicrodotPin", required = true)
            protected String microdotPin;
            @XmlElement(name = "MicrodotAppliedDate", required = true)
            protected XMLGregorianCalendar microdotAppliedDate;

            /**
             * Gets the value of the microdotUserGroup property.
             * 
             * @return
             *     possible object is
             *     {@link CodeAndDescriptionElement }
             *     
             */
            public CodeAndDescriptionElement getMicrodotUserGroup() {
                return microdotUserGroup;
            }

            /**
             * Sets the value of the microdotUserGroup property.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeAndDescriptionElement }
             *     
             */
            public void setMicrodotUserGroup(CodeAndDescriptionElement value) {
                this.microdotUserGroup = value;
            }

            /**
             * Gets the value of the microdotPin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMicrodotPin() {
                return microdotPin;
            }

            /**
             * Sets the value of the microdotPin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMicrodotPin(String value) {
                this.microdotPin = value;
            }

            /**
             * Gets the value of the microdotAppliedDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getMicrodotAppliedDate() {
                return microdotAppliedDate;
            }

            /**
             * Sets the value of the microdotAppliedDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setMicrodotAppliedDate(XMLGregorianCalendar value) {
                this.microdotAppliedDate = value;
            }

        }

    }

}
