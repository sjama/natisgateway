
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for QueryPersonToNominateResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryPersonToNominateResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="driversLicenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="leanerLicenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessOrSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstName3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryPersonToNominateResponseDetail", propOrder = {
    "driversLicenceNumber",
    "leanerLicenceNumber",
    "initials",
    "businessOrSurname",
    "firstName1",
    "firstName2",
    "firstName3",
    "dateOfBirth"
})
public class QueryPersonToNominateResponseDetail {

    protected String driversLicenceNumber;
    protected String leanerLicenceNumber;
    protected String initials;
    protected String businessOrSurname;
    protected String firstName1;
    protected String firstName2;
    protected String firstName3;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfBirth;

    /**
     * Gets the value of the driversLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriversLicenceNumber() {
        return driversLicenceNumber;
    }

    /**
     * Sets the value of the driversLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriversLicenceNumber(String value) {
        this.driversLicenceNumber = value;
    }

    /**
     * Gets the value of the leanerLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeanerLicenceNumber() {
        return leanerLicenceNumber;
    }

    /**
     * Sets the value of the leanerLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeanerLicenceNumber(String value) {
        this.leanerLicenceNumber = value;
    }

    /**
     * Gets the value of the initials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitials() {
        return initials;
    }

    /**
     * Sets the value of the initials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitials(String value) {
        this.initials = value;
    }

    /**
     * Gets the value of the businessOrSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessOrSurname() {
        return businessOrSurname;
    }

    /**
     * Sets the value of the businessOrSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessOrSurname(String value) {
        this.businessOrSurname = value;
    }

    /**
     * Gets the value of the firstName1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName1() {
        return firstName1;
    }

    /**
     * Sets the value of the firstName1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName1(String value) {
        this.firstName1 = value;
    }

    /**
     * Gets the value of the firstName2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName2() {
        return firstName2;
    }

    /**
     * Sets the value of the firstName2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName2(String value) {
        this.firstName2 = value;
    }

    /**
     * Gets the value of the firstName3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName3() {
        return firstName3;
    }

    /**
     * Sets the value of the firstName3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName3(String value) {
        this.firstName3 = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

}
