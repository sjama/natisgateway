
package tasima.common.ws.schema;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X5008LicenceRenewalFeeAssessment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X5008LicenceRenewalFeeAssessment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="preFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="postFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="licenseFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="convenienceFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="adminFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="outstandingDebtForOffences" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="minAmountDue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="maxAmountDue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X5008LicenceRenewalFeeAssessment", propOrder = {

})
public class X5008LicenceRenewalFeeAssessment {

    @XmlElement(required = true)
    protected BigDecimal preFees;
    @XmlElement(required = true)
    protected BigDecimal postFees;
    @XmlElement(required = true)
    protected BigDecimal licenseFees;
    @XmlElement(required = true)
    protected BigDecimal convenienceFee;
    @XmlElement(required = true)
    protected BigDecimal adminFee;
    @XmlElement(required = true)
    protected BigDecimal outstandingDebtForOffences;
    @XmlElement(required = true)
    protected BigDecimal minAmountDue;
    @XmlElement(required = true)
    protected BigDecimal maxAmountDue;

    /**
     * Gets the value of the preFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPreFees() {
        return preFees;
    }

    /**
     * Sets the value of the preFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPreFees(BigDecimal value) {
        this.preFees = value;
    }

    /**
     * Gets the value of the postFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPostFees() {
        return postFees;
    }

    /**
     * Sets the value of the postFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPostFees(BigDecimal value) {
        this.postFees = value;
    }

    /**
     * Gets the value of the licenseFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLicenseFees() {
        return licenseFees;
    }

    /**
     * Sets the value of the licenseFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLicenseFees(BigDecimal value) {
        this.licenseFees = value;
    }

    /**
     * Gets the value of the convenienceFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConvenienceFee() {
        return convenienceFee;
    }

    /**
     * Sets the value of the convenienceFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConvenienceFee(BigDecimal value) {
        this.convenienceFee = value;
    }

    /**
     * Gets the value of the adminFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAdminFee() {
        return adminFee;
    }

    /**
     * Sets the value of the adminFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAdminFee(BigDecimal value) {
        this.adminFee = value;
    }

    /**
     * Gets the value of the outstandingDebtForOffences property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOutstandingDebtForOffences() {
        return outstandingDebtForOffences;
    }

    /**
     * Sets the value of the outstandingDebtForOffences property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOutstandingDebtForOffences(BigDecimal value) {
        this.outstandingDebtForOffences = value;
    }

    /**
     * Gets the value of the minAmountDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinAmountDue() {
        return minAmountDue;
    }

    /**
     * Sets the value of the minAmountDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinAmountDue(BigDecimal value) {
        this.minAmountDue = value;
    }

    /**
     * Gets the value of the maxAmountDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxAmountDue() {
        return maxAmountDue;
    }

    /**
     * Sets the value of the maxAmountDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxAmountDue(BigDecimal value) {
        this.maxAmountDue = value;
    }

}
