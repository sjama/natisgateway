
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for E0706_DltcInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="E0706_DltcInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infrastructureNumberAndType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="infrastructurePersonName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="infrastructureUserGroupName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "E0706_DltcInfo", propOrder = {
    "infrastructureNumberAndType",
    "infrastructurePersonName",
    "infrastructureUserGroupName"
})
public class E0706DltcInfo {

    @XmlElement(required = true)
    protected String infrastructureNumberAndType;
    @XmlElement(required = true)
    protected String infrastructurePersonName;
    protected String infrastructureUserGroupName;

    /**
     * Gets the value of the infrastructureNumberAndType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfrastructureNumberAndType() {
        return infrastructureNumberAndType;
    }

    /**
     * Sets the value of the infrastructureNumberAndType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfrastructureNumberAndType(String value) {
        this.infrastructureNumberAndType = value;
    }

    /**
     * Gets the value of the infrastructurePersonName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfrastructurePersonName() {
        return infrastructurePersonName;
    }

    /**
     * Sets the value of the infrastructurePersonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfrastructurePersonName(String value) {
        this.infrastructurePersonName = value;
    }

    /**
     * Gets the value of the infrastructureUserGroupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfrastructureUserGroupName() {
        return infrastructureUserGroupName;
    }

    /**
     * Sets the value of the infrastructureUserGroupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfrastructureUserGroupName(String value) {
        this.infrastructureUserGroupName = value;
    }

}
