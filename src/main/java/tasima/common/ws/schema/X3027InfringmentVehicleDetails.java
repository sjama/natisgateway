
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3027InfringmentVehicleDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027InfringmentVehicleDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegtN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="9"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MvCertN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GVM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MakeCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MainColourCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027InfringmentVehicleDetails", propOrder = {
    "regtN",
    "mvCertN",
    "gvm",
    "makeCd",
    "modelName",
    "mainColourCd"
})
public class X3027InfringmentVehicleDetails {

    @XmlElement(name = "RegtN")
    protected String regtN;
    @XmlElement(name = "MvCertN")
    protected String mvCertN;
    @XmlElement(name = "GVM")
    protected String gvm;
    @XmlElement(name = "MakeCd")
    protected String makeCd;
    @XmlElement(name = "ModelName")
    protected String modelName;
    @XmlElement(name = "MainColourCd")
    protected String mainColourCd;

    /**
     * Gets the value of the regtN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegtN() {
        return regtN;
    }

    /**
     * Sets the value of the regtN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegtN(String value) {
        this.regtN = value;
    }

    /**
     * Gets the value of the mvCertN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMvCertN() {
        return mvCertN;
    }

    /**
     * Sets the value of the mvCertN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMvCertN(String value) {
        this.mvCertN = value;
    }

    /**
     * Gets the value of the gvm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGVM() {
        return gvm;
    }

    /**
     * Sets the value of the gvm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGVM(String value) {
        this.gvm = value;
    }

    /**
     * Gets the value of the makeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakeCd() {
        return makeCd;
    }

    /**
     * Sets the value of the makeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakeCd(String value) {
        this.makeCd = value;
    }

    /**
     * Gets the value of the modelName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * Sets the value of the modelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelName(String value) {
        this.modelName = value;
    }

    /**
     * Gets the value of the mainColourCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainColourCd() {
        return mainColourCd;
    }

    /**
     * Sets the value of the mainColourCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainColourCd(String value) {
        this.mainColourCd = value;
    }

}
