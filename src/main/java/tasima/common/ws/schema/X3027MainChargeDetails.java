
package tasima.common.ws.schema;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3027MainChargeDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027MainChargeDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[\d]{4,5}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Description">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;whiteSpace value="preserve"/>
 *               &lt;maxLength value="350"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SpeedRead1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *               &lt;totalDigits value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SpeedRead2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *               &lt;totalDigits value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RedTime" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AmberTime" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VehicleImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="LicenseImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="ChargeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PenaltyAmt">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}Money">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DiscountedAmt">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}Money">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DemeritPoints" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;totalDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027MainChargeDetails", propOrder = {
    "code",
    "description",
    "speedRead1",
    "speedRead2",
    "redTime",
    "amberTime",
    "vehicleImage",
    "licenseImage",
    "chargeType",
    "penaltyAmt",
    "discountedAmt",
    "demeritPoints"
})
public class X3027MainChargeDetails {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "SpeedRead1")
    protected BigDecimal speedRead1;
    @XmlElement(name = "SpeedRead2")
    protected BigDecimal speedRead2;
    @XmlElement(name = "RedTime")
    protected BigInteger redTime;
    @XmlElement(name = "AmberTime")
    protected BigInteger amberTime;
    @XmlElement(name = "VehicleImage")
    protected byte[] vehicleImage;
    @XmlElement(name = "LicenseImage")
    protected byte[] licenseImage;
    @XmlElement(name = "ChargeType", required = true)
    protected String chargeType;
    @XmlElement(name = "PenaltyAmt", required = true)
    protected BigDecimal penaltyAmt;
    @XmlElement(name = "DiscountedAmt", required = true)
    protected BigDecimal discountedAmt;
    @XmlElement(name = "DemeritPoints")
    protected BigInteger demeritPoints;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the speedRead1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSpeedRead1() {
        return speedRead1;
    }

    /**
     * Sets the value of the speedRead1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSpeedRead1(BigDecimal value) {
        this.speedRead1 = value;
    }

    /**
     * Gets the value of the speedRead2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSpeedRead2() {
        return speedRead2;
    }

    /**
     * Sets the value of the speedRead2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSpeedRead2(BigDecimal value) {
        this.speedRead2 = value;
    }

    /**
     * Gets the value of the redTime property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRedTime() {
        return redTime;
    }

    /**
     * Sets the value of the redTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRedTime(BigInteger value) {
        this.redTime = value;
    }

    /**
     * Gets the value of the amberTime property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAmberTime() {
        return amberTime;
    }

    /**
     * Sets the value of the amberTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAmberTime(BigInteger value) {
        this.amberTime = value;
    }

    /**
     * Gets the value of the vehicleImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getVehicleImage() {
        return vehicleImage;
    }

    /**
     * Sets the value of the vehicleImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setVehicleImage(byte[] value) {
        this.vehicleImage = value;
    }

    /**
     * Gets the value of the licenseImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getLicenseImage() {
        return licenseImage;
    }

    /**
     * Sets the value of the licenseImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setLicenseImage(byte[] value) {
        this.licenseImage = value;
    }

    /**
     * Gets the value of the chargeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeType(String value) {
        this.chargeType = value;
    }

    /**
     * Gets the value of the penaltyAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPenaltyAmt() {
        return penaltyAmt;
    }

    /**
     * Sets the value of the penaltyAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPenaltyAmt(BigDecimal value) {
        this.penaltyAmt = value;
    }

    /**
     * Gets the value of the discountedAmt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountedAmt() {
        return discountedAmt;
    }

    /**
     * Sets the value of the discountedAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountedAmt(BigDecimal value) {
        this.discountedAmt = value;
    }

    /**
     * Gets the value of the demeritPoints property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDemeritPoints() {
        return demeritPoints;
    }

    /**
     * Sets the value of the demeritPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDemeritPoints(BigInteger value) {
        this.demeritPoints = value;
    }

}
