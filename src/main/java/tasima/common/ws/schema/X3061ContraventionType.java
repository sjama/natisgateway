
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for x3061ContraventionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="x3061ContraventionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementNoticeN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="mainCharge" type="{http://tasima/common/ws/schema/}x3061ChargeType"/>
 *         &lt;element name="alternativeCharge" type="{http://tasima/common/ws/schema/}x3061ChargeType" minOccurs="0"/>
 *         &lt;element name="reference" type="{http://tasima/common/ws/schema/}x3061VehicleReferenceType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "x3061ContraventionType", propOrder = {
    "infringementNoticeN",
    "mainCharge",
    "alternativeCharge",
    "reference"
})
public class X3061ContraventionType {

    @XmlElement(required = true)
    protected String infringementNoticeN;
    @XmlElement(required = true)
    protected X3061ChargeType mainCharge;
    protected X3061ChargeType alternativeCharge;
    @XmlElement(required = true)
    protected String reference;

    /**
     * Gets the value of the infringementNoticeN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeN() {
        return infringementNoticeN;
    }

    /**
     * Sets the value of the infringementNoticeN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeN(String value) {
        this.infringementNoticeN = value;
    }

    /**
     * Gets the value of the mainCharge property.
     * 
     * @return
     *     possible object is
     *     {@link X3061ChargeType }
     *     
     */
    public X3061ChargeType getMainCharge() {
        return mainCharge;
    }

    /**
     * Sets the value of the mainCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061ChargeType }
     *     
     */
    public void setMainCharge(X3061ChargeType value) {
        this.mainCharge = value;
    }

    /**
     * Gets the value of the alternativeCharge property.
     * 
     * @return
     *     possible object is
     *     {@link X3061ChargeType }
     *     
     */
    public X3061ChargeType getAlternativeCharge() {
        return alternativeCharge;
    }

    /**
     * Sets the value of the alternativeCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061ChargeType }
     *     
     */
    public void setAlternativeCharge(X3061ChargeType value) {
        this.alternativeCharge = value;
    }

    /**
     * Gets the value of the reference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReference() {
        return reference;
    }

    /**
     * Sets the value of the reference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReference(String value) {
        this.reference = value;
    }

}
