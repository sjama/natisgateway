
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicleImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="licenceImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleImage",
    "licenceImage"
})
@XmlRootElement(name = "GetInfringementImagesResponse")
public class GetInfringementImagesResponse {

    protected byte[] vehicleImage;
    protected byte[] licenceImage;

    /**
     * Gets the value of the vehicleImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getVehicleImage() {
        return vehicleImage;
    }

    /**
     * Sets the value of the vehicleImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setVehicleImage(byte[] value) {
        this.vehicleImage = value;
    }

    /**
     * Gets the value of the licenceImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getLicenceImage() {
        return licenceImage;
    }

    /**
     * Sets the value of the licenceImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setLicenceImage(byte[] value) {
        this.licenceImage = value;
    }

}
