
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accident" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tarn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *                   &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *                   &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *                   &lt;element name="noOfVehicles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="noOfPedestrians" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="totalKilled" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="totalSeriouslyInjured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="totalSlightlyInjured" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accident"
})
@XmlRootElement(name = "QueryAccidentsResponse")
public class QueryAccidentsResponse {

    protected List<QueryAccidentsResponse.Accident> accident;

    /**
     * Gets the value of the accident property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accident property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccident().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryAccidentsResponse.Accident }
     * 
     * 
     */
    public List<QueryAccidentsResponse.Accident> getAccident() {
        if (accident == null) {
            accident = new ArrayList<QueryAccidentsResponse.Accident>();
        }
        return this.accident;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tarn" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}time"/>
     *         &lt;element name="longitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
     *         &lt;element name="latitude" type="{http://www.w3.org/2001/XMLSchema}double"/>
     *         &lt;element name="noOfVehicles" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="noOfPedestrians" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="totalKilled" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="totalSeriouslyInjured" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="totalSlightlyInjured" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tarn",
        "date",
        "time",
        "longitude",
        "latitude",
        "noOfVehicles",
        "noOfPedestrians",
        "totalKilled",
        "totalSeriouslyInjured",
        "totalSlightlyInjured"
    })
    public static class Accident {

        @XmlElement(required = true)
        protected String tarn;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar date;
        @XmlElement(required = true)
        @XmlSchemaType(name = "time")
        protected XMLGregorianCalendar time;
        protected double longitude;
        protected double latitude;
        protected int noOfVehicles;
        protected int noOfPedestrians;
        protected int totalKilled;
        protected int totalSeriouslyInjured;
        protected int totalSlightlyInjured;

        /**
         * Gets the value of the tarn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTarn() {
            return tarn;
        }

        /**
         * Sets the value of the tarn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTarn(String value) {
            this.tarn = value;
        }

        /**
         * Gets the value of the date property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Sets the value of the date property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }

        /**
         * Gets the value of the time property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTime() {
            return time;
        }

        /**
         * Sets the value of the time property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTime(XMLGregorianCalendar value) {
            this.time = value;
        }

        /**
         * Gets the value of the longitude property.
         * 
         */
        public double getLongitude() {
            return longitude;
        }

        /**
         * Sets the value of the longitude property.
         * 
         */
        public void setLongitude(double value) {
            this.longitude = value;
        }

        /**
         * Gets the value of the latitude property.
         * 
         */
        public double getLatitude() {
            return latitude;
        }

        /**
         * Sets the value of the latitude property.
         * 
         */
        public void setLatitude(double value) {
            this.latitude = value;
        }

        /**
         * Gets the value of the noOfVehicles property.
         * 
         */
        public int getNoOfVehicles() {
            return noOfVehicles;
        }

        /**
         * Sets the value of the noOfVehicles property.
         * 
         */
        public void setNoOfVehicles(int value) {
            this.noOfVehicles = value;
        }

        /**
         * Gets the value of the noOfPedestrians property.
         * 
         */
        public int getNoOfPedestrians() {
            return noOfPedestrians;
        }

        /**
         * Sets the value of the noOfPedestrians property.
         * 
         */
        public void setNoOfPedestrians(int value) {
            this.noOfPedestrians = value;
        }

        /**
         * Gets the value of the totalKilled property.
         * 
         */
        public int getTotalKilled() {
            return totalKilled;
        }

        /**
         * Sets the value of the totalKilled property.
         * 
         */
        public void setTotalKilled(int value) {
            this.totalKilled = value;
        }

        /**
         * Gets the value of the totalSeriouslyInjured property.
         * 
         */
        public int getTotalSeriouslyInjured() {
            return totalSeriouslyInjured;
        }

        /**
         * Sets the value of the totalSeriouslyInjured property.
         * 
         */
        public void setTotalSeriouslyInjured(int value) {
            this.totalSeriouslyInjured = value;
        }

        /**
         * Gets the value of the totalSlightlyInjured property.
         * 
         */
        public int getTotalSlightlyInjured() {
            return totalSlightlyInjured;
        }

        /**
         * Sets the value of the totalSlightlyInjured property.
         * 
         */
        public void setTotalSlightlyInjured(int value) {
            this.totalSlightlyInjured = value;
        }

    }

}
