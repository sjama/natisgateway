
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringement" type="{http://tasima/common/ws/schema/}InfringementType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringement"
})
@XmlRootElement(name = "GetInfringementForRevocationResponse")
public class GetInfringementForRevocationResponse {

    protected InfringementType infringement;

    /**
     * Gets the value of the infringement property.
     * 
     * @return
     *     possible object is
     *     {@link InfringementType }
     *     
     */
    public InfringementType getInfringement() {
        return infringement;
    }

    /**
     * Sets the value of the infringement property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfringementType }
     *     
     */
    public void setInfringement(InfringementType value) {
        this.infringement = value;
    }

}
