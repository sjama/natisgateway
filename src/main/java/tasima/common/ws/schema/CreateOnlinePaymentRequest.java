
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="menuCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="entityReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentGatewayType" type="{http://tasima/common/ws/schema/}GatewayCode" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "CreateOnlinePaymentRequest")
public class CreateOnlinePaymentRequest {

    @XmlElement(required = true)
    protected String menuCode;
    @XmlElement(required = true)
    protected String entityReference;
    protected String paymentGatewayType;

    /**
     * Gets the value of the menuCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMenuCode() {
        return menuCode;
    }

    /**
     * Sets the value of the menuCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMenuCode(String value) {
        this.menuCode = value;
    }

    /**
     * Gets the value of the entityReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityReference() {
        return entityReference;
    }

    /**
     * Sets the value of the entityReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityReference(String value) {
        this.entityReference = value;
    }

    /**
     * Gets the value of the paymentGatewayType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentGatewayType() {
        return paymentGatewayType;
    }

    /**
     * Sets the value of the paymentGatewayType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentGatewayType(String value) {
        this.paymentGatewayType = value;
    }

}
