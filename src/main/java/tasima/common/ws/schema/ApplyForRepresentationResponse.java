
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="infringementCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aarto05c" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "ApplyForRepresentationResponse")
public class ApplyForRepresentationResponse {

    @XmlElement(required = true)
    protected String infringementCertificateNumber;
    @XmlElement(name = "aarto05c")
    protected byte[] aarto05C;

    /**
     * Gets the value of the infringementCertificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementCertificateNumber() {
        return infringementCertificateNumber;
    }

    /**
     * Sets the value of the infringementCertificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementCertificateNumber(String value) {
        this.infringementCertificateNumber = value;
    }

    /**
     * Gets the value of the aarto05C property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto05C() {
        return aarto05C;
    }

    /**
     * Sets the value of the aarto05C property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto05C(byte[] value) {
        this.aarto05C = value;
    }

}
