
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="isFleetNomination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="infringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringementNoticeNumber" minOccurs="0"/>
 *         &lt;element name="licenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *         &lt;element name="identificationType" type="{http://tasima/common/ws/schema/}InfringerIdType" minOccurs="0"/>
 *         &lt;element name="identificationNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "isFleetNomination",
    "infringementNoticeNumber",
    "licenceNumber",
    "identificationType",
    "identificationNumber"
})
@XmlRootElement(name = "InfringementForNominationRequest")
public class InfringementForNominationRequest {

    protected boolean isFleetNomination;
    protected String infringementNoticeNumber;
    protected String licenceNumber;
    protected String identificationType;
    protected String identificationNumber;

    /**
     * Gets the value of the isFleetNomination property.
     * 
     */
    public boolean isIsFleetNomination() {
        return isFleetNomination;
    }

    /**
     * Sets the value of the isFleetNomination property.
     * 
     */
    public void setIsFleetNomination(boolean value) {
        this.isFleetNomination = value;
    }

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the licenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenceNumber() {
        return licenceNumber;
    }

    /**
     * Sets the value of the licenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenceNumber(String value) {
        this.licenceNumber = value;
    }

    /**
     * Gets the value of the identificationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationType() {
        return identificationType;
    }

    /**
     * Sets the value of the identificationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationType(String value) {
        this.identificationType = value;
    }

    /**
     * Gets the value of the identificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * Sets the value of the identificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationNumber(String value) {
        this.identificationNumber = value;
    }

}
