
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3027OfficerInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027OfficerInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfficerInfTypeCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfficerInfN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OfficerInfRegtStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfficerInitials" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OfficerSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfficerIdDocTypeCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfficerIdDocN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027OfficerInformation", propOrder = {
    "officerInfTypeCd",
    "officerInfN",
    "officerInfRegtStat",
    "officerInitials",
    "officerSurname",
    "officerIdDocTypeCd",
    "officerIdDocN"
})
public class X3027OfficerInformation {

    @XmlElement(name = "OfficerInfTypeCd")
    protected String officerInfTypeCd;
    @XmlElement(name = "OfficerInfN")
    protected String officerInfN;
    @XmlElement(name = "OfficerInfRegtStat")
    protected String officerInfRegtStat;
    @XmlElement(name = "OfficerInitials")
    protected String officerInitials;
    @XmlElement(name = "OfficerSurname")
    protected String officerSurname;
    @XmlElement(name = "OfficerIdDocTypeCd")
    protected String officerIdDocTypeCd;
    @XmlElement(name = "OfficerIdDocN")
    protected String officerIdDocN;

    /**
     * Gets the value of the officerInfTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerInfTypeCd() {
        return officerInfTypeCd;
    }

    /**
     * Sets the value of the officerInfTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerInfTypeCd(String value) {
        this.officerInfTypeCd = value;
    }

    /**
     * Gets the value of the officerInfN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerInfN() {
        return officerInfN;
    }

    /**
     * Sets the value of the officerInfN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerInfN(String value) {
        this.officerInfN = value;
    }

    /**
     * Gets the value of the officerInfRegtStat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerInfRegtStat() {
        return officerInfRegtStat;
    }

    /**
     * Sets the value of the officerInfRegtStat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerInfRegtStat(String value) {
        this.officerInfRegtStat = value;
    }

    /**
     * Gets the value of the officerInitials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerInitials() {
        return officerInitials;
    }

    /**
     * Sets the value of the officerInitials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerInitials(String value) {
        this.officerInitials = value;
    }

    /**
     * Gets the value of the officerSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerSurname() {
        return officerSurname;
    }

    /**
     * Sets the value of the officerSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerSurname(String value) {
        this.officerSurname = value;
    }

    /**
     * Gets the value of the officerIdDocTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerIdDocTypeCd() {
        return officerIdDocTypeCd;
    }

    /**
     * Sets the value of the officerIdDocTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerIdDocTypeCd(String value) {
        this.officerIdDocTypeCd = value;
    }

    /**
     * Gets the value of the officerIdDocN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerIdDocN() {
        return officerIdDocN;
    }

    /**
     * Sets the value of the officerIdDocN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerIdDocN(String value) {
        this.officerIdDocN = value;
    }

}
