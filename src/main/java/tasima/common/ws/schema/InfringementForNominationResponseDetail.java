
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InfringementForNominationResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InfringementForNominationResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringement" type="{http://tasima/common/ws/schema/}InfringementType" minOccurs="0"/>
 *         &lt;element name="vehicleImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="licenceImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfringementForNominationResponseDetail", propOrder = {
    "infringement",
    "vehicleImage",
    "licenceImage"
})
public class InfringementForNominationResponseDetail {

    protected InfringementType infringement;
    protected byte[] vehicleImage;
    protected byte[] licenceImage;

    /**
     * Gets the value of the infringement property.
     * 
     * @return
     *     possible object is
     *     {@link InfringementType }
     *     
     */
    public InfringementType getInfringement() {
        return infringement;
    }

    /**
     * Sets the value of the infringement property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfringementType }
     *     
     */
    public void setInfringement(InfringementType value) {
        this.infringement = value;
    }

    /**
     * Gets the value of the vehicleImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getVehicleImage() {
        return vehicleImage;
    }

    /**
     * Sets the value of the vehicleImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setVehicleImage(byte[] value) {
        this.vehicleImage = value;
    }

    /**
     * Gets the value of the licenceImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getLicenceImage() {
        return licenceImage;
    }

    /**
     * Sets the value of the licenceImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setLicenceImage(byte[] value) {
        this.licenceImage = value;
    }

}
