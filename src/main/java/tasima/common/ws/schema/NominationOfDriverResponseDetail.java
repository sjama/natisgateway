
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NominationOfDriverResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NominationOfDriverResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="newInfringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aarto05b" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NominationOfDriverResponseDetail", propOrder = {

})
public class NominationOfDriverResponseDetail {

    protected String newInfringementNoticeNumber;
    @XmlElement(name = "aarto05b", required = true)
    protected byte[] aarto05B;

    /**
     * Gets the value of the newInfringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewInfringementNoticeNumber() {
        return newInfringementNoticeNumber;
    }

    /**
     * Sets the value of the newInfringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewInfringementNoticeNumber(String value) {
        this.newInfringementNoticeNumber = value;
    }

    /**
     * Gets the value of the aarto05B property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto05B() {
        return aarto05B;
    }

    /**
     * Sets the value of the aarto05B property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto05B(byte[] value) {
        this.aarto05B = value;
    }

}
