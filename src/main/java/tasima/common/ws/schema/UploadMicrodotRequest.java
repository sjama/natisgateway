
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VinOrChassisNumber" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
 *         &lt;element name="MicrodotPIN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;whiteSpace value="collapse"/>
 *               &lt;pattern value="[A-Z0-9]{1,17}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="effectiveDate" type="{http://tasima/common/ws/schema/}Date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vinOrChassisNumber",
    "microdotPIN",
    "effectiveDate"
})
@XmlRootElement(name = "UploadMicrodotRequest")
public class UploadMicrodotRequest {

    @XmlElement(name = "VinOrChassisNumber", required = true)
    protected String vinOrChassisNumber;
    @XmlElement(name = "MicrodotPIN", required = true)
    protected String microdotPIN;
    @XmlElement(required = true)
    protected XMLGregorianCalendar effectiveDate;

    /**
     * Gets the value of the vinOrChassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinOrChassisNumber() {
        return vinOrChassisNumber;
    }

    /**
     * Sets the value of the vinOrChassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinOrChassisNumber(String value) {
        this.vinOrChassisNumber = value;
    }

    /**
     * Gets the value of the microdotPIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMicrodotPIN() {
        return microdotPIN;
    }

    /**
     * Sets the value of the microdotPIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMicrodotPIN(String value) {
        this.microdotPIN = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

}
