
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TestPreBookingApplicant complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestPreBookingApplicant">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idDocumentTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businessOrSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestPreBookingApplicant", propOrder = {
    "idDocumentNumber",
    "idDocumentTypeName",
    "initials",
    "businessOrSurname"
})
public class TestPreBookingApplicant {

    @XmlElement(required = true)
    protected String idDocumentNumber;
    @XmlElement(required = true)
    protected String idDocumentTypeName;
    protected String initials;
    @XmlElement(required = true)
    protected String businessOrSurname;

    /**
     * Gets the value of the idDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentNumber() {
        return idDocumentNumber;
    }

    /**
     * Sets the value of the idDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentNumber(String value) {
        this.idDocumentNumber = value;
    }

    /**
     * Gets the value of the idDocumentTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentTypeName() {
        return idDocumentTypeName;
    }

    /**
     * Sets the value of the idDocumentTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentTypeName(String value) {
        this.idDocumentTypeName = value;
    }

    /**
     * Gets the value of the initials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitials() {
        return initials;
    }

    /**
     * Sets the value of the initials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitials(String value) {
        this.initials = value;
    }

    /**
     * Gets the value of the businessOrSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessOrSurname() {
        return businessOrSurname;
    }

    /**
     * Sets the value of the businessOrSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessOrSurname(String value) {
        this.businessOrSurname = value;
    }

}
