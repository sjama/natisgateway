
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aarto05d" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementCertificateNumber",
    "aarto05D"
})
@XmlRootElement(name = "OptForCourtResponse")
public class OptForCourtResponse {

    @XmlElement(required = true)
    protected String infringementCertificateNumber;
    @XmlElement(name = "aarto05d")
    protected byte[] aarto05D;

    /**
     * Gets the value of the infringementCertificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementCertificateNumber() {
        return infringementCertificateNumber;
    }

    /**
     * Sets the value of the infringementCertificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementCertificateNumber(String value) {
        this.infringementCertificateNumber = value;
    }

    /**
     * Gets the value of the aarto05D property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto05D() {
        return aarto05D;
    }

    /**
     * Sets the value of the aarto05D property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto05D(byte[] value) {
        this.aarto05D = value;
    }

}
