
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request based on infringer identification
 * 
 * <p>Java class for SearchOptionInfringerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchOptionInfringerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Infringer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdDocTypeCd" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                   &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchOptionInfringerType", propOrder = {
    "infringer"
})
public class SearchOptionInfringerType {

    @XmlElement(name = "Infringer", required = true)
    protected SearchOptionInfringerType.Infringer infringer;

    /**
     * Gets the value of the infringer property.
     * 
     * @return
     *     possible object is
     *     {@link SearchOptionInfringerType.Infringer }
     *     
     */
    public SearchOptionInfringerType.Infringer getInfringer() {
        return infringer;
    }

    /**
     * Sets the value of the infringer property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchOptionInfringerType.Infringer }
     *     
     */
    public void setInfringer(SearchOptionInfringerType.Infringer value) {
        this.infringer = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocTypeCd" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *         &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocTypeCd",
        "idDocN"
    })
    public static class Infringer {

        @XmlElement(name = "IdDocTypeCd", required = true)
        protected String idDocTypeCd;
        @XmlElement(name = "IdDocN", required = true)
        protected String idDocN;

        /**
         * Gets the value of the idDocTypeCd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocTypeCd() {
            return idDocTypeCd;
        }

        /**
         * Sets the value of the idDocTypeCd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocTypeCd(String value) {
            this.idDocTypeCd = value;
        }

        /**
         * Gets the value of the idDocN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocN() {
            return idDocN;
        }

        /**
         * Sets the value of the idDocN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocN(String value) {
            this.idDocN = value;
        }

    }

}
