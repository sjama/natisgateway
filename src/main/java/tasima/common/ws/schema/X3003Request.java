
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}X3003RegisterNumberElement" minOccurs="0"/>
 *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}X3003VinOrChassisNumber" minOccurs="0"/>
 *         &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}X3003EngineNumber" minOccurs="0"/>
 *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "X3003Request")
public class X3003Request {

    @XmlElement(name = "RegisterNumber")
    protected String registerNumber;
    @XmlElement(name = "VinOrChassis")
    protected String vinOrChassis;
    @XmlElement(name = "EngineNumber")
    protected String engineNumber;
    @XmlElement(name = "LicenceNumber")
    protected String licenceNumber;

    /**
     * Gets the value of the registerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterNumber() {
        return registerNumber;
    }

    /**
     * Sets the value of the registerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterNumber(String value) {
        this.registerNumber = value;
    }

    /**
     * Gets the value of the vinOrChassis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinOrChassis() {
        return vinOrChassis;
    }

    /**
     * Sets the value of the vinOrChassis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinOrChassis(String value) {
        this.vinOrChassis = value;
    }

    /**
     * Gets the value of the engineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineNumber() {
        return engineNumber;
    }

    /**
     * Sets the value of the engineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineNumber(String value) {
        this.engineNumber = value;
    }

    /**
     * Gets the value of the licenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenceNumber() {
        return licenceNumber;
    }

    /**
     * Sets the value of the licenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenceNumber(String value) {
        this.licenceNumber = value;
    }

}
