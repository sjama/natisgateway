
package tasima.common.ws.schema;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InfringementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InfringementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="infringementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="infringementStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="infringementStatusReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parkingNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operatorNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originatingNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nominatedNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="summonsNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="infringementStatusChangeDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="infringementCaptureDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="idDocumentNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="location" type="{http://tasima/common/ws/schema/}LocationType"/>
 *         &lt;element name="vehicle" type="{http://tasima/common/ws/schema/}VehicleType" minOccurs="0"/>
 *         &lt;element name="mainCharge" type="{http://tasima/common/ws/schema/}ChargeType"/>
 *         &lt;element name="alternativeCharge" type="{http://tasima/common/ws/schema/}ChargeType" minOccurs="0"/>
 *         &lt;element name="fees" type="{http://tasima/common/ws/schema/}FeeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="totalOutstanding" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aartoOptions" type="{http://tasima/common/ws/schema/}AartoOptionsType" minOccurs="0"/>
 *         &lt;element name="sapoDocuments" type="{http://tasima/common/ws/schema/}SapoDocumentUpdateType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfringementType", propOrder = {
    "infringementNoticeNumber",
    "infringementDate",
    "infringementStatusDescription",
    "infringementStatusReason",
    "parkingNoticeNumber",
    "operatorNoticeNumber",
    "originatingNoticeNumber",
    "nominatedNoticeNumber",
    "summonsNumber",
    "infringementStatusChangeDate",
    "infringementCaptureDate",
    "idDocumentNumber",
    "location",
    "vehicle",
    "mainCharge",
    "alternativeCharge",
    "fees",
    "totalOutstanding",
    "aartoOptions",
    "sapoDocuments"
})
public class InfringementType {

    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar infringementDate;
    @XmlElement(required = true)
    protected String infringementStatusDescription;
    protected String infringementStatusReason;
    protected String parkingNoticeNumber;
    protected String operatorNoticeNumber;
    protected String originatingNoticeNumber;
    protected String nominatedNoticeNumber;
    protected String summonsNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar infringementStatusChangeDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar infringementCaptureDate;
    protected String idDocumentNumber;
    @XmlElement(required = true)
    protected LocationType location;
    protected VehicleType vehicle;
    @XmlElement(required = true)
    protected ChargeType mainCharge;
    protected ChargeType alternativeCharge;
    protected List<FeeType> fees;
    protected BigDecimal totalOutstanding;
    protected AartoOptionsType aartoOptions;
    protected List<SapoDocumentUpdateType> sapoDocuments;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the infringementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementDate() {
        return infringementDate;
    }

    /**
     * Sets the value of the infringementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementDate(XMLGregorianCalendar value) {
        this.infringementDate = value;
    }

    /**
     * Gets the value of the infringementStatusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementStatusDescription() {
        return infringementStatusDescription;
    }

    /**
     * Sets the value of the infringementStatusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementStatusDescription(String value) {
        this.infringementStatusDescription = value;
    }

    /**
     * Gets the value of the infringementStatusReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementStatusReason() {
        return infringementStatusReason;
    }

    /**
     * Sets the value of the infringementStatusReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementStatusReason(String value) {
        this.infringementStatusReason = value;
    }

    /**
     * Gets the value of the parkingNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingNoticeNumber() {
        return parkingNoticeNumber;
    }

    /**
     * Sets the value of the parkingNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingNoticeNumber(String value) {
        this.parkingNoticeNumber = value;
    }

    /**
     * Gets the value of the operatorNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorNoticeNumber() {
        return operatorNoticeNumber;
    }

    /**
     * Sets the value of the operatorNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorNoticeNumber(String value) {
        this.operatorNoticeNumber = value;
    }

    /**
     * Gets the value of the originatingNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginatingNoticeNumber() {
        return originatingNoticeNumber;
    }

    /**
     * Sets the value of the originatingNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginatingNoticeNumber(String value) {
        this.originatingNoticeNumber = value;
    }

    /**
     * Gets the value of the nominatedNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNominatedNoticeNumber() {
        return nominatedNoticeNumber;
    }

    /**
     * Sets the value of the nominatedNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNominatedNoticeNumber(String value) {
        this.nominatedNoticeNumber = value;
    }

    /**
     * Gets the value of the summonsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsNumber() {
        return summonsNumber;
    }

    /**
     * Sets the value of the summonsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsNumber(String value) {
        this.summonsNumber = value;
    }

    /**
     * Gets the value of the infringementStatusChangeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementStatusChangeDate() {
        return infringementStatusChangeDate;
    }

    /**
     * Sets the value of the infringementStatusChangeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementStatusChangeDate(XMLGregorianCalendar value) {
        this.infringementStatusChangeDate = value;
    }

    /**
     * Gets the value of the infringementCaptureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementCaptureDate() {
        return infringementCaptureDate;
    }

    /**
     * Sets the value of the infringementCaptureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementCaptureDate(XMLGregorianCalendar value) {
        this.infringementCaptureDate = value;
    }

    /**
     * Gets the value of the idDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentNumber() {
        return idDocumentNumber;
    }

    /**
     * Sets the value of the idDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentNumber(String value) {
        this.idDocumentNumber = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleType }
     *     
     */
    public VehicleType getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleType }
     *     
     */
    public void setVehicle(VehicleType value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the mainCharge property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeType }
     *     
     */
    public ChargeType getMainCharge() {
        return mainCharge;
    }

    /**
     * Sets the value of the mainCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeType }
     *     
     */
    public void setMainCharge(ChargeType value) {
        this.mainCharge = value;
    }

    /**
     * Gets the value of the alternativeCharge property.
     * 
     * @return
     *     possible object is
     *     {@link ChargeType }
     *     
     */
    public ChargeType getAlternativeCharge() {
        return alternativeCharge;
    }

    /**
     * Sets the value of the alternativeCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargeType }
     *     
     */
    public void setAlternativeCharge(ChargeType value) {
        this.alternativeCharge = value;
    }

    /**
     * Gets the value of the fees property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fees property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFees().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeType }
     * 
     * 
     */
    public List<FeeType> getFees() {
        if (fees == null) {
            fees = new ArrayList<FeeType>();
        }
        return this.fees;
    }

    /**
     * Gets the value of the totalOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalOutstanding() {
        return totalOutstanding;
    }

    /**
     * Sets the value of the totalOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalOutstanding(BigDecimal value) {
        this.totalOutstanding = value;
    }

    /**
     * Gets the value of the aartoOptions property.
     * 
     * @return
     *     possible object is
     *     {@link AartoOptionsType }
     *     
     */
    public AartoOptionsType getAartoOptions() {
        return aartoOptions;
    }

    /**
     * Sets the value of the aartoOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link AartoOptionsType }
     *     
     */
    public void setAartoOptions(AartoOptionsType value) {
        this.aartoOptions = value;
    }

    /**
     * Gets the value of the sapoDocuments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sapoDocuments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSapoDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SapoDocumentUpdateType }
     * 
     * 
     */
    public List<SapoDocumentUpdateType> getSapoDocuments() {
        if (sapoDocuments == null) {
            sapoDocuments = new ArrayList<SapoDocumentUpdateType>();
        }
        return this.sapoDocuments;
    }

}
