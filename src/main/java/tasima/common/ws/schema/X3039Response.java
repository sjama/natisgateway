
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="executionResult" type="{http://tasima/common/ws/schema/}ExecutionResult"/>
 *         &lt;element name="response" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="newInfringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="aartoCertificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "executionResult",
    "response"
})
@XmlRootElement(name = "X3039Response")
public class X3039Response {

    @XmlElement(required = true)
    protected ExecutionResult executionResult;
    protected X3039Response.Response response;

    /**
     * Gets the value of the executionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionResult }
     *     
     */
    public ExecutionResult getExecutionResult() {
        return executionResult;
    }

    /**
     * Sets the value of the executionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionResult }
     *     
     */
    public void setExecutionResult(ExecutionResult value) {
        this.executionResult = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link X3039Response.Response }
     *     
     */
    public X3039Response.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3039Response.Response }
     *     
     */
    public void setResponse(X3039Response.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="newInfringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="aartoCertificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "newInfringementNoticeNumber",
        "aartoCertificate"
    })
    public static class Response {

        @XmlElement(required = true)
        protected String newInfringementNoticeNumber;
        @XmlElement(required = true)
        protected byte[] aartoCertificate;

        /**
         * Gets the value of the newInfringementNoticeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewInfringementNoticeNumber() {
            return newInfringementNoticeNumber;
        }

        /**
         * Sets the value of the newInfringementNoticeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewInfringementNoticeNumber(String value) {
            this.newInfringementNoticeNumber = value;
        }

        /**
         * Gets the value of the aartoCertificate property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getAartoCertificate() {
            return aartoCertificate;
        }

        /**
         * Sets the value of the aartoCertificate property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setAartoCertificate(byte[] value) {
            this.aartoCertificate = value;
        }

    }

}
