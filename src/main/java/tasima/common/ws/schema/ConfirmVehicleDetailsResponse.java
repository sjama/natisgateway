
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="titleHolder" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="surnameOrNameOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="licenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="registerNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="vinOrChassis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="engineNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="controlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="make" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="seriesName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nationalVehicleClassification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="dateOfLiabilityOfFirstLicensing" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="vehicleStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="odomoterReading" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="dateOfOdometerReading" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="registered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="licensed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "titleHolder",
    "vehicle"
})
@XmlRootElement(name = "ConfirmVehicleDetailsResponse")
public class ConfirmVehicleDetailsResponse {

    protected ConfirmVehicleDetailsResponse.TitleHolder titleHolder;
    @XmlElement(required = true)
    protected ConfirmVehicleDetailsResponse.Vehicle vehicle;

    /**
     * Gets the value of the titleHolder property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmVehicleDetailsResponse.TitleHolder }
     *     
     */
    public ConfirmVehicleDetailsResponse.TitleHolder getTitleHolder() {
        return titleHolder;
    }

    /**
     * Sets the value of the titleHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmVehicleDetailsResponse.TitleHolder }
     *     
     */
    public void setTitleHolder(ConfirmVehicleDetailsResponse.TitleHolder value) {
        this.titleHolder = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmVehicleDetailsResponse.Vehicle }
     *     
     */
    public ConfirmVehicleDetailsResponse.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmVehicleDetailsResponse.Vehicle }
     *     
     */
    public void setVehicle(ConfirmVehicleDetailsResponse.Vehicle value) {
        this.vehicle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="surnameOrNameOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumentNumber",
        "nationality",
        "surnameOrNameOfBusiness",
        "initials"
    })
    public static class TitleHolder {

        @XmlElement(required = true)
        protected String idDocumentNumber;
        @XmlElement(required = true)
        protected String nationality;
        @XmlElement(required = true)
        protected String surnameOrNameOfBusiness;
        protected String initials;

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

        /**
         * Gets the value of the nationality property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNationality() {
            return nationality;
        }

        /**
         * Sets the value of the nationality property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNationality(String value) {
            this.nationality = value;
        }

        /**
         * Gets the value of the surnameOrNameOfBusiness property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurnameOrNameOfBusiness() {
            return surnameOrNameOfBusiness;
        }

        /**
         * Sets the value of the surnameOrNameOfBusiness property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurnameOrNameOfBusiness(String value) {
            this.surnameOrNameOfBusiness = value;
        }

        /**
         * Gets the value of the initials property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInitials() {
            return initials;
        }

        /**
         * Sets the value of the initials property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInitials(String value) {
            this.initials = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="licenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="registerNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="vinOrChassis" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="engineNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="controlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="make" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="seriesName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nationalVehicleClassification" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="dateOfLiabilityOfFirstLicensing" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="vehicleStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="odomoterReading" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="dateOfOdometerReading" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="registered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="licensed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "licenceNumber",
        "registerNumber",
        "vinOrChassis",
        "engineNumber",
        "controlNumber",
        "make",
        "seriesName",
        "nationalVehicleClassification",
        "dateOfLiabilityOfFirstLicensing",
        "vehicleStatus",
        "odomoterReading",
        "dateOfOdometerReading",
        "registered",
        "licensed"
    })
    public static class Vehicle {

        @XmlElement(required = true)
        protected String licenceNumber;
        @XmlElement(required = true)
        protected String registerNumber;
        @XmlElement(required = true)
        protected String vinOrChassis;
        @XmlElement(required = true)
        protected String engineNumber;
        @XmlElement(required = true)
        protected String controlNumber;
        @XmlElement(required = true)
        protected String make;
        @XmlElement(required = true)
        protected String seriesName;
        @XmlElement(required = true)
        protected String nationalVehicleClassification;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateOfLiabilityOfFirstLicensing;
        @XmlElement(required = true)
        protected String vehicleStatus;
        protected Integer odomoterReading;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateOfOdometerReading;
        protected boolean registered;
        protected boolean licensed;

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the controlNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getControlNumber() {
            return controlNumber;
        }

        /**
         * Sets the value of the controlNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setControlNumber(String value) {
            this.controlNumber = value;
        }

        /**
         * Gets the value of the make property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMake() {
            return make;
        }

        /**
         * Sets the value of the make property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMake(String value) {
            this.make = value;
        }

        /**
         * Gets the value of the seriesName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSeriesName() {
            return seriesName;
        }

        /**
         * Sets the value of the seriesName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSeriesName(String value) {
            this.seriesName = value;
        }

        /**
         * Gets the value of the nationalVehicleClassification property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNationalVehicleClassification() {
            return nationalVehicleClassification;
        }

        /**
         * Sets the value of the nationalVehicleClassification property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNationalVehicleClassification(String value) {
            this.nationalVehicleClassification = value;
        }

        /**
         * Gets the value of the dateOfLiabilityOfFirstLicensing property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateOfLiabilityOfFirstLicensing() {
            return dateOfLiabilityOfFirstLicensing;
        }

        /**
         * Sets the value of the dateOfLiabilityOfFirstLicensing property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateOfLiabilityOfFirstLicensing(XMLGregorianCalendar value) {
            this.dateOfLiabilityOfFirstLicensing = value;
        }

        /**
         * Gets the value of the vehicleStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleStatus() {
            return vehicleStatus;
        }

        /**
         * Sets the value of the vehicleStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleStatus(String value) {
            this.vehicleStatus = value;
        }

        /**
         * Gets the value of the odomoterReading property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOdomoterReading() {
            return odomoterReading;
        }

        /**
         * Sets the value of the odomoterReading property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOdomoterReading(Integer value) {
            this.odomoterReading = value;
        }

        /**
         * Gets the value of the dateOfOdometerReading property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateOfOdometerReading() {
            return dateOfOdometerReading;
        }

        /**
         * Sets the value of the dateOfOdometerReading property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateOfOdometerReading(XMLGregorianCalendar value) {
            this.dateOfOdometerReading = value;
        }

        /**
         * Gets the value of the registered property.
         * 
         */
        public boolean isRegistered() {
            return registered;
        }

        /**
         * Sets the value of the registered property.
         * 
         */
        public void setRegistered(boolean value) {
            this.registered = value;
        }

        /**
         * Gets the value of the licensed property.
         * 
         */
        public boolean isLicensed() {
            return licensed;
        }

        /**
         * Sets the value of the licensed property.
         * 
         */
        public void setLicensed(boolean value) {
            this.licensed = value;
        }

    }

}
