
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for X3027DataCaptureDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027DataCaptureDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CaptureD" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="UserN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027DataCaptureDetails", propOrder = {
    "captureD",
    "userN",
    "userName"
})
public class X3027DataCaptureDetails {

    @XmlElement(name = "CaptureD", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar captureD;
    @XmlElement(name = "UserN", required = true)
    protected String userN;
    @XmlElement(name = "UserName", required = true)
    protected String userName;

    /**
     * Gets the value of the captureD property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCaptureD() {
        return captureD;
    }

    /**
     * Sets the value of the captureD property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCaptureD(XMLGregorianCalendar value) {
        this.captureD = value;
    }

    /**
     * Gets the value of the userN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserN() {
        return userN;
    }

    /**
     * Sets the value of the userN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserN(String value) {
        this.userN = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

}
