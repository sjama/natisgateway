
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicles" type="{http://tasima/common/ws/schema/}X5008VehicleDetail" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="messages" type="{http://tasima/common/ws/schema/}ThirdpartyInformationMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicles",
    "messages"
})
@XmlRootElement(name = "GetVehiclesAndLicenseExpiryDatesResponse")
public class GetVehiclesAndLicenseExpiryDatesResponse {

    protected List<X5008VehicleDetail> vehicles;
    protected List<ThirdpartyInformationMessage> messages;

    /**
     * Gets the value of the vehicles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X5008VehicleDetail }
     * 
     * 
     */
    public List<X5008VehicleDetail> getVehicles() {
        if (vehicles == null) {
            vehicles = new ArrayList<X5008VehicleDetail>();
        }
        return this.vehicles;
    }

    /**
     * Gets the value of the messages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ThirdpartyInformationMessage }
     * 
     * 
     */
    public List<ThirdpartyInformationMessage> getMessages() {
        if (messages == null) {
            messages = new ArrayList<ThirdpartyInformationMessage>();
        }
        return this.messages;
    }

}
