
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Response format for confirm vehicle details
 * 
 * <p>Java class for X3004ResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3004ResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="person">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idDocumentType" type="{http://tasima/common/ws/schema/}X3004CodeDesc"/>
 *                   &lt;element name="idDocumentNumber" type="{http://tasima/common/ws/schema/}X3004IdDocumentNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="titleHolder">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="surnameOrNameOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="confirm" type="{http://tasima/common/ws/schema/}X3004YesNoType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="owner">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="surnameOrNameOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="confirm" type="{http://tasima/common/ws/schema/}X3004YesNoType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="registerNumber" type="{http://tasima/common/ws/schema/}X3004RegisterNumber"/>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}X3004LicenceNumber" minOccurs="0"/>
 *                   &lt;element name="vinOrChassis" type="{http://tasima/common/ws/schema/}X3004VinOrChassisNumber" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3004ResponseDetail", propOrder = {
    "person",
    "titleHolder",
    "owner",
    "vehicle"
})
public class X3004ResponseDetail {

    @XmlElement(required = true)
    protected X3004ResponseDetail.Person person;
    @XmlElement(required = true)
    protected X3004ResponseDetail.TitleHolder titleHolder;
    @XmlElement(required = true)
    protected X3004ResponseDetail.Owner owner;
    @XmlElement(required = true)
    protected X3004ResponseDetail.Vehicle vehicle;

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link X3004ResponseDetail.Person }
     *     
     */
    public X3004ResponseDetail.Person getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3004ResponseDetail.Person }
     *     
     */
    public void setPerson(X3004ResponseDetail.Person value) {
        this.person = value;
    }

    /**
     * Gets the value of the titleHolder property.
     * 
     * @return
     *     possible object is
     *     {@link X3004ResponseDetail.TitleHolder }
     *     
     */
    public X3004ResponseDetail.TitleHolder getTitleHolder() {
        return titleHolder;
    }

    /**
     * Sets the value of the titleHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3004ResponseDetail.TitleHolder }
     *     
     */
    public void setTitleHolder(X3004ResponseDetail.TitleHolder value) {
        this.titleHolder = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link X3004ResponseDetail.Owner }
     *     
     */
    public X3004ResponseDetail.Owner getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3004ResponseDetail.Owner }
     *     
     */
    public void setOwner(X3004ResponseDetail.Owner value) {
        this.owner = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X3004ResponseDetail.Vehicle }
     *     
     */
    public X3004ResponseDetail.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3004ResponseDetail.Vehicle }
     *     
     */
    public void setVehicle(X3004ResponseDetail.Vehicle value) {
        this.vehicle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="surnameOrNameOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="confirm" type="{http://tasima/common/ws/schema/}X3004YesNoType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "surnameOrNameOfBusiness",
        "initials",
        "confirm"
    })
    public static class Owner {

        protected String surnameOrNameOfBusiness;
        protected String initials;
        @XmlElement(required = true)
        protected X3004YesNoType confirm;

        /**
         * Gets the value of the surnameOrNameOfBusiness property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurnameOrNameOfBusiness() {
            return surnameOrNameOfBusiness;
        }

        /**
         * Sets the value of the surnameOrNameOfBusiness property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurnameOrNameOfBusiness(String value) {
            this.surnameOrNameOfBusiness = value;
        }

        /**
         * Gets the value of the initials property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInitials() {
            return initials;
        }

        /**
         * Sets the value of the initials property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInitials(String value) {
            this.initials = value;
        }

        /**
         * Gets the value of the confirm property.
         * 
         * @return
         *     possible object is
         *     {@link X3004YesNoType }
         *     
         */
        public X3004YesNoType getConfirm() {
            return confirm;
        }

        /**
         * Sets the value of the confirm property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3004YesNoType }
         *     
         */
        public void setConfirm(X3004YesNoType value) {
            this.confirm = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idDocumentType" type="{http://tasima/common/ws/schema/}X3004CodeDesc"/>
     *         &lt;element name="idDocumentNumber" type="{http://tasima/common/ws/schema/}X3004IdDocumentNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumentType",
        "idDocumentNumber"
    })
    public static class Person {

        @XmlElement(required = true)
        protected X3004CodeDesc idDocumentType;
        @XmlElement(required = true)
        protected String idDocumentNumber;

        /**
         * Gets the value of the idDocumentType property.
         * 
         * @return
         *     possible object is
         *     {@link X3004CodeDesc }
         *     
         */
        public X3004CodeDesc getIdDocumentType() {
            return idDocumentType;
        }

        /**
         * Sets the value of the idDocumentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3004CodeDesc }
         *     
         */
        public void setIdDocumentType(X3004CodeDesc value) {
            this.idDocumentType = value;
        }

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="surnameOrNameOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="initials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="confirm" type="{http://tasima/common/ws/schema/}X3004YesNoType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "surnameOrNameOfBusiness",
        "initials",
        "confirm"
    })
    public static class TitleHolder {

        protected String surnameOrNameOfBusiness;
        protected String initials;
        @XmlElement(required = true)
        protected X3004YesNoType confirm;

        /**
         * Gets the value of the surnameOrNameOfBusiness property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurnameOrNameOfBusiness() {
            return surnameOrNameOfBusiness;
        }

        /**
         * Sets the value of the surnameOrNameOfBusiness property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurnameOrNameOfBusiness(String value) {
            this.surnameOrNameOfBusiness = value;
        }

        /**
         * Gets the value of the initials property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInitials() {
            return initials;
        }

        /**
         * Sets the value of the initials property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInitials(String value) {
            this.initials = value;
        }

        /**
         * Gets the value of the confirm property.
         * 
         * @return
         *     possible object is
         *     {@link X3004YesNoType }
         *     
         */
        public X3004YesNoType getConfirm() {
            return confirm;
        }

        /**
         * Sets the value of the confirm property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3004YesNoType }
         *     
         */
        public void setConfirm(X3004YesNoType value) {
            this.confirm = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="registerNumber" type="{http://tasima/common/ws/schema/}X3004RegisterNumber"/>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}X3004LicenceNumber" minOccurs="0"/>
     *         &lt;element name="vinOrChassis" type="{http://tasima/common/ws/schema/}X3004VinOrChassisNumber" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "registerNumber",
        "licenceNumber",
        "vinOrChassis"
    })
    public static class Vehicle {

        @XmlElement(required = true)
        protected String registerNumber;
        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;
        protected String vinOrChassis;

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

    }

}
