
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Add Chassis vehicle register to query
 * 
 * <p>Java class for E3024SearchOptionVehicleRegisterNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="E3024SearchOptionVehicleRegisterNumber">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tasima/common/ws/schema/}E3024SearchOptionType">
 *       &lt;sequence>
 *         &lt;element name="VehicleRegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "E3024SearchOptionVehicleRegisterNumber", propOrder = {
    "vehicleRegisterNumber"
})
public class E3024SearchOptionVehicleRegisterNumber
    extends E3024SearchOptionType
{

    @XmlElement(name = "VehicleRegisterNumber", required = true)
    protected String vehicleRegisterNumber;

    /**
     * Gets the value of the vehicleRegisterNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleRegisterNumber() {
        return vehicleRegisterNumber;
    }

    /**
     * Sets the value of the vehicleRegisterNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleRegisterNumber(String value) {
        this.vehicleRegisterNumber = value;
    }

}
