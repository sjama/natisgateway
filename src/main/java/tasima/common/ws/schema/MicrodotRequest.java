
package tasima.common.ws.schema;

import javax.xml.datatype.XMLGregorianCalendar;

public class MicrodotRequest {

    protected String vinOrChassisNumber;
    protected String microdotPIN;
    protected String effectiveDate;

    /**
     * Gets the value of the vinOrChassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinOrChassisNumber() {
        return vinOrChassisNumber;
    }

    /**
     * Sets the value of the vinOrChassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinOrChassisNumber(String value) {
        this.vinOrChassisNumber = value;
    }

    /**
     * Gets the value of the microdotPIN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMicrodotPIN() {
        return microdotPIN;
    }

    /**
     * Sets the value of the microdotPIN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMicrodotPIN(String value) {
        this.microdotPIN = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

}
