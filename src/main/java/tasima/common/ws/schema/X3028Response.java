
package tasima.common.ws.schema;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Infringement" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
 *                   &lt;element name="Date" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="Time" type="{http://tasima/common/ws/schema/}Time"/>
 *                   &lt;element name="Location">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="StreetNameA" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="40"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="Suburb" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="35"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="CityTown" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="30"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="IssAuthority" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Vehicle" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="LicenseNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Charge">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Classification">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="40"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="Description">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="350"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="DemeritPoints" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                                   &lt;totalDigits value="3"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="FeesList">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Debt" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="DebtType">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;maxLength value="40"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="DebtAmt" type="{http://tasima/common/ws/schema/}Money"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ConvFee" type="{http://tasima/common/ws/schema/}Money"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Payment" type="{http://tasima/common/ws/schema/}X3028Payment"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringement"
})
@XmlRootElement(name = "X3028Response")
public class X3028Response {

    @XmlElement(name = "Infringement")
    protected List<X3028Response.Infringement> infringement;

    /**
     * Gets the value of the infringement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infringement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfringement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X3028Response.Infringement }
     * 
     * 
     */
    public List<X3028Response.Infringement> getInfringement() {
        if (infringement == null) {
            infringement = new ArrayList<X3028Response.Infringement>();
        }
        return this.infringement;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
     *         &lt;element name="Date" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="Time" type="{http://tasima/common/ws/schema/}Time"/>
     *         &lt;element name="Location">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="StreetNameA" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="40"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="Suburb" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="35"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="CityTown" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="30"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="IssAuthority" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Vehicle" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="LicenseNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Charge">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Classification">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="40"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="Description">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="350"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="DemeritPoints" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *                         &lt;totalDigits value="3"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="FeesList">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Debt" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="DebtType">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;maxLength value="40"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="DebtAmt" type="{http://tasima/common/ws/schema/}Money"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ConvFee" type="{http://tasima/common/ws/schema/}Money"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Payment" type="{http://tasima/common/ws/schema/}X3028Payment"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "infringementNoticeNumber",
        "date",
        "time",
        "location",
        "vehicle",
        "charge",
        "feesList",
        "payment"
    })
    public static class Infringement {

        @XmlElement(name = "InfringementNoticeNumber", required = true)
        protected String infringementNoticeNumber;
        @XmlElement(name = "Date", required = true)
        protected XMLGregorianCalendar date;
        @XmlElement(name = "Time", required = true)
        protected XMLGregorianCalendar time;
        @XmlElement(name = "Location", required = true)
        protected X3028Response.Infringement.Location location;
        @XmlElement(name = "Vehicle")
        protected X3028Response.Infringement.Vehicle vehicle;
        @XmlElement(name = "Charge", required = true)
        protected X3028Response.Infringement.Charge charge;
        @XmlElement(name = "FeesList", required = true)
        protected X3028Response.Infringement.FeesList feesList;
        @XmlElement(name = "Payment", required = true)
        protected X3028Payment payment;

        /**
         * Gets the value of the infringementNoticeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringementNoticeNumber() {
            return infringementNoticeNumber;
        }

        /**
         * Sets the value of the infringementNoticeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringementNoticeNumber(String value) {
            this.infringementNoticeNumber = value;
        }

        /**
         * Gets the value of the date property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Sets the value of the date property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }

        /**
         * Gets the value of the time property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTime() {
            return time;
        }

        /**
         * Sets the value of the time property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTime(XMLGregorianCalendar value) {
            this.time = value;
        }

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link X3028Response.Infringement.Location }
         *     
         */
        public X3028Response.Infringement.Location getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3028Response.Infringement.Location }
         *     
         */
        public void setLocation(X3028Response.Infringement.Location value) {
            this.location = value;
        }

        /**
         * Gets the value of the vehicle property.
         * 
         * @return
         *     possible object is
         *     {@link X3028Response.Infringement.Vehicle }
         *     
         */
        public X3028Response.Infringement.Vehicle getVehicle() {
            return vehicle;
        }

        /**
         * Sets the value of the vehicle property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3028Response.Infringement.Vehicle }
         *     
         */
        public void setVehicle(X3028Response.Infringement.Vehicle value) {
            this.vehicle = value;
        }

        /**
         * Gets the value of the charge property.
         * 
         * @return
         *     possible object is
         *     {@link X3028Response.Infringement.Charge }
         *     
         */
        public X3028Response.Infringement.Charge getCharge() {
            return charge;
        }

        /**
         * Sets the value of the charge property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3028Response.Infringement.Charge }
         *     
         */
        public void setCharge(X3028Response.Infringement.Charge value) {
            this.charge = value;
        }

        /**
         * Gets the value of the feesList property.
         * 
         * @return
         *     possible object is
         *     {@link X3028Response.Infringement.FeesList }
         *     
         */
        public X3028Response.Infringement.FeesList getFeesList() {
            return feesList;
        }

        /**
         * Sets the value of the feesList property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3028Response.Infringement.FeesList }
         *     
         */
        public void setFeesList(X3028Response.Infringement.FeesList value) {
            this.feesList = value;
        }

        /**
         * Gets the value of the payment property.
         * 
         * @return
         *     possible object is
         *     {@link X3028Payment }
         *     
         */
        public X3028Payment getPayment() {
            return payment;
        }

        /**
         * Sets the value of the payment property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3028Payment }
         *     
         */
        public void setPayment(X3028Payment value) {
            this.payment = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Classification">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="40"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="Description">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="350"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="DemeritPoints" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
         *               &lt;totalDigits value="3"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "classification",
            "description",
            "demeritPoints"
        })
        public static class Charge {

            @XmlElement(name = "Classification", required = true)
            protected String classification;
            @XmlElement(name = "Description", required = true)
            protected String description;
            @XmlElement(name = "DemeritPoints")
            protected BigInteger demeritPoints;

            /**
             * Gets the value of the classification property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClassification() {
                return classification;
            }

            /**
             * Sets the value of the classification property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClassification(String value) {
                this.classification = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Gets the value of the demeritPoints property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDemeritPoints() {
                return demeritPoints;
            }

            /**
             * Sets the value of the demeritPoints property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDemeritPoints(BigInteger value) {
                this.demeritPoints = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Debt" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="DebtType">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;maxLength value="40"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="DebtAmt" type="{http://tasima/common/ws/schema/}Money"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ConvFee" type="{http://tasima/common/ws/schema/}Money"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "debt",
            "convFee"
        })
        public static class FeesList {

            @XmlElement(name = "Debt")
            protected List<X3028Response.Infringement.FeesList.Debt> debt;
            @XmlElement(name = "ConvFee", required = true)
            protected BigDecimal convFee;

            /**
             * Gets the value of the debt property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the debt property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDebt().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link X3028Response.Infringement.FeesList.Debt }
             * 
             * 
             */
            public List<X3028Response.Infringement.FeesList.Debt> getDebt() {
                if (debt == null) {
                    debt = new ArrayList<X3028Response.Infringement.FeesList.Debt>();
                }
                return this.debt;
            }

            /**
             * Gets the value of the convFee property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getConvFee() {
                return convFee;
            }

            /**
             * Sets the value of the convFee property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setConvFee(BigDecimal value) {
                this.convFee = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="DebtType">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;maxLength value="40"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="DebtAmt" type="{http://tasima/common/ws/schema/}Money"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "debtType",
                "debtAmt"
            })
            public static class Debt {

                @XmlElement(name = "DebtType", required = true)
                protected String debtType;
                @XmlElement(name = "DebtAmt", required = true)
                protected BigDecimal debtAmt;

                /**
                 * Gets the value of the debtType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDebtType() {
                    return debtType;
                }

                /**
                 * Sets the value of the debtType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDebtType(String value) {
                    this.debtType = value;
                }

                /**
                 * Gets the value of the debtAmt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDebtAmt() {
                    return debtAmt;
                }

                /**
                 * Sets the value of the debtAmt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDebtAmt(BigDecimal value) {
                    this.debtAmt = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="StreetNameA" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="40"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="Suburb" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="35"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="CityTown" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="30"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="IssAuthority" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "streetNameA",
            "suburb",
            "cityTown",
            "issAuthority"
        })
        public static class Location {

            @XmlElement(name = "StreetNameA")
            protected String streetNameA;
            @XmlElement(name = "Suburb")
            protected String suburb;
            @XmlElement(name = "CityTown")
            protected String cityTown;
            @XmlElement(name = "IssAuthority", required = true)
            protected CodeAndDescriptionElement issAuthority;

            /**
             * Gets the value of the streetNameA property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreetNameA() {
                return streetNameA;
            }

            /**
             * Sets the value of the streetNameA property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreetNameA(String value) {
                this.streetNameA = value;
            }

            /**
             * Gets the value of the suburb property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSuburb() {
                return suburb;
            }

            /**
             * Sets the value of the suburb property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSuburb(String value) {
                this.suburb = value;
            }

            /**
             * Gets the value of the cityTown property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityTown() {
                return cityTown;
            }

            /**
             * Sets the value of the cityTown property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityTown(String value) {
                this.cityTown = value;
            }

            /**
             * Gets the value of the issAuthority property.
             * 
             * @return
             *     possible object is
             *     {@link CodeAndDescriptionElement }
             *     
             */
            public CodeAndDescriptionElement getIssAuthority() {
                return issAuthority;
            }

            /**
             * Sets the value of the issAuthority property.
             * 
             * @param value
             *     allowed object is
             *     {@link CodeAndDescriptionElement }
             *     
             */
            public void setIssAuthority(CodeAndDescriptionElement value) {
                this.issAuthority = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="LicenseNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "licenseNumber"
        })
        public static class Vehicle {

            @XmlElement(name = "LicenseNumber")
            protected String licenseNumber;

            /**
             * Gets the value of the licenseNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLicenseNumber() {
                return licenseNumber;
            }

            /**
             * Sets the value of the licenseNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLicenseNumber(String value) {
                this.licenseNumber = value;
            }

        }

    }

}
