
package tasima.common.ws.schema;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringement" type="{http://tasima/common/ws/schema/}InfringementType" minOccurs="0"/>
 *         &lt;element name="maxInstalments" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="fees" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalAmountDue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringement",
    "maxInstalments",
    "fees",
    "totalAmountDue"
})
@XmlRootElement(name = "GetInfringementForInstalmentApplicationResponse")
public class GetInfringementForInstalmentApplicationResponse {

    protected InfringementType infringement;
    protected Integer maxInstalments;
    protected BigDecimal fees;
    protected BigDecimal totalAmountDue;

    /**
     * Gets the value of the infringement property.
     * 
     * @return
     *     possible object is
     *     {@link InfringementType }
     *     
     */
    public InfringementType getInfringement() {
        return infringement;
    }

    /**
     * Sets the value of the infringement property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfringementType }
     *     
     */
    public void setInfringement(InfringementType value) {
        this.infringement = value;
    }

    /**
     * Gets the value of the maxInstalments property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxInstalments() {
        return maxInstalments;
    }

    /**
     * Sets the value of the maxInstalments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxInstalments(Integer value) {
        this.maxInstalments = value;
    }

    /**
     * Gets the value of the fees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFees() {
        return fees;
    }

    /**
     * Sets the value of the fees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFees(BigDecimal value) {
        this.fees = value;
    }

    /**
     * Gets the value of the totalAmountDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmountDue() {
        return totalAmountDue;
    }

    /**
     * Sets the value of the totalAmountDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmountDue(BigDecimal value) {
        this.totalAmountDue = value;
    }

}
