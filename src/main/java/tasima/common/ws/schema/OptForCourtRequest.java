
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aarto10" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="summonsAddress" type="{http://tasima/common/ws/schema/}PersonAddress"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementNoticeNumber",
    "aarto10",
    "summonsAddress"
})
@XmlRootElement(name = "OptForCourtRequest")
public class OptForCourtRequest {

    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected byte[] aarto10;
    @XmlElement(required = true)
    protected PersonAddress summonsAddress;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the aarto10 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto10() {
        return aarto10;
    }

    /**
     * Sets the value of the aarto10 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto10(byte[] value) {
        this.aarto10 = value;
    }

    /**
     * Gets the value of the summonsAddress property.
     * 
     * @return
     *     possible object is
     *     {@link PersonAddress }
     *     
     */
    public PersonAddress getSummonsAddress() {
        return summonsAddress;
    }

    /**
     * Sets the value of the summonsAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAddress }
     *     
     */
    public void setSummonsAddress(PersonAddress value) {
        this.summonsAddress = value;
    }

}
