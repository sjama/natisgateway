
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dltcList" type="{http://tasima/common/ws/schema/}E0706_DltcInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dltcList"
})
@XmlRootElement(name = "QueryDltcResponse")
public class QueryDltcResponse {

    protected List<E0706DltcInfo> dltcList;

    /**
     * Gets the value of the dltcList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dltcList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDltcList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link E0706DltcInfo }
     * 
     * 
     */
    public List<E0706DltcInfo> getDltcList() {
        if (dltcList == null) {
            dltcList = new ArrayList<E0706DltcInfo>();
        }
        return this.dltcList;
    }

}
