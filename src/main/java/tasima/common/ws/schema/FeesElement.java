
package tasima.common.ws.schema;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeesElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeesElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalOutstandingFees" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="fee" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="transactionTypeCode" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="amount" type="{http://tasima/common/ws/schema/}Money"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeesElement", propOrder = {
    "totalOutstandingFees",
    "fee"
})
public class FeesElement {

    @XmlElement(required = true)
    protected BigDecimal totalOutstandingFees;
    @XmlElement(required = true)
    protected List<FeesElement.Fee> fee;

    /**
     * Gets the value of the totalOutstandingFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalOutstandingFees() {
        return totalOutstandingFees;
    }

    /**
     * Sets the value of the totalOutstandingFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalOutstandingFees(BigDecimal value) {
        this.totalOutstandingFees = value;
    }

    /**
     * Gets the value of the fee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeesElement.Fee }
     * 
     * 
     */
    public List<FeesElement.Fee> getFee() {
        if (fee == null) {
            fee = new ArrayList<FeesElement.Fee>();
        }
        return this.fee;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="transactionTypeCode" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="amount" type="{http://tasima/common/ws/schema/}Money"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transactionTypeCode",
        "amount"
    })
    public static class Fee {

        @XmlElement(required = true)
        protected CodeAndDescriptionElement transactionTypeCode;
        @XmlElement(required = true)
        protected BigDecimal amount;

        /**
         * Gets the value of the transactionTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getTransactionTypeCode() {
            return transactionTypeCode;
        }

        /**
         * Sets the value of the transactionTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setTransactionTypeCode(CodeAndDescriptionElement value) {
            this.transactionTypeCode = value;
        }

        /**
         * Gets the value of the amount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Sets the value of the amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

    }

}
