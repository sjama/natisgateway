
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *         &lt;element name="idDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *         &lt;element name="driversLicenceNumber" type="{http://tasima/common/ws/schema/}DriversLicenceNumberElement" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idDocumentTypeCode",
    "idDocumentNumber",
    "driversLicenceNumber"
})
@XmlRootElement(name = "QueryPersonToNominateRequest")
public class QueryPersonToNominateRequest {

    @XmlElement(required = true)
    protected String idDocumentTypeCode;
    @XmlElement(required = true)
    protected String idDocumentNumber;
    protected String driversLicenceNumber;

    /**
     * Gets the value of the idDocumentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentTypeCode() {
        return idDocumentTypeCode;
    }

    /**
     * Sets the value of the idDocumentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentTypeCode(String value) {
        this.idDocumentTypeCode = value;
    }

    /**
     * Gets the value of the idDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentNumber() {
        return idDocumentNumber;
    }

    /**
     * Sets the value of the idDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentNumber(String value) {
        this.idDocumentNumber = value;
    }

    /**
     * Gets the value of the driversLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriversLicenceNumber() {
        return driversLicenceNumber;
    }

    /**
     * Sets the value of the driversLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriversLicenceNumber(String value) {
        this.driversLicenceNumber = value;
    }

}
