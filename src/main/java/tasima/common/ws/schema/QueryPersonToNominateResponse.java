
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExecutionResult" type="{http://tasima/common/ws/schema/}ExecutionResult"/>
 *         &lt;element name="Result" type="{http://tasima/common/ws/schema/}QueryPersonToNominateResponseDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "executionResult",
    "result"
})
@XmlRootElement(name = "QueryPersonToNominateResponse")
public class QueryPersonToNominateResponse {

    @XmlElement(name = "ExecutionResult", required = true)
    protected ExecutionResult executionResult;
    @XmlElement(name = "Result")
    protected QueryPersonToNominateResponseDetail result;

    /**
     * Gets the value of the executionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionResult }
     *     
     */
    public ExecutionResult getExecutionResult() {
        return executionResult;
    }

    /**
     * Sets the value of the executionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionResult }
     *     
     */
    public void setExecutionResult(ExecutionResult value) {
        this.executionResult = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link QueryPersonToNominateResponseDetail }
     *     
     */
    public QueryPersonToNominateResponseDetail getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryPersonToNominateResponseDetail }
     *     
     */
    public void setResult(QueryPersonToNominateResponseDetail value) {
        this.result = value;
    }

}
