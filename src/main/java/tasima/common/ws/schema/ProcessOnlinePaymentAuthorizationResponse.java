
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Messages" type="{http://tasima/common/ws/schema/}InformationMessage" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SuccessStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messages",
    "successStatus"
})
@XmlRootElement(name = "ProcessOnlinePaymentAuthorizationResponse")
public class ProcessOnlinePaymentAuthorizationResponse {

    @XmlElement(name = "Messages")
    protected List<InformationMessage> messages;
    @XmlElement(name = "SuccessStatus")
    protected boolean successStatus;

    /**
     * Gets the value of the messages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformationMessage }
     * 
     * 
     */
    public List<InformationMessage> getMessages() {
        if (messages == null) {
            messages = new ArrayList<InformationMessage>();
        }
        return this.messages;
    }

    /**
     * Gets the value of the successStatus property.
     * 
     */
    public boolean isSuccessStatus() {
        return successStatus;
    }

    /**
     * Sets the value of the successStatus property.
     * 
     */
    public void setSuccessStatus(boolean value) {
        this.successStatus = value;
    }

}
