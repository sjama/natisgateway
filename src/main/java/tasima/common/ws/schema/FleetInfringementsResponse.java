
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExecutionResult" type="{http://tasima/common/ws/schema/}ExecutionResult"/>
 *         &lt;element name="infringements" type="{http://tasima/common/ws/schema/}InfringementType" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "executionResult",
    "infringements"
})
@XmlRootElement(name = "FleetInfringementsResponse")
public class FleetInfringementsResponse {

    @XmlElement(name = "ExecutionResult", required = true)
    protected ExecutionResult executionResult;
    protected List<InfringementType> infringements;

    /**
     * Gets the value of the executionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionResult }
     *     
     */
    public ExecutionResult getExecutionResult() {
        return executionResult;
    }

    /**
     * Sets the value of the executionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionResult }
     *     
     */
    public void setExecutionResult(ExecutionResult value) {
        this.executionResult = value;
    }

    /**
     * Gets the value of the infringements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infringements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfringements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InfringementType }
     * 
     * 
     */
    public List<InfringementType> getInfringements() {
        if (infringements == null) {
            infringements = new ArrayList<InfringementType>();
        }
        return this.infringements;
    }

}
