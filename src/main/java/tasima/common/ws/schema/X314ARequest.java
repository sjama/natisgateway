
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Receiver">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                   &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
 *                   &lt;element name="ControlNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChangeDate" type="{http://tasima/common/ws/schema/}Date"/>
 *         &lt;element name="ReceiverDetails" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Proxy" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                             &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Representative" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                             &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "receiver",
    "vehicle",
    "changeDate",
    "receiverDetails"
})
@XmlRootElement(name = "X314ARequest")
public class X314ARequest {

    @XmlElement(name = "Receiver", required = true)
    protected X314ARequest.Receiver receiver;
    @XmlElement(name = "Vehicle", required = true)
    protected X314ARequest.Vehicle vehicle;
    @XmlElement(name = "ChangeDate", required = true)
    protected XMLGregorianCalendar changeDate;
    @XmlElement(name = "ReceiverDetails")
    protected X314ARequest.ReceiverDetails receiverDetails;

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link X314ARequest.Receiver }
     *     
     */
    public X314ARequest.Receiver getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link X314ARequest.Receiver }
     *     
     */
    public void setReceiver(X314ARequest.Receiver value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X314ARequest.Vehicle }
     *     
     */
    public X314ARequest.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X314ARequest.Vehicle }
     *     
     */
    public void setVehicle(X314ARequest.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChangeDate(XMLGregorianCalendar value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the receiverDetails property.
     * 
     * @return
     *     possible object is
     *     {@link X314ARequest.ReceiverDetails }
     *     
     */
    public X314ARequest.ReceiverDetails getReceiverDetails() {
        return receiverDetails;
    }

    /**
     * Sets the value of the receiverDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link X314ARequest.ReceiverDetails }
     *     
     */
    public void setReceiverDetails(X314ARequest.ReceiverDetails value) {
        this.receiverDetails = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *         &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumentType",
        "idDocumentNumber"
    })
    public static class Receiver {

        @XmlElement(name = "IdDocumentType", required = true)
        protected String idDocumentType;
        @XmlElement(name = "IdDocumentNumber", required = true)
        protected String idDocumentNumber;

        /**
         * Gets the value of the idDocumentType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentType() {
            return idDocumentType;
        }

        /**
         * Sets the value of the idDocumentType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentType(String value) {
            this.idDocumentType = value;
        }

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Proxy" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *                   &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Representative" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *                   &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class ReceiverDetails {

        @XmlElement(name = "Proxy")
        protected X314ARequest.ReceiverDetails.Proxy proxy;
        @XmlElement(name = "Representative")
        protected X314ARequest.ReceiverDetails.Representative representative;

        /**
         * Gets the value of the proxy property.
         * 
         * @return
         *     possible object is
         *     {@link X314ARequest.ReceiverDetails.Proxy }
         *     
         */
        public X314ARequest.ReceiverDetails.Proxy getProxy() {
            return proxy;
        }

        /**
         * Sets the value of the proxy property.
         * 
         * @param value
         *     allowed object is
         *     {@link X314ARequest.ReceiverDetails.Proxy }
         *     
         */
        public void setProxy(X314ARequest.ReceiverDetails.Proxy value) {
            this.proxy = value;
        }

        /**
         * Gets the value of the representative property.
         * 
         * @return
         *     possible object is
         *     {@link X314ARequest.ReceiverDetails.Representative }
         *     
         */
        public X314ARequest.ReceiverDetails.Representative getRepresentative() {
            return representative;
        }

        /**
         * Sets the value of the representative property.
         * 
         * @param value
         *     allowed object is
         *     {@link X314ARequest.ReceiverDetails.Representative }
         *     
         */
        public void setRepresentative(X314ARequest.ReceiverDetails.Representative value) {
            this.representative = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
         *         &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idDocumentType",
            "idDocumentNumber"
        })
        public static class Proxy {

            @XmlElement(name = "IdDocumentType", required = true)
            protected String idDocumentType;
            @XmlElement(name = "IdDocumentNumber", required = true)
            protected String idDocumentNumber;

            /**
             * Gets the value of the idDocumentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentType() {
                return idDocumentType;
            }

            /**
             * Sets the value of the idDocumentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentType(String value) {
                this.idDocumentType = value;
            }

            /**
             * Gets the value of the idDocumentNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentNumber() {
                return idDocumentNumber;
            }

            /**
             * Sets the value of the idDocumentNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentNumber(String value) {
                this.idDocumentNumber = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
         *         &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idDocumentType",
            "idDocumentNumber"
        })
        public static class Representative {

            @XmlElement(name = "IdDocumentType", required = true)
            protected String idDocumentType;
            @XmlElement(name = "IdDocumentNumber", required = true)
            protected String idDocumentNumber;

            /**
             * Gets the value of the idDocumentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentType() {
                return idDocumentType;
            }

            /**
             * Sets the value of the idDocumentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentType(String value) {
                this.idDocumentType = value;
            }

            /**
             * Gets the value of the idDocumentNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentNumber() {
                return idDocumentNumber;
            }

            /**
             * Sets the value of the idDocumentNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentNumber(String value) {
                this.idDocumentNumber = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
     *         &lt;element name="ControlNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vinOrChassis",
        "controlNumber",
        "registerNumber",
        "licenceNumber"
    })
    public static class Vehicle {

        @XmlElement(name = "VinOrChassis", required = true)
        protected String vinOrChassis;
        @XmlElement(name = "ControlNumber", required = true)
        protected String controlNumber;
        @XmlElement(name = "RegisterNumber")
        protected String registerNumber;
        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the controlNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getControlNumber() {
            return controlNumber;
        }

        /**
         * Sets the value of the controlNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setControlNumber(String value) {
            this.controlNumber = value;
        }

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

    }

}
