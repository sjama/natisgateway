
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for x3061DrivingLicenceCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="x3061DrivingLicenceCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="B"/>
 *     &lt;enumeration value="EB"/>
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="C1"/>
 *     &lt;enumeration value="EC"/>
 *     &lt;enumeration value="EC1"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "x3061DrivingLicenceCodeType")
@XmlEnum
public enum X3061DrivingLicenceCodeType {


    /**
     * Motor vehicle, not exceeding 3500 Kg
     * 
     */
    B("B"),

    /**
     * Articulated MV, not exceeding 3500 Kg
     * 
     */
    EB("EB"),

    /**
     * Motor vehicle, exceeding 16000 Kg
     * 
     */
    C("C"),

    /**
     * Motor vehicle, 3500 Kg gt 16000 Kg
     * 
     */
    @XmlEnumValue("C1")
    C_1("C1"),

    /**
     * Articulated MV, exceeding 16000 Kg
     * 
     */
    EC("EC"),

    /**
     * Articulated MV, 3500 Kg gt 16000 Kg
     * 
     */
    @XmlEnumValue("EC1")
    EC_1("EC1");
    private final String value;

    X3061DrivingLicenceCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static X3061DrivingLicenceCodeType fromValue(String v) {
        for (X3061DrivingLicenceCodeType c: X3061DrivingLicenceCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
