
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="executionResult" type="{http://tasima/common/ws/schema/}ExecutionResult"/>
 *         &lt;element name="response" type="{http://tasima/common/ws/schema/}TestPreBookingAvailabilityResponseDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "executionResult",
    "response"
})
@XmlRootElement(name = "TestPreBookingAvailabilityResponse")
public class TestPreBookingAvailabilityResponse {

    @XmlElement(required = true)
    protected ExecutionResult executionResult;
    protected TestPreBookingAvailabilityResponseDetail response;

    /**
     * Gets the value of the executionResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionResult }
     *     
     */
    public ExecutionResult getExecutionResult() {
        return executionResult;
    }

    /**
     * Sets the value of the executionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionResult }
     *     
     */
    public void setExecutionResult(ExecutionResult value) {
        this.executionResult = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link TestPreBookingAvailabilityResponseDetail }
     *     
     */
    public TestPreBookingAvailabilityResponseDetail getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link TestPreBookingAvailabilityResponseDetail }
     *     
     */
    public void setResponse(TestPreBookingAvailabilityResponseDetail value) {
        this.response = value;
    }

}
