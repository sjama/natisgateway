
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request based on infringement notice number
 * 
 * <p>Java class for SearchOptionInfringementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchOptionInfringementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchOptionInfringementType", propOrder = {
    "infringementNoticeNumber"
})
public class SearchOptionInfringementType {

    @XmlElement(name = "InfringementNoticeNumber", required = true)
    protected String infringementNoticeNumber;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

}
