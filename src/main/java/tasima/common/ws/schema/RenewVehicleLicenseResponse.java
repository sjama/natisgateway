
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="preferedAddress" type="{http://tasima/common/ws/schema/}X5009PersonAddress"/>
 *         &lt;element name="invoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://tasima/common/ws/schema/}OperationStatus"/>
 *         &lt;element name="messages" type="{http://tasima/common/ws/schema/}ThirdpartyInformationMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "preferedAddress",
    "invoiceNumber",
    "transactionNumber",
    "status",
    "messages"
})
@XmlRootElement(name = "RenewVehicleLicenseResponse")
public class RenewVehicleLicenseResponse {

    @XmlElement(required = true)
    protected X5009PersonAddress preferedAddress;
    protected String invoiceNumber;
    protected String transactionNumber;
    @XmlElement(required = true)
    protected OperationStatus status;
    protected List<ThirdpartyInformationMessage> messages;

    /**
     * Gets the value of the preferedAddress property.
     * 
     * @return
     *     possible object is
     *     {@link X5009PersonAddress }
     *     
     */
    public X5009PersonAddress getPreferedAddress() {
        return preferedAddress;
    }

    /**
     * Sets the value of the preferedAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link X5009PersonAddress }
     *     
     */
    public void setPreferedAddress(X5009PersonAddress value) {
        this.preferedAddress = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the transactionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * Sets the value of the transactionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionNumber(String value) {
        this.transactionNumber = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link OperationStatus }
     *     
     */
    public OperationStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationStatus }
     *     
     */
    public void setStatus(OperationStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ThirdpartyInformationMessage }
     * 
     * 
     */
    public List<ThirdpartyInformationMessage> getMessages() {
        if (messages == null) {
            messages = new ArrayList<ThirdpartyInformationMessage>();
        }
        return this.messages;
    }

}
