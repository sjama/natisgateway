
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for X3027SummonsCertificate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027SummonsCertificate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SummonsCertN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SummonsCertStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SummonsCertStatChgD" type="{http://tasima/common/ws/schema/}Date"/>
 *         &lt;element name="SummonsAddressLine1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SummonsAddressLine2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SummonsAddressLine3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SummonsSuburb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SummonsCityTown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SummonsPostalCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[\d]{4}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027SummonsCertificate", propOrder = {
    "summonsCertN",
    "summonsCertStat",
    "summonsCertStatChgD",
    "summonsAddressLine1",
    "summonsAddressLine2",
    "summonsAddressLine3",
    "summonsSuburb",
    "summonsCityTown",
    "summonsPostalCode"
})
public class X3027SummonsCertificate {

    @XmlElement(name = "SummonsCertN", required = true)
    protected String summonsCertN;
    @XmlElement(name = "SummonsCertStat", required = true)
    protected String summonsCertStat;
    @XmlElement(name = "SummonsCertStatChgD", required = true)
    protected XMLGregorianCalendar summonsCertStatChgD;
    @XmlElement(name = "SummonsAddressLine1", required = true)
    protected String summonsAddressLine1;
    @XmlElement(name = "SummonsAddressLine2")
    protected String summonsAddressLine2;
    @XmlElement(name = "SummonsAddressLine3")
    protected String summonsAddressLine3;
    @XmlElement(name = "SummonsSuburb")
    protected String summonsSuburb;
    @XmlElement(name = "SummonsCityTown")
    protected String summonsCityTown;
    @XmlElement(name = "SummonsPostalCode", required = true)
    protected String summonsPostalCode;

    /**
     * Gets the value of the summonsCertN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsCertN() {
        return summonsCertN;
    }

    /**
     * Sets the value of the summonsCertN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsCertN(String value) {
        this.summonsCertN = value;
    }

    /**
     * Gets the value of the summonsCertStat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsCertStat() {
        return summonsCertStat;
    }

    /**
     * Sets the value of the summonsCertStat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsCertStat(String value) {
        this.summonsCertStat = value;
    }

    /**
     * Gets the value of the summonsCertStatChgD property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSummonsCertStatChgD() {
        return summonsCertStatChgD;
    }

    /**
     * Sets the value of the summonsCertStatChgD property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSummonsCertStatChgD(XMLGregorianCalendar value) {
        this.summonsCertStatChgD = value;
    }

    /**
     * Gets the value of the summonsAddressLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsAddressLine1() {
        return summonsAddressLine1;
    }

    /**
     * Sets the value of the summonsAddressLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsAddressLine1(String value) {
        this.summonsAddressLine1 = value;
    }

    /**
     * Gets the value of the summonsAddressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsAddressLine2() {
        return summonsAddressLine2;
    }

    /**
     * Sets the value of the summonsAddressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsAddressLine2(String value) {
        this.summonsAddressLine2 = value;
    }

    /**
     * Gets the value of the summonsAddressLine3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsAddressLine3() {
        return summonsAddressLine3;
    }

    /**
     * Sets the value of the summonsAddressLine3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsAddressLine3(String value) {
        this.summonsAddressLine3 = value;
    }

    /**
     * Gets the value of the summonsSuburb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsSuburb() {
        return summonsSuburb;
    }

    /**
     * Sets the value of the summonsSuburb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsSuburb(String value) {
        this.summonsSuburb = value;
    }

    /**
     * Gets the value of the summonsCityTown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsCityTown() {
        return summonsCityTown;
    }

    /**
     * Sets the value of the summonsCityTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsCityTown(String value) {
        this.summonsCityTown = value;
    }

    /**
     * Gets the value of the summonsPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummonsPostalCode() {
        return summonsPostalCode;
    }

    /**
     * Sets the value of the summonsPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummonsPostalCode(String value) {
        this.summonsPostalCode = value;
    }

}
