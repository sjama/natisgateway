
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Response detail for X31A7 request
 * 
 * <p>Java class for X31A7Result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X31A7Result">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorisationStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="AuthorisationExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X31A7Result", propOrder = {
    "authorisationStartDate",
    "authorisationExpiryDate"
})
public class X31A7Result {

    @XmlElement(name = "AuthorisationStartDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar authorisationStartDate;
    @XmlElement(name = "AuthorisationExpiryDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar authorisationExpiryDate;

    /**
     * Gets the value of the authorisationStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAuthorisationStartDate() {
        return authorisationStartDate;
    }

    /**
     * Sets the value of the authorisationStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAuthorisationStartDate(XMLGregorianCalendar value) {
        this.authorisationStartDate = value;
    }

    /**
     * Gets the value of the authorisationExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAuthorisationExpiryDate() {
        return authorisationExpiryDate;
    }

    /**
     * Sets the value of the authorisationExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAuthorisationExpiryDate(XMLGregorianCalendar value) {
        this.authorisationExpiryDate = value;
    }

}
