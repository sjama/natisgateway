
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Owner">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdDocType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                   &lt;element name="IdDocNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OwnerProxy" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdDocType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                   &lt;element name="IdDocNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OwnerRepresentative" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdDocType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                   &lt;element name="IdDocNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="ControlNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TravelDateFrom" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TravelDateTo" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "owner",
    "ownerProxy",
    "ownerRepresentative",
    "vehicle",
    "travelDateFrom",
    "travelDateTo"
})
@XmlRootElement(name = "X31A7Request")
public class X31A7Request {

    @XmlElement(name = "Owner", required = true)
    protected X31A7Request.Owner owner;
    @XmlElement(name = "OwnerProxy")
    protected X31A7Request.OwnerProxy ownerProxy;
    @XmlElement(name = "OwnerRepresentative")
    protected X31A7Request.OwnerRepresentative ownerRepresentative;
    @XmlElement(name = "Vehicle", required = true)
    protected X31A7Request.Vehicle vehicle;
    @XmlElement(name = "TravelDateFrom", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar travelDateFrom;
    @XmlElement(name = "TravelDateTo", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar travelDateTo;

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link X31A7Request.Owner }
     *     
     */
    public X31A7Request.Owner getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link X31A7Request.Owner }
     *     
     */
    public void setOwner(X31A7Request.Owner value) {
        this.owner = value;
    }

    /**
     * Gets the value of the ownerProxy property.
     * 
     * @return
     *     possible object is
     *     {@link X31A7Request.OwnerProxy }
     *     
     */
    public X31A7Request.OwnerProxy getOwnerProxy() {
        return ownerProxy;
    }

    /**
     * Sets the value of the ownerProxy property.
     * 
     * @param value
     *     allowed object is
     *     {@link X31A7Request.OwnerProxy }
     *     
     */
    public void setOwnerProxy(X31A7Request.OwnerProxy value) {
        this.ownerProxy = value;
    }

    /**
     * Gets the value of the ownerRepresentative property.
     * 
     * @return
     *     possible object is
     *     {@link X31A7Request.OwnerRepresentative }
     *     
     */
    public X31A7Request.OwnerRepresentative getOwnerRepresentative() {
        return ownerRepresentative;
    }

    /**
     * Sets the value of the ownerRepresentative property.
     * 
     * @param value
     *     allowed object is
     *     {@link X31A7Request.OwnerRepresentative }
     *     
     */
    public void setOwnerRepresentative(X31A7Request.OwnerRepresentative value) {
        this.ownerRepresentative = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X31A7Request.Vehicle }
     *     
     */
    public X31A7Request.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X31A7Request.Vehicle }
     *     
     */
    public void setVehicle(X31A7Request.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the travelDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTravelDateFrom() {
        return travelDateFrom;
    }

    /**
     * Sets the value of the travelDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTravelDateFrom(XMLGregorianCalendar value) {
        this.travelDateFrom = value;
    }

    /**
     * Gets the value of the travelDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTravelDateTo() {
        return travelDateTo;
    }

    /**
     * Sets the value of the travelDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTravelDateTo(XMLGregorianCalendar value) {
        this.travelDateTo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *         &lt;element name="IdDocNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocType",
        "idDocNumber"
    })
    public static class Owner {

        @XmlElement(name = "IdDocType", required = true)
        protected String idDocType;
        @XmlElement(name = "IdDocNumber", required = true)
        protected String idDocNumber;

        /**
         * Gets the value of the idDocType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocType() {
            return idDocType;
        }

        /**
         * Sets the value of the idDocType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocType(String value) {
            this.idDocType = value;
        }

        /**
         * Gets the value of the idDocNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocNumber() {
            return idDocNumber;
        }

        /**
         * Sets the value of the idDocNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocNumber(String value) {
            this.idDocNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *         &lt;element name="IdDocNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocType",
        "idDocNumber"
    })
    public static class OwnerProxy {

        @XmlElement(name = "IdDocType", required = true)
        protected String idDocType;
        @XmlElement(name = "IdDocNumber", required = true)
        protected String idDocNumber;

        /**
         * Gets the value of the idDocType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocType() {
            return idDocType;
        }

        /**
         * Sets the value of the idDocType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocType(String value) {
            this.idDocType = value;
        }

        /**
         * Gets the value of the idDocNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocNumber() {
            return idDocNumber;
        }

        /**
         * Sets the value of the idDocNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocNumber(String value) {
            this.idDocNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *         &lt;element name="IdDocNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocType",
        "idDocNumber"
    })
    public static class OwnerRepresentative {

        @XmlElement(name = "IdDocType", required = true)
        protected String idDocType;
        @XmlElement(name = "IdDocNumber", required = true)
        protected String idDocNumber;

        /**
         * Gets the value of the idDocType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocType() {
            return idDocType;
        }

        /**
         * Sets the value of the idDocType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocType(String value) {
            this.idDocType = value;
        }

        /**
         * Gets the value of the idDocNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocNumber() {
            return idDocNumber;
        }

        /**
         * Sets the value of the idDocNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocNumber(String value) {
            this.idDocNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="ControlNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vinOrChassis",
        "registerNumber",
        "licenceNumber",
        "controlNumber"
    })
    public static class Vehicle {

        @XmlElement(name = "VinOrChassis", required = true)
        protected String vinOrChassis;
        @XmlElement(name = "RegisterNumber")
        protected String registerNumber;
        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;
        @XmlElement(name = "ControlNumber", required = true)
        protected String controlNumber;

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the controlNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getControlNumber() {
            return controlNumber;
        }

        /**
         * Sets the value of the controlNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setControlNumber(String value) {
            this.controlNumber = value;
        }

    }

}
