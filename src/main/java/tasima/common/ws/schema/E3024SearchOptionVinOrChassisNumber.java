
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Add Chassis number or VIN to query
 * 
 * <p>Java class for E3024SearchOptionVinOrChassisNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="E3024SearchOptionVinOrChassisNumber">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tasima/common/ws/schema/}E3024SearchOptionType">
 *       &lt;sequence>
 *         &lt;element name="VinOrChassisNumber" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "E3024SearchOptionVinOrChassisNumber", propOrder = {
    "vinOrChassisNumber"
})
public class E3024SearchOptionVinOrChassisNumber
    extends E3024SearchOptionType
{

    @XmlElement(name = "VinOrChassisNumber", required = true)
    protected String vinOrChassisNumber;

    /**
     * Gets the value of the vinOrChassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVinOrChassisNumber() {
        return vinOrChassisNumber;
    }

    /**
     * Sets the value of the vinOrChassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVinOrChassisNumber(String value) {
        this.vinOrChassisNumber = value;
    }

}
