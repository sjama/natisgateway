
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="revocationApplicationReason" type="{http://tasima/common/ws/schema/}RevocationApplicationReason"/>
 *         &lt;element name="aarto14" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementNoticeNumber",
    "revocationApplicationReason",
    "aarto14"
})
@XmlRootElement(name = "ApplyForRevocationRequest")
public class ApplyForRevocationRequest {

    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected String revocationApplicationReason;
    @XmlElement(required = true)
    protected byte[] aarto14;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the revocationApplicationReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevocationApplicationReason() {
        return revocationApplicationReason;
    }

    /**
     * Sets the value of the revocationApplicationReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevocationApplicationReason(String value) {
        this.revocationApplicationReason = value;
    }

    /**
     * Gets the value of the aarto14 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto14() {
        return aarto14;
    }

    /**
     * Sets the value of the aarto14 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto14(byte[] value) {
        this.aarto14 = value;
    }

}
