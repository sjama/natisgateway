
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * Request data based on infringer identification particulars
 * 
 * 
 * <p>Java class for X3026SearchOptionInfringer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3026SearchOptionInfringer">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tasima/common/ws/schema/}X3026SearchOptionType">
 *       &lt;sequence>
 *         &lt;element name="IdDocTypeCd" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *         &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *         &lt;element name="InfringementFromDate" type="{http://tasima/common/ws/schema/}Date"/>
 *         &lt;element name="InfringementToDate" type="{http://tasima/common/ws/schema/}Date"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3026SearchOptionInfringer", propOrder = {
    "idDocTypeCd",
    "idDocN",
    "infringementFromDate",
    "infringementToDate"
})
public class X3026SearchOptionInfringer
    extends X3026SearchOptionType
{

    @XmlElement(name = "IdDocTypeCd", required = true)
    protected String idDocTypeCd;
    @XmlElement(name = "IdDocN", required = true)
    protected String idDocN;
    @XmlElement(name = "InfringementFromDate", required = true)
    protected XMLGregorianCalendar infringementFromDate;
    @XmlElement(name = "InfringementToDate", required = true)
    protected XMLGregorianCalendar infringementToDate;

    /**
     * Gets the value of the idDocTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocTypeCd() {
        return idDocTypeCd;
    }

    /**
     * Sets the value of the idDocTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocTypeCd(String value) {
        this.idDocTypeCd = value;
    }

    /**
     * Gets the value of the idDocN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocN() {
        return idDocN;
    }

    /**
     * Sets the value of the idDocN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocN(String value) {
        this.idDocN = value;
    }

    /**
     * Gets the value of the infringementFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementFromDate() {
        return infringementFromDate;
    }

    /**
     * Sets the value of the infringementFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementFromDate(XMLGregorianCalendar value) {
        this.infringementFromDate = value;
    }

    /**
     * Gets the value of the infringementToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringementToDate() {
        return infringementToDate;
    }

    /**
     * Sets the value of the infringementToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringementToDate(XMLGregorianCalendar value) {
        this.infringementToDate = value;
    }

}
