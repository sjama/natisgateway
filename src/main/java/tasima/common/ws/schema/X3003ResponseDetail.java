
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for X3003ResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3003ResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}X3003RegisterNumberElement"/>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}X3003VinOrChassisNumber" minOccurs="0"/>
 *                   &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}X3003EngineNumber" minOccurs="0"/>
 *                   &lt;element name="Make" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="ModelName" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="Category" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="Driven" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="Description" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="NetPower" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="EngineDisplacement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="FuelType" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="GVM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AxlesTotal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="NoOfWheels" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OverallWidth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RoadworthyStatus" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="Tare" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="MainColour" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="AxlesDriven" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OverallLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OverallHeight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RoadworthyTestDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="RoadworthyStatusDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="SapMark" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="SapMarkDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="SapClearanceStatus" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="SapClearanceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="LifeStatus" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="VehicleState" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="VehicleStateDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="RegistrationType" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
 *                   &lt;element name="VehicleCertificateNumber" type="{http://tasima/common/ws/schema/}X3003ControlNumber" minOccurs="0"/>
 *                   &lt;element name="LicenceChangeDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="LicenceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="PrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="PrePrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3003ResponseDetail", propOrder = {
    "vehicle"
})
public class X3003ResponseDetail {

    @XmlElement(name = "Vehicle", required = true)
    protected X3003ResponseDetail.Vehicle vehicle;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X3003ResponseDetail.Vehicle }
     *     
     */
    public X3003ResponseDetail.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3003ResponseDetail.Vehicle }
     *     
     */
    public void setVehicle(X3003ResponseDetail.Vehicle value) {
        this.vehicle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}X3003RegisterNumberElement"/>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}X3003VinOrChassisNumber" minOccurs="0"/>
     *         &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}X3003EngineNumber" minOccurs="0"/>
     *         &lt;element name="Make" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="ModelName" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="Category" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="Driven" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="Description" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="NetPower" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="EngineDisplacement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="FuelType" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="GVM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AxlesTotal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="NoOfWheels" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OverallWidth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RoadworthyStatus" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="Tare" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="MainColour" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="AxlesDriven" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OverallLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OverallHeight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RoadworthyTestDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="RoadworthyStatusDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="SapMark" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="SapMarkDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="SapClearanceStatus" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="SapClearanceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="LifeStatus" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="VehicleState" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="VehicleStateDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="RegistrationType" type="{http://tasima/common/ws/schema/}X3003CodeAndDescriptionElement"/>
     *         &lt;element name="VehicleCertificateNumber" type="{http://tasima/common/ws/schema/}X3003ControlNumber" minOccurs="0"/>
     *         &lt;element name="LicenceChangeDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="LicenceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="PrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="PrePrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}X3003LicenceNumberElement" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Vehicle {

        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;
        @XmlElement(name = "RegisterNumber", required = true)
        protected String registerNumber;
        @XmlElement(name = "VinOrChassis")
        protected String vinOrChassis;
        @XmlElement(name = "EngineNumber")
        protected String engineNumber;
        @XmlElement(name = "Make", required = true)
        protected X3003CodeAndDescriptionElement make;
        @XmlElement(name = "ModelName", required = true)
        protected X3003CodeAndDescriptionElement modelName;
        @XmlElement(name = "Category", required = true)
        protected X3003CodeAndDescriptionElement category;
        @XmlElement(name = "Driven", required = true)
        protected X3003CodeAndDescriptionElement driven;
        @XmlElement(name = "Description", required = true)
        protected X3003CodeAndDescriptionElement description;
        @XmlElement(name = "NetPower")
        protected Integer netPower;
        @XmlElement(name = "EngineDisplacement")
        protected Integer engineDisplacement;
        @XmlElement(name = "FuelType")
        protected X3003CodeAndDescriptionElement fuelType;
        @XmlElement(name = "GVM")
        protected Integer gvm;
        @XmlElement(name = "AxlesTotal")
        protected Integer axlesTotal;
        @XmlElement(name = "NoOfWheels")
        protected Integer noOfWheels;
        @XmlElement(name = "OverallWidth")
        protected Integer overallWidth;
        @XmlElement(name = "RoadworthyStatus", required = true)
        protected X3003CodeAndDescriptionElement roadworthyStatus;
        @XmlElement(name = "Tare")
        protected int tare;
        @XmlElement(name = "MainColour")
        protected X3003CodeAndDescriptionElement mainColour;
        @XmlElement(name = "AxlesDriven")
        protected Integer axlesDriven;
        @XmlElement(name = "OverallLength")
        protected Integer overallLength;
        @XmlElement(name = "OverallHeight")
        protected Integer overallHeight;
        @XmlElement(name = "RoadworthyTestDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar roadworthyTestDate;
        @XmlElement(name = "RoadworthyStatusDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar roadworthyStatusDate;
        @XmlElement(name = "SapMark", required = true)
        protected X3003CodeAndDescriptionElement sapMark;
        @XmlElement(name = "SapMarkDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar sapMarkDate;
        @XmlElement(name = "SapClearanceStatus", required = true)
        protected X3003CodeAndDescriptionElement sapClearanceStatus;
        @XmlElement(name = "SapClearanceDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar sapClearanceDate;
        @XmlElement(name = "LifeStatus", required = true)
        protected X3003CodeAndDescriptionElement lifeStatus;
        @XmlElement(name = "VehicleState", required = true)
        protected X3003CodeAndDescriptionElement vehicleState;
        @XmlElement(name = "VehicleStateDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar vehicleStateDate;
        @XmlElement(name = "RegistrationType", required = true)
        protected X3003CodeAndDescriptionElement registrationType;
        @XmlElement(name = "VehicleCertificateNumber")
        protected String vehicleCertificateNumber;
        @XmlElement(name = "LicenceChangeDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar licenceChangeDate;
        @XmlElement(name = "LicenceExpiryDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar licenceExpiryDate;
        @XmlElement(name = "PreviousLicenceNumber")
        protected String previousLicenceNumber;
        @XmlElement(name = "PrePreviousLicenceNumber")
        protected String prePreviousLicenceNumber;
        @XmlElement(name = "PrePrePreviousLicenceNumber")
        protected String prePrePreviousLicenceNumber;

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the make property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getMake() {
            return make;
        }

        /**
         * Sets the value of the make property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setMake(X3003CodeAndDescriptionElement value) {
            this.make = value;
        }

        /**
         * Gets the value of the modelName property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getModelName() {
            return modelName;
        }

        /**
         * Sets the value of the modelName property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setModelName(X3003CodeAndDescriptionElement value) {
            this.modelName = value;
        }

        /**
         * Gets the value of the category property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getCategory() {
            return category;
        }

        /**
         * Sets the value of the category property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setCategory(X3003CodeAndDescriptionElement value) {
            this.category = value;
        }

        /**
         * Gets the value of the driven property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getDriven() {
            return driven;
        }

        /**
         * Sets the value of the driven property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setDriven(X3003CodeAndDescriptionElement value) {
            this.driven = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setDescription(X3003CodeAndDescriptionElement value) {
            this.description = value;
        }

        /**
         * Gets the value of the netPower property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNetPower() {
            return netPower;
        }

        /**
         * Sets the value of the netPower property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNetPower(Integer value) {
            this.netPower = value;
        }

        /**
         * Gets the value of the engineDisplacement property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getEngineDisplacement() {
            return engineDisplacement;
        }

        /**
         * Sets the value of the engineDisplacement property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setEngineDisplacement(Integer value) {
            this.engineDisplacement = value;
        }

        /**
         * Gets the value of the fuelType property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getFuelType() {
            return fuelType;
        }

        /**
         * Sets the value of the fuelType property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setFuelType(X3003CodeAndDescriptionElement value) {
            this.fuelType = value;
        }

        /**
         * Gets the value of the gvm property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGVM() {
            return gvm;
        }

        /**
         * Sets the value of the gvm property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGVM(Integer value) {
            this.gvm = value;
        }

        /**
         * Gets the value of the axlesTotal property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAxlesTotal() {
            return axlesTotal;
        }

        /**
         * Sets the value of the axlesTotal property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAxlesTotal(Integer value) {
            this.axlesTotal = value;
        }

        /**
         * Gets the value of the noOfWheels property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNoOfWheels() {
            return noOfWheels;
        }

        /**
         * Sets the value of the noOfWheels property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNoOfWheels(Integer value) {
            this.noOfWheels = value;
        }

        /**
         * Gets the value of the overallWidth property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOverallWidth() {
            return overallWidth;
        }

        /**
         * Sets the value of the overallWidth property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOverallWidth(Integer value) {
            this.overallWidth = value;
        }

        /**
         * Gets the value of the roadworthyStatus property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getRoadworthyStatus() {
            return roadworthyStatus;
        }

        /**
         * Sets the value of the roadworthyStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setRoadworthyStatus(X3003CodeAndDescriptionElement value) {
            this.roadworthyStatus = value;
        }

        /**
         * Gets the value of the tare property.
         * 
         */
        public int getTare() {
            return tare;
        }

        /**
         * Sets the value of the tare property.
         * 
         */
        public void setTare(int value) {
            this.tare = value;
        }

        /**
         * Gets the value of the mainColour property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getMainColour() {
            return mainColour;
        }

        /**
         * Sets the value of the mainColour property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setMainColour(X3003CodeAndDescriptionElement value) {
            this.mainColour = value;
        }

        /**
         * Gets the value of the axlesDriven property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAxlesDriven() {
            return axlesDriven;
        }

        /**
         * Sets the value of the axlesDriven property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAxlesDriven(Integer value) {
            this.axlesDriven = value;
        }

        /**
         * Gets the value of the overallLength property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOverallLength() {
            return overallLength;
        }

        /**
         * Sets the value of the overallLength property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOverallLength(Integer value) {
            this.overallLength = value;
        }

        /**
         * Gets the value of the overallHeight property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOverallHeight() {
            return overallHeight;
        }

        /**
         * Sets the value of the overallHeight property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOverallHeight(Integer value) {
            this.overallHeight = value;
        }

        /**
         * Gets the value of the roadworthyTestDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRoadworthyTestDate() {
            return roadworthyTestDate;
        }

        /**
         * Sets the value of the roadworthyTestDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRoadworthyTestDate(XMLGregorianCalendar value) {
            this.roadworthyTestDate = value;
        }

        /**
         * Gets the value of the roadworthyStatusDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRoadworthyStatusDate() {
            return roadworthyStatusDate;
        }

        /**
         * Sets the value of the roadworthyStatusDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRoadworthyStatusDate(XMLGregorianCalendar value) {
            this.roadworthyStatusDate = value;
        }

        /**
         * Gets the value of the sapMark property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getSapMark() {
            return sapMark;
        }

        /**
         * Sets the value of the sapMark property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setSapMark(X3003CodeAndDescriptionElement value) {
            this.sapMark = value;
        }

        /**
         * Gets the value of the sapMarkDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSapMarkDate() {
            return sapMarkDate;
        }

        /**
         * Sets the value of the sapMarkDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSapMarkDate(XMLGregorianCalendar value) {
            this.sapMarkDate = value;
        }

        /**
         * Gets the value of the sapClearanceStatus property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getSapClearanceStatus() {
            return sapClearanceStatus;
        }

        /**
         * Sets the value of the sapClearanceStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setSapClearanceStatus(X3003CodeAndDescriptionElement value) {
            this.sapClearanceStatus = value;
        }

        /**
         * Gets the value of the sapClearanceDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSapClearanceDate() {
            return sapClearanceDate;
        }

        /**
         * Sets the value of the sapClearanceDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSapClearanceDate(XMLGregorianCalendar value) {
            this.sapClearanceDate = value;
        }

        /**
         * Gets the value of the lifeStatus property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getLifeStatus() {
            return lifeStatus;
        }

        /**
         * Sets the value of the lifeStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setLifeStatus(X3003CodeAndDescriptionElement value) {
            this.lifeStatus = value;
        }

        /**
         * Gets the value of the vehicleState property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getVehicleState() {
            return vehicleState;
        }

        /**
         * Sets the value of the vehicleState property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setVehicleState(X3003CodeAndDescriptionElement value) {
            this.vehicleState = value;
        }

        /**
         * Gets the value of the vehicleStateDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVehicleStateDate() {
            return vehicleStateDate;
        }

        /**
         * Sets the value of the vehicleStateDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVehicleStateDate(XMLGregorianCalendar value) {
            this.vehicleStateDate = value;
        }

        /**
         * Gets the value of the registrationType property.
         * 
         * @return
         *     possible object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public X3003CodeAndDescriptionElement getRegistrationType() {
            return registrationType;
        }

        /**
         * Sets the value of the registrationType property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3003CodeAndDescriptionElement }
         *     
         */
        public void setRegistrationType(X3003CodeAndDescriptionElement value) {
            this.registrationType = value;
        }

        /**
         * Gets the value of the vehicleCertificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCertificateNumber() {
            return vehicleCertificateNumber;
        }

        /**
         * Sets the value of the vehicleCertificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCertificateNumber(String value) {
            this.vehicleCertificateNumber = value;
        }

        /**
         * Gets the value of the licenceChangeDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceChangeDate() {
            return licenceChangeDate;
        }

        /**
         * Sets the value of the licenceChangeDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceChangeDate(XMLGregorianCalendar value) {
            this.licenceChangeDate = value;
        }

        /**
         * Gets the value of the licenceExpiryDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceExpiryDate() {
            return licenceExpiryDate;
        }

        /**
         * Sets the value of the licenceExpiryDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceExpiryDate(XMLGregorianCalendar value) {
            this.licenceExpiryDate = value;
        }

        /**
         * Gets the value of the previousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousLicenceNumber() {
            return previousLicenceNumber;
        }

        /**
         * Sets the value of the previousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousLicenceNumber(String value) {
            this.previousLicenceNumber = value;
        }

        /**
         * Gets the value of the prePreviousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrePreviousLicenceNumber() {
            return prePreviousLicenceNumber;
        }

        /**
         * Sets the value of the prePreviousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrePreviousLicenceNumber(String value) {
            this.prePreviousLicenceNumber = value;
        }

        /**
         * Gets the value of the prePrePreviousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrePrePreviousLicenceNumber() {
            return prePrePreviousLicenceNumber;
        }

        /**
         * Sets the value of the prePrePreviousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrePrePreviousLicenceNumber(String value) {
            this.prePrePreviousLicenceNumber = value;
        }

    }

}
