
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="infringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringementNoticeNumber"/>
 *         &lt;element name="infringer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}InfringerIdType"/>
 *                   &lt;element name="idDocumentNumber">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="13"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="repIdDocumentTypeCode" type="{http://tasima/common/ws/schema/}InfringerIdType" minOccurs="0"/>
 *                   &lt;element name="repIdDocumentNumber" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="13"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="licCardN" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="12"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="learnerLicN" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="12"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="status" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="01"/>
 *                         &lt;enumeration value="02"/>
 *                         &lt;enumeration value="03"/>
 *                         &lt;enumeration value="04"/>
 *                         &lt;enumeration value="05"/>
 *                         &lt;enumeration value="06"/>
 *                         &lt;enumeration value="07"/>
 *                         &lt;enumeration value="08"/>
 *                         &lt;enumeration value="09"/>
 *                         &lt;enumeration value="10"/>
 *                         &lt;enumeration value="11"/>
 *                         &lt;enumeration value="12"/>
 *                         &lt;enumeration value="13"/>
 *                         &lt;enumeration value="14"/>
 *                         &lt;enumeration value="15"/>
 *                         &lt;enumeration value="16"/>
 *                         &lt;enumeration value="17"/>
 *                         &lt;enumeration value="18"/>
 *                         &lt;enumeration value="19"/>
 *                         &lt;enumeration value="20"/>
 *                         &lt;enumeration value="21"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementNoticeNumber",
    "infringer"
})
@XmlRootElement(name = "QueryInfringementsForWebsiteRequest")
public class QueryInfringementsForWebsiteRequest {

    protected String infringementNoticeNumber;
    protected QueryInfringementsForWebsiteRequest.Infringer infringer;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the infringer property.
     * 
     * @return
     *     possible object is
     *     {@link QueryInfringementsForWebsiteRequest.Infringer }
     *     
     */
    public QueryInfringementsForWebsiteRequest.Infringer getInfringer() {
        return infringer;
    }

    /**
     * Sets the value of the infringer property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryInfringementsForWebsiteRequest.Infringer }
     *     
     */
    public void setInfringer(QueryInfringementsForWebsiteRequest.Infringer value) {
        this.infringer = value;
    }


    /**
     * Request data based on infringer identification
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}InfringerIdType"/>
     *         &lt;element name="idDocumentNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="13"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="repIdDocumentTypeCode" type="{http://tasima/common/ws/schema/}InfringerIdType" minOccurs="0"/>
     *         &lt;element name="repIdDocumentNumber" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="13"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="licCardN" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="12"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="learnerLicN" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="12"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="status" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="01"/>
     *               &lt;enumeration value="02"/>
     *               &lt;enumeration value="03"/>
     *               &lt;enumeration value="04"/>
     *               &lt;enumeration value="05"/>
     *               &lt;enumeration value="06"/>
     *               &lt;enumeration value="07"/>
     *               &lt;enumeration value="08"/>
     *               &lt;enumeration value="09"/>
     *               &lt;enumeration value="10"/>
     *               &lt;enumeration value="11"/>
     *               &lt;enumeration value="12"/>
     *               &lt;enumeration value="13"/>
     *               &lt;enumeration value="14"/>
     *               &lt;enumeration value="15"/>
     *               &lt;enumeration value="16"/>
     *               &lt;enumeration value="17"/>
     *               &lt;enumeration value="18"/>
     *               &lt;enumeration value="19"/>
     *               &lt;enumeration value="20"/>
     *               &lt;enumeration value="21"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumentTypeCode",
        "idDocumentNumber",
        "repIdDocumentTypeCode",
        "repIdDocumentNumber",
        "licCardN",
        "learnerLicN",
        "startDate",
        "endDate",
        "status"
    })
    public static class Infringer {

        @XmlElement(required = true)
        protected String idDocumentTypeCode;
        @XmlElement(required = true)
        protected String idDocumentNumber;
        protected String repIdDocumentTypeCode;
        protected String repIdDocumentNumber;
        protected String licCardN;
        protected String learnerLicN;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar startDate;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar endDate;
        protected String status;

        /**
         * Gets the value of the idDocumentTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentTypeCode() {
            return idDocumentTypeCode;
        }

        /**
         * Sets the value of the idDocumentTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentTypeCode(String value) {
            this.idDocumentTypeCode = value;
        }

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

        /**
         * Gets the value of the repIdDocumentTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepIdDocumentTypeCode() {
            return repIdDocumentTypeCode;
        }

        /**
         * Sets the value of the repIdDocumentTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepIdDocumentTypeCode(String value) {
            this.repIdDocumentTypeCode = value;
        }

        /**
         * Gets the value of the repIdDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepIdDocumentNumber() {
            return repIdDocumentNumber;
        }

        /**
         * Sets the value of the repIdDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepIdDocumentNumber(String value) {
            this.repIdDocumentNumber = value;
        }

        /**
         * Gets the value of the licCardN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicCardN() {
            return licCardN;
        }

        /**
         * Sets the value of the licCardN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicCardN(String value) {
            this.licCardN = value;
        }

        /**
         * Gets the value of the learnerLicN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLearnerLicN() {
            return learnerLicN;
        }

        /**
         * Sets the value of the learnerLicN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLearnerLicN(String value) {
            this.learnerLicN = value;
        }

        /**
         * Gets the value of the startDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getStartDate() {
            return startDate;
        }

        /**
         * Sets the value of the startDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setStartDate(XMLGregorianCalendar value) {
            this.startDate = value;
        }

        /**
         * Gets the value of the endDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEndDate() {
            return endDate;
        }

        /**
         * Sets the value of the endDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEndDate(XMLGregorianCalendar value) {
            this.endDate = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

    }

}
