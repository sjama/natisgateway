
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenericInfringementLocationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericInfringementLocationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="province" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *         &lt;element name="cityOrTown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="streetNameA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="streetNameB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="directionFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="directionTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gpsXCoordinate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gpsYCoordinate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherLocationInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issingAuthority" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericInfringementLocationType", propOrder = {
    "province",
    "cityOrTown",
    "suburb",
    "streetNameA",
    "streetNameB",
    "directionFrom",
    "directionTo",
    "gpsXCoordinate",
    "gpsYCoordinate",
    "otherLocationInfo",
    "issingAuthority"
})
public class GenericInfringementLocationType {

    protected CodeAndDescriptionElement province;
    protected String cityOrTown;
    protected String suburb;
    protected String streetNameA;
    protected String streetNameB;
    protected String directionFrom;
    protected String directionTo;
    protected String gpsXCoordinate;
    protected String gpsYCoordinate;
    protected String otherLocationInfo;
    @XmlElement(required = true)
    protected CodeAndDescriptionElement issingAuthority;

    /**
     * Gets the value of the province property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getProvince() {
        return province;
    }

    /**
     * Sets the value of the province property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setProvince(CodeAndDescriptionElement value) {
        this.province = value;
    }

    /**
     * Gets the value of the cityOrTown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityOrTown() {
        return cityOrTown;
    }

    /**
     * Sets the value of the cityOrTown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityOrTown(String value) {
        this.cityOrTown = value;
    }

    /**
     * Gets the value of the suburb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     * Sets the value of the suburb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuburb(String value) {
        this.suburb = value;
    }

    /**
     * Gets the value of the streetNameA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetNameA() {
        return streetNameA;
    }

    /**
     * Sets the value of the streetNameA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetNameA(String value) {
        this.streetNameA = value;
    }

    /**
     * Gets the value of the streetNameB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreetNameB() {
        return streetNameB;
    }

    /**
     * Sets the value of the streetNameB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreetNameB(String value) {
        this.streetNameB = value;
    }

    /**
     * Gets the value of the directionFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectionFrom() {
        return directionFrom;
    }

    /**
     * Sets the value of the directionFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectionFrom(String value) {
        this.directionFrom = value;
    }

    /**
     * Gets the value of the directionTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirectionTo() {
        return directionTo;
    }

    /**
     * Sets the value of the directionTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirectionTo(String value) {
        this.directionTo = value;
    }

    /**
     * Gets the value of the gpsXCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGpsXCoordinate() {
        return gpsXCoordinate;
    }

    /**
     * Sets the value of the gpsXCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGpsXCoordinate(String value) {
        this.gpsXCoordinate = value;
    }

    /**
     * Gets the value of the gpsYCoordinate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGpsYCoordinate() {
        return gpsYCoordinate;
    }

    /**
     * Sets the value of the gpsYCoordinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGpsYCoordinate(String value) {
        this.gpsYCoordinate = value;
    }

    /**
     * Gets the value of the otherLocationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherLocationInfo() {
        return otherLocationInfo;
    }

    /**
     * Sets the value of the otherLocationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherLocationInfo(String value) {
        this.otherLocationInfo = value;
    }

    /**
     * Gets the value of the issingAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getIssingAuthority() {
        return issingAuthority;
    }

    /**
     * Sets the value of the issingAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setIssingAuthority(CodeAndDescriptionElement value) {
        this.issingAuthority = value;
    }

}
