
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementNoticeNumber"
})
@XmlRootElement(name = "X3027Request")
public class X3027Request {

    @XmlElement(name = "InfringementNoticeNumber", required = true)
    protected String infringementNoticeNumber;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

}
