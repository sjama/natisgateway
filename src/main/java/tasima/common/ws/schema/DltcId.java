
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DltcId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DltcId">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="infrastructureType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[0-9]{2}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dltcInfrastuctureNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[0-9A-Z]{8}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DltcId", propOrder = {

})
public class DltcId {

    @XmlElement(required = true)
    protected String infrastructureType;
    @XmlElement(required = true)
    protected String dltcInfrastuctureNumber;

    /**
     * Gets the value of the infrastructureType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfrastructureType() {
        return infrastructureType;
    }

    /**
     * Sets the value of the infrastructureType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfrastructureType(String value) {
        this.infrastructureType = value;
    }

    /**
     * Gets the value of the dltcInfrastuctureNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDltcInfrastuctureNumber() {
        return dltcInfrastuctureNumber;
    }

    /**
     * Sets the value of the dltcInfrastuctureNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDltcInfrastuctureNumber(String value) {
        this.dltcInfrastuctureNumber = value;
    }

}
