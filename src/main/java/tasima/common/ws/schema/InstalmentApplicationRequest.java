
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="methodOfPayment" type="{http://tasima/common/ws/schema/}MethodOfPayment"/>
 *         &lt;element name="nrOfInstalments" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="bankDetails" type="{http://tasima/common/ws/schema/}BankDetails" minOccurs="0"/>
 *         &lt;element name="aarto04" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementNoticeNumber",
    "methodOfPayment",
    "nrOfInstalments",
    "bankDetails",
    "aarto04"
})
@XmlRootElement(name = "InstalmentApplicationRequest")
public class InstalmentApplicationRequest {

    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected String methodOfPayment;
    protected int nrOfInstalments;
    protected BankDetails bankDetails;
    @XmlElement(required = true)
    protected byte[] aarto04;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the methodOfPayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodOfPayment() {
        return methodOfPayment;
    }

    /**
     * Sets the value of the methodOfPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodOfPayment(String value) {
        this.methodOfPayment = value;
    }

    /**
     * Gets the value of the nrOfInstalments property.
     * 
     */
    public int getNrOfInstalments() {
        return nrOfInstalments;
    }

    /**
     * Sets the value of the nrOfInstalments property.
     * 
     */
    public void setNrOfInstalments(int value) {
        this.nrOfInstalments = value;
    }

    /**
     * Gets the value of the bankDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BankDetails }
     *     
     */
    public BankDetails getBankDetails() {
        return bankDetails;
    }

    /**
     * Sets the value of the bankDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankDetails }
     *     
     */
    public void setBankDetails(BankDetails value) {
        this.bankDetails = value;
    }

    /**
     * Gets the value of the aarto04 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto04() {
        return aarto04;
    }

    /**
     * Sets the value of the aarto04 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto04(byte[] value) {
        this.aarto04 = value;
    }

}
