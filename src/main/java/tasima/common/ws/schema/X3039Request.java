
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}X3039IdDocumentTypeElement"/>
 *         &lt;element name="idDocumentNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InfringeNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nomineeIdDocumentTypeCode" type="{http://tasima/common/ws/schema/}X3039IdDocumentTypeElement"/>
 *         &lt;element name="nomineeIdDocumentNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nomineeLicenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idDocumentTypeCode",
    "idDocumentNumber",
    "infringeNoticeNumber",
    "nomineeIdDocumentTypeCode",
    "nomineeIdDocumentNumber",
    "nomineeLicenceNumber"
})
@XmlRootElement(name = "X3039Request")
public class X3039Request {

    @XmlElement(required = true)
    protected String idDocumentTypeCode;
    @XmlElement(required = true)
    protected String idDocumentNumber;
    @XmlElement(name = "InfringeNoticeNumber", required = true)
    protected String infringeNoticeNumber;
    @XmlElement(required = true)
    protected String nomineeIdDocumentTypeCode;
    @XmlElement(required = true)
    protected String nomineeIdDocumentNumber;
    @XmlElement(required = true)
    protected String nomineeLicenceNumber;

    /**
     * Gets the value of the idDocumentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentTypeCode() {
        return idDocumentTypeCode;
    }

    /**
     * Sets the value of the idDocumentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentTypeCode(String value) {
        this.idDocumentTypeCode = value;
    }

    /**
     * Gets the value of the idDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentNumber() {
        return idDocumentNumber;
    }

    /**
     * Sets the value of the idDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentNumber(String value) {
        this.idDocumentNumber = value;
    }

    /**
     * Gets the value of the infringeNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringeNoticeNumber() {
        return infringeNoticeNumber;
    }

    /**
     * Sets the value of the infringeNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringeNoticeNumber(String value) {
        this.infringeNoticeNumber = value;
    }

    /**
     * Gets the value of the nomineeIdDocumentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomineeIdDocumentTypeCode() {
        return nomineeIdDocumentTypeCode;
    }

    /**
     * Sets the value of the nomineeIdDocumentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomineeIdDocumentTypeCode(String value) {
        this.nomineeIdDocumentTypeCode = value;
    }

    /**
     * Gets the value of the nomineeIdDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomineeIdDocumentNumber() {
        return nomineeIdDocumentNumber;
    }

    /**
     * Sets the value of the nomineeIdDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomineeIdDocumentNumber(String value) {
        this.nomineeIdDocumentNumber = value;
    }

    /**
     * Gets the value of the nomineeLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomineeLicenceNumber() {
        return nomineeLicenceNumber;
    }

    /**
     * Sets the value of the nomineeLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomineeLicenceNumber(String value) {
        this.nomineeLicenceNumber = value;
    }

}
