
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Infringement" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
 *                   &lt;element name="InfringeStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InfringeStatusChangeDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="InfringementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="CaptureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="Infringer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdDocTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="InfringementCertificate" type="{http://tasima/common/ws/schema/}X3026InfringementCertificate" maxOccurs="unbounded"/>
 *                   &lt;element name="RepresentationStatus" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="RevocationStatus" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="CourtCaseStatus" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringement"
})
@XmlRootElement(name = "X3026Response")
public class X3026Response {

    @XmlElement(name = "Infringement", required = true)
    protected List<X3026Response.Infringement> infringement;

    /**
     * Gets the value of the infringement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infringement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfringement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X3026Response.Infringement }
     * 
     * 
     */
    public List<X3026Response.Infringement> getInfringement() {
        if (infringement == null) {
            infringement = new ArrayList<X3026Response.Infringement>();
        }
        return this.infringement;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
     *         &lt;element name="InfringeStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InfringeStatusChangeDate" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="InfringementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="CaptureDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="Infringer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdDocTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="InfringementCertificate" type="{http://tasima/common/ws/schema/}X3026InfringementCertificate" maxOccurs="unbounded"/>
     *         &lt;element name="RepresentationStatus" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="RevocationStatus" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="CourtCaseStatus" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "infringementNoticeNumber",
        "infringeStatus",
        "infringeStatusChangeDate",
        "infringementDate",
        "captureDate",
        "infringer",
        "infringementCertificate",
        "representationStatus",
        "revocationStatus",
        "courtCaseStatus"
    })
    public static class Infringement {

        @XmlElement(name = "InfringementNoticeNumber", required = true)
        protected String infringementNoticeNumber;
        @XmlElement(name = "InfringeStatus", required = true)
        protected String infringeStatus;
        @XmlElement(name = "InfringeStatusChangeDate", required = true)
        protected XMLGregorianCalendar infringeStatusChangeDate;
        @XmlElement(name = "InfringementDate", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar infringementDate;
        @XmlElement(name = "CaptureDate", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar captureDate;
        @XmlElement(name = "Infringer", required = true)
        protected X3026Response.Infringement.Infringer infringer;
        @XmlElement(name = "InfringementCertificate", required = true)
        protected List<X3026InfringementCertificate> infringementCertificate;
        @XmlElement(name = "RepresentationStatus")
        protected String representationStatus;
        @XmlElement(name = "RevocationStatus")
        protected String revocationStatus;
        @XmlElement(name = "CourtCaseStatus")
        protected String courtCaseStatus;

        /**
         * Gets the value of the infringementNoticeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringementNoticeNumber() {
            return infringementNoticeNumber;
        }

        /**
         * Sets the value of the infringementNoticeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringementNoticeNumber(String value) {
            this.infringementNoticeNumber = value;
        }

        /**
         * Gets the value of the infringeStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringeStatus() {
            return infringeStatus;
        }

        /**
         * Sets the value of the infringeStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringeStatus(String value) {
            this.infringeStatus = value;
        }

        /**
         * Gets the value of the infringeStatusChangeDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getInfringeStatusChangeDate() {
            return infringeStatusChangeDate;
        }

        /**
         * Sets the value of the infringeStatusChangeDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setInfringeStatusChangeDate(XMLGregorianCalendar value) {
            this.infringeStatusChangeDate = value;
        }

        /**
         * Gets the value of the infringementDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getInfringementDate() {
            return infringementDate;
        }

        /**
         * Sets the value of the infringementDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setInfringementDate(XMLGregorianCalendar value) {
            this.infringementDate = value;
        }

        /**
         * Gets the value of the captureDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCaptureDate() {
            return captureDate;
        }

        /**
         * Sets the value of the captureDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCaptureDate(XMLGregorianCalendar value) {
            this.captureDate = value;
        }

        /**
         * Gets the value of the infringer property.
         * 
         * @return
         *     possible object is
         *     {@link X3026Response.Infringement.Infringer }
         *     
         */
        public X3026Response.Infringement.Infringer getInfringer() {
            return infringer;
        }

        /**
         * Sets the value of the infringer property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3026Response.Infringement.Infringer }
         *     
         */
        public void setInfringer(X3026Response.Infringement.Infringer value) {
            this.infringer = value;
        }

        /**
         * Gets the value of the infringementCertificate property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the infringementCertificate property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfringementCertificate().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link X3026InfringementCertificate }
         * 
         * 
         */
        public List<X3026InfringementCertificate> getInfringementCertificate() {
            if (infringementCertificate == null) {
                infringementCertificate = new ArrayList<X3026InfringementCertificate>();
            }
            return this.infringementCertificate;
        }

        /**
         * Gets the value of the representationStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepresentationStatus() {
            return representationStatus;
        }

        /**
         * Sets the value of the representationStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepresentationStatus(String value) {
            this.representationStatus = value;
        }

        /**
         * Gets the value of the revocationStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRevocationStatus() {
            return revocationStatus;
        }

        /**
         * Sets the value of the revocationStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRevocationStatus(String value) {
            this.revocationStatus = value;
        }

        /**
         * Gets the value of the courtCaseStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCourtCaseStatus() {
            return courtCaseStatus;
        }

        /**
         * Sets the value of the courtCaseStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCourtCaseStatus(String value) {
            this.courtCaseStatus = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdDocTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idDocTypeDescription",
            "idDocN"
        })
        public static class Infringer {

            @XmlElement(name = "IdDocTypeDescription", required = true)
            protected String idDocTypeDescription;
            @XmlElement(name = "IdDocN", required = true)
            protected String idDocN;

            /**
             * Gets the value of the idDocTypeDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocTypeDescription() {
                return idDocTypeDescription;
            }

            /**
             * Sets the value of the idDocTypeDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocTypeDescription(String value) {
                this.idDocTypeDescription = value;
            }

            /**
             * Gets the value of the idDocN property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocN() {
                return idDocN;
            }

            /**
             * Sets the value of the idDocN property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocN(String value) {
                this.idDocN = value;
            }

        }

    }

}
