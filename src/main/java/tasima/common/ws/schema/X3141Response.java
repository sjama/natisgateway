
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionStatus" type="{http://tasima/common/ws/schema/}OperationStatus"/>
 *         &lt;element name="messages" type="{http://tasima/common/ws/schema/}InformationMessage" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="result" type="{http://tasima/common/ws/schema/}X3141SucessResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionStatus",
    "messages",
    "result"
})
@XmlRootElement(name = "X3141Response")
public class X3141Response {

    @XmlElement(required = true)
    protected OperationStatus transactionStatus;
    protected List<InformationMessage> messages;
    protected X3141SucessResult result;

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link OperationStatus }
     *     
     */
    public OperationStatus getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationStatus }
     *     
     */
    public void setTransactionStatus(OperationStatus value) {
        this.transactionStatus = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformationMessage }
     * 
     * 
     */
    public List<InformationMessage> getMessages() {
        if (messages == null) {
            messages = new ArrayList<InformationMessage>();
        }
        return this.messages;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link X3141SucessResult }
     *     
     */
    public X3141SucessResult getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141SucessResult }
     *     
     */
    public void setResult(X3141SucessResult value) {
        this.result = value;
    }

}
