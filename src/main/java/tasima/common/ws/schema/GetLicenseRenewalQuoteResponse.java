
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicles" type="{http://tasima/common/ws/schema/}X5008VehicleDetail" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="feeAssessment" type="{http://tasima/common/ws/schema/}X5008LicenceRenewalFeeAssessment" minOccurs="0"/>
 *         &lt;element name="allowedToContinue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="noDiscCondition" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="owner" type="{http://tasima/common/ws/schema/}X5008PersonElement" minOccurs="0"/>
 *         &lt;element name="messages" type="{http://tasima/common/ws/schema/}ThirdpartyInformationMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicles",
    "feeAssessment",
    "allowedToContinue",
    "noDiscCondition",
    "owner",
    "messages"
})
@XmlRootElement(name = "GetLicenseRenewalQuoteResponse")
public class GetLicenseRenewalQuoteResponse {

    protected List<X5008VehicleDetail> vehicles;
    protected X5008LicenceRenewalFeeAssessment feeAssessment;
    protected boolean allowedToContinue;
    protected Boolean noDiscCondition;
    protected X5008PersonElement owner;
    protected List<ThirdpartyInformationMessage> messages;

    /**
     * Gets the value of the vehicles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vehicles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVehicles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X5008VehicleDetail }
     * 
     * 
     */
    public List<X5008VehicleDetail> getVehicles() {
        if (vehicles == null) {
            vehicles = new ArrayList<X5008VehicleDetail>();
        }
        return this.vehicles;
    }

    /**
     * Gets the value of the feeAssessment property.
     * 
     * @return
     *     possible object is
     *     {@link X5008LicenceRenewalFeeAssessment }
     *     
     */
    public X5008LicenceRenewalFeeAssessment getFeeAssessment() {
        return feeAssessment;
    }

    /**
     * Sets the value of the feeAssessment property.
     * 
     * @param value
     *     allowed object is
     *     {@link X5008LicenceRenewalFeeAssessment }
     *     
     */
    public void setFeeAssessment(X5008LicenceRenewalFeeAssessment value) {
        this.feeAssessment = value;
    }

    /**
     * Gets the value of the allowedToContinue property.
     * 
     */
    public boolean isAllowedToContinue() {
        return allowedToContinue;
    }

    /**
     * Sets the value of the allowedToContinue property.
     * 
     */
    public void setAllowedToContinue(boolean value) {
        this.allowedToContinue = value;
    }

    /**
     * Gets the value of the noDiscCondition property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoDiscCondition() {
        return noDiscCondition;
    }

    /**
     * Sets the value of the noDiscCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoDiscCondition(Boolean value) {
        this.noDiscCondition = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link X5008PersonElement }
     *     
     */
    public X5008PersonElement getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link X5008PersonElement }
     *     
     */
    public void setOwner(X5008PersonElement value) {
        this.owner = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ThirdpartyInformationMessage }
     * 
     * 
     */
    public List<ThirdpartyInformationMessage> getMessages() {
        if (messages == null) {
            messages = new ArrayList<ThirdpartyInformationMessage>();
        }
        return this.messages;
    }

}
