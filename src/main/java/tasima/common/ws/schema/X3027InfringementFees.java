
package tasima.common.ws.schema;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3027InfringementFees complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027InfringementFees">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PenaltyAmtOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="CLFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="RepFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="EOFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="RevFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="DishChqFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="DishInstFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="WarrantFeeOutstanding" type="{http://tasima/common/ws/schema/}Money"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027InfringementFees", propOrder = {
    "penaltyAmtOutstanding",
    "clFeeOutstanding",
    "repFeeOutstanding",
    "eoFeeOutstanding",
    "revFeeOutstanding",
    "dishChqFeeOutstanding",
    "dishInstFeeOutstanding",
    "warrantFeeOutstanding"
})
public class X3027InfringementFees {

    @XmlElement(name = "PenaltyAmtOutstanding", required = true)
    protected BigDecimal penaltyAmtOutstanding;
    @XmlElement(name = "CLFeeOutstanding", required = true)
    protected BigDecimal clFeeOutstanding;
    @XmlElement(name = "RepFeeOutstanding", required = true)
    protected BigDecimal repFeeOutstanding;
    @XmlElement(name = "EOFeeOutstanding", required = true)
    protected BigDecimal eoFeeOutstanding;
    @XmlElement(name = "RevFeeOutstanding", required = true)
    protected BigDecimal revFeeOutstanding;
    @XmlElement(name = "DishChqFeeOutstanding", required = true)
    protected BigDecimal dishChqFeeOutstanding;
    @XmlElement(name = "DishInstFeeOutstanding", required = true)
    protected BigDecimal dishInstFeeOutstanding;
    @XmlElement(name = "WarrantFeeOutstanding", required = true)
    protected BigDecimal warrantFeeOutstanding;

    /**
     * Gets the value of the penaltyAmtOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPenaltyAmtOutstanding() {
        return penaltyAmtOutstanding;
    }

    /**
     * Sets the value of the penaltyAmtOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPenaltyAmtOutstanding(BigDecimal value) {
        this.penaltyAmtOutstanding = value;
    }

    /**
     * Gets the value of the clFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCLFeeOutstanding() {
        return clFeeOutstanding;
    }

    /**
     * Sets the value of the clFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCLFeeOutstanding(BigDecimal value) {
        this.clFeeOutstanding = value;
    }

    /**
     * Gets the value of the repFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRepFeeOutstanding() {
        return repFeeOutstanding;
    }

    /**
     * Sets the value of the repFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRepFeeOutstanding(BigDecimal value) {
        this.repFeeOutstanding = value;
    }

    /**
     * Gets the value of the eoFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEOFeeOutstanding() {
        return eoFeeOutstanding;
    }

    /**
     * Sets the value of the eoFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEOFeeOutstanding(BigDecimal value) {
        this.eoFeeOutstanding = value;
    }

    /**
     * Gets the value of the revFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRevFeeOutstanding() {
        return revFeeOutstanding;
    }

    /**
     * Sets the value of the revFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRevFeeOutstanding(BigDecimal value) {
        this.revFeeOutstanding = value;
    }

    /**
     * Gets the value of the dishChqFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDishChqFeeOutstanding() {
        return dishChqFeeOutstanding;
    }

    /**
     * Sets the value of the dishChqFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDishChqFeeOutstanding(BigDecimal value) {
        this.dishChqFeeOutstanding = value;
    }

    /**
     * Gets the value of the dishInstFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDishInstFeeOutstanding() {
        return dishInstFeeOutstanding;
    }

    /**
     * Sets the value of the dishInstFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDishInstFeeOutstanding(BigDecimal value) {
        this.dishInstFeeOutstanding = value;
    }

    /**
     * Gets the value of the warrantFeeOutstanding property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWarrantFeeOutstanding() {
        return warrantFeeOutstanding;
    }

    /**
     * Sets the value of the warrantFeeOutstanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWarrantFeeOutstanding(BigDecimal value) {
        this.warrantFeeOutstanding = value;
    }

}
