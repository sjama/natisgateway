
package tasima.common.ws.schema;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tasks" type="{http://tasima/common/ws/schema/}E3024Task" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MaxTasksShown" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tasks",
    "maxTasksShown"
})
@XmlRootElement(name = "QueryTaskManagementSystemResponse")
public class QueryTaskManagementSystemResponse {

    @XmlElement(name = "Tasks")
    protected List<E3024Task> tasks;
    @XmlElement(name = "MaxTasksShown", required = true)
    protected BigInteger maxTasksShown;

    /**
     * Gets the value of the tasks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tasks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTasks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link E3024Task }
     * 
     * 
     */
    public List<E3024Task> getTasks() {
        if (tasks == null) {
            tasks = new ArrayList<E3024Task>();
        }
        return this.tasks;
    }

    /**
     * Gets the value of the maxTasksShown property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxTasksShown() {
        return maxTasksShown;
    }

    /**
     * Sets the value of the maxTasksShown property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxTasksShown(BigInteger value) {
        this.maxTasksShown = value;
    }

}
