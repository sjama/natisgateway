
package tasima.common.ws.schema;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for QueryVehicleDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryVehicleDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
 *                   &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
 *                   &lt;element name="SapMark" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="LicenceChangeDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="LicenceLiabilityDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="LicenceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="RegAuthorityOfLicensing" type="{http://tasima/common/ws/schema/}AuthorityElement" minOccurs="0"/>
 *                   &lt;element name="RegAuthorityOfLicenceNumber" type="{http://tasima/common/ws/schema/}AuthorityElement" minOccurs="0"/>
 *                   &lt;element name="ModelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Category" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Driven" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Description" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="MainColour" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="GVM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="EngineDisplacement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="Tare" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="FuelType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="NetPower" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RegistrationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="RegistrationQualifier" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="RegistrationQualifierDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="RoadworthyStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="RoadworthyStatusDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="RoadworthyTestDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="VehicleState" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="VehicleStateDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="DataOwner" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="Transmission" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="GearboxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DifferentialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FirstLicensingDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="CountryOfExport" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="CountryOfImport" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="LifeStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="SapClearanceReason" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="SapClearanceStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="SapClearanceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="SapMarkDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="VehicleUsage" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="EconomicSector" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *                   &lt;element name="CertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PreviousCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="PrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="AxlesTotal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="NoOfWheels" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OverallWidth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="AxlesDriven" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OverallLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="OverallHeight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="CapacitySitting" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="CapacityStanding" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="LicenceFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="RegistrationType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="VtsNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="VtsName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExaminerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExaminerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Exemption" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *                   &lt;element name="RoadUseIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="Overdue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryVehicleDetails", propOrder = {
    "vehicle"
})
public class QueryVehicleDetails {

    @XmlElement(name = "Vehicle", required = true)
    protected QueryVehicleDetails.Vehicle vehicle;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link QueryVehicleDetails.Vehicle }
     *     
     */
    public QueryVehicleDetails.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryVehicleDetails.Vehicle }
     *     
     */
    public void setVehicle(QueryVehicleDetails.Vehicle value) {
        this.vehicle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement"/>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
     *         &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber" minOccurs="0"/>
     *         &lt;element name="SapMark" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="LicenceChangeDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="LicenceLiabilityDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="LicenceExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="RegAuthorityOfLicensing" type="{http://tasima/common/ws/schema/}AuthorityElement" minOccurs="0"/>
     *         &lt;element name="RegAuthorityOfLicenceNumber" type="{http://tasima/common/ws/schema/}AuthorityElement" minOccurs="0"/>
     *         &lt;element name="ModelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Category" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Driven" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Description" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="MainColour" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="GVM" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="EngineDisplacement" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="Tare" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="FuelType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="NetPower" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RegistrationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="RegistrationQualifier" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="RegistrationQualifierDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="RoadworthyStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="RoadworthyStatusDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="RoadworthyTestDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="VehicleState" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="VehicleStateDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="DataOwner" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="Transmission" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="GearboxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DifferentialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FirstLicensingDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="CountryOfExport" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="CountryOfImport" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="LifeStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="SapClearanceReason" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="SapClearanceStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="SapClearanceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="SapMarkDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="VehicleUsage" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="EconomicSector" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
     *         &lt;element name="CertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PreviousCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="PrePreviousLicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="AxlesTotal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="NoOfWheels" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OverallWidth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="AxlesDriven" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OverallLength" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="OverallHeight" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="CapacitySitting" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="CapacityStanding" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="LicenceFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="RegistrationType" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="VtsNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VtsName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExaminerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExaminerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Exemption" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
     *         &lt;element name="RoadUseIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="Overdue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Vehicle {

        @XmlElement(name = "RegisterNumber", required = true)
        protected String registerNumber;
        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;
        @XmlElement(name = "VinOrChassis")
        protected String vinOrChassis;
        @XmlElement(name = "EngineNumber")
        protected String engineNumber;
        @XmlElement(name = "SapMark", required = true)
        protected CodeAndDescriptionElement sapMark;
        @XmlElement(name = "LicenceChangeDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar licenceChangeDate;
        @XmlElement(name = "LicenceLiabilityDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar licenceLiabilityDate;
        @XmlElement(name = "LicenceExpiryDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar licenceExpiryDate;
        @XmlElement(name = "RegAuthorityOfLicensing")
        protected AuthorityElement regAuthorityOfLicensing;
        @XmlElement(name = "RegAuthorityOfLicenceNumber")
        protected AuthorityElement regAuthorityOfLicenceNumber;
        @XmlElement(name = "ModelName", required = true)
        protected CodeAndDescriptionElement modelName;
        @XmlElement(name = "Category", required = true)
        protected CodeAndDescriptionElement category;
        @XmlElement(name = "Driven", required = true)
        protected CodeAndDescriptionElement driven;
        @XmlElement(name = "Description", required = true)
        protected CodeAndDescriptionElement description;
        @XmlElement(name = "Make", required = true)
        protected CodeAndDescriptionElement make;
        @XmlElement(name = "MainColour")
        protected CodeAndDescriptionElement mainColour;
        @XmlElement(name = "GVM")
        protected Integer gvm;
        @XmlElement(name = "EngineDisplacement")
        protected Integer engineDisplacement;
        @XmlElement(name = "Tare")
        protected int tare;
        @XmlElement(name = "FuelType")
        protected CodeAndDescriptionElement fuelType;
        @XmlElement(name = "NetPower")
        protected Integer netPower;
        @XmlElement(name = "RegistrationDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar registrationDate;
        @XmlElement(name = "RegistrationQualifier", required = true)
        protected CodeAndDescriptionElement registrationQualifier;
        @XmlElement(name = "RegistrationQualifierDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar registrationQualifierDate;
        @XmlElement(name = "RoadworthyStatus", required = true)
        protected CodeAndDescriptionElement roadworthyStatus;
        @XmlElement(name = "RoadworthyStatusDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar roadworthyStatusDate;
        @XmlElement(name = "RoadworthyTestDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar roadworthyTestDate;
        @XmlElement(name = "VehicleState", required = true)
        protected CodeAndDescriptionElement vehicleState;
        @XmlElement(name = "VehicleStateDate", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar vehicleStateDate;
        @XmlElement(name = "DataOwner", required = true)
        protected CodeAndDescriptionElement dataOwner;
        @XmlElement(name = "Timestamp")
        protected Long timestamp;
        @XmlElement(name = "Transmission")
        protected CodeAndDescriptionElement transmission;
        @XmlElement(name = "GearboxNumber")
        protected String gearboxNumber;
        @XmlElement(name = "DifferentialNumber")
        protected String differentialNumber;
        @XmlElement(name = "FirstLicensingDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar firstLicensingDate;
        @XmlElement(name = "CountryOfExport")
        protected CodeAndDescriptionElement countryOfExport;
        @XmlElement(name = "CountryOfImport")
        protected CodeAndDescriptionElement countryOfImport;
        @XmlElement(name = "LifeStatus", required = true)
        protected CodeAndDescriptionElement lifeStatus;
        @XmlElement(name = "ModelNumber")
        protected Integer modelNumber;
        @XmlElement(name = "SapClearanceReason")
        protected CodeAndDescriptionElement sapClearanceReason;
        @XmlElement(name = "SapClearanceStatus", required = true)
        protected CodeAndDescriptionElement sapClearanceStatus;
        @XmlElement(name = "SapClearanceDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar sapClearanceDate;
        @XmlElement(name = "SapMarkDate")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar sapMarkDate;
        @XmlElement(name = "VehicleUsage")
        protected CodeAndDescriptionElement vehicleUsage;
        @XmlElement(name = "EconomicSector")
        protected CodeAndDescriptionElement economicSector;
        @XmlElement(name = "CertificateNumber", required = true)
        protected String certificateNumber;
        @XmlElement(name = "PreviousCertificateNumber")
        protected String previousCertificateNumber;
        @XmlElement(name = "PreviousLicenceNumber")
        protected String previousLicenceNumber;
        @XmlElement(name = "PrePreviousLicenceNumber")
        protected String prePreviousLicenceNumber;
        @XmlElement(name = "AxlesTotal")
        protected Integer axlesTotal;
        @XmlElement(name = "NoOfWheels")
        protected Integer noOfWheels;
        @XmlElement(name = "OverallWidth")
        protected Integer overallWidth;
        @XmlElement(name = "AxlesDriven")
        protected Integer axlesDriven;
        @XmlElement(name = "OverallLength")
        protected Integer overallLength;
        @XmlElement(name = "OverallHeight")
        protected Integer overallHeight;
        @XmlElement(name = "CapacitySitting")
        protected Integer capacitySitting;
        @XmlElement(name = "CapacityStanding")
        protected Integer capacityStanding;
        @XmlElement(name = "LicenceFee", required = true)
        protected BigDecimal licenceFee;
        @XmlElement(name = "RegistrationType", required = true)
        protected CodeAndDescriptionElement registrationType;
        @XmlElement(name = "VtsNumber")
        protected String vtsNumber;
        @XmlElement(name = "VtsName")
        protected String vtsName;
        @XmlElement(name = "ExaminerNumber")
        protected String examinerNumber;
        @XmlElement(name = "ExaminerName")
        protected String examinerName;
        @XmlElement(name = "Exemption", required = true)
        protected CodeAndDescriptionElement exemption;
        @XmlElement(name = "RoadUseIndicator")
        protected boolean roadUseIndicator;
        @XmlElement(name = "Overdue")
        protected boolean overdue;

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the sapMark property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapMark() {
            return sapMark;
        }

        /**
         * Sets the value of the sapMark property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapMark(CodeAndDescriptionElement value) {
            this.sapMark = value;
        }

        /**
         * Gets the value of the licenceChangeDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceChangeDate() {
            return licenceChangeDate;
        }

        /**
         * Sets the value of the licenceChangeDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceChangeDate(XMLGregorianCalendar value) {
            this.licenceChangeDate = value;
        }

        /**
         * Gets the value of the licenceLiabilityDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceLiabilityDate() {
            return licenceLiabilityDate;
        }

        /**
         * Sets the value of the licenceLiabilityDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceLiabilityDate(XMLGregorianCalendar value) {
            this.licenceLiabilityDate = value;
        }

        /**
         * Gets the value of the licenceExpiryDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLicenceExpiryDate() {
            return licenceExpiryDate;
        }

        /**
         * Sets the value of the licenceExpiryDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLicenceExpiryDate(XMLGregorianCalendar value) {
            this.licenceExpiryDate = value;
        }

        /**
         * Gets the value of the regAuthorityOfLicensing property.
         * 
         * @return
         *     possible object is
         *     {@link AuthorityElement }
         *     
         */
        public AuthorityElement getRegAuthorityOfLicensing() {
            return regAuthorityOfLicensing;
        }

        /**
         * Sets the value of the regAuthorityOfLicensing property.
         * 
         * @param value
         *     allowed object is
         *     {@link AuthorityElement }
         *     
         */
        public void setRegAuthorityOfLicensing(AuthorityElement value) {
            this.regAuthorityOfLicensing = value;
        }

        /**
         * Gets the value of the regAuthorityOfLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link AuthorityElement }
         *     
         */
        public AuthorityElement getRegAuthorityOfLicenceNumber() {
            return regAuthorityOfLicenceNumber;
        }

        /**
         * Sets the value of the regAuthorityOfLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link AuthorityElement }
         *     
         */
        public void setRegAuthorityOfLicenceNumber(AuthorityElement value) {
            this.regAuthorityOfLicenceNumber = value;
        }

        /**
         * Gets the value of the modelName property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getModelName() {
            return modelName;
        }

        /**
         * Sets the value of the modelName property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setModelName(CodeAndDescriptionElement value) {
            this.modelName = value;
        }

        /**
         * Gets the value of the category property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getCategory() {
            return category;
        }

        /**
         * Sets the value of the category property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setCategory(CodeAndDescriptionElement value) {
            this.category = value;
        }

        /**
         * Gets the value of the driven property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDriven() {
            return driven;
        }

        /**
         * Sets the value of the driven property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDriven(CodeAndDescriptionElement value) {
            this.driven = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDescription(CodeAndDescriptionElement value) {
            this.description = value;
        }

        /**
         * Gets the value of the make property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getMake() {
            return make;
        }

        /**
         * Sets the value of the make property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setMake(CodeAndDescriptionElement value) {
            this.make = value;
        }

        /**
         * Gets the value of the mainColour property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getMainColour() {
            return mainColour;
        }

        /**
         * Sets the value of the mainColour property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setMainColour(CodeAndDescriptionElement value) {
            this.mainColour = value;
        }

        /**
         * Gets the value of the gvm property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getGVM() {
            return gvm;
        }

        /**
         * Sets the value of the gvm property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setGVM(Integer value) {
            this.gvm = value;
        }

        /**
         * Gets the value of the engineDisplacement property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getEngineDisplacement() {
            return engineDisplacement;
        }

        /**
         * Sets the value of the engineDisplacement property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setEngineDisplacement(Integer value) {
            this.engineDisplacement = value;
        }

        /**
         * Gets the value of the tare property.
         * 
         */
        public int getTare() {
            return tare;
        }

        /**
         * Sets the value of the tare property.
         * 
         */
        public void setTare(int value) {
            this.tare = value;
        }

        /**
         * Gets the value of the fuelType property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getFuelType() {
            return fuelType;
        }

        /**
         * Sets the value of the fuelType property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setFuelType(CodeAndDescriptionElement value) {
            this.fuelType = value;
        }

        /**
         * Gets the value of the netPower property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNetPower() {
            return netPower;
        }

        /**
         * Sets the value of the netPower property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNetPower(Integer value) {
            this.netPower = value;
        }

        /**
         * Gets the value of the registrationDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRegistrationDate() {
            return registrationDate;
        }

        /**
         * Sets the value of the registrationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRegistrationDate(XMLGregorianCalendar value) {
            this.registrationDate = value;
        }

        /**
         * Gets the value of the registrationQualifier property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRegistrationQualifier() {
            return registrationQualifier;
        }

        /**
         * Sets the value of the registrationQualifier property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRegistrationQualifier(CodeAndDescriptionElement value) {
            this.registrationQualifier = value;
        }

        /**
         * Gets the value of the registrationQualifierDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRegistrationQualifierDate() {
            return registrationQualifierDate;
        }

        /**
         * Sets the value of the registrationQualifierDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRegistrationQualifierDate(XMLGregorianCalendar value) {
            this.registrationQualifierDate = value;
        }

        /**
         * Gets the value of the roadworthyStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRoadworthyStatus() {
            return roadworthyStatus;
        }

        /**
         * Sets the value of the roadworthyStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRoadworthyStatus(CodeAndDescriptionElement value) {
            this.roadworthyStatus = value;
        }

        /**
         * Gets the value of the roadworthyStatusDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRoadworthyStatusDate() {
            return roadworthyStatusDate;
        }

        /**
         * Sets the value of the roadworthyStatusDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRoadworthyStatusDate(XMLGregorianCalendar value) {
            this.roadworthyStatusDate = value;
        }

        /**
         * Gets the value of the roadworthyTestDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRoadworthyTestDate() {
            return roadworthyTestDate;
        }

        /**
         * Sets the value of the roadworthyTestDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRoadworthyTestDate(XMLGregorianCalendar value) {
            this.roadworthyTestDate = value;
        }

        /**
         * Gets the value of the vehicleState property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getVehicleState() {
            return vehicleState;
        }

        /**
         * Sets the value of the vehicleState property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setVehicleState(CodeAndDescriptionElement value) {
            this.vehicleState = value;
        }

        /**
         * Gets the value of the vehicleStateDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getVehicleStateDate() {
            return vehicleStateDate;
        }

        /**
         * Sets the value of the vehicleStateDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setVehicleStateDate(XMLGregorianCalendar value) {
            this.vehicleStateDate = value;
        }

        /**
         * Gets the value of the dataOwner property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getDataOwner() {
            return dataOwner;
        }

        /**
         * Sets the value of the dataOwner property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setDataOwner(CodeAndDescriptionElement value) {
            this.dataOwner = value;
        }

        /**
         * Gets the value of the timestamp property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getTimestamp() {
            return timestamp;
        }

        /**
         * Sets the value of the timestamp property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setTimestamp(Long value) {
            this.timestamp = value;
        }

        /**
         * Gets the value of the transmission property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getTransmission() {
            return transmission;
        }

        /**
         * Sets the value of the transmission property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setTransmission(CodeAndDescriptionElement value) {
            this.transmission = value;
        }

        /**
         * Gets the value of the gearboxNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGearboxNumber() {
            return gearboxNumber;
        }

        /**
         * Sets the value of the gearboxNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGearboxNumber(String value) {
            this.gearboxNumber = value;
        }

        /**
         * Gets the value of the differentialNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDifferentialNumber() {
            return differentialNumber;
        }

        /**
         * Sets the value of the differentialNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDifferentialNumber(String value) {
            this.differentialNumber = value;
        }

        /**
         * Gets the value of the firstLicensingDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFirstLicensingDate() {
            return firstLicensingDate;
        }

        /**
         * Sets the value of the firstLicensingDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFirstLicensingDate(XMLGregorianCalendar value) {
            this.firstLicensingDate = value;
        }

        /**
         * Gets the value of the countryOfExport property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getCountryOfExport() {
            return countryOfExport;
        }

        /**
         * Sets the value of the countryOfExport property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setCountryOfExport(CodeAndDescriptionElement value) {
            this.countryOfExport = value;
        }

        /**
         * Gets the value of the countryOfImport property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getCountryOfImport() {
            return countryOfImport;
        }

        /**
         * Sets the value of the countryOfImport property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setCountryOfImport(CodeAndDescriptionElement value) {
            this.countryOfImport = value;
        }

        /**
         * Gets the value of the lifeStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getLifeStatus() {
            return lifeStatus;
        }

        /**
         * Sets the value of the lifeStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setLifeStatus(CodeAndDescriptionElement value) {
            this.lifeStatus = value;
        }

        /**
         * Gets the value of the modelNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getModelNumber() {
            return modelNumber;
        }

        /**
         * Sets the value of the modelNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setModelNumber(Integer value) {
            this.modelNumber = value;
        }

        /**
         * Gets the value of the sapClearanceReason property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapClearanceReason() {
            return sapClearanceReason;
        }

        /**
         * Sets the value of the sapClearanceReason property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapClearanceReason(CodeAndDescriptionElement value) {
            this.sapClearanceReason = value;
        }

        /**
         * Gets the value of the sapClearanceStatus property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getSapClearanceStatus() {
            return sapClearanceStatus;
        }

        /**
         * Sets the value of the sapClearanceStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setSapClearanceStatus(CodeAndDescriptionElement value) {
            this.sapClearanceStatus = value;
        }

        /**
         * Gets the value of the sapClearanceDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSapClearanceDate() {
            return sapClearanceDate;
        }

        /**
         * Sets the value of the sapClearanceDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSapClearanceDate(XMLGregorianCalendar value) {
            this.sapClearanceDate = value;
        }

        /**
         * Gets the value of the sapMarkDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getSapMarkDate() {
            return sapMarkDate;
        }

        /**
         * Sets the value of the sapMarkDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setSapMarkDate(XMLGregorianCalendar value) {
            this.sapMarkDate = value;
        }

        /**
         * Gets the value of the vehicleUsage property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getVehicleUsage() {
            return vehicleUsage;
        }

        /**
         * Sets the value of the vehicleUsage property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setVehicleUsage(CodeAndDescriptionElement value) {
            this.vehicleUsage = value;
        }

        /**
         * Gets the value of the economicSector property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getEconomicSector() {
            return economicSector;
        }

        /**
         * Sets the value of the economicSector property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setEconomicSector(CodeAndDescriptionElement value) {
            this.economicSector = value;
        }

        /**
         * Gets the value of the certificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCertificateNumber() {
            return certificateNumber;
        }

        /**
         * Sets the value of the certificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCertificateNumber(String value) {
            this.certificateNumber = value;
        }

        /**
         * Gets the value of the previousCertificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousCertificateNumber() {
            return previousCertificateNumber;
        }

        /**
         * Sets the value of the previousCertificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousCertificateNumber(String value) {
            this.previousCertificateNumber = value;
        }

        /**
         * Gets the value of the previousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreviousLicenceNumber() {
            return previousLicenceNumber;
        }

        /**
         * Sets the value of the previousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreviousLicenceNumber(String value) {
            this.previousLicenceNumber = value;
        }

        /**
         * Gets the value of the prePreviousLicenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrePreviousLicenceNumber() {
            return prePreviousLicenceNumber;
        }

        /**
         * Sets the value of the prePreviousLicenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrePreviousLicenceNumber(String value) {
            this.prePreviousLicenceNumber = value;
        }

        /**
         * Gets the value of the axlesTotal property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAxlesTotal() {
            return axlesTotal;
        }

        /**
         * Sets the value of the axlesTotal property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAxlesTotal(Integer value) {
            this.axlesTotal = value;
        }

        /**
         * Gets the value of the noOfWheels property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNoOfWheels() {
            return noOfWheels;
        }

        /**
         * Sets the value of the noOfWheels property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNoOfWheels(Integer value) {
            this.noOfWheels = value;
        }

        /**
         * Gets the value of the overallWidth property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOverallWidth() {
            return overallWidth;
        }

        /**
         * Sets the value of the overallWidth property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOverallWidth(Integer value) {
            this.overallWidth = value;
        }

        /**
         * Gets the value of the axlesDriven property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getAxlesDriven() {
            return axlesDriven;
        }

        /**
         * Sets the value of the axlesDriven property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setAxlesDriven(Integer value) {
            this.axlesDriven = value;
        }

        /**
         * Gets the value of the overallLength property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOverallLength() {
            return overallLength;
        }

        /**
         * Sets the value of the overallLength property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOverallLength(Integer value) {
            this.overallLength = value;
        }

        /**
         * Gets the value of the overallHeight property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOverallHeight() {
            return overallHeight;
        }

        /**
         * Sets the value of the overallHeight property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOverallHeight(Integer value) {
            this.overallHeight = value;
        }

        /**
         * Gets the value of the capacitySitting property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCapacitySitting() {
            return capacitySitting;
        }

        /**
         * Sets the value of the capacitySitting property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCapacitySitting(Integer value) {
            this.capacitySitting = value;
        }

        /**
         * Gets the value of the capacityStanding property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCapacityStanding() {
            return capacityStanding;
        }

        /**
         * Sets the value of the capacityStanding property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCapacityStanding(Integer value) {
            this.capacityStanding = value;
        }

        /**
         * Gets the value of the licenceFee property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getLicenceFee() {
            return licenceFee;
        }

        /**
         * Sets the value of the licenceFee property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setLicenceFee(BigDecimal value) {
            this.licenceFee = value;
        }

        /**
         * Gets the value of the registrationType property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getRegistrationType() {
            return registrationType;
        }

        /**
         * Sets the value of the registrationType property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setRegistrationType(CodeAndDescriptionElement value) {
            this.registrationType = value;
        }

        /**
         * Gets the value of the vtsNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVtsNumber() {
            return vtsNumber;
        }

        /**
         * Sets the value of the vtsNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVtsNumber(String value) {
            this.vtsNumber = value;
        }

        /**
         * Gets the value of the vtsName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVtsName() {
            return vtsName;
        }

        /**
         * Sets the value of the vtsName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVtsName(String value) {
            this.vtsName = value;
        }

        /**
         * Gets the value of the examinerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExaminerNumber() {
            return examinerNumber;
        }

        /**
         * Sets the value of the examinerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExaminerNumber(String value) {
            this.examinerNumber = value;
        }

        /**
         * Gets the value of the examinerName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExaminerName() {
            return examinerName;
        }

        /**
         * Sets the value of the examinerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExaminerName(String value) {
            this.examinerName = value;
        }

        /**
         * Gets the value of the exemption property.
         * 
         * @return
         *     possible object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public CodeAndDescriptionElement getExemption() {
            return exemption;
        }

        /**
         * Sets the value of the exemption property.
         * 
         * @param value
         *     allowed object is
         *     {@link CodeAndDescriptionElement }
         *     
         */
        public void setExemption(CodeAndDescriptionElement value) {
            this.exemption = value;
        }

        /**
         * Gets the value of the roadUseIndicator property.
         * 
         */
        public boolean isRoadUseIndicator() {
            return roadUseIndicator;
        }

        /**
         * Sets the value of the roadUseIndicator property.
         * 
         */
        public void setRoadUseIndicator(boolean value) {
            this.roadUseIndicator = value;
        }

        /**
         * Gets the value of the overdue property.
         * 
         */
        public boolean isOverdue() {
            return overdue;
        }

        /**
         * Sets the value of the overdue property.
         * 
         */
        public void setOverdue(boolean value) {
            this.overdue = value;
        }

    }

}
