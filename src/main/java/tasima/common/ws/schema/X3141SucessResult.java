
package tasima.common.ws.schema;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3141SucessResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3141SucessResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegistrationFeeAmount" type="{http://tasima/common/ws/schema/}Money"/>
 *         &lt;element name="PaymentReferenceNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;pattern value="[A-Z0-9]{12}"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VehicleParticulars">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
 *                   &lt;element name="VehicleCertificateNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TitleHolder" type="{http://tasima/common/ws/schema/}X3141_PersonIdentification"/>
 *         &lt;element name="Owner" type="{http://tasima/common/ws/schema/}X3141_PersonIdentification"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3141SucessResult", propOrder = {
    "registrationFeeAmount",
    "paymentReferenceNumber",
    "vehicleParticulars",
    "titleHolder",
    "owner"
})
public class X3141SucessResult {

    @XmlElement(name = "RegistrationFeeAmount", required = true)
    protected BigDecimal registrationFeeAmount;
    @XmlElement(name = "PaymentReferenceNumber", required = true)
    protected String paymentReferenceNumber;
    @XmlElement(name = "VehicleParticulars", required = true)
    protected X3141SucessResult.VehicleParticulars vehicleParticulars;
    @XmlElement(name = "TitleHolder", required = true)
    protected X3141PersonIdentification titleHolder;
    @XmlElement(name = "Owner", required = true)
    protected X3141PersonIdentification owner;

    /**
     * Gets the value of the registrationFeeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRegistrationFeeAmount() {
        return registrationFeeAmount;
    }

    /**
     * Sets the value of the registrationFeeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRegistrationFeeAmount(BigDecimal value) {
        this.registrationFeeAmount = value;
    }

    /**
     * Gets the value of the paymentReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReferenceNumber() {
        return paymentReferenceNumber;
    }

    /**
     * Sets the value of the paymentReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReferenceNumber(String value) {
        this.paymentReferenceNumber = value;
    }

    /**
     * Gets the value of the vehicleParticulars property.
     * 
     * @return
     *     possible object is
     *     {@link X3141SucessResult.VehicleParticulars }
     *     
     */
    public X3141SucessResult.VehicleParticulars getVehicleParticulars() {
        return vehicleParticulars;
    }

    /**
     * Sets the value of the vehicleParticulars property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141SucessResult.VehicleParticulars }
     *     
     */
    public void setVehicleParticulars(X3141SucessResult.VehicleParticulars value) {
        this.vehicleParticulars = value;
    }

    /**
     * Gets the value of the titleHolder property.
     * 
     * @return
     *     possible object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public X3141PersonIdentification getTitleHolder() {
        return titleHolder;
    }

    /**
     * Sets the value of the titleHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public void setTitleHolder(X3141PersonIdentification value) {
        this.titleHolder = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public X3141PersonIdentification getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public void setOwner(X3141PersonIdentification value) {
        this.owner = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
     *         &lt;element name="VehicleCertificateNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class VehicleParticulars {

        @XmlElement(name = "RegisterNumber")
        protected String registerNumber;
        @XmlElement(name = "VinOrChassis")
        protected String vinOrChassis;
        @XmlElement(name = "VehicleCertificateNumber", required = true)
        protected String vehicleCertificateNumber;

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the vehicleCertificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCertificateNumber() {
            return vehicleCertificateNumber;
        }

        /**
         * Sets the value of the vehicleCertificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCertificateNumber(String value) {
            this.vehicleCertificateNumber = value;
        }

    }

}
