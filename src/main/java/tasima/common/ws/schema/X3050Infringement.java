
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for X3050Infringement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3050Infringement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicle" type="{http://tasima/common/ws/schema/}X3050Vehicle"/>
 *         &lt;element name="charge">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="mainCharge" type="{http://tasima/common/ws/schema/}X3050Charge"/>
 *                   &lt;element name="alternativeCharge" type="{http://tasima/common/ws/schema/}X3050Charge" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="fees" type="{http://tasima/common/ws/schema/}X3050Fees" minOccurs="0"/>
 *         &lt;element name="infringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="infringementStatus" type="{http://tasima/common/ws/schema/}X3050CodeDesc"/>
 *         &lt;element name="infringeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="captureDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="location" type="{http://tasima/common/ws/schema/}X3050Location"/>
 *         &lt;element name="parkingNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operatorNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3050Infringement", propOrder = {
    "vehicle",
    "charge",
    "fees",
    "infringementNoticeNumber",
    "infringementStatus",
    "infringeDate",
    "captureDate",
    "location",
    "parkingNoticeNumber",
    "operatorNoticeNumber"
})
public class X3050Infringement {

    @XmlElement(required = true)
    protected X3050Vehicle vehicle;
    @XmlElement(required = true)
    protected X3050Infringement.Charge charge;
    protected X3050Fees fees;
    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected X3050CodeDesc infringementStatus;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar infringeDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar captureDate;
    @XmlElement(required = true)
    protected X3050Location location;
    protected String parkingNoticeNumber;
    protected String operatorNoticeNumber;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Vehicle }
     *     
     */
    public X3050Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Vehicle }
     *     
     */
    public void setVehicle(X3050Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the charge property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Infringement.Charge }
     *     
     */
    public X3050Infringement.Charge getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Infringement.Charge }
     *     
     */
    public void setCharge(X3050Infringement.Charge value) {
        this.charge = value;
    }

    /**
     * Gets the value of the fees property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Fees }
     *     
     */
    public X3050Fees getFees() {
        return fees;
    }

    /**
     * Sets the value of the fees property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Fees }
     *     
     */
    public void setFees(X3050Fees value) {
        this.fees = value;
    }

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the infringementStatus property.
     * 
     * @return
     *     possible object is
     *     {@link X3050CodeDesc }
     *     
     */
    public X3050CodeDesc getInfringementStatus() {
        return infringementStatus;
    }

    /**
     * Sets the value of the infringementStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050CodeDesc }
     *     
     */
    public void setInfringementStatus(X3050CodeDesc value) {
        this.infringementStatus = value;
    }

    /**
     * Gets the value of the infringeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringeDate() {
        return infringeDate;
    }

    /**
     * Sets the value of the infringeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringeDate(XMLGregorianCalendar value) {
        this.infringeDate = value;
    }

    /**
     * Gets the value of the captureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCaptureDate() {
        return captureDate;
    }

    /**
     * Sets the value of the captureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCaptureDate(XMLGregorianCalendar value) {
        this.captureDate = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Location }
     *     
     */
    public X3050Location getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Location }
     *     
     */
    public void setLocation(X3050Location value) {
        this.location = value;
    }

    /**
     * Gets the value of the parkingNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingNoticeNumber() {
        return parkingNoticeNumber;
    }

    /**
     * Sets the value of the parkingNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingNoticeNumber(String value) {
        this.parkingNoticeNumber = value;
    }

    /**
     * Gets the value of the operatorNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorNoticeNumber() {
        return operatorNoticeNumber;
    }

    /**
     * Sets the value of the operatorNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorNoticeNumber(String value) {
        this.operatorNoticeNumber = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="mainCharge" type="{http://tasima/common/ws/schema/}X3050Charge"/>
     *         &lt;element name="alternativeCharge" type="{http://tasima/common/ws/schema/}X3050Charge" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mainCharge",
        "alternativeCharge"
    })
    public static class Charge {

        @XmlElement(required = true)
        protected X3050Charge mainCharge;
        protected X3050Charge alternativeCharge;

        /**
         * Gets the value of the mainCharge property.
         * 
         * @return
         *     possible object is
         *     {@link X3050Charge }
         *     
         */
        public X3050Charge getMainCharge() {
            return mainCharge;
        }

        /**
         * Sets the value of the mainCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3050Charge }
         *     
         */
        public void setMainCharge(X3050Charge value) {
            this.mainCharge = value;
        }

        /**
         * Gets the value of the alternativeCharge property.
         * 
         * @return
         *     possible object is
         *     {@link X3050Charge }
         *     
         */
        public X3050Charge getAlternativeCharge() {
            return alternativeCharge;
        }

        /**
         * Sets the value of the alternativeCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3050Charge }
         *     
         */
        public void setAlternativeCharge(X3050Charge value) {
            this.alternativeCharge = value;
        }

    }

}
