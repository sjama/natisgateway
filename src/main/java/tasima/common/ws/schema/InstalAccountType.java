
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InstalAccountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InstalAccountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="effD" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="penaltyInstalment" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="feeInstalment" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstalAccountType", propOrder = {
    "effD",
    "penaltyInstalment",
    "feeInstalment"
})
public class InstalAccountType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effD;
    protected double penaltyInstalment;
    protected double feeInstalment;

    /**
     * Gets the value of the effD property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffD() {
        return effD;
    }

    /**
     * Sets the value of the effD property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffD(XMLGregorianCalendar value) {
        this.effD = value;
    }

    /**
     * Gets the value of the penaltyInstalment property.
     * 
     */
    public double getPenaltyInstalment() {
        return penaltyInstalment;
    }

    /**
     * Sets the value of the penaltyInstalment property.
     * 
     */
    public void setPenaltyInstalment(double value) {
        this.penaltyInstalment = value;
    }

    /**
     * Gets the value of the feeInstalment property.
     * 
     */
    public double getFeeInstalment() {
        return feeInstalment;
    }

    /**
     * Sets the value of the feeInstalment property.
     * 
     */
    public void setFeeInstalment(double value) {
        this.feeInstalment = value;
    }

}
