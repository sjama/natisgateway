
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="infringementCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aarto05e" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "ApplyForRevocationResponse")
public class ApplyForRevocationResponse {

    @XmlElement(required = true)
    protected String infringementCertificateNumber;
    @XmlElement(name = "aarto05e")
    protected byte[] aarto05E;

    /**
     * Gets the value of the infringementCertificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementCertificateNumber() {
        return infringementCertificateNumber;
    }

    /**
     * Sets the value of the infringementCertificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementCertificateNumber(String value) {
        this.infringementCertificateNumber = value;
    }

    /**
     * Gets the value of the aarto05E property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto05E() {
        return aarto05E;
    }

    /**
     * Sets the value of the aarto05E property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto05E(byte[] value) {
        this.aarto05E = value;
    }

}
