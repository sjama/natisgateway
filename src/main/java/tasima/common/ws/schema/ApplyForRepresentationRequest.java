
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringementNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aarto08" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="supportingDoc1" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="supportingDoc2" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="supportingDoc3" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="supportingDoc4" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringementNoticeNumber",
    "aarto08",
    "supportingDoc1",
    "supportingDoc2",
    "supportingDoc3",
    "supportingDoc4"
})
@XmlRootElement(name = "ApplyForRepresentationRequest")
public class ApplyForRepresentationRequest {

    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected byte[] aarto08;
    protected byte[] supportingDoc1;
    protected byte[] supportingDoc2;
    protected byte[] supportingDoc3;
    protected byte[] supportingDoc4;

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the aarto08 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto08() {
        return aarto08;
    }

    /**
     * Sets the value of the aarto08 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto08(byte[] value) {
        this.aarto08 = value;
    }

    /**
     * Gets the value of the supportingDoc1 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSupportingDoc1() {
        return supportingDoc1;
    }

    /**
     * Sets the value of the supportingDoc1 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSupportingDoc1(byte[] value) {
        this.supportingDoc1 = value;
    }

    /**
     * Gets the value of the supportingDoc2 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSupportingDoc2() {
        return supportingDoc2;
    }

    /**
     * Sets the value of the supportingDoc2 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSupportingDoc2(byte[] value) {
        this.supportingDoc2 = value;
    }

    /**
     * Gets the value of the supportingDoc3 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSupportingDoc3() {
        return supportingDoc3;
    }

    /**
     * Sets the value of the supportingDoc3 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSupportingDoc3(byte[] value) {
        this.supportingDoc3 = value;
    }

    /**
     * Gets the value of the supportingDoc4 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSupportingDoc4() {
        return supportingDoc4;
    }

    /**
     * Sets the value of the supportingDoc4 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSupportingDoc4(byte[] value) {
        this.supportingDoc4 = value;
    }

}
