
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InfringementVehicleElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InfringementVehicleElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="licenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement"/>
 *         &lt;element name="make" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *         &lt;element name="modelName" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *         &lt;element name="colour" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *         &lt;element name="vehicleImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="licenceImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfringementVehicleElement", propOrder = {
    "licenceNumber",
    "make",
    "modelName",
    "colour",
    "vehicleImage",
    "licenceImage"
})
public class InfringementVehicleElement {

    @XmlElement(required = true)
    protected String licenceNumber;
    protected CodeAndDescriptionElement make;
    protected CodeAndDescriptionElement modelName;
    protected CodeAndDescriptionElement colour;
    protected byte[] vehicleImage;
    protected byte[] licenceImage;

    /**
     * Gets the value of the licenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenceNumber() {
        return licenceNumber;
    }

    /**
     * Sets the value of the licenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenceNumber(String value) {
        this.licenceNumber = value;
    }

    /**
     * Gets the value of the make property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getMake() {
        return make;
    }

    /**
     * Sets the value of the make property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setMake(CodeAndDescriptionElement value) {
        this.make = value;
    }

    /**
     * Gets the value of the modelName property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getModelName() {
        return modelName;
    }

    /**
     * Sets the value of the modelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setModelName(CodeAndDescriptionElement value) {
        this.modelName = value;
    }

    /**
     * Gets the value of the colour property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getColour() {
        return colour;
    }

    /**
     * Sets the value of the colour property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setColour(CodeAndDescriptionElement value) {
        this.colour = value;
    }

    /**
     * Gets the value of the vehicleImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getVehicleImage() {
        return vehicleImage;
    }

    /**
     * Sets the value of the vehicleImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setVehicleImage(byte[] value) {
        this.vehicleImage = value;
    }

    /**
     * Gets the value of the licenceImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getLicenceImage() {
        return licenceImage;
    }

    /**
     * Sets the value of the licenceImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setLicenceImage(byte[] value) {
        this.licenceImage = value;
    }

}
