
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Response for vehicle release
 * 
 * <p>Java class for X314ASuccessResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X314ASuccessResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RC2">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="RegAuthority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement"/>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
 *                   &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber"/>
 *                   &lt;element name="Make" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ModelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="VehicleCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Driven" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="VehicleDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Tare">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="5"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="FirstLicenceLiabiltyDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="VehicleLifeStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Seller">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdDocumentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                             &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Purchaser">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IdDocumentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *                             &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="FirstRegistrationLiabiltyDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="IssueDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="IssuedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="VehicleCertificateNumber">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="12"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Barcode" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="UserGroupCode">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="4"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="Watermark" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X314ASuccessResult", propOrder = {
    "rc2"
})
public class X314ASuccessResult {

    @XmlElement(name = "RC2", required = true)
    protected X314ASuccessResult.RC2 rc2;

    /**
     * Gets the value of the rc2 property.
     * 
     * @return
     *     possible object is
     *     {@link X314ASuccessResult.RC2 }
     *     
     */
    public X314ASuccessResult.RC2 getRC2() {
        return rc2;
    }

    /**
     * Sets the value of the rc2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link X314ASuccessResult.RC2 }
     *     
     */
    public void setRC2(X314ASuccessResult.RC2 value) {
        this.rc2 = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="RegAuthority" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement"/>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber"/>
     *         &lt;element name="EngineNumber" type="{http://tasima/common/ws/schema/}EngineNumber"/>
     *         &lt;element name="Make" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ModelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="VehicleCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Driven" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="VehicleDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Tare">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="5"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="FirstLicenceLiabiltyDate" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="VehicleLifeStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Seller">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdDocumentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *                   &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Purchaser">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IdDocumentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
     *                   &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="FirstRegistrationLiabiltyDate" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="IssueDate" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="IssuedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="VehicleCertificateNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="12"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Barcode" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="UserGroupCode">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="4"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="Watermark" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class RC2 {

        @XmlElement(name = "RegAuthority", required = true)
        protected String regAuthority;
        @XmlElement(name = "RegisterNumber", required = true)
        protected String registerNumber;
        @XmlElement(name = "VinOrChassis", required = true)
        protected String vinOrChassis;
        @XmlElement(name = "EngineNumber", required = true)
        protected String engineNumber;
        @XmlElement(name = "Make", required = true)
        protected String make;
        @XmlElement(name = "ModelName", required = true)
        protected String modelName;
        @XmlElement(name = "VehicleCategory", required = true)
        protected String vehicleCategory;
        @XmlElement(name = "Driven", required = true)
        protected String driven;
        @XmlElement(name = "VehicleDescription", required = true)
        protected String vehicleDescription;
        @XmlElement(name = "Tare", required = true)
        protected String tare;
        @XmlElement(name = "FirstLicenceLiabiltyDate", required = true)
        protected XMLGregorianCalendar firstLicenceLiabiltyDate;
        @XmlElement(name = "VehicleLifeStatus", required = true)
        protected String vehicleLifeStatus;
        @XmlElement(name = "Seller", required = true)
        protected X314ASuccessResult.RC2 .Seller seller;
        @XmlElement(name = "Purchaser", required = true)
        protected X314ASuccessResult.RC2 .Purchaser purchaser;
        @XmlElement(name = "FirstRegistrationLiabiltyDate", required = true)
        protected XMLGregorianCalendar firstRegistrationLiabiltyDate;
        @XmlElement(name = "IssueDate", required = true)
        protected XMLGregorianCalendar issueDate;
        @XmlElement(name = "IssuedBy", required = true)
        protected String issuedBy;
        @XmlElement(name = "VehicleCertificateNumber", required = true)
        protected String vehicleCertificateNumber;
        @XmlElement(name = "Barcode", required = true)
        protected byte[] barcode;
        @XmlElement(name = "UserGroupCode", required = true)
        protected String userGroupCode;
        @XmlElement(name = "DateTime", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar dateTime;
        @XmlElement(name = "Watermark", required = true)
        protected byte[] watermark;

        /**
         * Gets the value of the regAuthority property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegAuthority() {
            return regAuthority;
        }

        /**
         * Sets the value of the regAuthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegAuthority(String value) {
            this.regAuthority = value;
        }

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the make property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMake() {
            return make;
        }

        /**
         * Sets the value of the make property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMake(String value) {
            this.make = value;
        }

        /**
         * Gets the value of the modelName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModelName() {
            return modelName;
        }

        /**
         * Sets the value of the modelName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModelName(String value) {
            this.modelName = value;
        }

        /**
         * Gets the value of the vehicleCategory property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCategory() {
            return vehicleCategory;
        }

        /**
         * Sets the value of the vehicleCategory property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCategory(String value) {
            this.vehicleCategory = value;
        }

        /**
         * Gets the value of the driven property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDriven() {
            return driven;
        }

        /**
         * Sets the value of the driven property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDriven(String value) {
            this.driven = value;
        }

        /**
         * Gets the value of the vehicleDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleDescription() {
            return vehicleDescription;
        }

        /**
         * Sets the value of the vehicleDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleDescription(String value) {
            this.vehicleDescription = value;
        }

        /**
         * Gets the value of the tare property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTare() {
            return tare;
        }

        /**
         * Sets the value of the tare property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTare(String value) {
            this.tare = value;
        }

        /**
         * Gets the value of the firstLicenceLiabiltyDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFirstLicenceLiabiltyDate() {
            return firstLicenceLiabiltyDate;
        }

        /**
         * Sets the value of the firstLicenceLiabiltyDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFirstLicenceLiabiltyDate(XMLGregorianCalendar value) {
            this.firstLicenceLiabiltyDate = value;
        }

        /**
         * Gets the value of the vehicleLifeStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleLifeStatus() {
            return vehicleLifeStatus;
        }

        /**
         * Sets the value of the vehicleLifeStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleLifeStatus(String value) {
            this.vehicleLifeStatus = value;
        }

        /**
         * Gets the value of the seller property.
         * 
         * @return
         *     possible object is
         *     {@link X314ASuccessResult.RC2 .Seller }
         *     
         */
        public X314ASuccessResult.RC2 .Seller getSeller() {
            return seller;
        }

        /**
         * Sets the value of the seller property.
         * 
         * @param value
         *     allowed object is
         *     {@link X314ASuccessResult.RC2 .Seller }
         *     
         */
        public void setSeller(X314ASuccessResult.RC2 .Seller value) {
            this.seller = value;
        }

        /**
         * Gets the value of the purchaser property.
         * 
         * @return
         *     possible object is
         *     {@link X314ASuccessResult.RC2 .Purchaser }
         *     
         */
        public X314ASuccessResult.RC2 .Purchaser getPurchaser() {
            return purchaser;
        }

        /**
         * Sets the value of the purchaser property.
         * 
         * @param value
         *     allowed object is
         *     {@link X314ASuccessResult.RC2 .Purchaser }
         *     
         */
        public void setPurchaser(X314ASuccessResult.RC2 .Purchaser value) {
            this.purchaser = value;
        }

        /**
         * Gets the value of the firstRegistrationLiabiltyDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getFirstRegistrationLiabiltyDate() {
            return firstRegistrationLiabiltyDate;
        }

        /**
         * Sets the value of the firstRegistrationLiabiltyDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setFirstRegistrationLiabiltyDate(XMLGregorianCalendar value) {
            this.firstRegistrationLiabiltyDate = value;
        }

        /**
         * Gets the value of the issueDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getIssueDate() {
            return issueDate;
        }

        /**
         * Sets the value of the issueDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setIssueDate(XMLGregorianCalendar value) {
            this.issueDate = value;
        }

        /**
         * Gets the value of the issuedBy property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssuedBy() {
            return issuedBy;
        }

        /**
         * Sets the value of the issuedBy property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssuedBy(String value) {
            this.issuedBy = value;
        }

        /**
         * Gets the value of the vehicleCertificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCertificateNumber() {
            return vehicleCertificateNumber;
        }

        /**
         * Sets the value of the vehicleCertificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCertificateNumber(String value) {
            this.vehicleCertificateNumber = value;
        }

        /**
         * Gets the value of the barcode property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getBarcode() {
            return barcode;
        }

        /**
         * Sets the value of the barcode property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setBarcode(byte[] value) {
            this.barcode = value;
        }

        /**
         * Gets the value of the userGroupCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserGroupCode() {
            return userGroupCode;
        }

        /**
         * Sets the value of the userGroupCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserGroupCode(String value) {
            this.userGroupCode = value;
        }

        /**
         * Gets the value of the dateTime property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateTime() {
            return dateTime;
        }

        /**
         * Sets the value of the dateTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateTime(XMLGregorianCalendar value) {
            this.dateTime = value;
        }

        /**
         * Gets the value of the watermark property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getWatermark() {
            return watermark;
        }

        /**
         * Sets the value of the watermark property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setWatermark(byte[] value) {
            this.watermark = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdDocumentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
         *         &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idDocumentType",
            "idDocumentNumber",
            "countryOfIssue",
            "name"
        })
        public static class Purchaser {

            @XmlElement(name = "IdDocumentType", required = true)
            protected String idDocumentType;
            @XmlElement(name = "IdDocumentNumber", required = true)
            protected String idDocumentNumber;
            @XmlElement(name = "CountryOfIssue", required = true)
            protected String countryOfIssue;
            @XmlElement(name = "Name", required = true)
            protected String name;

            /**
             * Gets the value of the idDocumentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentType() {
                return idDocumentType;
            }

            /**
             * Sets the value of the idDocumentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentType(String value) {
                this.idDocumentType = value;
            }

            /**
             * Gets the value of the idDocumentNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentNumber() {
                return idDocumentNumber;
            }

            /**
             * Sets the value of the idDocumentNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentNumber(String value) {
                this.idDocumentNumber = value;
            }

            /**
             * Gets the value of the countryOfIssue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfIssue() {
                return countryOfIssue;
            }

            /**
             * Sets the value of the countryOfIssue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfIssue(String value) {
                this.countryOfIssue = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IdDocumentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
         *         &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "idDocumentType",
            "idDocumentNumber",
            "countryOfIssue",
            "name"
        })
        public static class Seller {

            @XmlElement(name = "IdDocumentType", required = true)
            protected String idDocumentType;
            @XmlElement(name = "IdDocumentNumber", required = true)
            protected String idDocumentNumber;
            @XmlElement(name = "CountryOfIssue", required = true)
            protected String countryOfIssue;
            @XmlElement(name = "Name", required = true)
            protected String name;

            /**
             * Gets the value of the idDocumentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentType() {
                return idDocumentType;
            }

            /**
             * Sets the value of the idDocumentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentType(String value) {
                this.idDocumentType = value;
            }

            /**
             * Gets the value of the idDocumentNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdDocumentNumber() {
                return idDocumentNumber;
            }

            /**
             * Sets the value of the idDocumentNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdDocumentNumber(String value) {
                this.idDocumentNumber = value;
            }

            /**
             * Gets the value of the countryOfIssue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfIssue() {
                return countryOfIssue;
            }

            /**
             * Sets the value of the countryOfIssue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfIssue(String value) {
                this.countryOfIssue = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

        }

    }

}
