
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3050ResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3050ResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infringements" type="{http://tasima/common/ws/schema/}X3050Infringement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3050ResponseDetail", propOrder = {
    "infringements"
})
public class X3050ResponseDetail {

    protected List<X3050Infringement> infringements;

    /**
     * Gets the value of the infringements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infringements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfringements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X3050Infringement }
     * 
     * 
     */
    public List<X3050Infringement> getInfringements() {
        if (infringements == null) {
            infringements = new ArrayList<X3050Infringement>();
        }
        return this.infringements;
    }

}
