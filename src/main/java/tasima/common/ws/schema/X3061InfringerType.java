
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for x3061InfringerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="x3061InfringerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="surname">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="initials">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="fullName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="identificationType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061IdDocumentTypeType">
 *               &lt;enumeration value="01"/>
 *               &lt;enumeration value="02"/>
 *               &lt;enumeration value="03"/>
 *               &lt;enumeration value="05"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="identificationNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="gender">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061GenderType">
 *               &lt;enumeration value="01"/>
 *               &lt;enumeration value="02"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dateOfBirth">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}date">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="countryOfIssueID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nationality">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="drivingLicenceType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061DrivingLicenceTypeType">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="drivingLicenceCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061DrivingLicenceCodeType">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="drivingLicenceNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="prdpCodeGoods" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061YesNoType">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="prdpCodeDangerous" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061YesNoType">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="prdpCodePassengers" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061YesNoType">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="leanerLicenceCodeType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://tasima/common/ws/schema/}x3061LeanerLicenceCodeType">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="foreignDrivingLicenceNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="countryOfIssueForeignDL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="homeContactNumber" type="{http://tasima/common/ws/schema/}x3061LandLineType" minOccurs="0"/>
 *         &lt;element name="workContactNumber" type="{http://tasima/common/ws/schema/}x3061LandLineType" minOccurs="0"/>
 *         &lt;element name="faxContactNumber" type="{http://tasima/common/ws/schema/}x3061LandLineType" minOccurs="0"/>
 *         &lt;element name="cellPhoneNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="emailAddress" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="streetAddress" type="{http://tasima/common/ws/schema/}x3061PersonAddress"/>
 *         &lt;element name="postalAddress" type="{http://tasima/common/ws/schema/}x3061PersonAddress"/>
 *         &lt;element name="employerName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="employerAddress" type="{http://tasima/common/ws/schema/}x3061PersonAddress" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "x3061InfringerType", propOrder = {
    "surname",
    "initials",
    "fullName",
    "identificationType",
    "identificationNumber",
    "gender",
    "dateOfBirth",
    "countryOfIssueID",
    "nationality",
    "drivingLicenceType",
    "drivingLicenceCode",
    "drivingLicenceNumber",
    "prdpCodeGoods",
    "prdpCodeDangerous",
    "prdpCodePassengers",
    "leanerLicenceCodeType",
    "foreignDrivingLicenceNumber",
    "countryOfIssueForeignDL",
    "homeContactNumber",
    "workContactNumber",
    "faxContactNumber",
    "cellPhoneNumber",
    "emailAddress",
    "streetAddress",
    "postalAddress",
    "employerName",
    "employerAddress"
})
public class X3061InfringerType {

    @XmlElement(required = true)
    protected String surname;
    @XmlElement(required = true)
    protected String initials;
    @XmlElement(required = true)
    protected String fullName;
    @XmlElement(required = true)
    protected String identificationType;
    @XmlElement(required = true)
    protected String identificationNumber;
    @XmlElement(required = true)
    protected String gender;
    @XmlElement(required = true)
    protected XMLGregorianCalendar dateOfBirth;
    protected String countryOfIssueID;
    @XmlElement(required = true)
    protected String nationality;
    @XmlElement(required = true)
    protected String drivingLicenceType;
    protected X3061DrivingLicenceCodeType drivingLicenceCode;
    protected String drivingLicenceNumber;
    protected X3061YesNoType prdpCodeGoods;
    protected X3061YesNoType prdpCodeDangerous;
    protected X3061YesNoType prdpCodePassengers;
    protected String leanerLicenceCodeType;
    protected String foreignDrivingLicenceNumber;
    protected String countryOfIssueForeignDL;
    protected X3061LandLineType homeContactNumber;
    protected X3061LandLineType workContactNumber;
    protected X3061LandLineType faxContactNumber;
    protected String cellPhoneNumber;
    protected String emailAddress;
    @XmlElement(required = true)
    protected X3061PersonAddress streetAddress;
    @XmlElement(required = true)
    protected X3061PersonAddress postalAddress;
    protected String employerName;
    protected X3061PersonAddress employerAddress;

    /**
     * Gets the value of the surname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the initials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitials() {
        return initials;
    }

    /**
     * Sets the value of the initials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitials(String value) {
        this.initials = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the identificationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationType() {
        return identificationType;
    }

    /**
     * Sets the value of the identificationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationType(String value) {
        this.identificationType = value;
    }

    /**
     * Gets the value of the identificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * Sets the value of the identificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationNumber(String value) {
        this.identificationNumber = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGender(String value) {
        this.gender = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the countryOfIssueID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfIssueID() {
        return countryOfIssueID;
    }

    /**
     * Sets the value of the countryOfIssueID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfIssueID(String value) {
        this.countryOfIssueID = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the drivingLicenceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrivingLicenceType() {
        return drivingLicenceType;
    }

    /**
     * Sets the value of the drivingLicenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrivingLicenceType(String value) {
        this.drivingLicenceType = value;
    }

    /**
     * Gets the value of the drivingLicenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link X3061DrivingLicenceCodeType }
     *     
     */
    public X3061DrivingLicenceCodeType getDrivingLicenceCode() {
        return drivingLicenceCode;
    }

    /**
     * Sets the value of the drivingLicenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061DrivingLicenceCodeType }
     *     
     */
    public void setDrivingLicenceCode(X3061DrivingLicenceCodeType value) {
        this.drivingLicenceCode = value;
    }

    /**
     * Gets the value of the drivingLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrivingLicenceNumber() {
        return drivingLicenceNumber;
    }

    /**
     * Sets the value of the drivingLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrivingLicenceNumber(String value) {
        this.drivingLicenceNumber = value;
    }

    /**
     * Gets the value of the prdpCodeGoods property.
     * 
     * @return
     *     possible object is
     *     {@link X3061YesNoType }
     *     
     */
    public X3061YesNoType getPrdpCodeGoods() {
        return prdpCodeGoods;
    }

    /**
     * Sets the value of the prdpCodeGoods property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061YesNoType }
     *     
     */
    public void setPrdpCodeGoods(X3061YesNoType value) {
        this.prdpCodeGoods = value;
    }

    /**
     * Gets the value of the prdpCodeDangerous property.
     * 
     * @return
     *     possible object is
     *     {@link X3061YesNoType }
     *     
     */
    public X3061YesNoType getPrdpCodeDangerous() {
        return prdpCodeDangerous;
    }

    /**
     * Sets the value of the prdpCodeDangerous property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061YesNoType }
     *     
     */
    public void setPrdpCodeDangerous(X3061YesNoType value) {
        this.prdpCodeDangerous = value;
    }

    /**
     * Gets the value of the prdpCodePassengers property.
     * 
     * @return
     *     possible object is
     *     {@link X3061YesNoType }
     *     
     */
    public X3061YesNoType getPrdpCodePassengers() {
        return prdpCodePassengers;
    }

    /**
     * Sets the value of the prdpCodePassengers property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061YesNoType }
     *     
     */
    public void setPrdpCodePassengers(X3061YesNoType value) {
        this.prdpCodePassengers = value;
    }

    /**
     * Gets the value of the leanerLicenceCodeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeanerLicenceCodeType() {
        return leanerLicenceCodeType;
    }

    /**
     * Sets the value of the leanerLicenceCodeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeanerLicenceCodeType(String value) {
        this.leanerLicenceCodeType = value;
    }

    /**
     * Gets the value of the foreignDrivingLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignDrivingLicenceNumber() {
        return foreignDrivingLicenceNumber;
    }

    /**
     * Sets the value of the foreignDrivingLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignDrivingLicenceNumber(String value) {
        this.foreignDrivingLicenceNumber = value;
    }

    /**
     * Gets the value of the countryOfIssueForeignDL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfIssueForeignDL() {
        return countryOfIssueForeignDL;
    }

    /**
     * Sets the value of the countryOfIssueForeignDL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfIssueForeignDL(String value) {
        this.countryOfIssueForeignDL = value;
    }

    /**
     * Gets the value of the homeContactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link X3061LandLineType }
     *     
     */
    public X3061LandLineType getHomeContactNumber() {
        return homeContactNumber;
    }

    /**
     * Sets the value of the homeContactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061LandLineType }
     *     
     */
    public void setHomeContactNumber(X3061LandLineType value) {
        this.homeContactNumber = value;
    }

    /**
     * Gets the value of the workContactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link X3061LandLineType }
     *     
     */
    public X3061LandLineType getWorkContactNumber() {
        return workContactNumber;
    }

    /**
     * Sets the value of the workContactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061LandLineType }
     *     
     */
    public void setWorkContactNumber(X3061LandLineType value) {
        this.workContactNumber = value;
    }

    /**
     * Gets the value of the faxContactNumber property.
     * 
     * @return
     *     possible object is
     *     {@link X3061LandLineType }
     *     
     */
    public X3061LandLineType getFaxContactNumber() {
        return faxContactNumber;
    }

    /**
     * Sets the value of the faxContactNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061LandLineType }
     *     
     */
    public void setFaxContactNumber(X3061LandLineType value) {
        this.faxContactNumber = value;
    }

    /**
     * Gets the value of the cellPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    /**
     * Sets the value of the cellPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellPhoneNumber(String value) {
        this.cellPhoneNumber = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the streetAddress property.
     * 
     * @return
     *     possible object is
     *     {@link X3061PersonAddress }
     *     
     */
    public X3061PersonAddress getStreetAddress() {
        return streetAddress;
    }

    /**
     * Sets the value of the streetAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061PersonAddress }
     *     
     */
    public void setStreetAddress(X3061PersonAddress value) {
        this.streetAddress = value;
    }

    /**
     * Gets the value of the postalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link X3061PersonAddress }
     *     
     */
    public X3061PersonAddress getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the value of the postalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061PersonAddress }
     *     
     */
    public void setPostalAddress(X3061PersonAddress value) {
        this.postalAddress = value;
    }

    /**
     * Gets the value of the employerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerName() {
        return employerName;
    }

    /**
     * Sets the value of the employerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerName(String value) {
        this.employerName = value;
    }

    /**
     * Gets the value of the employerAddress property.
     * 
     * @return
     *     possible object is
     *     {@link X3061PersonAddress }
     *     
     */
    public X3061PersonAddress getEmployerAddress() {
        return employerAddress;
    }

    /**
     * Sets the value of the employerAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061PersonAddress }
     *     
     */
    public void setEmployerAddress(X3061PersonAddress value) {
        this.employerAddress = value;
    }

}
