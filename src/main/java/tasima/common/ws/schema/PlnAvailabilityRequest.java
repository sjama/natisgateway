
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="provincialPersonalisedLicenceNumber" type="{http://tasima/common/ws/schema/}ProvincialPersonalisedLicenceNumberElement"/>
 *         &lt;element name="specificLicenceNumber" type="{http://tasima/common/ws/schema/}SpecificLicenceNumberElement"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "provincialPersonalisedLicenceNumber",
    "specificLicenceNumber"
})
@XmlRootElement(name = "PlnAvailabilityRequest")
public class PlnAvailabilityRequest {

    protected ProvincialPersonalisedLicenceNumberElement provincialPersonalisedLicenceNumber;
    protected SpecificLicenceNumberElement specificLicenceNumber;

    /**
     * Gets the value of the provincialPersonalisedLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link ProvincialPersonalisedLicenceNumberElement }
     *     
     */
    public ProvincialPersonalisedLicenceNumberElement getProvincialPersonalisedLicenceNumber() {
        return provincialPersonalisedLicenceNumber;
    }

    /**
     * Sets the value of the provincialPersonalisedLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvincialPersonalisedLicenceNumberElement }
     *     
     */
    public void setProvincialPersonalisedLicenceNumber(ProvincialPersonalisedLicenceNumberElement value) {
        this.provincialPersonalisedLicenceNumber = value;
    }

    /**
     * Gets the value of the specificLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link SpecificLicenceNumberElement }
     *     
     */
    public SpecificLicenceNumberElement getSpecificLicenceNumber() {
        return specificLicenceNumber;
    }

    /**
     * Sets the value of the specificLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecificLicenceNumberElement }
     *     
     */
    public void setSpecificLicenceNumber(SpecificLicenceNumberElement value) {
        this.specificLicenceNumber = value;
    }

}
