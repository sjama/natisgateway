
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="referenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="arn3" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "ChangeOfTitleHolderResponse")
public class ChangeOfTitleHolderResponse {

    @XmlElement(required = true)
    protected String referenceNumber;
    @XmlElement(required = true)
    protected byte[] arn3;

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the arn3 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getArn3() {
        return arn3;
    }

    /**
     * Sets the value of the arn3 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setArn3(byte[] value) {
        this.arn3 = value;
    }

}
