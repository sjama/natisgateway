
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fleetNomination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="infringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringementNoticeNumber"/>
 *         &lt;element name="nomineeIdDocumentTypeCode" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *         &lt;element name="nomineeIdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *         &lt;element name="nomineeDriversLicenceNumber" type="{http://tasima/common/ws/schema/}DriversLicenceNumberElement" minOccurs="0"/>
 *         &lt;element name="aarto07" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="driversLicence" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="supportingDocument1" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="supportingDocument2" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fleetNomination",
    "infringementNoticeNumber",
    "nomineeIdDocumentTypeCode",
    "nomineeIdDocumentNumber",
    "nomineeDriversLicenceNumber",
    "aarto07",
    "driversLicence",
    "supportingDocument1",
    "supportingDocument2"
})
@XmlRootElement(name = "NominationOfDriverRequest")
public class NominationOfDriverRequest {

    protected boolean fleetNomination;
    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected String nomineeIdDocumentTypeCode;
    @XmlElement(required = true)
    protected String nomineeIdDocumentNumber;
    protected String nomineeDriversLicenceNumber;
    protected byte[] aarto07;
    protected byte[] driversLicence;
    protected byte[] supportingDocument1;
    protected byte[] supportingDocument2;

    /**
     * Gets the value of the fleetNomination property.
     * 
     */
    public boolean isFleetNomination() {
        return fleetNomination;
    }

    /**
     * Sets the value of the fleetNomination property.
     * 
     */
    public void setFleetNomination(boolean value) {
        this.fleetNomination = value;
    }

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the nomineeIdDocumentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomineeIdDocumentTypeCode() {
        return nomineeIdDocumentTypeCode;
    }

    /**
     * Sets the value of the nomineeIdDocumentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomineeIdDocumentTypeCode(String value) {
        this.nomineeIdDocumentTypeCode = value;
    }

    /**
     * Gets the value of the nomineeIdDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomineeIdDocumentNumber() {
        return nomineeIdDocumentNumber;
    }

    /**
     * Sets the value of the nomineeIdDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomineeIdDocumentNumber(String value) {
        this.nomineeIdDocumentNumber = value;
    }

    /**
     * Gets the value of the nomineeDriversLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomineeDriversLicenceNumber() {
        return nomineeDriversLicenceNumber;
    }

    /**
     * Sets the value of the nomineeDriversLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomineeDriversLicenceNumber(String value) {
        this.nomineeDriversLicenceNumber = value;
    }

    /**
     * Gets the value of the aarto07 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAarto07() {
        return aarto07;
    }

    /**
     * Sets the value of the aarto07 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAarto07(byte[] value) {
        this.aarto07 = value;
    }

    /**
     * Gets the value of the driversLicence property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDriversLicence() {
        return driversLicence;
    }

    /**
     * Sets the value of the driversLicence property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDriversLicence(byte[] value) {
        this.driversLicence = value;
    }

    /**
     * Gets the value of the supportingDocument1 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSupportingDocument1() {
        return supportingDocument1;
    }

    /**
     * Sets the value of the supportingDocument1 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSupportingDocument1(byte[] value) {
        this.supportingDocument1 = value;
    }

    /**
     * Gets the value of the supportingDocument2 property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSupportingDocument2() {
        return supportingDocument2;
    }

    /**
     * Sets the value of the supportingDocument2 property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSupportingDocument2(byte[] value) {
        this.supportingDocument2 = value;
    }

}
