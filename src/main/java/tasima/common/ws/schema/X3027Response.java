
package tasima.common.ws.schema;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Infringement">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
 *                   &lt;element name="ParkNoticeN" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="16"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="OperInfringeNoticeN" type="{http://tasima/common/ws/schema/}InfringeNoticeN" minOccurs="0"/>
 *                   &lt;element name="InfringeStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InfringeStatChgD" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="InfringeD" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="AutCd" type="{http://tasima/common/ws/schema/}AuthorityCode"/>
 *                   &lt;element name="ProvCd" type="{http://tasima/common/ws/schema/}ProvinceCode" minOccurs="0"/>
 *                   &lt;element name="CityTown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="StreetNameA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="StreetNameB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RouteN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DirectionTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="GpsXCoord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="GpsYCoord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OtherLocInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Officer" type="{http://tasima/common/ws/schema/}X3027OfficerInformation" minOccurs="0"/>
 *                   &lt;element name="DataCaptureDetail" type="{http://tasima/common/ws/schema/}X3027DataCaptureDetails"/>
 *                   &lt;element name="MainCharge" type="{http://tasima/common/ws/schema/}X3027MainChargeDetails"/>
 *                   &lt;element name="AlternativeCharge" type="{http://tasima/common/ws/schema/}X3027AlternativeChargeDetails" minOccurs="0"/>
 *                   &lt;element name="Infringer" type="{http://tasima/common/ws/schema/}X3027InfringerDetails"/>
 *                   &lt;element name="InfringeMv" type="{http://tasima/common/ws/schema/}X3027InfringmentVehicleDetails"/>
 *                   &lt;element name="RepStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RepStatChgD" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="RevStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RevStatChgD" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="CourtCaseStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CourtCaseStatChgD" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *                   &lt;element name="InfringementCert" type="{http://tasima/common/ws/schema/}X3027InfringementCertificate" maxOccurs="unbounded"/>
 *                   &lt;element name="SummonsCert" type="{http://tasima/common/ws/schema/}X3027SummonsCertificate" minOccurs="0"/>
 *                   &lt;element name="InfringementFees" type="{http://tasima/common/ws/schema/}X3027InfringementFees"/>
 *                   &lt;element name="InfringementPayments" type="{http://tasima/common/ws/schema/}X3027InfringementPayment"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringement"
})
@XmlRootElement(name = "X3027Response")
public class X3027Response {

    @XmlElement(name = "Infringement", required = true)
    protected X3027Response.Infringement infringement;

    /**
     * Gets the value of the infringement property.
     * 
     * @return
     *     possible object is
     *     {@link X3027Response.Infringement }
     *     
     */
    public X3027Response.Infringement getInfringement() {
        return infringement;
    }

    /**
     * Sets the value of the infringement property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3027Response.Infringement }
     *     
     */
    public void setInfringement(X3027Response.Infringement value) {
        this.infringement = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="InfringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringeNoticeN"/>
     *         &lt;element name="ParkNoticeN" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="16"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="OperInfringeNoticeN" type="{http://tasima/common/ws/schema/}InfringeNoticeN" minOccurs="0"/>
     *         &lt;element name="InfringeStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InfringeStatChgD" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="InfringeD" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="AutCd" type="{http://tasima/common/ws/schema/}AuthorityCode"/>
     *         &lt;element name="ProvCd" type="{http://tasima/common/ws/schema/}ProvinceCode" minOccurs="0"/>
     *         &lt;element name="CityTown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="StreetNameA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="StreetNameB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RouteN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DirectionTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="GpsXCoord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="GpsYCoord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OtherLocInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Officer" type="{http://tasima/common/ws/schema/}X3027OfficerInformation" minOccurs="0"/>
     *         &lt;element name="DataCaptureDetail" type="{http://tasima/common/ws/schema/}X3027DataCaptureDetails"/>
     *         &lt;element name="MainCharge" type="{http://tasima/common/ws/schema/}X3027MainChargeDetails"/>
     *         &lt;element name="AlternativeCharge" type="{http://tasima/common/ws/schema/}X3027AlternativeChargeDetails" minOccurs="0"/>
     *         &lt;element name="Infringer" type="{http://tasima/common/ws/schema/}X3027InfringerDetails"/>
     *         &lt;element name="InfringeMv" type="{http://tasima/common/ws/schema/}X3027InfringmentVehicleDetails"/>
     *         &lt;element name="RepStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RepStatChgD" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="RevStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RevStatChgD" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="CourtCaseStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CourtCaseStatChgD" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
     *         &lt;element name="InfringementCert" type="{http://tasima/common/ws/schema/}X3027InfringementCertificate" maxOccurs="unbounded"/>
     *         &lt;element name="SummonsCert" type="{http://tasima/common/ws/schema/}X3027SummonsCertificate" minOccurs="0"/>
     *         &lt;element name="InfringementFees" type="{http://tasima/common/ws/schema/}X3027InfringementFees"/>
     *         &lt;element name="InfringementPayments" type="{http://tasima/common/ws/schema/}X3027InfringementPayment"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "infringementNoticeNumber",
        "parkNoticeN",
        "operInfringeNoticeN",
        "infringeStat",
        "infringeStatChgD",
        "infringeD",
        "autCd",
        "provCd",
        "cityTown",
        "suburb",
        "streetNameA",
        "streetNameB",
        "routeN",
        "directionTo",
        "gpsXCoord",
        "gpsYCoord",
        "otherLocInfo",
        "officer",
        "dataCaptureDetail",
        "mainCharge",
        "alternativeCharge",
        "infringer",
        "infringeMv",
        "repStat",
        "repStatChgD",
        "revStat",
        "revStatChgD",
        "courtCaseStat",
        "courtCaseStatChgD",
        "infringementCert",
        "summonsCert",
        "infringementFees",
        "infringementPayments"
    })
    public static class Infringement {

        @XmlElement(name = "InfringementNoticeNumber", required = true)
        protected String infringementNoticeNumber;
        @XmlElement(name = "ParkNoticeN")
        protected String parkNoticeN;
        @XmlElement(name = "OperInfringeNoticeN")
        protected String operInfringeNoticeN;
        @XmlElement(name = "InfringeStat", required = true)
        protected String infringeStat;
        @XmlElement(name = "InfringeStatChgD", required = true)
        protected XMLGregorianCalendar infringeStatChgD;
        @XmlElement(name = "InfringeD", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar infringeD;
        @XmlElement(name = "AutCd", required = true)
        protected String autCd;
        @XmlElement(name = "ProvCd")
        protected String provCd;
        @XmlElement(name = "CityTown")
        protected String cityTown;
        @XmlElement(name = "Suburb")
        protected String suburb;
        @XmlElement(name = "StreetNameA")
        protected String streetNameA;
        @XmlElement(name = "StreetNameB")
        protected String streetNameB;
        @XmlElement(name = "RouteN")
        protected String routeN;
        @XmlElement(name = "DirectionTo")
        protected String directionTo;
        @XmlElement(name = "GpsXCoord")
        protected String gpsXCoord;
        @XmlElement(name = "GpsYCoord")
        protected String gpsYCoord;
        @XmlElement(name = "OtherLocInfo")
        protected String otherLocInfo;
        @XmlElement(name = "Officer")
        protected X3027OfficerInformation officer;
        @XmlElement(name = "DataCaptureDetail", required = true)
        protected X3027DataCaptureDetails dataCaptureDetail;
        @XmlElement(name = "MainCharge", required = true)
        protected X3027MainChargeDetails mainCharge;
        @XmlElement(name = "AlternativeCharge")
        protected X3027AlternativeChargeDetails alternativeCharge;
        @XmlElement(name = "Infringer", required = true)
        protected X3027InfringerDetails infringer;
        @XmlElement(name = "InfringeMv", required = true)
        protected X3027InfringmentVehicleDetails infringeMv;
        @XmlElement(name = "RepStat")
        protected String repStat;
        @XmlElement(name = "RepStatChgD")
        protected XMLGregorianCalendar repStatChgD;
        @XmlElement(name = "RevStat")
        protected String revStat;
        @XmlElement(name = "RevStatChgD")
        protected XMLGregorianCalendar revStatChgD;
        @XmlElement(name = "CourtCaseStat")
        protected String courtCaseStat;
        @XmlElement(name = "CourtCaseStatChgD")
        protected XMLGregorianCalendar courtCaseStatChgD;
        @XmlElement(name = "InfringementCert", required = true)
        protected List<X3027InfringementCertificate> infringementCert;
        @XmlElement(name = "SummonsCert")
        protected X3027SummonsCertificate summonsCert;
        @XmlElement(name = "InfringementFees", required = true)
        protected X3027InfringementFees infringementFees;
        @XmlElement(name = "InfringementPayments", required = true)
        protected BigDecimal infringementPayments;

        /**
         * Gets the value of the infringementNoticeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringementNoticeNumber() {
            return infringementNoticeNumber;
        }

        /**
         * Sets the value of the infringementNoticeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringementNoticeNumber(String value) {
            this.infringementNoticeNumber = value;
        }

        /**
         * Gets the value of the parkNoticeN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParkNoticeN() {
            return parkNoticeN;
        }

        /**
         * Sets the value of the parkNoticeN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParkNoticeN(String value) {
            this.parkNoticeN = value;
        }

        /**
         * Gets the value of the operInfringeNoticeN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperInfringeNoticeN() {
            return operInfringeNoticeN;
        }

        /**
         * Sets the value of the operInfringeNoticeN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperInfringeNoticeN(String value) {
            this.operInfringeNoticeN = value;
        }

        /**
         * Gets the value of the infringeStat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringeStat() {
            return infringeStat;
        }

        /**
         * Sets the value of the infringeStat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringeStat(String value) {
            this.infringeStat = value;
        }

        /**
         * Gets the value of the infringeStatChgD property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getInfringeStatChgD() {
            return infringeStatChgD;
        }

        /**
         * Sets the value of the infringeStatChgD property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setInfringeStatChgD(XMLGregorianCalendar value) {
            this.infringeStatChgD = value;
        }

        /**
         * Gets the value of the infringeD property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getInfringeD() {
            return infringeD;
        }

        /**
         * Sets the value of the infringeD property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setInfringeD(XMLGregorianCalendar value) {
            this.infringeD = value;
        }

        /**
         * Gets the value of the autCd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAutCd() {
            return autCd;
        }

        /**
         * Sets the value of the autCd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAutCd(String value) {
            this.autCd = value;
        }

        /**
         * Gets the value of the provCd property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProvCd() {
            return provCd;
        }

        /**
         * Sets the value of the provCd property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProvCd(String value) {
            this.provCd = value;
        }

        /**
         * Gets the value of the cityTown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCityTown() {
            return cityTown;
        }

        /**
         * Sets the value of the cityTown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCityTown(String value) {
            this.cityTown = value;
        }

        /**
         * Gets the value of the suburb property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuburb() {
            return suburb;
        }

        /**
         * Sets the value of the suburb property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuburb(String value) {
            this.suburb = value;
        }

        /**
         * Gets the value of the streetNameA property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreetNameA() {
            return streetNameA;
        }

        /**
         * Sets the value of the streetNameA property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreetNameA(String value) {
            this.streetNameA = value;
        }

        /**
         * Gets the value of the streetNameB property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreetNameB() {
            return streetNameB;
        }

        /**
         * Sets the value of the streetNameB property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreetNameB(String value) {
            this.streetNameB = value;
        }

        /**
         * Gets the value of the routeN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRouteN() {
            return routeN;
        }

        /**
         * Sets the value of the routeN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRouteN(String value) {
            this.routeN = value;
        }

        /**
         * Gets the value of the directionTo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirectionTo() {
            return directionTo;
        }

        /**
         * Sets the value of the directionTo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirectionTo(String value) {
            this.directionTo = value;
        }

        /**
         * Gets the value of the gpsXCoord property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGpsXCoord() {
            return gpsXCoord;
        }

        /**
         * Sets the value of the gpsXCoord property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGpsXCoord(String value) {
            this.gpsXCoord = value;
        }

        /**
         * Gets the value of the gpsYCoord property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGpsYCoord() {
            return gpsYCoord;
        }

        /**
         * Sets the value of the gpsYCoord property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGpsYCoord(String value) {
            this.gpsYCoord = value;
        }

        /**
         * Gets the value of the otherLocInfo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOtherLocInfo() {
            return otherLocInfo;
        }

        /**
         * Sets the value of the otherLocInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOtherLocInfo(String value) {
            this.otherLocInfo = value;
        }

        /**
         * Gets the value of the officer property.
         * 
         * @return
         *     possible object is
         *     {@link X3027OfficerInformation }
         *     
         */
        public X3027OfficerInformation getOfficer() {
            return officer;
        }

        /**
         * Sets the value of the officer property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027OfficerInformation }
         *     
         */
        public void setOfficer(X3027OfficerInformation value) {
            this.officer = value;
        }

        /**
         * Gets the value of the dataCaptureDetail property.
         * 
         * @return
         *     possible object is
         *     {@link X3027DataCaptureDetails }
         *     
         */
        public X3027DataCaptureDetails getDataCaptureDetail() {
            return dataCaptureDetail;
        }

        /**
         * Sets the value of the dataCaptureDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027DataCaptureDetails }
         *     
         */
        public void setDataCaptureDetail(X3027DataCaptureDetails value) {
            this.dataCaptureDetail = value;
        }

        /**
         * Gets the value of the mainCharge property.
         * 
         * @return
         *     possible object is
         *     {@link X3027MainChargeDetails }
         *     
         */
        public X3027MainChargeDetails getMainCharge() {
            return mainCharge;
        }

        /**
         * Sets the value of the mainCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027MainChargeDetails }
         *     
         */
        public void setMainCharge(X3027MainChargeDetails value) {
            this.mainCharge = value;
        }

        /**
         * Gets the value of the alternativeCharge property.
         * 
         * @return
         *     possible object is
         *     {@link X3027AlternativeChargeDetails }
         *     
         */
        public X3027AlternativeChargeDetails getAlternativeCharge() {
            return alternativeCharge;
        }

        /**
         * Sets the value of the alternativeCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027AlternativeChargeDetails }
         *     
         */
        public void setAlternativeCharge(X3027AlternativeChargeDetails value) {
            this.alternativeCharge = value;
        }

        /**
         * Gets the value of the infringer property.
         * 
         * @return
         *     possible object is
         *     {@link X3027InfringerDetails }
         *     
         */
        public X3027InfringerDetails getInfringer() {
            return infringer;
        }

        /**
         * Sets the value of the infringer property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027InfringerDetails }
         *     
         */
        public void setInfringer(X3027InfringerDetails value) {
            this.infringer = value;
        }

        /**
         * Gets the value of the infringeMv property.
         * 
         * @return
         *     possible object is
         *     {@link X3027InfringmentVehicleDetails }
         *     
         */
        public X3027InfringmentVehicleDetails getInfringeMv() {
            return infringeMv;
        }

        /**
         * Sets the value of the infringeMv property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027InfringmentVehicleDetails }
         *     
         */
        public void setInfringeMv(X3027InfringmentVehicleDetails value) {
            this.infringeMv = value;
        }

        /**
         * Gets the value of the repStat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRepStat() {
            return repStat;
        }

        /**
         * Sets the value of the repStat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRepStat(String value) {
            this.repStat = value;
        }

        /**
         * Gets the value of the repStatChgD property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRepStatChgD() {
            return repStatChgD;
        }

        /**
         * Sets the value of the repStatChgD property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRepStatChgD(XMLGregorianCalendar value) {
            this.repStatChgD = value;
        }

        /**
         * Gets the value of the revStat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRevStat() {
            return revStat;
        }

        /**
         * Sets the value of the revStat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRevStat(String value) {
            this.revStat = value;
        }

        /**
         * Gets the value of the revStatChgD property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRevStatChgD() {
            return revStatChgD;
        }

        /**
         * Sets the value of the revStatChgD property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRevStatChgD(XMLGregorianCalendar value) {
            this.revStatChgD = value;
        }

        /**
         * Gets the value of the courtCaseStat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCourtCaseStat() {
            return courtCaseStat;
        }

        /**
         * Sets the value of the courtCaseStat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCourtCaseStat(String value) {
            this.courtCaseStat = value;
        }

        /**
         * Gets the value of the courtCaseStatChgD property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCourtCaseStatChgD() {
            return courtCaseStatChgD;
        }

        /**
         * Sets the value of the courtCaseStatChgD property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCourtCaseStatChgD(XMLGregorianCalendar value) {
            this.courtCaseStatChgD = value;
        }

        /**
         * Gets the value of the infringementCert property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the infringementCert property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfringementCert().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link X3027InfringementCertificate }
         * 
         * 
         */
        public List<X3027InfringementCertificate> getInfringementCert() {
            if (infringementCert == null) {
                infringementCert = new ArrayList<X3027InfringementCertificate>();
            }
            return this.infringementCert;
        }

        /**
         * Gets the value of the summonsCert property.
         * 
         * @return
         *     possible object is
         *     {@link X3027SummonsCertificate }
         *     
         */
        public X3027SummonsCertificate getSummonsCert() {
            return summonsCert;
        }

        /**
         * Sets the value of the summonsCert property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027SummonsCertificate }
         *     
         */
        public void setSummonsCert(X3027SummonsCertificate value) {
            this.summonsCert = value;
        }

        /**
         * Gets the value of the infringementFees property.
         * 
         * @return
         *     possible object is
         *     {@link X3027InfringementFees }
         *     
         */
        public X3027InfringementFees getInfringementFees() {
            return infringementFees;
        }

        /**
         * Sets the value of the infringementFees property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3027InfringementFees }
         *     
         */
        public void setInfringementFees(X3027InfringementFees value) {
            this.infringementFees = value;
        }

        /**
         * Gets the value of the infringementPayments property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getInfringementPayments() {
            return infringementPayments;
        }

        /**
         * Sets the value of the infringementPayments property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setInfringementPayments(BigDecimal value) {
            this.infringementPayments = value;
        }

    }

}
