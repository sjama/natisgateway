
package tasima.common.ws.schema;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VehicleLicenceRenewalAmounts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VehicleLicenceRenewalAmounts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="preFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="postFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="licFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="txanFees" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="minAmountDue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="maxAmountDue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehicleLicenceRenewalAmounts", propOrder = {
    "preFees",
    "postFees",
    "licFees",
    "txanFees",
    "minAmountDue",
    "maxAmountDue"
})
public class VehicleLicenceRenewalAmounts {

    @XmlElement(required = true)
    protected BigDecimal preFees;
    @XmlElement(required = true)
    protected BigDecimal postFees;
    @XmlElement(required = true)
    protected BigDecimal licFees;
    @XmlElement(required = true)
    protected BigDecimal txanFees;
    @XmlElement(required = true)
    protected BigDecimal minAmountDue;
    @XmlElement(required = true)
    protected BigDecimal maxAmountDue;

    /**
     * Gets the value of the preFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPreFees() {
        return preFees;
    }

    /**
     * Sets the value of the preFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPreFees(BigDecimal value) {
        this.preFees = value;
    }

    /**
     * Gets the value of the postFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPostFees() {
        return postFees;
    }

    /**
     * Sets the value of the postFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPostFees(BigDecimal value) {
        this.postFees = value;
    }

    /**
     * Gets the value of the licFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLicFees() {
        return licFees;
    }

    /**
     * Sets the value of the licFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLicFees(BigDecimal value) {
        this.licFees = value;
    }

    /**
     * Gets the value of the txanFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTxanFees() {
        return txanFees;
    }

    /**
     * Sets the value of the txanFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTxanFees(BigDecimal value) {
        this.txanFees = value;
    }

    /**
     * Gets the value of the minAmountDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinAmountDue() {
        return minAmountDue;
    }

    /**
     * Sets the value of the minAmountDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinAmountDue(BigDecimal value) {
        this.minAmountDue = value;
    }

    /**
     * Gets the value of the maxAmountDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxAmountDue() {
        return maxAmountDue;
    }

    /**
     * Sets the value of the maxAmountDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxAmountDue(BigDecimal value) {
        this.maxAmountDue = value;
    }

}
