
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for X3027InfringementCertificate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027InfringementCertificate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfringeCertN">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="InfringeCertCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InfringeCertStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InfringeCertStatChgD" type="{http://tasima/common/ws/schema/}Date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027InfringementCertificate", propOrder = {
    "infringeCertN",
    "infringeCertCd",
    "infringeCertStat",
    "infringeCertStatChgD"
})
public class X3027InfringementCertificate {

    @XmlElement(name = "InfringeCertN", required = true)
    protected String infringeCertN;
    @XmlElement(name = "InfringeCertCd", required = true)
    protected String infringeCertCd;
    @XmlElement(name = "InfringeCertStat", required = true)
    protected String infringeCertStat;
    @XmlElement(name = "InfringeCertStatChgD", required = true)
    protected XMLGregorianCalendar infringeCertStatChgD;

    /**
     * Gets the value of the infringeCertN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringeCertN() {
        return infringeCertN;
    }

    /**
     * Sets the value of the infringeCertN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringeCertN(String value) {
        this.infringeCertN = value;
    }

    /**
     * Gets the value of the infringeCertCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringeCertCd() {
        return infringeCertCd;
    }

    /**
     * Sets the value of the infringeCertCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringeCertCd(String value) {
        this.infringeCertCd = value;
    }

    /**
     * Gets the value of the infringeCertStat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringeCertStat() {
        return infringeCertStat;
    }

    /**
     * Sets the value of the infringeCertStat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringeCertStat(String value) {
        this.infringeCertStat = value;
    }

    /**
     * Gets the value of the infringeCertStatChgD property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringeCertStatChgD() {
        return infringeCertStatChgD;
    }

    /**
     * Sets the value of the infringeCertStatChgD property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringeCertStatChgD(XMLGregorianCalendar value) {
        this.infringeCertStatChgD = value;
    }

}
