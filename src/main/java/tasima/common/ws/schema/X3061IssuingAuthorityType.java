
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for x3061IssuingAuthorityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="x3061IssuingAuthorityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorityCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="magisterialCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="officerInfrastructureNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "x3061IssuingAuthorityType", propOrder = {
    "authorityCode",
    "magisterialCode",
    "officerInfrastructureNumber"
})
public class X3061IssuingAuthorityType {

    @XmlElement(required = true)
    protected String authorityCode;
    @XmlElement(required = true)
    protected String magisterialCode;
    @XmlElement(required = true)
    protected String officerInfrastructureNumber;

    /**
     * Gets the value of the authorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorityCode() {
        return authorityCode;
    }

    /**
     * Sets the value of the authorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorityCode(String value) {
        this.authorityCode = value;
    }

    /**
     * Gets the value of the magisterialCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMagisterialCode() {
        return magisterialCode;
    }

    /**
     * Sets the value of the magisterialCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMagisterialCode(String value) {
        this.magisterialCode = value;
    }

    /**
     * Gets the value of the officerInfrastructureNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerInfrastructureNumber() {
        return officerInfrastructureNumber;
    }

    /**
     * Sets the value of the officerInfrastructureNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerInfrastructureNumber(String value) {
        this.officerInfrastructureNumber = value;
    }

}
