
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="person">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *                   &lt;element name="idDocumentNumber">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="13"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="vinOrChassis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="registerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="licenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="engineNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="controlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "person",
    "vehicle"
})
@XmlRootElement(name = "ConfirmVehicleDetailsRequest")
public class ConfirmVehicleDetailsRequest {

    @XmlElement(required = true)
    protected ConfirmVehicleDetailsRequest.Person person;
    @XmlElement(required = true)
    protected ConfirmVehicleDetailsRequest.Vehicle vehicle;

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmVehicleDetailsRequest.Person }
     *     
     */
    public ConfirmVehicleDetailsRequest.Person getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmVehicleDetailsRequest.Person }
     *     
     */
    public void setPerson(ConfirmVehicleDetailsRequest.Person value) {
        this.person = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link ConfirmVehicleDetailsRequest.Vehicle }
     *     
     */
    public ConfirmVehicleDetailsRequest.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfirmVehicleDetailsRequest.Vehicle }
     *     
     */
    public void setVehicle(ConfirmVehicleDetailsRequest.Vehicle value) {
        this.vehicle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
     *         &lt;element name="idDocumentNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="13"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumentTypeCode",
        "idDocumentNumber"
    })
    public static class Person {

        @XmlElement(required = true)
        protected String idDocumentTypeCode;
        @XmlElement(required = true)
        protected String idDocumentNumber;

        /**
         * Gets the value of the idDocumentTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentTypeCode() {
            return idDocumentTypeCode;
        }

        /**
         * Sets the value of the idDocumentTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentTypeCode(String value) {
            this.idDocumentTypeCode = value;
        }

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="vinOrChassis" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="registerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="licenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="engineNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="controlNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vinOrChassis",
        "registerNumber",
        "licenceNumber",
        "engineNumber",
        "controlNumber"
    })
    public static class Vehicle {

        @XmlElement(required = true)
        protected String vinOrChassis;
        protected String registerNumber;
        protected String licenceNumber;
        @XmlElement(required = true)
        protected String engineNumber;
        @XmlElement(required = true)
        protected String controlNumber;

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the engineNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEngineNumber() {
            return engineNumber;
        }

        /**
         * Sets the value of the engineNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEngineNumber(String value) {
            this.engineNumber = value;
        }

        /**
         * Gets the value of the controlNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getControlNumber() {
            return controlNumber;
        }

        /**
         * Sets the value of the controlNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setControlNumber(String value) {
            this.controlNumber = value;
        }

    }

}
