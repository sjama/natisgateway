
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 * Request data based on Infringement capture date range
 * 
 * 
 * <p>Java class for X3026SearchOptionInfringementCaptureDate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3026SearchOptionInfringementCaptureDate">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tasima/common/ws/schema/}X3026SearchOptionType">
 *       &lt;sequence>
 *         &lt;element name="CaptureFromDate" type="{http://tasima/common/ws/schema/}Date"/>
 *         &lt;element name="CaptureToDate" type="{http://tasima/common/ws/schema/}Date"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3026SearchOptionInfringementCaptureDate", propOrder = {
    "captureFromDate",
    "captureToDate"
})
public class X3026SearchOptionInfringementCaptureDate
    extends X3026SearchOptionType
{

    @XmlElement(name = "CaptureFromDate", required = true)
    protected XMLGregorianCalendar captureFromDate;
    @XmlElement(name = "CaptureToDate", required = true)
    protected XMLGregorianCalendar captureToDate;

    /**
     * Gets the value of the captureFromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCaptureFromDate() {
        return captureFromDate;
    }

    /**
     * Sets the value of the captureFromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCaptureFromDate(XMLGregorianCalendar value) {
        this.captureFromDate = value;
    }

    /**
     * Gets the value of the captureToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCaptureToDate() {
        return captureToDate;
    }

    /**
     * Sets the value of the captureToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCaptureToDate(XMLGregorianCalendar value) {
        this.captureToDate = value;
    }

}
