
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="Query" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}X3050IdDocumentTypeElement"/>
 *                     &lt;element name="idDocumentNumber">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="13"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="QueryIssuingAuthority" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="issuingAuthority">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="4"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="QueryServiceProvider" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="serviceProviderCode">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="4"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="Filter" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CaptureDate" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CaptureFromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                             &lt;element name="CaptureToDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="InfringementType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InfringementStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InfringeNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IncludeImages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="ResultSize" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                         &lt;maxInclusive value="50"/>
 *                         &lt;minExclusive value="0"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "query",
    "queryIssuingAuthority",
    "queryServiceProvider",
    "filter"
})
@XmlRootElement(name = "X3050Request")
public class X3050Request {

    @XmlElement(name = "Query")
    protected X3050Request.Query query;
    @XmlElement(name = "QueryIssuingAuthority")
    protected X3050Request.QueryIssuingAuthority queryIssuingAuthority;
    @XmlElement(name = "QueryServiceProvider")
    protected X3050Request.QueryServiceProvider queryServiceProvider;
    @XmlElement(name = "Filter")
    protected X3050Request.Filter filter;

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Request.Query }
     *     
     */
    public X3050Request.Query getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Request.Query }
     *     
     */
    public void setQuery(X3050Request.Query value) {
        this.query = value;
    }

    /**
     * Gets the value of the queryIssuingAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Request.QueryIssuingAuthority }
     *     
     */
    public X3050Request.QueryIssuingAuthority getQueryIssuingAuthority() {
        return queryIssuingAuthority;
    }

    /**
     * Sets the value of the queryIssuingAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Request.QueryIssuingAuthority }
     *     
     */
    public void setQueryIssuingAuthority(X3050Request.QueryIssuingAuthority value) {
        this.queryIssuingAuthority = value;
    }

    /**
     * Gets the value of the queryServiceProvider property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Request.QueryServiceProvider }
     *     
     */
    public X3050Request.QueryServiceProvider getQueryServiceProvider() {
        return queryServiceProvider;
    }

    /**
     * Sets the value of the queryServiceProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Request.QueryServiceProvider }
     *     
     */
    public void setQueryServiceProvider(X3050Request.QueryServiceProvider value) {
        this.queryServiceProvider = value;
    }

    /**
     * Gets the value of the filter property.
     * 
     * @return
     *     possible object is
     *     {@link X3050Request.Filter }
     *     
     */
    public X3050Request.Filter getFilter() {
        return filter;
    }

    /**
     * Sets the value of the filter property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3050Request.Filter }
     *     
     */
    public void setFilter(X3050Request.Filter value) {
        this.filter = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CaptureDate" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CaptureFromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *                   &lt;element name="CaptureToDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="InfringementType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InfringementStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InfringeNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IncludeImages" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="ResultSize" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
     *               &lt;maxInclusive value="50"/>
     *               &lt;minExclusive value="0"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "captureDate",
        "infringementType",
        "infringementStatus",
        "infringeNoticeNumber",
        "includeImages",
        "resultSize"
    })
    public static class Filter {

        @XmlElement(name = "CaptureDate")
        protected X3050Request.Filter.CaptureDate captureDate;
        @XmlElement(name = "InfringementType")
        protected String infringementType;
        @XmlElement(name = "InfringementStatus")
        protected String infringementStatus;
        @XmlElement(name = "InfringeNoticeNumber")
        protected String infringeNoticeNumber;
        @XmlElement(name = "IncludeImages", defaultValue = "false")
        protected Boolean includeImages;
        @XmlElement(name = "ResultSize", defaultValue = "50")
        protected Integer resultSize;

        /**
         * Gets the value of the captureDate property.
         * 
         * @return
         *     possible object is
         *     {@link X3050Request.Filter.CaptureDate }
         *     
         */
        public X3050Request.Filter.CaptureDate getCaptureDate() {
            return captureDate;
        }

        /**
         * Sets the value of the captureDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link X3050Request.Filter.CaptureDate }
         *     
         */
        public void setCaptureDate(X3050Request.Filter.CaptureDate value) {
            this.captureDate = value;
        }

        /**
         * Gets the value of the infringementType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringementType() {
            return infringementType;
        }

        /**
         * Sets the value of the infringementType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringementType(String value) {
            this.infringementType = value;
        }

        /**
         * Gets the value of the infringementStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringementStatus() {
            return infringementStatus;
        }

        /**
         * Sets the value of the infringementStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringementStatus(String value) {
            this.infringementStatus = value;
        }

        /**
         * Gets the value of the infringeNoticeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfringeNoticeNumber() {
            return infringeNoticeNumber;
        }

        /**
         * Sets the value of the infringeNoticeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfringeNoticeNumber(String value) {
            this.infringeNoticeNumber = value;
        }

        /**
         * Gets the value of the includeImages property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIncludeImages() {
            return includeImages;
        }

        /**
         * Sets the value of the includeImages property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIncludeImages(Boolean value) {
            this.includeImages = value;
        }

        /**
         * Gets the value of the resultSize property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getResultSize() {
            return resultSize;
        }

        /**
         * Sets the value of the resultSize property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setResultSize(Integer value) {
            this.resultSize = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CaptureFromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
         *         &lt;element name="CaptureToDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "captureFromDate",
            "captureToDate"
        })
        public static class CaptureDate {

            @XmlElement(name = "CaptureFromDate")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar captureFromDate;
            @XmlElement(name = "CaptureToDate")
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar captureToDate;

            /**
             * Gets the value of the captureFromDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCaptureFromDate() {
                return captureFromDate;
            }

            /**
             * Sets the value of the captureFromDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCaptureFromDate(XMLGregorianCalendar value) {
                this.captureFromDate = value;
            }

            /**
             * Gets the value of the captureToDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getCaptureToDate() {
                return captureToDate;
            }

            /**
             * Sets the value of the captureToDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setCaptureToDate(XMLGregorianCalendar value) {
                this.captureToDate = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}X3050IdDocumentTypeElement"/>
     *         &lt;element name="idDocumentNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="13"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumentTypeCode",
        "idDocumentNumber"
    })
    public static class Query {

        @XmlElement(required = true)
        protected String idDocumentTypeCode;
        @XmlElement(required = true)
        protected String idDocumentNumber;

        /**
         * Gets the value of the idDocumentTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentTypeCode() {
            return idDocumentTypeCode;
        }

        /**
         * Sets the value of the idDocumentTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentTypeCode(String value) {
            this.idDocumentTypeCode = value;
        }

        /**
         * Gets the value of the idDocumentNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentNumber() {
            return idDocumentNumber;
        }

        /**
         * Sets the value of the idDocumentNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentNumber(String value) {
            this.idDocumentNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="issuingAuthority">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="4"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "issuingAuthority"
    })
    public static class QueryIssuingAuthority {

        @XmlElement(required = true)
        protected String issuingAuthority;

        /**
         * Gets the value of the issuingAuthority property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssuingAuthority() {
            return issuingAuthority;
        }

        /**
         * Sets the value of the issuingAuthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssuingAuthority(String value) {
            this.issuingAuthority = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="serviceProviderCode">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="4"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceProviderCode"
    })
    public static class QueryServiceProvider {

        @XmlElement(required = true)
        protected String serviceProviderCode;

        /**
         * Gets the value of the serviceProviderCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceProviderCode() {
            return serviceProviderCode;
        }

        /**
         * Sets the value of the serviceProviderCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceProviderCode(String value) {
            this.serviceProviderCode = value;
        }

    }

}
