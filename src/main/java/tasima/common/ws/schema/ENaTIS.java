
package tasima.common.ws.schema;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "eNaTIS", targetNamespace = "http://tasima/common/ws/schema/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ENaTIS {


    /**
     * 
     * @param x3050Request
     * @return
     *     returns tasima.common.ws.schema.X3050Response
     */
    @WebMethod(operationName = "X3050")
    @WebResult(name = "X3050Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3050Response")
    public X3050Response x3050(
        @WebParam(name = "X3050Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3050Request")
        X3050Request x3050Request);

    /**
     * 
     * @param getInfringementImagesRequest
     * @return
     *     returns tasima.common.ws.schema.GetInfringementImagesResponse
     */
    @WebMethod(operationName = "GetInfringementImages")
    @WebResult(name = "GetInfringementImagesResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementImagesResponse")
    public GetInfringementImagesResponse getInfringementImages(
        @WebParam(name = "GetInfringementImagesRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementImagesRequest")
        GetInfringementImagesRequest getInfringementImagesRequest);

    /**
     * 
     * @param renewVehicleLicenseRequest
     * @return
     *     returns tasima.common.ws.schema.RenewVehicleLicenseResponse
     */
    @WebMethod(operationName = "RenewVehicleLicense")
    @WebResult(name = "RenewVehicleLicenseResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "RenewVehicleLicenseResponse")
    public RenewVehicleLicenseResponse renewVehicleLicense(
        @WebParam(name = "RenewVehicleLicenseRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "RenewVehicleLicenseRequest")
        RenewVehicleLicenseRequest renewVehicleLicenseRequest);

    /**
     * 
     * @param isFleetUserRequest
     * @return
     *     returns tasima.common.ws.schema.IsFleetUserResponse
     */
    @WebMethod(operationName = "IsFleetUser")
    @WebResult(name = "IsFleetUserResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "IsFleetUserResponse")
    public IsFleetUserResponse isFleetUser(
        @WebParam(name = "IsFleetUserRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "IsFleetUserRequest")
        IsFleetUserRequest isFleetUserRequest);

    /**
     * 
     * @param transaction915DUpdateRequest
     * @return
     *     returns tasima.common.ws.schema.Transaction915DUpdateResponse
     */
    @WebMethod(operationName = "Transaction915DUpdate")
    @WebResult(name = "Transaction915DUpdateResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "Transaction915DUpdateResponse")
    public Transaction915DUpdateResponse transaction915DUpdate(
        @WebParam(name = "Transaction915DUpdateRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "Transaction915DUpdateRequest")
        Transaction915DUpdateRequest transaction915DUpdateRequest);

    /**
     * 
     * @param queryPersonToNominateRequest
     * @return
     *     returns tasima.common.ws.schema.QueryPersonToNominateResponse
     */
    @WebMethod(operationName = "QueryPersonToNominate")
    @WebResult(name = "QueryPersonToNominateResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryPersonToNominateResponse")
    public QueryPersonToNominateResponse queryPersonToNominate(
        @WebParam(name = "QueryPersonToNominateRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryPersonToNominateRequest")
        QueryPersonToNominateRequest queryPersonToNominateRequest);

    /**
     * 
     * @param getProvincesRequest
     * @return
     *     returns tasima.common.ws.schema.GetProvincesResponse
     */
    @WebMethod(operationName = "GetProvinces")
    @WebResult(name = "GetProvincesResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetProvincesResponse")
    public GetProvincesResponse getProvinces(
        @WebParam(name = "GetProvincesRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetProvincesRequest")
        GetProvincesRequest getProvincesRequest);

    /**
     * 
     * @param x31A7Request
     * @return
     *     returns tasima.common.ws.schema.X31A7Response
     */
    @WebMethod(operationName = "X31A7")
    @WebResult(name = "X31A7Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X31A7Response")
    public X31A7Response x31A7(
        @WebParam(name = "X31A7Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X31A7Request")
        X31A7Request x31A7Request);

    /**
     * 
     * @param queryTaskManagementSystemRequest
     * @return
     *     returns tasima.common.ws.schema.QueryTaskManagementSystemResponse
     */
    @WebMethod(operationName = "QueryTaskManagementSystem")
    @WebResult(name = "QueryTaskManagementSystemResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryTaskManagementSystemResponse")
    public QueryTaskManagementSystemResponse queryTaskManagementSystem(
        @WebParam(name = "QueryTaskManagementSystemRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryTaskManagementSystemRequest")
        QueryTaskManagementSystemRequest queryTaskManagementSystemRequest);

    /**
     * 
     * @param optForCourtRequest
     * @return
     *     returns tasima.common.ws.schema.OptForCourtResponse
     */
    @WebMethod(operationName = "OptForCourt")
    @WebResult(name = "OptForCourtResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "OptForCourtResponse")
    public OptForCourtResponse optForCourt(
        @WebParam(name = "OptForCourtRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "OptForCourtRequest")
        OptForCourtRequest optForCourtRequest);

    /**
     * 
     * @param x3003Request
     * @return
     *     returns tasima.common.ws.schema.X3003Response
     */
    @WebMethod(operationName = "X3003")
    @WebResult(name = "X3003Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3003Response")
    public X3003Response x3003(
        @WebParam(name = "X3003Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3003Request")
        X3003Request x3003Request);

    /**
     * 
     * @param infringementForNominationRequest
     * @return
     *     returns tasima.common.ws.schema.InfringementForNominationResponse
     */
    @WebMethod(operationName = "InfringementForNomination")
    @WebResult(name = "InfringementForNominationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "InfringementForNominationResponse")
    public InfringementForNominationResponse infringementForNomination(
        @WebParam(name = "InfringementForNominationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "InfringementForNominationRequest")
        InfringementForNominationRequest infringementForNominationRequest);

    /**
     * 
     * @param minimalVehicleQueryRequest
     * @return
     *     returns tasima.common.ws.schema.MinimalVehicleQueryResponse
     */
    @WebMethod(operationName = "MinimalVehicleQuery")
    @WebResult(name = "MinimalVehicleQueryResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "MinimalVehicleQueryResponse")
    public MinimalVehicleQueryResponse minimalVehicleQuery(
        @WebParam(name = "MinimalVehicleQueryRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "MinimalVehicleQueryRequest")
        MinimalVehicleQueryRequest minimalVehicleQueryRequest);

    /**
     * 
     * @param changeOfTitleHolderRequest
     * @return
     *     returns tasima.common.ws.schema.ChangeOfTitleHolderResponse
     */
    @WebMethod(operationName = "ChangeOfTitleHolder")
    @WebResult(name = "ChangeOfTitleHolderResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "ChangeOfTitleHolderResponse")
    public ChangeOfTitleHolderResponse changeOfTitleHolder(
        @WebParam(name = "ChangeOfTitleHolderRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "ChangeOfTitleHolderRequest")
        ChangeOfTitleHolderRequest changeOfTitleHolderRequest);

    /**
     * 
     * @param nominationOfDriverRequest
     * @return
     *     returns tasima.common.ws.schema.NominationOfDriverResponse
     */
    @WebMethod(operationName = "NominationOfDriver")
    @WebResult(name = "NominationOfDriverResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "NominationOfDriverResponse")
    public NominationOfDriverResponse nominationOfDriver(
        @WebParam(name = "NominationOfDriverRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "NominationOfDriverRequest")
        NominationOfDriverRequest nominationOfDriverRequest);

    /**
     * 
     * @param x3027Request
     * @return
     *     returns tasima.common.ws.schema.X3027Response
     */
    @WebMethod(operationName = "X3027")
    @WebResult(name = "X3027Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3027Response")
    public X3027Response x3027(
        @WebParam(name = "X3027Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3027Request")
        X3027Request x3027Request);

    /**
     * 
     * @param fleetInfringementsRequest
     * @return
     *     returns tasima.common.ws.schema.FleetInfringementsResponse
     */
    @WebMethod(operationName = "FleetInfringements")
    @WebResult(name = "FleetInfringementsResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "FleetInfringementsResponse")
    public FleetInfringementsResponse fleetInfringements(
        @WebParam(name = "FleetInfringementsRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "FleetInfringementsRequest")
        FleetInfringementsRequest fleetInfringementsRequest);

    /**
     * 
     * @param testPreBookingAvailabilityRequest
     * @return
     *     returns tasima.common.ws.schema.TestPreBookingAvailabilityResponse
     */
    @WebMethod(operationName = "TestPreBookingAvailability")
    @WebResult(name = "TestPreBookingAvailabilityResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "TestPreBookingAvailabilityResponse")
    public TestPreBookingAvailabilityResponse testPreBookingAvailability(
        @WebParam(name = "TestPreBookingAvailabilityRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "TestPreBookingAvailabilityRequest")
        TestPreBookingAvailabilityRequest testPreBookingAvailabilityRequest);

    /**
     * 
     * @param x3141Request
     * @return
     *     returns tasima.common.ws.schema.X3141Response
     */
    @WebMethod(operationName = "X3141")
    @WebResult(name = "X3141Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3141Response")
    public X3141Response x3141(
        @WebParam(name = "X3141Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3141Request")
        X3141Request x3141Request);

    /**
     * 
     * @param updatePreferedAddressRequest
     * @return
     *     returns tasima.common.ws.schema.UpdatePreferedAddressResponse
     */
    @WebMethod(operationName = "UpdatePreferedAddress")
    @WebResult(name = "UpdatePreferedAddressResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "UpdatePreferedAddressResponse")
    public UpdatePreferedAddressResponse updatePreferedAddress(
        @WebParam(name = "UpdatePreferedAddressRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "UpdatePreferedAddressRequest")
        UpdatePreferedAddressRequest updatePreferedAddressRequest);

    /**
     * 
     * @param x3004Request
     * @return
     *     returns tasima.common.ws.schema.X3004Response
     */
    @WebMethod(operationName = "X3004")
    @WebResult(name = "X3004Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3004Response")
    public X3004Response x3004(
        @WebParam(name = "X3004Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3004Request")
        X3004Request x3004Request);

    /**
     * 
     * @param getInfringementForInstalmentApplicationRequest
     * @return
     *     returns tasima.common.ws.schema.GetInfringementForInstalmentApplicationResponse
     */
    @WebMethod(operationName = "GetInfringementForInstalmentApplication")
    @WebResult(name = "GetInfringementForInstalmentApplicationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementForInstalmentApplicationResponse")
    public GetInfringementForInstalmentApplicationResponse getInfringementForInstalmentApplication(
        @WebParam(name = "GetInfringementForInstalmentApplicationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementForInstalmentApplicationRequest")
        GetInfringementForInstalmentApplicationRequest getInfringementForInstalmentApplicationRequest);

    /**
     * 
     * @param testPreBookingScheduleAppointmentRequest
     * @return
     *     returns tasima.common.ws.schema.TestPreBookingScheduleAppointmentResponse
     */
    @WebMethod(operationName = "TestPreBookingScheduleAppointment")
    @WebResult(name = "TestPreBookingScheduleAppointmentResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "TestPreBookingScheduleAppointmentResponse")
    public TestPreBookingScheduleAppointmentResponse testPreBookingScheduleAppointment(
        @WebParam(name = "TestPreBookingScheduleAppointmentRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "TestPreBookingScheduleAppointmentRequest")
        TestPreBookingScheduleAppointmentRequest testPreBookingScheduleAppointmentRequest);

    /**
     * 
     * @param x3048Request
     * @return
     *     returns tasima.common.ws.schema.X3048Response
     */
    @WebMethod(operationName = "X3048")
    @WebResult(name = "X3048Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3048Response")
    public X3048Response x3048(
        @WebParam(name = "X3048Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3048Request")
        X3048Request x3048Request);

    /**
     * 
     * @param onlinePaymentReceiptRequest
     * @return
     *     returns tasima.common.ws.schema.OnlinePaymentReceiptResponse
     */
    @WebMethod(operationName = "OnlinePaymentReceipt")
    @WebResult(name = "OnlinePaymentReceiptResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "OnlinePaymentReceiptResponse")
    public OnlinePaymentReceiptResponse onlinePaymentReceipt(
        @WebParam(name = "OnlinePaymentReceiptRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "OnlinePaymentReceiptRequest")
        OnlinePaymentReceiptRequest onlinePaymentReceiptRequest);

    /**
     * 
     * @param x3029Request
     * @return
     *     returns tasima.common.ws.schema.X3029Response
     */
    @WebMethod(operationName = "X3029")
    @WebResult(name = "X3029Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3029Response")
    public X3029Response x3029(
        @WebParam(name = "X3029Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3029Request")
        X3029Request x3029Request);

    /**
     * 
     * @param retrieveDocRequest
     * @return
     *     returns tasima.common.ws.schema.RetrieveDocResponse
     */
    @WebMethod(operationName = "RetrieveDoc")
    @WebResult(name = "RetrieveDocResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "RetrieveDocResponse")
    public RetrieveDocResponse retrieveDoc(
        @WebParam(name = "RetrieveDocRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "RetrieveDocRequest")
        RetrieveDocRequest retrieveDocRequest);

    /**
     * 
     * @param maintainPasswordRequest
     * @return
     *     returns tasima.common.ws.schema.MaintainPasswordResponse
     */
    @WebMethod(operationName = "MaintainPassword")
    @WebResult(name = "MaintainPasswordResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "MaintainPasswordResponse")
    public MaintainPasswordResponse maintainPassword(
        @WebParam(name = "MaintainPasswordRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "MaintainPasswordRequest")
        MaintainPasswordRequest maintainPasswordRequest);

    /**
     * 
     * @param getVehicleLicenseRenewalInformationRequest
     * @return
     *     returns tasima.common.ws.schema.GetVehicleLicenseRenewalInformationResponse
     */
    @WebMethod(operationName = "GetVehicleLicenseRenewalInformation")
    @WebResult(name = "GetVehicleLicenseRenewalInformationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetVehicleLicenseRenewalInformationResponse")
    public GetVehicleLicenseRenewalInformationResponse getVehicleLicenseRenewalInformation(
        @WebParam(name = "GetVehicleLicenseRenewalInformationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetVehicleLicenseRenewalInformationRequest")
        GetVehicleLicenseRenewalInformationRequest getVehicleLicenseRenewalInformationRequest);

    /**
     * 
     * @param queryDltcRequest
     * @return
     *     returns tasima.common.ws.schema.QueryDltcResponse
     */
    @WebMethod(operationName = "QueryDltc")
    @WebResult(name = "QueryDltcResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryDltcResponse")
    public QueryDltcResponse queryDltc(
        @WebParam(name = "QueryDltcRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryDltcRequest")
        QueryDltcRequest queryDltcRequest);

    /**
     * 
     * @param confirmVehicleDetailsRequest
     * @return
     *     returns tasima.common.ws.schema.ConfirmVehicleDetailsResponse
     */
    @WebMethod(operationName = "ConfirmVehicleDetails")
    @WebResult(name = "ConfirmVehicleDetailsResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "ConfirmVehicleDetailsResponse")
    public ConfirmVehicleDetailsResponse confirmVehicleDetails(
        @WebParam(name = "ConfirmVehicleDetailsRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "ConfirmVehicleDetailsRequest")
        ConfirmVehicleDetailsRequest confirmVehicleDetailsRequest);

    /**
     * 
     * @param x3039Request
     * @return
     *     returns tasima.common.ws.schema.X3039Response
     */
    @WebMethod(operationName = "X3039")
    @WebResult(name = "X3039Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3039Response")
    public X3039Response x3039(
        @WebParam(name = "X3039Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3039Request")
        X3039Request x3039Request);

    /**
     * 
     * @param instalmentApplicationRequest
     * @return
     *     returns tasima.common.ws.schema.InstalmentApplicationResponse
     */
    @WebMethod(operationName = "InstalmentApplication")
    @WebResult(name = "InstalmentApplicationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "InstalmentApplicationResponse")
    public InstalmentApplicationResponse instalmentApplication(
        @WebParam(name = "InstalmentApplicationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "InstalmentApplicationRequest")
        InstalmentApplicationRequest instalmentApplicationRequest);

    /**
     * 
     * @param x3026Request
     * @return
     *     returns tasima.common.ws.schema.X3026Response
     */
    @WebMethod(operationName = "X3026")
    @WebResult(name = "X3026Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3026Response")
    public X3026Response x3026(
        @WebParam(name = "X3026Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3026Request")
        X3026Request x3026Request);

    /**
     * 
     * @param getInfringementToOptForCourtRequest
     * @return
     *     returns tasima.common.ws.schema.GetInfringementToOptForCourtResponse
     */
    @WebMethod(operationName = "GetInfringementToOptForCourt")
    @WebResult(name = "GetInfringementToOptForCourtResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementToOptForCourtResponse")
    public GetInfringementToOptForCourtResponse getInfringementToOptForCourt(
        @WebParam(name = "GetInfringementToOptForCourtRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementToOptForCourtRequest")
        GetInfringementToOptForCourtRequest getInfringementToOptForCourtRequest);

    /**
     * 
     * @param x3067Request
     * @return
     *     returns tasima.common.ws.schema.X3067Response
     */
    @WebMethod(operationName = "X3067")
    @WebResult(name = "X3067Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3067Response")
    public X3067Response x3067(
        @WebParam(name = "X3067Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3067Request")
        X3067Request x3067Request);

    /**
     * 
     * @param x3061Request
     * @return
     *     returns tasima.common.ws.schema.X3061Response
     */
    @WebMethod(operationName = "X3061")
    @WebResult(name = "X3061Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3061Response")
    public X3061Response x3061(
        @WebParam(name = "X3061Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3061Request")
        X3061Request x3061Request);

    /**
     * 
     * @param createOnlinePaymentRequest
     * @return
     *     returns tasima.common.ws.schema.CreateOnlinePaymentResponse
     */
    @WebMethod(operationName = "CreateOnlinePayment")
    @WebResult(name = "CreateOnlinePaymentResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "CreateOnlinePaymentResponse")
    public CreateOnlinePaymentResponse createOnlinePayment(
        @WebParam(name = "CreateOnlinePaymentRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "CreateOnlinePaymentRequest")
        CreateOnlinePaymentRequest createOnlinePaymentRequest);

    /**
     * 
     * @param instalmentAccountRequest
     * @return
     *     returns tasima.common.ws.schema.InstalmentAccountResponse
     */
    @WebMethod(operationName = "InstalmentAccount")
    @WebResult(name = "InstalmentAccountResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "InstalmentAccountResponse")
    public InstalmentAccountResponse instalmentAccount(
        @WebParam(name = "InstalmentAccountRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "InstalmentAccountRequest")
        InstalmentAccountRequest instalmentAccountRequest);

    /**
     * 
     * @param sapVehicleQueryRequest
     * @return
     *     returns tasima.common.ws.schema.SAPVehicleQueryResponse
     */
    @WebMethod(operationName = "SAPVehicleQuery")
    @WebResult(name = "SAPVehicleQueryResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "SAPVehicleQueryResponse")
    public SAPVehicleQueryResponse sapVehicleQuery(
        @WebParam(name = "SAPVehicleQueryRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "SAPVehicleQueryRequest")
        SAPVehicleQueryRequest sapVehicleQueryRequest);

    /**
     * 
     * @param genericVehicleQueryRequest
     * @return
     *     returns tasima.common.ws.schema.GenericVehicleQueryResponse
     */
    @WebMethod(operationName = "GenericVehicleQuery")
    @WebResult(name = "GenericVehicleQueryResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GenericVehicleQueryResponse")
    public GenericVehicleQueryResponse genericVehicleQuery(
        @WebParam(name = "GenericVehicleQueryRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GenericVehicleQueryRequest")
        GenericVehicleQueryRequest genericVehicleQueryRequest);

    /**
     * 
     * @param getLicenseRenewalQuoteRequest
     * @return
     *     returns tasima.common.ws.schema.GetLicenseRenewalQuoteResponse
     */
    @WebMethod(operationName = "GetLicenseRenewalQuote")
    @WebResult(name = "GetLicenseRenewalQuoteResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetLicenseRenewalQuoteResponse")
    public GetLicenseRenewalQuoteResponse getLicenseRenewalQuote(
        @WebParam(name = "GetLicenseRenewalQuoteRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetLicenseRenewalQuoteRequest")
        GetLicenseRenewalQuoteRequest getLicenseRenewalQuoteRequest);

    /**
     * 
     * @param x3028Request
     * @return
     *     returns tasima.common.ws.schema.X3028Response
     */
    @WebMethod(operationName = "X3028")
    @WebResult(name = "X3028Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3028Response")
    public X3028Response x3028(
        @WebParam(name = "X3028Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3028Request")
        X3028Request x3028Request);

    /**
     * 
     * @param getOnlinePaymentGatewayConfigurationRequest
     * @return
     *     returns tasima.common.ws.schema.GetOnlinePaymentGatewayConfigurationResponse
     */
    @WebMethod(operationName = "GetOnlinePaymentGatewayConfiguration")
    @WebResult(name = "GetOnlinePaymentGatewayConfigurationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetOnlinePaymentGatewayConfigurationResponse")
    public GetOnlinePaymentGatewayConfigurationResponse getOnlinePaymentGatewayConfiguration(
        @WebParam(name = "GetOnlinePaymentGatewayConfigurationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetOnlinePaymentGatewayConfigurationRequest")
        GetOnlinePaymentGatewayConfigurationRequest getOnlinePaymentGatewayConfigurationRequest);

    /**
     * 
     * @param getVehiclesAndLicenseExpiryDatesRequest
     * @return
     *     returns tasima.common.ws.schema.GetVehiclesAndLicenseExpiryDatesResponse
     */
    @WebMethod(operationName = "GetVehiclesAndLicenseExpiryDates")
    @WebResult(name = "GetVehiclesAndLicenseExpiryDatesResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetVehiclesAndLicenseExpiryDatesResponse")
    public GetVehiclesAndLicenseExpiryDatesResponse getVehiclesAndLicenseExpiryDates(
        @WebParam(name = "GetVehiclesAndLicenseExpiryDatesRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetVehiclesAndLicenseExpiryDatesRequest")
        GetVehiclesAndLicenseExpiryDatesRequest getVehiclesAndLicenseExpiryDatesRequest);

    /**
     * 
     * @param getInfringementForRepresentationRequest
     * @return
     *     returns tasima.common.ws.schema.GetInfringementForRepresentationResponse
     */
    @WebMethod(operationName = "GetInfringementForRepresentation")
    @WebResult(name = "GetInfringementForRepresentationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementForRepresentationResponse")
    public GetInfringementForRepresentationResponse getInfringementForRepresentation(
        @WebParam(name = "GetInfringementForRepresentationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementForRepresentationRequest")
        GetInfringementForRepresentationRequest getInfringementForRepresentationRequest);

    /**
     * 
     * @param infringementQueryRequest
     * @return
     *     returns tasima.common.ws.schema.InfringementQueryResponse
     */
    @WebMethod(operationName = "InfringementQuery")
    @WebResult(name = "InfringementQueryResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "InfringementQueryResponse")
    public InfringementQueryResponse infringementQuery(
        @WebParam(name = "InfringementQueryRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "InfringementQueryRequest")
        InfringementQueryRequest infringementQueryRequest);

    /**
     * 
     * @param queryPaymentStatusRequest
     * @return
     *     returns tasima.common.ws.schema.QueryPaymentStatusResponse
     */
    @WebMethod(operationName = "QueryPaymentStatus")
    @WebResult(name = "QueryPaymentStatusResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryPaymentStatusResponse")
    public QueryPaymentStatusResponse queryPaymentStatus(
        @WebParam(name = "QueryPaymentStatusRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryPaymentStatusRequest")
        QueryPaymentStatusRequest queryPaymentStatusRequest);

    /**
     * 
     * @param queryInfringementsForWebsiteRequest
     * @return
     *     returns tasima.common.ws.schema.QueryInfringementsForWebsiteResponse
     */
    @WebMethod(operationName = "QueryInfringementsForWebsite")
    @WebResult(name = "QueryInfringementsForWebsiteResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryInfringementsForWebsiteResponse")
    public QueryInfringementsForWebsiteResponse queryInfringementsForWebsite(
        @WebParam(name = "QueryInfringementsForWebsiteRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryInfringementsForWebsiteRequest")
        QueryInfringementsForWebsiteRequest queryInfringementsForWebsiteRequest);

    /**
     * 
     * @param applyForRepresentationRequest
     * @return
     *     returns tasima.common.ws.schema.ApplyForRepresentationResponse
     */
    @WebMethod(operationName = "ApplyForRepresentation")
    @WebResult(name = "ApplyForRepresentationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "ApplyForRepresentationResponse")
    public ApplyForRepresentationResponse applyForRepresentation(
        @WebParam(name = "ApplyForRepresentationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "ApplyForRepresentationRequest")
        ApplyForRepresentationRequest applyForRepresentationRequest);

    /**
     * 
     * @param queryAccidentsRequest
     * @return
     *     returns tasima.common.ws.schema.QueryAccidentsResponse
     */
    @WebMethod(operationName = "QueryAccidents")
    @WebResult(name = "QueryAccidentsResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryAccidentsResponse")
    public QueryAccidentsResponse queryAccidents(
        @WebParam(name = "QueryAccidentsRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryAccidentsRequest")
        QueryAccidentsRequest queryAccidentsRequest);

    /**
     * 
     * @param lookupQueryRequest
     * @return
     *     returns tasima.common.ws.schema.LookupQueryResponse
     */
    @WebMethod(operationName = "LookupQuery")
    @WebResult(name = "LookupQueryResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "LookupQueryResponse")
    public LookupQueryResponse lookupQuery(
        @WebParam(name = "LookupQueryRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "LookupQueryRequest")
        LookupQueryRequest lookupQueryRequest);

    /**
     * 
     * @param getInfringementForRevocationRequest
     * @return
     *     returns tasima.common.ws.schema.GetInfringementForRevocationResponse
     */
    @WebMethod(operationName = "GetInfringementForRevocation")
    @WebResult(name = "GetInfringementForRevocationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementForRevocationResponse")
    public GetInfringementForRevocationResponse getInfringementForRevocation(
        @WebParam(name = "GetInfringementForRevocationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetInfringementForRevocationRequest")
        GetInfringementForRevocationRequest getInfringementForRevocationRequest);

    /**
     * 
     * @param queryInfringementDetailForWebsiteRequest
     * @return
     *     returns tasima.common.ws.schema.QueryInfringementDetailForWebsiteResponse
     */
    @WebMethod(operationName = "QueryInfringementDetailForWebsite")
    @WebResult(name = "QueryInfringementDetailForWebsiteResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryInfringementDetailForWebsiteResponse")
    public QueryInfringementDetailForWebsiteResponse queryInfringementDetailForWebsite(
        @WebParam(name = "QueryInfringementDetailForWebsiteRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryInfringementDetailForWebsiteRequest")
        QueryInfringementDetailForWebsiteRequest queryInfringementDetailForWebsiteRequest);

    /**
     * 
     * @param queryDriverQualificationStatusRequest
     * @return
     *     returns tasima.common.ws.schema.QueryDriverQualificationStatusResponse
     */
    @WebMethod(operationName = "QueryDriverQualificationStatus")
    @WebResult(name = "QueryDriverQualificationStatusResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryDriverQualificationStatusResponse")
    public QueryDriverQualificationStatusResponse queryDriverQualificationStatus(
        @WebParam(name = "QueryDriverQualificationStatusRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "QueryDriverQualificationStatusRequest")
        QueryDriverQualificationStatusRequest queryDriverQualificationStatusRequest);

    /**
     * 
     * @param plnAvailabilityRequest
     * @return
     *     returns tasima.common.ws.schema.PlnAvailabilityResponse
     */
    @WebMethod(operationName = "PlnAvailability")
    @WebResult(name = "PlnAvailabilityResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "PlnAvailabilityResponse")
    public PlnAvailabilityResponse plnAvailability(
        @WebParam(name = "PlnAvailabilityRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "PlnAvailabilityRequest")
        PlnAvailabilityRequest plnAvailabilityRequest);

    /**
     * 
     * @param x314ARequest
     * @return
     *     returns tasima.common.ws.schema.X314AResponse
     */
    @WebMethod(operationName = "X314A")
    @WebResult(name = "X314AResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "X314AResponse")
    public X314AResponse x314A(
        @WebParam(name = "X314ARequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "X314ARequest")
        X314ARequest x314ARequest);

    /**
     * 
     * @param x3903Request
     * @return
     *     returns tasima.common.ws.schema.X3903Response
     */
    @WebMethod(operationName = "X3903")
    @WebResult(name = "X3903Response", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3903Response")
    public X3903Response x3903(
        @WebParam(name = "X3903Request", targetNamespace = "http://tasima/common/ws/schema/", partName = "X3903Request")
        X3903Request x3903Request);

    /**
     * 
     * @param processOnlinePaymentAuthorizationRequest
     * @return
     *     returns tasima.common.ws.schema.ProcessOnlinePaymentAuthorizationResponse
     */
    @WebMethod(operationName = "ProcessOnlinePaymentAuthorization")
    @WebResult(name = "ProcessOnlinePaymentAuthorizationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "ProcessOnlinePaymentAuthorizationResponse")
    public ProcessOnlinePaymentAuthorizationResponse processOnlinePaymentAuthorization(
        @WebParam(name = "ProcessOnlinePaymentAuthorizationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "ProcessOnlinePaymentAuthorizationRequest")
        ProcessOnlinePaymentAuthorizationRequest processOnlinePaymentAuthorizationRequest);

    /**
     * 
     * @param applyForRevocationRequest
     * @return
     *     returns tasima.common.ws.schema.ApplyForRevocationResponse
     */
    @WebMethod(operationName = "ApplyForRevocation")
    @WebResult(name = "ApplyForRevocationResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "ApplyForRevocationResponse")
    public ApplyForRevocationResponse applyForRevocation(
        @WebParam(name = "ApplyForRevocationRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "ApplyForRevocationRequest")
        ApplyForRevocationRequest applyForRevocationRequest);

    /**
     * 
     * @param getPostOfficeDestinationsRequest
     * @return
     *     returns tasima.common.ws.schema.GetPostOfficeDestinationsResponse
     */
    @WebMethod(operationName = "GetPostOfficeDestinations")
    @WebResult(name = "GetPostOfficeDestinationsResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetPostOfficeDestinationsResponse")
    public GetPostOfficeDestinationsResponse getPostOfficeDestinations(
        @WebParam(name = "GetPostOfficeDestinationsRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "GetPostOfficeDestinationsRequest")
        GetPostOfficeDestinationsRequest getPostOfficeDestinationsRequest);

    /**
     * 
     * @param uploadMicrodotRequest
     * @return
     *     returns tasima.common.ws.schema.UploadMicrodotResponse
     */
    @WebMethod(operationName = "UploadMicrodot")
    @WebResult(name = "UploadMicrodotResponse", targetNamespace = "http://tasima/common/ws/schema/", partName = "UploadMicrodotResponse")
    public UploadMicrodotResponse uploadMicrodot(
        @WebParam(name = "UploadMicrodotRequest", targetNamespace = "http://tasima/common/ws/schema/", partName = "UploadMicrodotRequest")
        UploadMicrodotRequest uploadMicrodotRequest);

}
