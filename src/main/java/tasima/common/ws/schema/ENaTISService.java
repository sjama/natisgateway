
package tasima.common.ws.schema;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "eNaTISService", targetNamespace = "http://tasima/common/ws/schema/", wsdlLocation = "file:/app/wildfly/webservice/NaTIS.wsdl")
public class ENaTISService
    extends Service
{

    private final static URL ENATISSERVICE_WSDL_LOCATION;
    private final static WebServiceException ENATISSERVICE_EXCEPTION;
    private final static QName ENATISSERVICE_QNAME = new QName("http://tasima/common/ws/schema/", "eNaTISService");

    static {
        URL url = null; 
        WebServiceException e = null;   
        //Users/jamasithole/Documents/RTMC/NaTIS.wsdl
        try {
           // url = new URL("file:/app/wildfly/webservice/NaTIS.wsdl");
            url = new URL("file:/Users/jamasithole/Documents/RTMC/NaTIS.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ENATISSERVICE_WSDL_LOCATION = url;
        ENATISSERVICE_EXCEPTION = e;
    }

    public ENaTISService() {
        super(__getWsdlLocation(), ENATISSERVICE_QNAME);
    }

    public ENaTISService(WebServiceFeature... features) {
        super(__getWsdlLocation(), ENATISSERVICE_QNAME, features);
    }

    public ENaTISService(URL wsdlLocation) {
        super(wsdlLocation, ENATISSERVICE_QNAME);
    }

    public ENaTISService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ENATISSERVICE_QNAME, features);
    }

    public ENaTISService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ENaTISService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ENaTIS
     */
    @WebEndpoint(name = "eNaTISSoap11")
    public ENaTIS getENaTISSoap11() {
        return super.getPort(new QName("http://tasima/common/ws/schema/", "eNaTISSoap11"), ENaTIS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ENaTIS
     */
    @WebEndpoint(name = "eNaTISSoap11")
    public ENaTIS getENaTISSoap11(WebServiceFeature... features) {
        return super.getPort(new QName("http://tasima/common/ws/schema/", "eNaTISSoap11"), ENaTIS.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ENATISSERVICE_EXCEPTION!= null) {
            throw ENATISSERVICE_EXCEPTION;
        }
        return ENATISSERVICE_WSDL_LOCATION;
    }

}
