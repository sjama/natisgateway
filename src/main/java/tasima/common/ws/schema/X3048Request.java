
package tasima.common.ws.schema;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Infringement">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FilmNumber">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;whiteSpace value="collapse"/>
 *                         &lt;pattern value="[A-Z0-9]{1,20}"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ReferenceNumber">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;whiteSpace value="collapse"/>
 *                         &lt;pattern value="[A-Z0-9]{1,10}"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="ProvinceCode" type="{http://tasima/common/ws/schema/}ProvinceCode"/>
 *                   &lt;element name="Suburb" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="35"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="CityTown" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="30"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="StreetNameA" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="StreetNameB" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="RouteNumber" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="10"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DirectionFrom" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="DirectionTo" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="40"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="GPSXCoord" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="[\s\dA-Z]{1,10}"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="GPSYCoord" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="[\s\dA-Z]{1,10}"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="OtherLocInfo" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="100"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="IssingAuthority" type="{http://tasima/common/ws/schema/}UserGroupCode"/>
 *                   &lt;element name="RegistrationAuthority" type="{http://tasima/common/ws/schema/}UserGroupCode"/>
 *                   &lt;element name="MagisterialDistrictCode" type="{http://tasima/common/ws/schema/}MagisterialDistrictCode"/>
 *                   &lt;element name="VehicleImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="LicenseImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="Date" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="Time" type="{http://tasima/common/ws/schema/}Time"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Charge">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Code">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="[\d]{4,5}"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Description" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;maxLength value="350"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="SpeedRead1" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                         &lt;fractionDigits value="2"/>
 *                         &lt;totalDigits value="5"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="SpeedRead2" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                         &lt;fractionDigits value="2"/>
 *                         &lt;totalDigits value="5"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="RedTime" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                         &lt;totalDigits value="3"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="AmberTime" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                         &lt;totalDigits value="3"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VehicleTypeCode">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="[\d]{2}"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="LicenseNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Officer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="InfrastructureNumber" type="{http://tasima/common/ws/schema/}InfrastructureNumber"/>
 *                   &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringement",
    "charge",
    "vehicle",
    "officer"
})
@XmlRootElement(name = "X3048Request")
public class X3048Request {

    @XmlElement(name = "Infringement", required = true)
    protected X3048Request.Infringement infringement;
    @XmlElement(name = "Charge", required = true)
    protected X3048Request.Charge charge;
    @XmlElement(name = "Vehicle", required = true)
    protected X3048Request.Vehicle vehicle;
    @XmlElement(name = "Officer", required = true)
    protected X3048Request.Officer officer;

    /**
     * Gets the value of the infringement property.
     * 
     * @return
     *     possible object is
     *     {@link X3048Request.Infringement }
     *     
     */
    public X3048Request.Infringement getInfringement() {
        return infringement;
    }

    /**
     * Sets the value of the infringement property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3048Request.Infringement }
     *     
     */
    public void setInfringement(X3048Request.Infringement value) {
        this.infringement = value;
    }

    /**
     * Gets the value of the charge property.
     * 
     * @return
     *     possible object is
     *     {@link X3048Request.Charge }
     *     
     */
    public X3048Request.Charge getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3048Request.Charge }
     *     
     */
    public void setCharge(X3048Request.Charge value) {
        this.charge = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X3048Request.Vehicle }
     *     
     */
    public X3048Request.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3048Request.Vehicle }
     *     
     */
    public void setVehicle(X3048Request.Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the officer property.
     * 
     * @return
     *     possible object is
     *     {@link X3048Request.Officer }
     *     
     */
    public X3048Request.Officer getOfficer() {
        return officer;
    }

    /**
     * Sets the value of the officer property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3048Request.Officer }
     *     
     */
    public void setOfficer(X3048Request.Officer value) {
        this.officer = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Code">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="[\d]{4,5}"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Description" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="350"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="SpeedRead1" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *               &lt;fractionDigits value="2"/>
     *               &lt;totalDigits value="5"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="SpeedRead2" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *               &lt;fractionDigits value="2"/>
     *               &lt;totalDigits value="5"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="RedTime" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *               &lt;totalDigits value="3"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="AmberTime" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *               &lt;totalDigits value="3"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description",
        "speedRead1",
        "speedRead2",
        "redTime",
        "amberTime"
    })
    public static class Charge {

        @XmlElement(name = "Code", required = true)
        protected String code;
        @XmlElement(name = "Description")
        protected String description;
        @XmlElement(name = "SpeedRead1")
        protected BigDecimal speedRead1;
        @XmlElement(name = "SpeedRead2")
        protected BigDecimal speedRead2;
        @XmlElement(name = "RedTime")
        protected BigInteger redTime;
        @XmlElement(name = "AmberTime")
        protected BigInteger amberTime;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Gets the value of the speedRead1 property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSpeedRead1() {
            return speedRead1;
        }

        /**
         * Sets the value of the speedRead1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSpeedRead1(BigDecimal value) {
            this.speedRead1 = value;
        }

        /**
         * Gets the value of the speedRead2 property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSpeedRead2() {
            return speedRead2;
        }

        /**
         * Sets the value of the speedRead2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSpeedRead2(BigDecimal value) {
            this.speedRead2 = value;
        }

        /**
         * Gets the value of the redTime property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getRedTime() {
            return redTime;
        }

        /**
         * Sets the value of the redTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setRedTime(BigInteger value) {
            this.redTime = value;
        }

        /**
         * Gets the value of the amberTime property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getAmberTime() {
            return amberTime;
        }

        /**
         * Sets the value of the amberTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setAmberTime(BigInteger value) {
            this.amberTime = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FilmNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;whiteSpace value="collapse"/>
     *               &lt;pattern value="[A-Z0-9]{1,20}"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="ReferenceNumber">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;whiteSpace value="collapse"/>
     *               &lt;pattern value="[A-Z0-9]{1,10}"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="ProvinceCode" type="{http://tasima/common/ws/schema/}ProvinceCode"/>
     *         &lt;element name="Suburb" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="35"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="CityTown" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="30"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="StreetNameA" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="StreetNameB" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="RouteNumber" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="10"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DirectionFrom" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="DirectionTo" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="40"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="GPSXCoord" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="[\s\dA-Z]{1,10}"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="GPSYCoord" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="[\s\dA-Z]{1,10}"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="OtherLocInfo" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="100"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="IssingAuthority" type="{http://tasima/common/ws/schema/}UserGroupCode"/>
     *         &lt;element name="RegistrationAuthority" type="{http://tasima/common/ws/schema/}UserGroupCode"/>
     *         &lt;element name="MagisterialDistrictCode" type="{http://tasima/common/ws/schema/}MagisterialDistrictCode"/>
     *         &lt;element name="VehicleImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="LicenseImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="Date" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="Time" type="{http://tasima/common/ws/schema/}Time"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "filmNumber",
        "referenceNumber",
        "provinceCode",
        "suburb",
        "cityTown",
        "streetNameA",
        "streetNameB",
        "routeNumber",
        "directionFrom",
        "directionTo",
        "gpsxCoord",
        "gpsyCoord",
        "otherLocInfo",
        "issingAuthority",
        "registrationAuthority",
        "magisterialDistrictCode",
        "vehicleImage",
        "licenseImage",
        "date",
        "time"
    })
    public static class Infringement {

        @XmlElement(name = "FilmNumber", required = true)
        protected String filmNumber;
        @XmlElement(name = "ReferenceNumber", required = true)
        protected String referenceNumber;
        @XmlElement(name = "ProvinceCode", required = true)
        protected String provinceCode;
        @XmlElement(name = "Suburb")
        protected String suburb;
        @XmlElement(name = "CityTown")
        protected String cityTown;
        @XmlElement(name = "StreetNameA")
        protected String streetNameA;
        @XmlElement(name = "StreetNameB")
        protected String streetNameB;
        @XmlElement(name = "RouteNumber")
        protected String routeNumber;
        @XmlElement(name = "DirectionFrom")
        protected String directionFrom;
        @XmlElement(name = "DirectionTo")
        protected String directionTo;
        @XmlElement(name = "GPSXCoord")
        protected String gpsxCoord;
        @XmlElement(name = "GPSYCoord")
        protected String gpsyCoord;
        @XmlElement(name = "OtherLocInfo")
        protected String otherLocInfo;
        @XmlElement(name = "IssingAuthority", required = true)
        protected String issingAuthority;
        @XmlElement(name = "RegistrationAuthority", required = true)
        protected String registrationAuthority;
        @XmlElement(name = "MagisterialDistrictCode", required = true)
        protected String magisterialDistrictCode;
        @XmlElement(name = "VehicleImage", required = true)
        protected byte[] vehicleImage;
        @XmlElement(name = "LicenseImage", required = true)
        protected byte[] licenseImage;
        @XmlElement(name = "Date", required = true)
        protected XMLGregorianCalendar date;
        @XmlElement(name = "Time", required = true)
        protected XMLGregorianCalendar time;

        /**
         * Gets the value of the filmNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFilmNumber() {
            return filmNumber;
        }

        /**
         * Sets the value of the filmNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFilmNumber(String value) {
            this.filmNumber = value;
        }

        /**
         * Gets the value of the referenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceNumber() {
            return referenceNumber;
        }

        /**
         * Sets the value of the referenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceNumber(String value) {
            this.referenceNumber = value;
        }

        /**
         * Gets the value of the provinceCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProvinceCode() {
            return provinceCode;
        }

        /**
         * Sets the value of the provinceCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProvinceCode(String value) {
            this.provinceCode = value;
        }

        /**
         * Gets the value of the suburb property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuburb() {
            return suburb;
        }

        /**
         * Sets the value of the suburb property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuburb(String value) {
            this.suburb = value;
        }

        /**
         * Gets the value of the cityTown property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCityTown() {
            return cityTown;
        }

        /**
         * Sets the value of the cityTown property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCityTown(String value) {
            this.cityTown = value;
        }

        /**
         * Gets the value of the streetNameA property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreetNameA() {
            return streetNameA;
        }

        /**
         * Sets the value of the streetNameA property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreetNameA(String value) {
            this.streetNameA = value;
        }

        /**
         * Gets the value of the streetNameB property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreetNameB() {
            return streetNameB;
        }

        /**
         * Sets the value of the streetNameB property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreetNameB(String value) {
            this.streetNameB = value;
        }

        /**
         * Gets the value of the routeNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRouteNumber() {
            return routeNumber;
        }

        /**
         * Sets the value of the routeNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRouteNumber(String value) {
            this.routeNumber = value;
        }

        /**
         * Gets the value of the directionFrom property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirectionFrom() {
            return directionFrom;
        }

        /**
         * Sets the value of the directionFrom property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirectionFrom(String value) {
            this.directionFrom = value;
        }

        /**
         * Gets the value of the directionTo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDirectionTo() {
            return directionTo;
        }

        /**
         * Sets the value of the directionTo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDirectionTo(String value) {
            this.directionTo = value;
        }

        /**
         * Gets the value of the gpsxCoord property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGPSXCoord() {
            return gpsxCoord;
        }

        /**
         * Sets the value of the gpsxCoord property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGPSXCoord(String value) {
            this.gpsxCoord = value;
        }

        /**
         * Gets the value of the gpsyCoord property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGPSYCoord() {
            return gpsyCoord;
        }

        /**
         * Sets the value of the gpsyCoord property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGPSYCoord(String value) {
            this.gpsyCoord = value;
        }

        /**
         * Gets the value of the otherLocInfo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOtherLocInfo() {
            return otherLocInfo;
        }

        /**
         * Sets the value of the otherLocInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOtherLocInfo(String value) {
            this.otherLocInfo = value;
        }

        /**
         * Gets the value of the issingAuthority property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssingAuthority() {
            return issingAuthority;
        }

        /**
         * Sets the value of the issingAuthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssingAuthority(String value) {
            this.issingAuthority = value;
        }

        /**
         * Gets the value of the registrationAuthority property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegistrationAuthority() {
            return registrationAuthority;
        }

        /**
         * Sets the value of the registrationAuthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegistrationAuthority(String value) {
            this.registrationAuthority = value;
        }

        /**
         * Gets the value of the magisterialDistrictCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMagisterialDistrictCode() {
            return magisterialDistrictCode;
        }

        /**
         * Sets the value of the magisterialDistrictCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMagisterialDistrictCode(String value) {
            this.magisterialDistrictCode = value;
        }

        /**
         * Gets the value of the vehicleImage property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getVehicleImage() {
            return vehicleImage;
        }

        /**
         * Sets the value of the vehicleImage property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setVehicleImage(byte[] value) {
            this.vehicleImage = value;
        }

        /**
         * Gets the value of the licenseImage property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getLicenseImage() {
            return licenseImage;
        }

        /**
         * Sets the value of the licenseImage property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setLicenseImage(byte[] value) {
            this.licenseImage = value;
        }

        /**
         * Gets the value of the date property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Sets the value of the date property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }

        /**
         * Gets the value of the time property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTime() {
            return time;
        }

        /**
         * Sets the value of the time property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTime(XMLGregorianCalendar value) {
            this.time = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="InfrastructureNumber" type="{http://tasima/common/ws/schema/}InfrastructureNumber"/>
     *         &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "infrastructureNumber",
        "signature"
    })
    public static class Officer {

        @XmlElement(name = "InfrastructureNumber", required = true)
        protected String infrastructureNumber;
        @XmlElement(name = "Signature", required = true)
        protected byte[] signature;

        /**
         * Gets the value of the infrastructureNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInfrastructureNumber() {
            return infrastructureNumber;
        }

        /**
         * Sets the value of the infrastructureNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInfrastructureNumber(String value) {
            this.infrastructureNumber = value;
        }

        /**
         * Gets the value of the signature property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getSignature() {
            return signature;
        }

        /**
         * Sets the value of the signature property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setSignature(byte[] value) {
            this.signature = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VehicleTypeCode">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="[\d]{2}"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="LicenseNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vehicleTypeCode",
        "licenseNumber"
    })
    public static class Vehicle {

        @XmlElement(name = "VehicleTypeCode", required = true)
        protected String vehicleTypeCode;
        @XmlElement(name = "LicenseNumber", required = true)
        protected String licenseNumber;

        /**
         * Gets the value of the vehicleTypeCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleTypeCode() {
            return vehicleTypeCode;
        }

        /**
         * Sets the value of the vehicleTypeCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleTypeCode(String value) {
            this.vehicleTypeCode = value;
        }

        /**
         * Gets the value of the licenseNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenseNumber() {
            return licenseNumber;
        }

        /**
         * Sets the value of the licenseNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenseNumber(String value) {
            this.licenseNumber = value;
        }

    }

}
