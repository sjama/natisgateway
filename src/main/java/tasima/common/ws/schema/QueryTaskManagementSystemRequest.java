
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDocumentType" type="{http://tasima/common/ws/schema/}IdDocumentTypeElement"/>
 *         &lt;element name="IdDocumentNumber" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *         &lt;element name="SearchOption" type="{http://tasima/common/ws/schema/}E3024SearchOptionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idDocumentType",
    "idDocumentNumber",
    "searchOption"
})
@XmlRootElement(name = "QueryTaskManagementSystemRequest")
public class QueryTaskManagementSystemRequest {

    @XmlElement(name = "IdDocumentType", required = true)
    protected String idDocumentType;
    @XmlElement(name = "IdDocumentNumber", required = true)
    protected String idDocumentNumber;
    @XmlElement(name = "SearchOption", required = true)
    protected E3024SearchOptionType searchOption;

    /**
     * Gets the value of the idDocumentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentType() {
        return idDocumentType;
    }

    /**
     * Sets the value of the idDocumentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentType(String value) {
        this.idDocumentType = value;
    }

    /**
     * Gets the value of the idDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentNumber() {
        return idDocumentNumber;
    }

    /**
     * Sets the value of the idDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentNumber(String value) {
        this.idDocumentNumber = value;
    }

    /**
     * Gets the value of the searchOption property.
     * 
     * @return
     *     possible object is
     *     {@link E3024SearchOptionType }
     *     
     */
    public E3024SearchOptionType getSearchOption() {
        return searchOption;
    }

    /**
     * Sets the value of the searchOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link E3024SearchOptionType }
     *     
     */
    public void setSearchOption(E3024SearchOptionType value) {
        this.searchOption = value;
    }

}
