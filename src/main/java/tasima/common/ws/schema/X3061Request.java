
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="selfPropelledVehicle" type="{http://tasima/common/ws/schema/}x3061VehicleType"/>
 *         &lt;element name="infringer" type="{http://tasima/common/ws/schema/}x3061InfringerType"/>
 *         &lt;element name="issuingAuthority" type="{http://tasima/common/ws/schema/}x3061IssuingAuthorityType"/>
 *         &lt;element name="locationDetail" type="{http://tasima/common/ws/schema/}x3061ContraventionLocationType"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element name="contraventions" type="{http://tasima/common/ws/schema/}x3061ContraventionType"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "selfPropelledVehicle",
    "infringer",
    "issuingAuthority",
    "locationDetail",
    "contraventions"
})
@XmlRootElement(name = "X3061Request")
public class X3061Request {

    @XmlElement(required = true)
    protected X3061VehicleType selfPropelledVehicle;
    @XmlElement(required = true)
    protected X3061InfringerType infringer;
    @XmlElement(required = true)
    protected X3061IssuingAuthorityType issuingAuthority;
    @XmlElement(required = true)
    protected X3061ContraventionLocationType locationDetail;
    @XmlElement(required = true)
    protected List<X3061ContraventionType> contraventions;

    /**
     * Gets the value of the selfPropelledVehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X3061VehicleType }
     *     
     */
    public X3061VehicleType getSelfPropelledVehicle() {
        return selfPropelledVehicle;
    }

    /**
     * Sets the value of the selfPropelledVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061VehicleType }
     *     
     */
    public void setSelfPropelledVehicle(X3061VehicleType value) {
        this.selfPropelledVehicle = value;
    }

    /**
     * Gets the value of the infringer property.
     * 
     * @return
     *     possible object is
     *     {@link X3061InfringerType }
     *     
     */
    public X3061InfringerType getInfringer() {
        return infringer;
    }

    /**
     * Sets the value of the infringer property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061InfringerType }
     *     
     */
    public void setInfringer(X3061InfringerType value) {
        this.infringer = value;
    }

    /**
     * Gets the value of the issuingAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link X3061IssuingAuthorityType }
     *     
     */
    public X3061IssuingAuthorityType getIssuingAuthority() {
        return issuingAuthority;
    }

    /**
     * Sets the value of the issuingAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061IssuingAuthorityType }
     *     
     */
    public void setIssuingAuthority(X3061IssuingAuthorityType value) {
        this.issuingAuthority = value;
    }

    /**
     * Gets the value of the locationDetail property.
     * 
     * @return
     *     possible object is
     *     {@link X3061ContraventionLocationType }
     *     
     */
    public X3061ContraventionLocationType getLocationDetail() {
        return locationDetail;
    }

    /**
     * Sets the value of the locationDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3061ContraventionLocationType }
     *     
     */
    public void setLocationDetail(X3061ContraventionLocationType value) {
        this.locationDetail = value;
    }

    /**
     * Gets the value of the contraventions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contraventions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContraventions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link X3061ContraventionType }
     * 
     * 
     */
    public List<X3061ContraventionType> getContraventions() {
        if (contraventions == null) {
            contraventions = new ArrayList<X3061ContraventionType>();
        }
        return this.contraventions;
    }

}
