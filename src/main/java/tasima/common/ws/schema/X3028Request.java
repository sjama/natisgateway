
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="infringerSearchOption" type="{http://tasima/common/ws/schema/}SearchOptionInfringerType"/>
 *         &lt;element name="infringementSearchOption" type="{http://tasima/common/ws/schema/}SearchOptionInfringementType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infringerSearchOption",
    "infringementSearchOption"
})
@XmlRootElement(name = "X3028Request")
public class X3028Request {

    protected SearchOptionInfringerType infringerSearchOption;
    protected SearchOptionInfringementType infringementSearchOption;

    /**
     * Gets the value of the infringerSearchOption property.
     * 
     * @return
     *     possible object is
     *     {@link SearchOptionInfringerType }
     *     
     */
    public SearchOptionInfringerType getInfringerSearchOption() {
        return infringerSearchOption;
    }

    /**
     * Sets the value of the infringerSearchOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchOptionInfringerType }
     *     
     */
    public void setInfringerSearchOption(SearchOptionInfringerType value) {
        this.infringerSearchOption = value;
    }

    /**
     * Gets the value of the infringementSearchOption property.
     * 
     * @return
     *     possible object is
     *     {@link SearchOptionInfringementType }
     *     
     */
    public SearchOptionInfringementType getInfringementSearchOption() {
        return infringementSearchOption;
    }

    /**
     * Sets the value of the infringementSearchOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchOptionInfringementType }
     *     
     */
    public void setInfringementSearchOption(SearchOptionInfringementType value) {
        this.infringementSearchOption = value;
    }

}
