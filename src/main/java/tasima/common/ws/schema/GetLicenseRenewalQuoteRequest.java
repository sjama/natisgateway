
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicleRegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
 *         &lt;element name="licenseRenewalNoticeNumber" type="{http://tasima/common/ws/schema/}VehicleLicenseRenewalNoticeNumber" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleRegisterNumber",
    "licenseRenewalNoticeNumber"
})
@XmlRootElement(name = "GetLicenseRenewalQuoteRequest")
public class GetLicenseRenewalQuoteRequest {

    protected String vehicleRegisterNumber;
    protected String licenseRenewalNoticeNumber;

    /**
     * Gets the value of the vehicleRegisterNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleRegisterNumber() {
        return vehicleRegisterNumber;
    }

    /**
     * Sets the value of the vehicleRegisterNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleRegisterNumber(String value) {
        this.vehicleRegisterNumber = value;
    }

    /**
     * Gets the value of the licenseRenewalNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseRenewalNoticeNumber() {
        return licenseRenewalNoticeNumber;
    }

    /**
     * Sets the value of the licenseRenewalNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseRenewalNoticeNumber(String value) {
        this.licenseRenewalNoticeNumber = value;
    }

}
