
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TestPreBookingAvailabilityResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TestPreBookingAvailabilityResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="availableTimeSlots" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded"/>
 *         &lt;element name="applicant" type="{http://tasima/common/ws/schema/}TestPreBookingApplicant"/>
 *         &lt;element name="dltcName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dltcAddres" type="{http://tasima/common/ws/schema/}PersonAddress"/>
 *         &lt;element name="dltcTelephoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="testCategoryName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="testTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TestPreBookingAvailabilityResponseDetail", propOrder = {
    "availableTimeSlots",
    "applicant",
    "dltcName",
    "dltcAddres",
    "dltcTelephoneNumber",
    "testCategoryName",
    "testTypeName"
})
public class TestPreBookingAvailabilityResponseDetail {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected List<XMLGregorianCalendar> availableTimeSlots;
    @XmlElement(required = true)
    protected TestPreBookingApplicant applicant;
    @XmlElement(required = true)
    protected String dltcName;
    @XmlElement(required = true)
    protected PersonAddress dltcAddres;
    @XmlElement(required = true)
    protected String dltcTelephoneNumber;
    @XmlElement(required = true)
    protected String testCategoryName;
    @XmlElement(required = true)
    protected String testTypeName;

    /**
     * Gets the value of the availableTimeSlots property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availableTimeSlots property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailableTimeSlots().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XMLGregorianCalendar }
     * 
     * 
     */
    public List<XMLGregorianCalendar> getAvailableTimeSlots() {
        if (availableTimeSlots == null) {
            availableTimeSlots = new ArrayList<XMLGregorianCalendar>();
        }
        return this.availableTimeSlots;
    }

    /**
     * Gets the value of the applicant property.
     * 
     * @return
     *     possible object is
     *     {@link TestPreBookingApplicant }
     *     
     */
    public TestPreBookingApplicant getApplicant() {
        return applicant;
    }

    /**
     * Sets the value of the applicant property.
     * 
     * @param value
     *     allowed object is
     *     {@link TestPreBookingApplicant }
     *     
     */
    public void setApplicant(TestPreBookingApplicant value) {
        this.applicant = value;
    }

    /**
     * Gets the value of the dltcName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDltcName() {
        return dltcName;
    }

    /**
     * Sets the value of the dltcName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDltcName(String value) {
        this.dltcName = value;
    }

    /**
     * Gets the value of the dltcAddres property.
     * 
     * @return
     *     possible object is
     *     {@link PersonAddress }
     *     
     */
    public PersonAddress getDltcAddres() {
        return dltcAddres;
    }

    /**
     * Sets the value of the dltcAddres property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAddress }
     *     
     */
    public void setDltcAddres(PersonAddress value) {
        this.dltcAddres = value;
    }

    /**
     * Gets the value of the dltcTelephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDltcTelephoneNumber() {
        return dltcTelephoneNumber;
    }

    /**
     * Sets the value of the dltcTelephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDltcTelephoneNumber(String value) {
        this.dltcTelephoneNumber = value;
    }

    /**
     * Gets the value of the testCategoryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestCategoryName() {
        return testCategoryName;
    }

    /**
     * Sets the value of the testCategoryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestCategoryName(String value) {
        this.testCategoryName = value;
    }

    /**
     * Gets the value of the testTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestTypeName() {
        return testTypeName;
    }

    /**
     * Sets the value of the testTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestTypeName(String value) {
        this.testTypeName = value;
    }

}
