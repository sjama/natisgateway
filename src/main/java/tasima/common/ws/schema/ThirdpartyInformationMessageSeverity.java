
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ThirdpartyInformationMessageSeverity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ThirdpartyInformationMessageSeverity">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INFO"/>
 *     &lt;enumeration value="WARN"/>
 *     &lt;enumeration value="ERROR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ThirdpartyInformationMessageSeverity")
@XmlEnum
public enum ThirdpartyInformationMessageSeverity {

    INFO,
    WARN,
    ERROR;

    public String value() {
        return name();
    }

    public static ThirdpartyInformationMessageSeverity fromValue(String v) {
        return valueOf(v);
    }

}
