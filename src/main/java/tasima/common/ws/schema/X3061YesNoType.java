
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for x3061YesNoType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="x3061YesNoType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Y"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "x3061YesNoType")
@XmlEnum
public enum X3061YesNoType {


    /**
     * YES
     * 
     */
    Y,

    /**
     * NO
     * 
     */
    N;

    public String value() {
        return name();
    }

    public static X3061YesNoType fromValue(String v) {
        return valueOf(v);
    }

}
