
package tasima.common.ws.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicleDetail" type="{http://tasima/common/ws/schema/}VehicleElement" minOccurs="0"/>
 *         &lt;element name="postalAddressDetail" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
 *         &lt;element name="residentialAddressDetail" type="{http://tasima/common/ws/schema/}PersonAddress" minOccurs="0"/>
 *         &lt;element name="renewalAmountsDetail" type="{http://tasima/common/ws/schema/}VehicleLicenceRenewalAmounts" minOccurs="0"/>
 *         &lt;element name="allowedToContinue" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="noLicenseDiskCondition" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="messages" type="{http://tasima/common/ws/schema/}InformationMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vehicleDetail",
    "postalAddressDetail",
    "residentialAddressDetail",
    "renewalAmountsDetail",
    "allowedToContinue",
    "noLicenseDiskCondition",
    "messages"
})
@XmlRootElement(name = "GetVehicleLicenseRenewalInformationResponse")
public class GetVehicleLicenseRenewalInformationResponse {

    protected VehicleElement vehicleDetail;
    protected PersonAddress postalAddressDetail;
    protected PersonAddress residentialAddressDetail;
    protected VehicleLicenceRenewalAmounts renewalAmountsDetail;
    protected boolean allowedToContinue;
    protected boolean noLicenseDiskCondition;
    protected List<InformationMessage> messages;

    /**
     * Gets the value of the vehicleDetail property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleElement }
     *     
     */
    public VehicleElement getVehicleDetail() {
        return vehicleDetail;
    }

    /**
     * Sets the value of the vehicleDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleElement }
     *     
     */
    public void setVehicleDetail(VehicleElement value) {
        this.vehicleDetail = value;
    }

    /**
     * Gets the value of the postalAddressDetail property.
     * 
     * @return
     *     possible object is
     *     {@link PersonAddress }
     *     
     */
    public PersonAddress getPostalAddressDetail() {
        return postalAddressDetail;
    }

    /**
     * Sets the value of the postalAddressDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAddress }
     *     
     */
    public void setPostalAddressDetail(PersonAddress value) {
        this.postalAddressDetail = value;
    }

    /**
     * Gets the value of the residentialAddressDetail property.
     * 
     * @return
     *     possible object is
     *     {@link PersonAddress }
     *     
     */
    public PersonAddress getResidentialAddressDetail() {
        return residentialAddressDetail;
    }

    /**
     * Sets the value of the residentialAddressDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAddress }
     *     
     */
    public void setResidentialAddressDetail(PersonAddress value) {
        this.residentialAddressDetail = value;
    }

    /**
     * Gets the value of the renewalAmountsDetail property.
     * 
     * @return
     *     possible object is
     *     {@link VehicleLicenceRenewalAmounts }
     *     
     */
    public VehicleLicenceRenewalAmounts getRenewalAmountsDetail() {
        return renewalAmountsDetail;
    }

    /**
     * Sets the value of the renewalAmountsDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link VehicleLicenceRenewalAmounts }
     *     
     */
    public void setRenewalAmountsDetail(VehicleLicenceRenewalAmounts value) {
        this.renewalAmountsDetail = value;
    }

    /**
     * Gets the value of the allowedToContinue property.
     * 
     */
    public boolean isAllowedToContinue() {
        return allowedToContinue;
    }

    /**
     * Sets the value of the allowedToContinue property.
     * 
     */
    public void setAllowedToContinue(boolean value) {
        this.allowedToContinue = value;
    }

    /**
     * Gets the value of the noLicenseDiskCondition property.
     * 
     */
    public boolean isNoLicenseDiskCondition() {
        return noLicenseDiskCondition;
    }

    /**
     * Sets the value of the noLicenseDiskCondition property.
     * 
     */
    public void setNoLicenseDiskCondition(boolean value) {
        this.noLicenseDiskCondition = value;
    }

    /**
     * Gets the value of the messages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformationMessage }
     * 
     * 
     */
    public List<InformationMessage> getMessages() {
        if (messages == null) {
            messages = new ArrayList<InformationMessage>();
        }
        return this.messages;
    }

}
