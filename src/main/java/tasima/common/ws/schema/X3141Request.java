
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Owner" type="{http://tasima/common/ws/schema/}X3141_PersonIdentification"/>
 *         &lt;element name="OwnerProxy" type="{http://tasima/common/ws/schema/}X3141_PersonIdentification" minOccurs="0"/>
 *         &lt;element name="OwnerRepresentative" type="{http://tasima/common/ws/schema/}X3141_PersonIdentification" minOccurs="0"/>
 *         &lt;element name="Vehicle">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
 *                   &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
 *                   &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
 *                   &lt;element name="RegistrationLiabiltyDate" type="{http://tasima/common/ws/schema/}Date"/>
 *                   &lt;element name="NatureOfOwnership" type="{http://tasima/common/ws/schema/}NatureOfOwnership"/>
 *                   &lt;element name="VehicleUsage" type="{http://tasima/common/ws/schema/}VehicleUsage"/>
 *                   &lt;element name="VehicleCertificateNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
 *                   &lt;element name="RegistrationReason">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://tasima/common/ws/schema/}RegistrationReason">
 *                         &lt;enumeration value="02"/>
 *                         &lt;enumeration value="04"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "owner",
    "ownerProxy",
    "ownerRepresentative",
    "vehicle"
})
@XmlRootElement(name = "X3141Request")
public class X3141Request {

    @XmlElement(name = "Owner", required = true)
    protected X3141PersonIdentification owner;
    @XmlElement(name = "OwnerProxy")
    protected X3141PersonIdentification ownerProxy;
    @XmlElement(name = "OwnerRepresentative")
    protected X3141PersonIdentification ownerRepresentative;
    @XmlElement(name = "Vehicle", required = true)
    protected X3141Request.Vehicle vehicle;

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public X3141PersonIdentification getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public void setOwner(X3141PersonIdentification value) {
        this.owner = value;
    }

    /**
     * Gets the value of the ownerProxy property.
     * 
     * @return
     *     possible object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public X3141PersonIdentification getOwnerProxy() {
        return ownerProxy;
    }

    /**
     * Sets the value of the ownerProxy property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public void setOwnerProxy(X3141PersonIdentification value) {
        this.ownerProxy = value;
    }

    /**
     * Gets the value of the ownerRepresentative property.
     * 
     * @return
     *     possible object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public X3141PersonIdentification getOwnerRepresentative() {
        return ownerRepresentative;
    }

    /**
     * Sets the value of the ownerRepresentative property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141PersonIdentification }
     *     
     */
    public void setOwnerRepresentative(X3141PersonIdentification value) {
        this.ownerRepresentative = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link X3141Request.Vehicle }
     *     
     */
    public X3141Request.Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link X3141Request.Vehicle }
     *     
     */
    public void setVehicle(X3141Request.Vehicle value) {
        this.vehicle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RegisterNumber" type="{http://tasima/common/ws/schema/}RegisterNumberElement" minOccurs="0"/>
     *         &lt;element name="VinOrChassis" type="{http://tasima/common/ws/schema/}VinOrChassisNumber" minOccurs="0"/>
     *         &lt;element name="LicenceNumber" type="{http://tasima/common/ws/schema/}LicenceNumberElement" minOccurs="0"/>
     *         &lt;element name="RegistrationLiabiltyDate" type="{http://tasima/common/ws/schema/}Date"/>
     *         &lt;element name="NatureOfOwnership" type="{http://tasima/common/ws/schema/}NatureOfOwnership"/>
     *         &lt;element name="VehicleUsage" type="{http://tasima/common/ws/schema/}VehicleUsage"/>
     *         &lt;element name="VehicleCertificateNumber" type="{http://tasima/common/ws/schema/}ControlNumber"/>
     *         &lt;element name="RegistrationReason">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://tasima/common/ws/schema/}RegistrationReason">
     *               &lt;enumeration value="02"/>
     *               &lt;enumeration value="04"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "registerNumber",
        "vinOrChassis",
        "licenceNumber",
        "registrationLiabiltyDate",
        "natureOfOwnership",
        "vehicleUsage",
        "vehicleCertificateNumber",
        "registrationReason"
    })
    public static class Vehicle {

        @XmlElement(name = "RegisterNumber")
        protected String registerNumber;
        @XmlElement(name = "VinOrChassis")
        protected String vinOrChassis;
        @XmlElement(name = "LicenceNumber")
        protected String licenceNumber;
        @XmlElement(name = "RegistrationLiabiltyDate", required = true)
        protected XMLGregorianCalendar registrationLiabiltyDate;
        @XmlElement(name = "NatureOfOwnership", required = true)
        protected String natureOfOwnership;
        @XmlElement(name = "VehicleUsage", required = true)
        protected String vehicleUsage;
        @XmlElement(name = "VehicleCertificateNumber", required = true)
        protected String vehicleCertificateNumber;
        @XmlElement(name = "RegistrationReason", required = true)
        protected String registrationReason;

        /**
         * Gets the value of the registerNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegisterNumber() {
            return registerNumber;
        }

        /**
         * Sets the value of the registerNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegisterNumber(String value) {
            this.registerNumber = value;
        }

        /**
         * Gets the value of the vinOrChassis property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVinOrChassis() {
            return vinOrChassis;
        }

        /**
         * Sets the value of the vinOrChassis property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVinOrChassis(String value) {
            this.vinOrChassis = value;
        }

        /**
         * Gets the value of the licenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLicenceNumber() {
            return licenceNumber;
        }

        /**
         * Sets the value of the licenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLicenceNumber(String value) {
            this.licenceNumber = value;
        }

        /**
         * Gets the value of the registrationLiabiltyDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getRegistrationLiabiltyDate() {
            return registrationLiabiltyDate;
        }

        /**
         * Sets the value of the registrationLiabiltyDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setRegistrationLiabiltyDate(XMLGregorianCalendar value) {
            this.registrationLiabiltyDate = value;
        }

        /**
         * Gets the value of the natureOfOwnership property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNatureOfOwnership() {
            return natureOfOwnership;
        }

        /**
         * Sets the value of the natureOfOwnership property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNatureOfOwnership(String value) {
            this.natureOfOwnership = value;
        }

        /**
         * Gets the value of the vehicleUsage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleUsage() {
            return vehicleUsage;
        }

        /**
         * Sets the value of the vehicleUsage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleUsage(String value) {
            this.vehicleUsage = value;
        }

        /**
         * Gets the value of the vehicleCertificateNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVehicleCertificateNumber() {
            return vehicleCertificateNumber;
        }

        /**
         * Sets the value of the vehicleCertificateNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVehicleCertificateNumber(String value) {
            this.vehicleCertificateNumber = value;
        }

        /**
         * Gets the value of the registrationReason property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegistrationReason() {
            return registrationReason;
        }

        /**
         * Sets the value of the registrationReason property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegistrationReason(String value) {
            this.registrationReason = value;
        }

    }

}
