
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for x3903Document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="x3903Document">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="docType" type="{http://tasima/common/ws/schema/}x3903SapoDocTypes"/>
 *         &lt;element name="refN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusChgD" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="idDocN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="initialAndSurname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="trackTraceN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postOfficeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "x3903Document", propOrder = {

})
public class X3903Document {

    @XmlElement(required = true)
    protected String docType;
    @XmlElement(required = true)
    protected String refN;
    @XmlElement(required = true)
    protected String statusCd;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar statusChgD;
    @XmlElement(required = true)
    protected String idDocN;
    @XmlElement(required = true)
    protected String initialAndSurname;
    @XmlElement(required = true)
    protected String trackTraceN;
    @XmlElement(required = true)
    protected String postOfficeName;

    /**
     * Gets the value of the docType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the refN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefN() {
        return refN;
    }

    /**
     * Sets the value of the refN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefN(String value) {
        this.refN = value;
    }

    /**
     * Gets the value of the statusCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCd() {
        return statusCd;
    }

    /**
     * Sets the value of the statusCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCd(String value) {
        this.statusCd = value;
    }

    /**
     * Gets the value of the statusChgD property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStatusChgD() {
        return statusChgD;
    }

    /**
     * Sets the value of the statusChgD property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStatusChgD(XMLGregorianCalendar value) {
        this.statusChgD = value;
    }

    /**
     * Gets the value of the idDocN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocN() {
        return idDocN;
    }

    /**
     * Sets the value of the idDocN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocN(String value) {
        this.idDocN = value;
    }

    /**
     * Gets the value of the initialAndSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitialAndSurname() {
        return initialAndSurname;
    }

    /**
     * Sets the value of the initialAndSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialAndSurname(String value) {
        this.initialAndSurname = value;
    }

    /**
     * Gets the value of the trackTraceN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackTraceN() {
        return trackTraceN;
    }

    /**
     * Sets the value of the trackTraceN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackTraceN(String value) {
        this.trackTraceN = value;
    }

    /**
     * Gets the value of the postOfficeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostOfficeName() {
        return postOfficeName;
    }

    /**
     * Sets the value of the postOfficeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostOfficeName(String value) {
        this.postOfficeName = value;
    }

}
