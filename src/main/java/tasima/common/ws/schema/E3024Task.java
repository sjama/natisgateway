
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for E3024Task complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="E3024Task">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaskRequestNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DateCreated" type="{http://tasima/common/ws/schema/}Date" minOccurs="0"/>
 *         &lt;element name="TaskRequest" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *         &lt;element name="UserGroup" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement" minOccurs="0"/>
 *         &lt;element name="Particulars" type="{http://tasima/common/ws/schema/}E3024TaskParticulars" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "E3024Task", propOrder = {
    "taskRequestNumber",
    "dateCreated",
    "taskRequest",
    "userGroup",
    "particulars"
})
public class E3024Task {

    @XmlElement(name = "TaskRequestNumber", required = true)
    protected String taskRequestNumber;
    @XmlElement(name = "DateCreated")
    protected XMLGregorianCalendar dateCreated;
    @XmlElement(name = "TaskRequest")
    protected CodeAndDescriptionElement taskRequest;
    @XmlElement(name = "UserGroup")
    protected CodeAndDescriptionElement userGroup;
    @XmlElement(name = "Particulars")
    protected E3024TaskParticulars particulars;

    /**
     * Gets the value of the taskRequestNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskRequestNumber() {
        return taskRequestNumber;
    }

    /**
     * Sets the value of the taskRequestNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskRequestNumber(String value) {
        this.taskRequestNumber = value;
    }

    /**
     * Gets the value of the dateCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateCreated() {
        return dateCreated;
    }

    /**
     * Sets the value of the dateCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateCreated(XMLGregorianCalendar value) {
        this.dateCreated = value;
    }

    /**
     * Gets the value of the taskRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getTaskRequest() {
        return taskRequest;
    }

    /**
     * Sets the value of the taskRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setTaskRequest(CodeAndDescriptionElement value) {
        this.taskRequest = value;
    }

    /**
     * Gets the value of the userGroup property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getUserGroup() {
        return userGroup;
    }

    /**
     * Sets the value of the userGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setUserGroup(CodeAndDescriptionElement value) {
        this.userGroup = value;
    }

    /**
     * Gets the value of the particulars property.
     * 
     * @return
     *     possible object is
     *     {@link E3024TaskParticulars }
     *     
     */
    public E3024TaskParticulars getParticulars() {
        return particulars;
    }

    /**
     * Sets the value of the particulars property.
     * 
     * @param value
     *     allowed object is
     *     {@link E3024TaskParticulars }
     *     
     */
    public void setParticulars(E3024TaskParticulars value) {
        this.particulars = value;
    }

}
