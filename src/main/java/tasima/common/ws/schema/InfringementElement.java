
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InfringementElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InfringementElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vehicle" type="{http://tasima/common/ws/schema/}InfringementVehicleElement"/>
 *         &lt;element name="charge">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="mainCharge" type="{http://tasima/common/ws/schema/}InfringementChargeElement"/>
 *                   &lt;element name="alternativeCharge" type="{http://tasima/common/ws/schema/}InfringementChargeElement" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="infringementNoticeNumber" type="{http://tasima/common/ws/schema/}InfringementNoticeNumber"/>
 *         &lt;element name="infringementStatus" type="{http://tasima/common/ws/schema/}CodeAndDescriptionElement"/>
 *         &lt;element name="infringeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="captureDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="location" type="{http://tasima/common/ws/schema/}GenericInfringementLocationType"/>
 *         &lt;element name="parkingNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operatorNoticeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fees" type="{http://tasima/common/ws/schema/}FeesElement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfringementElement", propOrder = {
    "vehicle",
    "charge",
    "infringementNoticeNumber",
    "infringementStatus",
    "infringeDate",
    "captureDate",
    "location",
    "parkingNoticeNumber",
    "operatorNoticeNumber",
    "fees"
})
public class InfringementElement {

    @XmlElement(required = true)
    protected InfringementVehicleElement vehicle;
    @XmlElement(required = true)
    protected InfringementElement.Charge charge;
    @XmlElement(required = true)
    protected String infringementNoticeNumber;
    @XmlElement(required = true)
    protected CodeAndDescriptionElement infringementStatus;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar infringeDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar captureDate;
    @XmlElement(required = true)
    protected GenericInfringementLocationType location;
    protected String parkingNoticeNumber;
    protected String operatorNoticeNumber;
    @XmlElement(required = true)
    protected FeesElement fees;

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link InfringementVehicleElement }
     *     
     */
    public InfringementVehicleElement getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfringementVehicleElement }
     *     
     */
    public void setVehicle(InfringementVehicleElement value) {
        this.vehicle = value;
    }

    /**
     * Gets the value of the charge property.
     * 
     * @return
     *     possible object is
     *     {@link InfringementElement.Charge }
     *     
     */
    public InfringementElement.Charge getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfringementElement.Charge }
     *     
     */
    public void setCharge(InfringementElement.Charge value) {
        this.charge = value;
    }

    /**
     * Gets the value of the infringementNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    /**
     * Sets the value of the infringementNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfringementNoticeNumber(String value) {
        this.infringementNoticeNumber = value;
    }

    /**
     * Gets the value of the infringementStatus property.
     * 
     * @return
     *     possible object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public CodeAndDescriptionElement getInfringementStatus() {
        return infringementStatus;
    }

    /**
     * Sets the value of the infringementStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodeAndDescriptionElement }
     *     
     */
    public void setInfringementStatus(CodeAndDescriptionElement value) {
        this.infringementStatus = value;
    }

    /**
     * Gets the value of the infringeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInfringeDate() {
        return infringeDate;
    }

    /**
     * Sets the value of the infringeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInfringeDate(XMLGregorianCalendar value) {
        this.infringeDate = value;
    }

    /**
     * Gets the value of the captureDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCaptureDate() {
        return captureDate;
    }

    /**
     * Sets the value of the captureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCaptureDate(XMLGregorianCalendar value) {
        this.captureDate = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link GenericInfringementLocationType }
     *     
     */
    public GenericInfringementLocationType getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericInfringementLocationType }
     *     
     */
    public void setLocation(GenericInfringementLocationType value) {
        this.location = value;
    }

    /**
     * Gets the value of the parkingNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingNoticeNumber() {
        return parkingNoticeNumber;
    }

    /**
     * Sets the value of the parkingNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingNoticeNumber(String value) {
        this.parkingNoticeNumber = value;
    }

    /**
     * Gets the value of the operatorNoticeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorNoticeNumber() {
        return operatorNoticeNumber;
    }

    /**
     * Sets the value of the operatorNoticeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorNoticeNumber(String value) {
        this.operatorNoticeNumber = value;
    }

    /**
     * Gets the value of the fees property.
     * 
     * @return
     *     possible object is
     *     {@link FeesElement }
     *     
     */
    public FeesElement getFees() {
        return fees;
    }

    /**
     * Sets the value of the fees property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeesElement }
     *     
     */
    public void setFees(FeesElement value) {
        this.fees = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="mainCharge" type="{http://tasima/common/ws/schema/}InfringementChargeElement"/>
     *         &lt;element name="alternativeCharge" type="{http://tasima/common/ws/schema/}InfringementChargeElement" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mainCharge",
        "alternativeCharge"
    })
    public static class Charge {

        @XmlElement(required = true)
        protected InfringementChargeElement mainCharge;
        protected InfringementChargeElement alternativeCharge;

        /**
         * Gets the value of the mainCharge property.
         * 
         * @return
         *     possible object is
         *     {@link InfringementChargeElement }
         *     
         */
        public InfringementChargeElement getMainCharge() {
            return mainCharge;
        }

        /**
         * Sets the value of the mainCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfringementChargeElement }
         *     
         */
        public void setMainCharge(InfringementChargeElement value) {
            this.mainCharge = value;
        }

        /**
         * Gets the value of the alternativeCharge property.
         * 
         * @return
         *     possible object is
         *     {@link InfringementChargeElement }
         *     
         */
        public InfringementChargeElement getAlternativeCharge() {
            return alternativeCharge;
        }

        /**
         * Sets the value of the alternativeCharge property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfringementChargeElement }
         *     
         */
        public void setAlternativeCharge(InfringementChargeElement value) {
            this.alternativeCharge = value;
        }

    }

}
