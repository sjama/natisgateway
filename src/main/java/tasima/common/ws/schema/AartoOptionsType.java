
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AartoOptionsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AartoOptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="discountExpiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="nextStatus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="nextStatusText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nominationExpiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="representationExpiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="instalmentExpiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="courtElectionExpiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AartoOptionsType", propOrder = {
    "discountExpiry",
    "nextStatus",
    "nextStatusText",
    "nominationExpiry",
    "representationExpiry",
    "instalmentExpiry",
    "courtElectionExpiry"
})
public class AartoOptionsType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar discountExpiry;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nextStatus;
    protected String nextStatusText;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nominationExpiry;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar representationExpiry;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar instalmentExpiry;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar courtElectionExpiry;

    /**
     * Gets the value of the discountExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDiscountExpiry() {
        return discountExpiry;
    }

    /**
     * Sets the value of the discountExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDiscountExpiry(XMLGregorianCalendar value) {
        this.discountExpiry = value;
    }

    /**
     * Gets the value of the nextStatus property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextStatus() {
        return nextStatus;
    }

    /**
     * Sets the value of the nextStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextStatus(XMLGregorianCalendar value) {
        this.nextStatus = value;
    }

    /**
     * Gets the value of the nextStatusText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextStatusText() {
        return nextStatusText;
    }

    /**
     * Sets the value of the nextStatusText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextStatusText(String value) {
        this.nextStatusText = value;
    }

    /**
     * Gets the value of the nominationExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNominationExpiry() {
        return nominationExpiry;
    }

    /**
     * Sets the value of the nominationExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNominationExpiry(XMLGregorianCalendar value) {
        this.nominationExpiry = value;
    }

    /**
     * Gets the value of the representationExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRepresentationExpiry() {
        return representationExpiry;
    }

    /**
     * Sets the value of the representationExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRepresentationExpiry(XMLGregorianCalendar value) {
        this.representationExpiry = value;
    }

    /**
     * Gets the value of the instalmentExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInstalmentExpiry() {
        return instalmentExpiry;
    }

    /**
     * Sets the value of the instalmentExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInstalmentExpiry(XMLGregorianCalendar value) {
        this.instalmentExpiry = value;
    }

    /**
     * Gets the value of the courtElectionExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCourtElectionExpiry() {
        return courtElectionExpiry;
    }

    /**
     * Sets the value of the courtElectionExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCourtElectionExpiry(XMLGregorianCalendar value) {
        this.courtElectionExpiry = value;
    }

}
