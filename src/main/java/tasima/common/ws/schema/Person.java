
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Person complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Person">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="idDocumentTypeCode" type="{http://tasima/common/ws/schema/}AllowedIdDocumentTypeElement"/>
 *         &lt;element name="idDocumentNumber">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="driversLicenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="learnersLicenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person", propOrder = {

})
public class Person {

    @XmlElement(required = true)
    protected String idDocumentTypeCode;
    @XmlElement(required = true)
    protected String idDocumentNumber;
    protected String driversLicenceNumber;
    protected String learnersLicenceNumber;

    /**
     * Gets the value of the idDocumentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentTypeCode() {
        return idDocumentTypeCode;
    }

    /**
     * Sets the value of the idDocumentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentTypeCode(String value) {
        this.idDocumentTypeCode = value;
    }

    /**
     * Gets the value of the idDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentNumber() {
        return idDocumentNumber;
    }

    /**
     * Sets the value of the idDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentNumber(String value) {
        this.idDocumentNumber = value;
    }

    /**
     * Gets the value of the driversLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriversLicenceNumber() {
        return driversLicenceNumber;
    }

    /**
     * Sets the value of the driversLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriversLicenceNumber(String value) {
        this.driversLicenceNumber = value;
    }

    /**
     * Gets the value of the learnersLicenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLearnersLicenceNumber() {
        return learnersLicenceNumber;
    }

    /**
     * Sets the value of the learnersLicenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLearnersLicenceNumber(String value) {
        this.learnersLicenceNumber = value;
    }

}
