
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for X3027InfringerDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="X3027InfringerDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDocTypeCd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocN" type="{http://tasima/common/ws/schema/}IdDocumentNumber"/>
 *         &lt;element name="CntryOfIssId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EmpName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HomePhoneN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WorkPhoneN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FaxN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CellN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperCardN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "X3027InfringerDetails", propOrder = {
    "idDocTypeCd",
    "idDocN",
    "cntryOfIssId",
    "empName",
    "homePhoneN",
    "workPhoneN",
    "faxN",
    "cellN",
    "email",
    "operCardN"
})
public class X3027InfringerDetails {

    @XmlElement(name = "IdDocTypeCd", required = true)
    protected String idDocTypeCd;
    @XmlElement(name = "IdDocN", required = true)
    protected String idDocN;
    @XmlElement(name = "CntryOfIssId", required = true)
    protected String cntryOfIssId;
    @XmlElement(name = "EmpName")
    protected String empName;
    @XmlElement(name = "HomePhoneN")
    protected String homePhoneN;
    @XmlElement(name = "WorkPhoneN")
    protected String workPhoneN;
    @XmlElement(name = "FaxN")
    protected String faxN;
    @XmlElement(name = "CellN")
    protected String cellN;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "OperCardN")
    protected String operCardN;

    /**
     * Gets the value of the idDocTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocTypeCd() {
        return idDocTypeCd;
    }

    /**
     * Sets the value of the idDocTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocTypeCd(String value) {
        this.idDocTypeCd = value;
    }

    /**
     * Gets the value of the idDocN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocN() {
        return idDocN;
    }

    /**
     * Sets the value of the idDocN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocN(String value) {
        this.idDocN = value;
    }

    /**
     * Gets the value of the cntryOfIssId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCntryOfIssId() {
        return cntryOfIssId;
    }

    /**
     * Sets the value of the cntryOfIssId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCntryOfIssId(String value) {
        this.cntryOfIssId = value;
    }

    /**
     * Gets the value of the empName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * Sets the value of the empName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpName(String value) {
        this.empName = value;
    }

    /**
     * Gets the value of the homePhoneN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomePhoneN() {
        return homePhoneN;
    }

    /**
     * Sets the value of the homePhoneN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomePhoneN(String value) {
        this.homePhoneN = value;
    }

    /**
     * Gets the value of the workPhoneN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkPhoneN() {
        return workPhoneN;
    }

    /**
     * Sets the value of the workPhoneN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkPhoneN(String value) {
        this.workPhoneN = value;
    }

    /**
     * Gets the value of the faxN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxN() {
        return faxN;
    }

    /**
     * Sets the value of the faxN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxN(String value) {
        this.faxN = value;
    }

    /**
     * Gets the value of the cellN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellN() {
        return cellN;
    }

    /**
     * Sets the value of the cellN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellN(String value) {
        this.cellN = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the operCardN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperCardN() {
        return operCardN;
    }

    /**
     * Sets the value of the operCardN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperCardN(String value) {
        this.operCardN = value;
    }

}
