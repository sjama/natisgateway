
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Add General Task request number to query
 * 
 * <p>Java class for E3024SearchOptionTaskNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="E3024SearchOptionTaskNumber">
 *   &lt;complexContent>
 *     &lt;extension base="{http://tasima/common/ws/schema/}E3024SearchOptionType">
 *       &lt;sequence>
 *         &lt;element name="TaskNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "E3024SearchOptionTaskNumber", propOrder = {
    "taskNumber"
})
public class E3024SearchOptionTaskNumber
    extends E3024SearchOptionType
{

    @XmlElement(name = "TaskNumber", required = true)
    protected String taskNumber;

    /**
     * Gets the value of the taskNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaskNumber() {
        return taskNumber;
    }

    /**
     * Sets the value of the taskNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaskNumber(String value) {
        this.taskNumber = value;
    }

}
