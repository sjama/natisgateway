
package tasima.common.ws.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="centralDocumentRepository" type="{http://tasima/common/ws/schema/}CentralDocumentRepositoryType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "centralDocumentRepository"
})
@XmlRootElement(name = "Transaction915DUpdateRequest")
public class Transaction915DUpdateRequest {

    protected CentralDocumentRepositoryType centralDocumentRepository;

    /**
     * Gets the value of the centralDocumentRepository property.
     * 
     * @return
     *     possible object is
     *     {@link CentralDocumentRepositoryType }
     *     
     */
    public CentralDocumentRepositoryType getCentralDocumentRepository() {
        return centralDocumentRepository;
    }

    /**
     * Sets the value of the centralDocumentRepository property.
     * 
     * @param value
     *     allowed object is
     *     {@link CentralDocumentRepositoryType }
     *     
     */
    public void setCentralDocumentRepository(CentralDocumentRepositoryType value) {
        this.centralDocumentRepository = value;
    }

}
